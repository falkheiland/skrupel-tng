package org.skrupeltng.selenium;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.skrupeltng.modules.ingame.database.TutorialStage;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.selenium.pages.AbstractPage;
import org.skrupeltng.selenium.pages.InitSetupPage;
import org.skrupeltng.selenium.pages.dashboard.AchievementsPage;
import org.skrupeltng.selenium.pages.dashboard.CreateNewGamePage;
import org.skrupeltng.selenium.pages.dashboard.ExistingGamesPage;
import org.skrupeltng.selenium.pages.dashboard.GameDetailsPage;
import org.skrupeltng.selenium.pages.dashboard.RegistrationPage;
import org.skrupeltng.selenium.pages.dashboard.ScenarioOverviewPage;
import org.skrupeltng.selenium.pages.dashboard.UserDetailsPage;
import org.skrupeltng.selenium.pages.ingame.ColonyOverviewPage;
import org.skrupeltng.selenium.pages.ingame.FleetDetailsPage;
import org.skrupeltng.selenium.pages.ingame.FleetOverviewPage;
import org.skrupeltng.selenium.pages.ingame.GalaxyMapPage;
import org.skrupeltng.selenium.pages.ingame.IngameMainPage;
import org.skrupeltng.selenium.pages.ingame.PlanetPage;
import org.skrupeltng.selenium.pages.ingame.ShipDetailsPage;
import org.skrupeltng.selenium.pages.ingame.ShipOverviewPage;
import org.skrupeltng.selenium.pages.ingame.StarbaseOverviewPage;
import org.skrupeltng.selenium.pages.ingame.StarbasePage;
import org.skrupeltng.selenium.pages.ingame.WebElementPlanetContainer;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SeleniumTest {

	private WebDriver driver;

	@Before
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "libs/chromedriver");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--window-size=1500,1000");
		options.setHeadless(true);
		driver = new ChromeDriver(options);
	}

	@After
	public void tearDown() {
		if (driver != null) {
			driver.quit();
		}
	}

	@Test
	public void testAInitSetup() {
		driver.get("http://localhost:8080");
		assertEquals("Skrupel TNG - Init setup", driver.getTitle());

		InitSetupPage initSetupPage = new InitSetupPage(driver);
		initSetupPage.init();
	}

	@Test
	public void testBTutorialStart() {
		IngameMainPage overviewPage = new IngameMainPage(driver);
		overviewPage.login("/login");

		ExistingGamesPage existingGamesPage = new ExistingGamesPage(driver);
		existingGamesPage.startTutorial();
	}

	@Test
	public void testCTutorialRoundOne() {
		IngameMainPage overviewPage = new IngameMainPage(driver);
		overviewPage.continueTutorial(TutorialStage.INITIAL_SELECT_HOME_PLANET);

		overviewPage.clickOnContinueTutorial(TutorialStage.INITIAL_SELECT_HOME_PLANET);
		overviewPage.checkTutorialStage(TutorialStage.COLONISTS);

		overviewPage.clickOnContinueTutorial(TutorialStage.COLONISTS);
		overviewPage.checkTutorialStage(TutorialStage.CANTOX);

		overviewPage.clickOnContinueTutorial(TutorialStage.CANTOX);
		overviewPage.checkTutorialStage(TutorialStage.SUPPLIES);

		overviewPage.clickOnContinueTutorial(TutorialStage.SUPPLIES);
		overviewPage.checkTutorialStage(TutorialStage.MINERALS);

		overviewPage.clickOnContinueTutorial(TutorialStage.MINERALS);
		overviewPage.checkTutorialStage(TutorialStage.OPEN_FACTORIES_FIRST);

		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_FACTORIES_FIRST);
		overviewPage.checkTutorialStage(TutorialStage.BUILD_FACTORIES_FIRST);

		PlanetPage planetPage = new PlanetPage(driver);
		planetPage.buildFactories();
		overviewPage.checkTutorialStage(TutorialStage.INITIAL_OPEN_STARBASE);

		overviewPage.clickOnContinueTutorial(TutorialStage.INITIAL_OPEN_STARBASE);
		overviewPage.checkTutorialStage(TutorialStage.OPEN_TECHLEVELS_FIRST);

		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_TECHLEVELS_FIRST);
		overviewPage.checkTutorialStage(TutorialStage.UPGRADE_TECHLEVELS_FIRST);

		StarbasePage starbasePage = new StarbasePage(driver);

		starbasePage.upgradeTechlevelFirst();
		overviewPage.checkTutorialStage(TutorialStage.OPEN_HULLS_FREIGHTER);

		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_HULLS_FREIGHTER);
		overviewPage.checkTutorialStage(TutorialStage.BUILD_HULLS_FREIGHTER);

		starbasePage.openSpaceFolds();
		overviewPage.checkTutorialStage(TutorialStage.OPEN_HULLS_FREIGHTER);
		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_HULLS_FREIGHTER);

		overviewPage.checkTutorialStage(TutorialStage.BUILD_HULLS_FREIGHTER);
		starbasePage.productShipPart(null, "Trova Freighter", 5, 1);

		overviewPage.checkTutorialStage(TutorialStage.OPEN_PROPULSION_FREIGHTER);
		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_PROPULSION_FREIGHTER);

		overviewPage.checkTutorialStage(TutorialStage.BUILD_PROPULSION_FREIGHTER);
		starbasePage.productShipPart(null, "Solarisplasmotan", 4, 1);

		overviewPage.checkTutorialStage(TutorialStage.OPEN_SHIP_CONSTRUCTON_FREIGHTER);
		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_SHIP_CONSTRUCTON_FREIGHTER);

		overviewPage.checkTutorialStage(TutorialStage.SELECT_SHIP_CONSTRUCTON_FREIGHTER);
		starbasePage.buildShip();

		overviewPage.checkTutorialStage(TutorialStage.ROUND_ONE);
		overviewPage.endTurn();

		overviewPage.waitShortly();
		overviewPage.checkTutorialStage(TutorialStage.OVERIVEW_MODAL);
		overviewPage.closePopup();
		overviewPage.checkTutorialStage(TutorialStage.OPEN_FREIGHTER_FIRST);
	}

	@Test
	public void testDTutorialRoundTwo() {
		IngameMainPage overviewPage = new IngameMainPage(driver);
		overviewPage.continueTutorial(TutorialStage.OPEN_FREIGHTER_FIRST);

		GalaxyMapPage galaxyMapPage = new GalaxyMapPage(driver);

		galaxyMapPage.selectShipOnPlanet("tutorial-click-element-stage-OPEN_FREIGHTER_FIRST");
		overviewPage.checkTutorialStage(TutorialStage.OPEN_FREIGHTER_TRANSPORT_FIRST);

		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_FREIGHTER_TRANSPORT_FIRST);
		overviewPage.checkTutorialStage(TutorialStage.TRANSPORT_FOR_FREIGHTER);

		ShipDetailsPage shipDetailsPage = new ShipDetailsPage(driver);

		shipDetailsPage.transportToShipForColonization(1);
		overviewPage.checkTutorialStage(TutorialStage.OPEN_FREIGHTER_NAVIGATION_FIRST);

		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_FREIGHTER_NAVIGATION_FIRST);
		overviewPage.checkTutorialStage(TutorialStage.ACTIVATE_FREIGHTER_COURSE_MODE_FIRST);

		overviewPage.clickOnContinueTutorial(TutorialStage.ACTIVATE_FREIGHTER_COURSE_MODE_FIRST);
		overviewPage.checkTutorialStage(TutorialStage.SELECT_FIRST_COLONY_FOR_FREIGHTER);

		overviewPage.clickOnContinueTutorial(TutorialStage.SELECT_FIRST_COLONY_FOR_FREIGHTER);
		overviewPage.checkTutorialStage(TutorialStage.SET_COURSE_FREIGHTER_FIRST);

		driver.findElement(By.id("skr-ingame-ship-save-course-button")).click();
		overviewPage.checkTutorialStage(TutorialStage.OPEN_HOME_PLANET_SECOND);

		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_HOME_PLANET_SECOND);
		overviewPage.checkTutorialStage(TutorialStage.OPEN_ORBITAL_SYSTEMS);

		ShipOverviewPage shipOverviewPage = new ShipOverviewPage(driver);
		shipOverviewPage.openShipOverview();
		overviewPage.checkTutorialStage(TutorialStage.OPEN_HOME_PLANET_SECOND);
		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_HOME_PLANET_SECOND);
		overviewPage.checkTutorialStage(TutorialStage.OPEN_ORBITAL_SYSTEMS);

		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_ORBITAL_SYSTEMS);
		overviewPage.checkTutorialStage(TutorialStage.OPEN_ORBITAL_SYSTEM_SLOT);

		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_ORBITAL_SYSTEM_SLOT);
		overviewPage.checkTutorialStage(TutorialStage.BUILD_ORBITAL_SYSTEM);

		overviewPage.clickOnContinueTutorial(TutorialStage.BUILD_ORBITAL_SYSTEM);
		overviewPage.checkTutorialStage(TutorialStage.ENEMY_COLONY);

		overviewPage.clickOnContinueTutorial(TutorialStage.ENEMY_COLONY);
		overviewPage.checkTutorialStage(TutorialStage.OPEN_STARBASE_SECOND);

		galaxyMapPage.selectHomeStarbase();
		overviewPage.checkTutorialStage(TutorialStage.OPEN_TECHLEVELS_SECOND);

		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_TECHLEVELS_SECOND);
		overviewPage.checkTutorialStage(TutorialStage.UPGRADE_TECHLEVELS_SECOND);

		StarbasePage starbasePage = new StarbasePage(driver);

		starbasePage.upgradeTechlevelSecond();

		overviewPage.checkTutorialStage(TutorialStage.OPEN_HULLS_BOMBER);

		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_HULLS_BOMBER);
		overviewPage.checkTutorialStage(TutorialStage.OPEN_BOMBER_DETAILS);

		driver.findElement(By.id("skr-ingame-ship-template-details-button-orion_14")).click();
		overviewPage.checkTutorialStage(TutorialStage.BOMBER_DETAILS);

		starbasePage.closeShipTemplateModal("orion_14");
		overviewPage.checkTutorialStage(TutorialStage.BUILD_HULLS_BOMBER);

		starbasePage.productShipPart(null, "Guadrol", 5, 1);
		overviewPage.checkTutorialStage(TutorialStage.OPEN_PROPULSION_BOMBER);

		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_PROPULSION_BOMBER);
		overviewPage.checkTutorialStage(TutorialStage.BUILD_PROPULSION_BOMBER);

		ColonyOverviewPage planetOverviewPage = new ColonyOverviewPage(driver);
		planetOverviewPage.openColonyOverview();
		overviewPage.checkTutorialStage(TutorialStage.OPEN_STARBASE_SECOND);
		galaxyMapPage.selectHomeStarbase();

		overviewPage.checkTutorialStage(TutorialStage.OPEN_PROPULSION_BOMBER);
		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_PROPULSION_BOMBER);

		starbasePage.productShipPart(null, "Solarisplasmotan", 4, 2);
		overviewPage.checkTutorialStage(TutorialStage.OPEN_ENERGY_BOMBER);

		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_ENERGY_BOMBER);
		overviewPage.checkTutorialStage(TutorialStage.BUILD_ENERGY_BOMBER);

		starbasePage.productShipPart(null, "Laser", 10, 4);
		overviewPage.checkTutorialStage(TutorialStage.OPEN_SHIP_CONSTRUCTON_BOMBER);

		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_SHIP_CONSTRUCTON_BOMBER);
		overviewPage.checkTutorialStage(TutorialStage.SELECT_SHIP_CONSTRUCTON_BOMBER);

		starbasePage.buildShip();

		overviewPage.checkTutorialStage(TutorialStage.ROUND_TWO);
		overviewPage.endTurn();

		overviewPage.checkTutorialStage(TutorialStage.SELECT_FREIGHTER_COLONIZATION);
	}

	@Test
	public void testETutorialRoundThree() {
		IngameMainPage overviewPage = new IngameMainPage(driver);
		overviewPage.continueTutorial(TutorialStage.SELECT_FREIGHTER_COLONIZATION);

		GalaxyMapPage galaxyMapPage = new GalaxyMapPage(driver);

		galaxyMapPage.selectShipOnPlanet("tutorial-click-element-stage-SELECT_FREIGHTER_COLONIZATION");
		overviewPage.checkTutorialStage(TutorialStage.OPEN_FREIGHTER_TRANSPORT_SECOND);

		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_FREIGHTER_TRANSPORT_SECOND);
		overviewPage.checkTutorialStage(TutorialStage.TRANSPORT_TO_NEW_COLONY);

		ShipDetailsPage shipDetailsPage = new ShipDetailsPage(driver);
		shipDetailsPage.colonisePlanet(0, 0, 0, 0);

		galaxyMapPage.selectShipOnPlanet("tutorial-click-element-stage-OPEN_BOMBER_FIRST");
		overviewPage.checkTutorialStage(TutorialStage.OPEN_TRANSPORTER_BOMBER_FIRST);

		FleetOverviewPage fleetOverviewPage = new FleetOverviewPage(driver);
		fleetOverviewPage.openFleetOverview();

		FleetDetailsPage fleetDetailsPage = new FleetDetailsPage(driver);
		fleetDetailsPage.createFleet();

		overviewPage.checkTutorialStage(TutorialStage.OPEN_BOMBER_FIRST);

		galaxyMapPage.selectShipOnPlanet("tutorial-click-element-stage-OPEN_BOMBER_FIRST");
		overviewPage.checkTutorialStage(TutorialStage.OPEN_TRANSPORTER_BOMBER_FIRST);

		shipDetailsPage.transportToShipForBomber(15);
		overviewPage.checkTutorialStage(TutorialStage.OPEN_NAVIGATION_BOMBER_FIRST);

		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_NAVIGATION_BOMBER_FIRST);
		overviewPage.checkTutorialStage(TutorialStage.SET_COURSE_TO_ENEMY);

		shipDetailsPage.navigateShipToEnemyPlanet();
		overviewPage.checkTutorialStage(TutorialStage.ROUND_THREE);
		overviewPage.endTurn();

		overviewPage.checkTutorialStage(TutorialStage.OPEN_NEW_COLONY);
	}

	@Test
	public void testFTutorialRoundFour() {
		IngameMainPage overviewPage = new IngameMainPage(driver);
		overviewPage.continueTutorial(TutorialStage.OPEN_NEW_COLONY);

		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_NEW_COLONY);
		overviewPage.checkTutorialStage(TutorialStage.OPEN_FACTORIES_SECOND);

		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_FACTORIES_SECOND);
		overviewPage.checkTutorialStage(TutorialStage.BUILD_FACTORIES_SECOND);

		PlanetPage planetPage = new PlanetPage(driver);
		planetPage.buildFactories();

		GalaxyMapPage galaxyMapPage = new GalaxyMapPage(driver);

		overviewPage.checkTutorialStage(TutorialStage.OPEN_FREIGHTER_THIRD);
		galaxyMapPage.selectShipOnPlanet("tutorial-click-element-stage-OPEN_FREIGHTER_THIRD");

		overviewPage.checkTutorialStage(TutorialStage.OPEN_ROUTES);
		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_ROUTES);

		ShipDetailsPage shipDetailsPage = new ShipDetailsPage(driver);
		overviewPage.checkTutorialStage(TutorialStage.SELECT_ROUTE_PLANET_ONE);

		shipDetailsPage.addHomePlanetToRoute();
		shipDetailsPage.addColonyToRoute();

		WebElement minFuelElem = driver.findElement(By.id("skr-ingame-route-min-fuel-input"));
		minFuelElem.clear();
		minFuelElem.sendKeys("20");

		driver.findElement(By.id("skr-ingame-route-primary-resource-select")).click();
		overviewPage.checkTutorialStage(TutorialStage.OPEN_BOMBER_IN_SPACE);

		galaxyMapPage.selectShipInSpace();
		overviewPage.checkTutorialStage(TutorialStage.OPEN_SCANNER_OVERVIEW);

		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_SCANNER_OVERVIEW);
		overviewPage.checkTutorialStage(TutorialStage.SCANNER_OVERVIEW);

		overviewPage.clickOnContinueTutorial(TutorialStage.SCANNER_OVERVIEW);
		overviewPage.checkTutorialStage(TutorialStage.SCANNER_DETAILS);

		overviewPage.clickOnContinueTutorial(TutorialStage.SCANNER_DETAILS);
		overviewPage.checkTutorialStage(TutorialStage.OPEN_TASKS);

		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_TASKS);
		overviewPage.checkTutorialStage(TutorialStage.TASKS);

		driver.findElement(By.id("VIRAL_INVASION")).click();
		driver.findElement(By.id("skr-ingame-ship-change-task-button")).click();
		overviewPage.checkTutorialStage(TutorialStage.ROUND_FOUR);

		overviewPage.endTurn();
		overviewPage.checkTutorialStage(TutorialStage.OPEN_NEWS);

		overviewPage.clickOnContinueTutorial(TutorialStage.OPEN_NEWS);
		overviewPage.checkTutorialStage(TutorialStage.NEWS);

		overviewPage.clickOnContinueTutorial(TutorialStage.NEWS);

		(new WebDriverWait(driver, AbstractPage.STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-tutorial-end-modal")));

		driver.findElement(By.id("skr-ingame-tutorial-finish-tutorial-button")).click();

		(new WebDriverWait(driver, AbstractPage.STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("open-games"));
	}

	@Test
	public void testGNewGame() {
		ExistingGamesPage existingGamesPage = new ExistingGamesPage(driver);
		existingGamesPage.login("/login");
		existingGamesPage.showCreateNewGame();

		CreateNewGamePage createNewGamePage = new CreateNewGamePage(driver);
		createNewGamePage.createNewGame("test2", WinCondition.NONE, 2L);

		GameDetailsPage gameDetailsPage = new GameDetailsPage(driver);
		gameDetailsPage.selectFactionForPlayer(3L);
		gameDetailsPage.startGame();

		IngameMainPage overviewPage = new IngameMainPage(driver);
		overviewPage.closePopup();

		StarbaseOverviewPage starbaseOverviewPage = new StarbaseOverviewPage(driver);
		starbaseOverviewPage.openStarbaseOverview();

		overviewPage.returnToDashboard();
		existingGamesPage.showCreateNewGame();

		createNewGamePage.createNewGame("test3", WinCondition.NONE, 3L);
	}

	@Test
	public void testHDeleteGame() {
		ExistingGamesPage existingGamesPage = new ExistingGamesPage(driver);
		existingGamesPage.login("/login");
		existingGamesPage.showCreateNewGame();

		CreateNewGamePage createNewGamePage = new CreateNewGamePage(driver);
		createNewGamePage.createNewGame("test4", WinCondition.NONE, 4L);

		GameDetailsPage gameDetailsPage = new GameDetailsPage(driver);
		gameDetailsPage.selectFactionForPlayer(5L);
		gameDetailsPage.startGame();

		IngameMainPage overviewPage = new IngameMainPage(driver);
		overviewPage.closePopup();
		gameDetailsPage.deleteGame(4L);
	}

	@Test
	public void testINewUser() {
		RegistrationPage registrationPage = new RegistrationPage(driver);
		registrationPage.register();

		ExistingGamesPage existingGamesPage = new ExistingGamesPage(driver);
		existingGamesPage.showExistingGames();
		existingGamesPage.checkTableButtons();

		UserDetailsPage userDetailsPage = new UserDetailsPage(driver);
		userDetailsPage.openUserDetails();
		userDetailsPage.deleteUser();
	}

	@Test
	public void testJScenarioOne() {
		IngameMainPage overviewPage = new IngameMainPage(driver);
		overviewPage.login("/login");

		ScenarioOverviewPage scenarioOverviewPage = new ScenarioOverviewPage(driver);
		scenarioOverviewPage.startOrContinueScenario();
		scenarioOverviewPage.checkRoundStart(true);

		GalaxyMapPage galaxyMapPage = new GalaxyMapPage(driver);
		galaxyMapPage.selectHomePlanet();

		PlanetPage planetPage = new PlanetPage(driver);
		planetPage.openFactories();
		planetPage.buildFactories();
		planetPage.showStarbase();

		StarbasePage starbasePage = new StarbasePage(driver);
		starbasePage.openUpgrades();
		starbasePage.upgradeScenarioTechlevelsFirst();

		starbasePage.openHulls();
		starbasePage.productShipPart(null, "Doays", 22, 1);
		starbasePage.waitShortly();

		starbasePage.openPropulsionSystems();
		starbasePage.productShipPart(null, "Solarisplasmotan", 9, 1);
		starbasePage.waitShortly();

		starbasePage.openEnergyWeapons();
		starbasePage.productShipPart(null, "Laser", 10, 1);
		starbasePage.waitShortly();

		starbasePage.startShipConstruction();
		starbasePage.buildShip();
		starbasePage.waitShortly();

		overviewPage.endTurn();
		scenarioOverviewPage.checkRoundStart(true);

		galaxyMapPage.selectHomePlanet();
		planetPage.autobuildFactories();
		planetPage.autobuildMines();

		galaxyMapPage.selectShipOnPlanet("skr-game-ship-mouse-planet");

		ShipDetailsPage shipDetailsPage = new ShipDetailsPage(driver);
		shipDetailsPage.transportToShipForColonization(2);

		Set<Point> alreadyVisitied = new HashSet<>();
		alreadyVisitied.add(shipDetailsPage.navigateToNearestPlanet(alreadyVisitied));

		overviewPage.endTurn();
		scenarioOverviewPage.checkRoundStart(true);

		galaxyMapPage.selectShipOnPlanet("skr-game-ship-mouse-planet");

		shipDetailsPage.colonisePlanet(1000, 100, 5, 21);
		alreadyVisitied.add(shipDetailsPage.navigateToNearestPlanet(alreadyVisitied));

		overviewPage.endTurn();
		scenarioOverviewPage.checkRoundStart(true);

		overviewPage.endTurn();
		scenarioOverviewPage.checkRoundStart(false);

		overviewPage.endTurn();
		scenarioOverviewPage.checkRoundStart(false);

		galaxyMapPage.selectShipOnPlanet("skr-game-ship-mouse-planet");
		shipDetailsPage.colonisePlanet(0, 0, 0, 0);

		overviewPage.endTurn();
		scenarioOverviewPage.checkRoundStart(true);

		galaxyMapPage.selectShipOnPlanet("skr-game-ship-mouse-planet");
		shipDetailsPage.openRoutePage();

		List<WebElementPlanetContainer> colonies = shipDetailsPage.getColonies();

		int number = 1;

		for (WebElementPlanetContainer colony : colonies) {
			shipDetailsPage.startRoutePicking();
			colony.getWebElement().click();

			(new WebDriverWait(driver, AbstractPage.STANDARD_TIMEOUT))
					.until(ExpectedConditions.not(ExpectedConditions.attributeContains(By.id("skr-route-select-by-map-button"), "class", "btn-danger")));

			if (colony.isStarbase()) {
				shipDetailsPage.addSelectionToRouteAsLeave(number);
			} else {
				shipDetailsPage.addSelectionToRouteAsTake(number);
			}

			number++;
		}

		WebElement minFuelElem = driver.findElement(By.id("skr-ingame-route-min-fuel-input"));
		minFuelElem.clear();
		minFuelElem.sendKeys("20");

		overviewPage.endTurn();
		scenarioOverviewPage.checkRoundStart(true);

		overviewPage.endTurn();
		scenarioOverviewPage.checkRoundStart(false);

		overviewPage.endTurn();
		scenarioOverviewPage.checkRoundStart(false);

		overviewPage.endTurn();
		scenarioOverviewPage.checkRoundStart(false);

		galaxyMapPage.selectHomeStarbase();
		starbasePage.openUpgrades();
		starbasePage.upgradeScenarioTechlevelsSecond();

		starbasePage.openHulls();
		starbasePage.productShipPart(null, "Zaat", 22, 1);
		starbasePage.waitShortly();

		starbasePage.openPropulsionSystems();
		starbasePage.productShipPart(null, "Solarisplasmotan", 9, 2);
		starbasePage.waitShortly();

		starbasePage.openEnergyWeapons();
		starbasePage.productShipPart(null, "Disruptor", 10, 4);
		starbasePage.waitShortly();

		starbasePage.openProjectileWeapons();
		starbasePage.productShipPart(null, "Gamma bombs", 10, 4);
		starbasePage.waitShortly();

		starbasePage.startShipConstruction();
		starbasePage.buildShip();
		starbasePage.waitShortly();

		overviewPage.endTurn();
		scenarioOverviewPage.checkRoundStart(false);

		galaxyMapPage.selectShipOnStarbasePlanet();
		shipDetailsPage.openProjectiles();
		shipDetailsPage.buildAllPossibleProjectiles();

		shipDetailsPage.transportToShipForBomber(25);
		shipDetailsPage.navigateShipToEnemyShip();

		overviewPage.endTurn();
		scenarioOverviewPage.checkScenarioFinished();
	}

	@Test
	public void testKAchievements() {
		IngameMainPage overviewPage = new IngameMainPage(driver);
		overviewPage.login("/login");

		AchievementsPage achievementsPage = new AchievementsPage(driver);
		achievementsPage.openAchievementsPage();
		achievementsPage.checkAchievements();
	}
}