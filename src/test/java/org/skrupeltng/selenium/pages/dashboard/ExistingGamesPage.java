package org.skrupeltng.selenium.pages.dashboard;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.skrupeltng.modules.ingame.database.TutorialStage;
import org.skrupeltng.selenium.pages.AbstractPage;

public class ExistingGamesPage extends AbstractPage {

	public ExistingGamesPage(WebDriver driver) {
		super(driver);
	}

	public void showExistingGames() {
		driver.findElement(By.id("skr-dashboard-open-games-button")).click();

		(new WebDriverWait(driver, AbstractPage.STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("open-games"));
	}

	public void startTutorial() {
		driver.findElement(By.id("skr-dashboard-start-tutorial-button")).click();

		System.out.println("Starting tutorial...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.and(
						ExpectedConditions.urlContains("ingame"),
						ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-tutorial-start-modal"))));

		driver.findElement(By.id("skr-ingame-start-tutorial-modal-button")).click();

		checkTutorialStage(TutorialStage.INITIAL_SELECT_HOME_PLANET);
	}

	public void showGameDetails() {
		driver.findElement(By.className("skr-game-details-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("game?id="));
	}

	public void showCreateNewGame() {
		driver.findElement(By.id("skr-dashboard-new-game-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("new-game"));

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-dashboard-newgame-create-button")));
	}

	public void checkPlayButtons() {
		int playButtonCount = driver.findElements(By.className("skr-play-button")).size();

		assertEquals(2, playButtonCount);
	}

	public void playGame(long gameId) {
		driver.findElement(By.id("skr-dashboard-play-game-button-" + gameId)).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("ingame/game?id=" + gameId));

	}

	public void checkTableButtons() {
		List<WebElement> joinGameButtons = driver.findElements(By.className("skr-join-game-button"));
		assertEquals(1, joinGameButtons.size());

		joinGameButtons.get(0).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("game?id="));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.className("skr-game-faction-select")));

	}
}