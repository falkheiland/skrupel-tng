package org.skrupeltng.selenium.pages.dashboard;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.skrupeltng.selenium.pages.AbstractPage;

public class ScenarioOverviewPage extends AbstractPage {

	public ScenarioOverviewPage(WebDriver driver) {
		super(driver);
	}

	public void startOrContinueScenario() {
		driver.findElement(By.id("skr-dashboard-scenarios-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-scenario-start-EARLY_GAME-button")));
		driver.findElement(By.id("skr-dashboard-scenarios-button")).click();

		driver.findElement(By.id("skr-scenario-start-EARLY_GAME-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("ingame/game?id="));
	}

	public void checkRoundStart(boolean newStageExpected) {
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("skr-close-overview-button")));

		driver.findElement(By.id("skr-close-overview-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.invisibilityOfElementLocated(By.id("skr-ingame-overview-modal")));

		if (newStageExpected) {
			(new WebDriverWait(driver, STANDARD_TIMEOUT))
					.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".navbar-nav #skr-scenario-text-menu")));

			driver.findElement(By.cssSelector(".navbar-nav .skr-scenario-text-menu-button")).click();
			(new WebDriverWait(driver, STANDARD_TIMEOUT))
					.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".navbar-nav #skr-scenario-text-menu")));
		}
	}

	public void checkScenarioFinished() {
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("scenario-overview"));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("skr-scenario-finished-modal")));

		driver.findElement(By.id("skr-scenario-finished-modal-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.invisibilityOfElementLocated(By.id("skr-scenario-finished-modal")));

		WebElement startScenarioTwoButton = driver.findElement(By.id("skr-scenario-start-MID_GAME-button"));
		assertTrue(startScenarioTwoButton.getAttribute("class").contains("btn-success"));
	}
}