package org.skrupeltng.selenium.pages.dashboard;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.skrupeltng.selenium.pages.AbstractPage;

public class AchievementsPage extends AbstractPage {

	public AchievementsPage(WebDriver driver) {
		super(driver);
	}

	public void openAchievementsPage() {
		System.out.println("Opening achievements page...");

		driver.findElement(By.id("skr-dashboard-account-menu-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("skr-dashboard-achievements-link")));

		driver.findElement(By.id("skr-dashboard-achievements-link")).click();
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.className("skr-achievement-item")));
	}

	public void checkAchievements() {
		List<WebElement> earned = driver.findElements(By.className("earned"));
		assertEquals(4, earned.size());

		assertTrue(driver.findElement(By.id("skr-achievement-item-TUTORIAL_FINISHED")).getAttribute("class").contains("earned"));
		assertTrue(driver.findElement(By.id("skr-achievement-item-SCENARIO_FINISHED_EARLY_GAME")).getAttribute("class").contains("earned"));
		assertTrue(driver.findElement(By.id("skr-achievement-item-PLANETS_COLONIZED_1")).getAttribute("class").contains("earned"));
		assertTrue(driver.findElement(By.id("skr-achievement-item-SHIPS_BUILT_1")).getAttribute("class").contains("earned"));
	}
}