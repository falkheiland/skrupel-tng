package org.skrupeltng.selenium.pages.ingame;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.skrupeltng.selenium.pages.AbstractPage;

public class FleetDetailsPage extends AbstractPage {

	public FleetDetailsPage(WebDriver driver) {
		super(driver);
	}

	public void createFleet() {
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-fleet-name-input")));

		driver.findElement(By.id("skr-ingame-fleet-name-input")).sendKeys("fleet1");

		driver.findElement(By.id("skr-fleet-change-name-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-fleet-navigation-button")));
	}
}