package org.skrupeltng.selenium.pages.ingame;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.skrupeltng.selenium.pages.AbstractPage;

public class ColonyOverviewPage extends AbstractPage {

	public ColonyOverviewPage(WebDriver driver) {
		super(driver);
	}

	public void openColonyOverview() {
		driver.findElement(By.id("skr-ingame-planets-button")).click();

		System.out.println("Loading colony overview...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.elementToBeClickable(By.className("skr-table-row")));

		driver.findElements(By.className("skr-table-row")).get(0).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.not(ExpectedConditions.elementToBeClickable(By.className("skr-table-row"))));

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-planet-selection")));

	}
}