package org.skrupeltng.selenium.pages.ingame;

import org.openqa.selenium.WebElement;

public class WebElementPlanetContainer {

	private WebElement webElement;
	private boolean starbase;

	public WebElement getWebElement() {
		return webElement;
	}

	public void setWebElement(WebElement webElement) {
		this.webElement = webElement;
	}

	public boolean isStarbase() {
		return starbase;
	}

	public void setStarbase(boolean starbase) {
		this.starbase = starbase;
	}
}
