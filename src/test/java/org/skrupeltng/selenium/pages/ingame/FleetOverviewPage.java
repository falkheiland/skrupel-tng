package org.skrupeltng.selenium.pages.ingame;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.skrupeltng.selenium.pages.AbstractPage;

public class FleetOverviewPage extends AbstractPage {

	public FleetOverviewPage(WebDriver driver) {
		super(driver);
	}

	public void openFleetOverview() {
		driver.findElement(By.id("skr-ingame-fleets-button")).click();

		System.out.println("Loading fleet list...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.elementToBeClickable(By.id("skr-fleet-overview-create-fleet-button")));

		driver.findElement(By.id("skr-fleet-overview-create-fleet-button")).click();
	}
}