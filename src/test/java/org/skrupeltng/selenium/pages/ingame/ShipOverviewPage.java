package org.skrupeltng.selenium.pages.ingame;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.skrupeltng.selenium.pages.AbstractPage;

public class ShipOverviewPage extends AbstractPage {

	public ShipOverviewPage(WebDriver driver) {
		super(driver);
	}

	public void openShipOverview() {
		driver.findElement(By.id("skr-ingame-ships-button")).click();

		System.out.println("Loading ships list...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.elementToBeClickable(By.className("skr-table-row")));

		driver.findElements(By.className("skr-table-row")).get(0).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.not(ExpectedConditions.elementToBeClickable(By.className("skr-table-row"))));

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ship-selection")));
	}
}