package org.skrupeltng.modules.ingame.service.round.ship;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.skrupeltng.modules.masterdata.database.ShipModule;
import org.skrupeltng.modules.masterdata.service.MasterDataService;

public class ShipRoundMovementUnitTest {

	@Mock
	private NewsService newsService;

	@Spy
	@InjectMocks
	private ShipRoundMovement subject;

	@Before
	public void setup() {
		MasterDataService.RANDOM.setSeed(1L);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldReduceNoFuel() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		int fuelConsumption = 100;

		int result = subject.checkFuelConsumptionReduction(ship, fuelConsumption);

		assertEquals(100, result);
	}

	@Test
	public void shouldReduceFuelBecauseNoWeapons() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		int fuelConsumption = 100;
		ship.setExperience(1);
		ship.getShipTemplate().setHangarCapacity(0);

		int result = subject.checkFuelConsumptionReduction(ship, fuelConsumption);

		assertEquals(92, result);
	}

	@Test
	public void shouldReduceFuelBecauseModule() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		int fuelConsumption = 100;
		ship.setExperience(1);
		ship.setShipModule(ShipModule.PROPULSON_ENHANCEMENT);

		int result = subject.checkFuelConsumptionReduction(ship, fuelConsumption);

		assertEquals(89, result);
	}

	@Test
	public void shouldNotReduceFuelConsumptionBecauseWrongPropulsion() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		PropulsionSystemTemplate propulsionSystemTemplate = new PropulsionSystemTemplate();
		ship.setPropulsionSystemTemplate(propulsionSystemTemplate);
		int fuelConsumption = 100;

		int result = subject.checkLinearEngineConsumption(ship, fuelConsumption);

		assertEquals(100, result);
		Mockito.verify(newsService, Mockito.never()).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any());
	}

	@Test
	public void shouldNotReduceFuelConsumptionBecauseNoFuelConsumption() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		PropulsionSystemTemplate propulsionSystemTemplate = new PropulsionSystemTemplate();
		propulsionSystemTemplate.setTechLevel(4);
		ship.setPropulsionSystemTemplate(propulsionSystemTemplate);
		int fuelConsumption = 0;

		int result = subject.checkLinearEngineConsumption(ship, fuelConsumption);

		assertEquals(0, result);
		Mockito.verify(newsService, Mockito.never()).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any());
	}

	@Test
	public void shouldReduceFuelConsumptionBecauseChance() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		PropulsionSystemTemplate propulsionSystemTemplate = new PropulsionSystemTemplate();
		propulsionSystemTemplate.setTechLevel(4);
		ship.setPropulsionSystemTemplate(propulsionSystemTemplate);
		int fuelConsumption = 100;

		MasterDataService.RANDOM.nextInt();
		MasterDataService.RANDOM.nextInt();
		MasterDataService.RANDOM.nextInt();
		MasterDataService.RANDOM.nextInt();
		MasterDataService.RANDOM.nextInt();

		int result = subject.checkLinearEngineConsumption(ship, fuelConsumption);

		assertEquals(63, result);
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_linear_engine_fuel_reduction, ship.createFullImagePath(), ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName(), 100, 63);
	}

	@Test
	public void shouldReduceNoFuelBecauseChance() {
		MasterDataService.RANDOM.setSeed(3L);

		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		PropulsionSystemTemplate propulsionSystemTemplate = new PropulsionSystemTemplate();
		propulsionSystemTemplate.setTechLevel(4);
		ship.setPropulsionSystemTemplate(propulsionSystemTemplate);
		int fuelConsumption = 100;

		int result = subject.checkLinearEngineConsumption(ship, fuelConsumption);

		assertEquals(100, result);
		Mockito.verify(newsService, Mockito.never()).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any());
	}
}