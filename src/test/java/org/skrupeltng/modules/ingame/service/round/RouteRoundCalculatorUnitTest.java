package org.skrupeltng.modules.ingame.service.round;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;

public class RouteRoundCalculatorUnitTest {

	@Spy
	@InjectMocks
	private final RouteRoundCalculator subject = new RouteRoundCalculator();

	@Mock
	private ShipRepository shipRepository;

	@Mock
	private ShipRouteEntryRepository shipRouteEntryRepository;

	@Mock
	private PlanetRepository planetRepository;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldNotLoadAnythingBecauseShipFull() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship.setSupplies(100);
		ship.setMineral1(900);

		Planet planet = Mockito.spy(new Planet());
		planet.setSupplies(900);

		int max = 1000;
		int shipTotal = 1000;
		int shipRes = 100;
		int planetRes = 900;

		subject.load(Ship::setSupplies, ship, max, shipTotal, shipRes, planetRes, planet, Planet::setSupplies);

		assertEquals(100, ship.getSupplies());
		assertEquals(900, ship.getMineral1());

		assertEquals(900, planet.getSupplies());

		Mockito.verify(ship, Mockito.times(1)).setSupplies(Mockito.anyInt());
		Mockito.verify(ship, Mockito.times(1)).setSupplies(100);
		Mockito.verify(planet, Mockito.times(1)).setSupplies(Mockito.anyInt());
		Mockito.verify(planet, Mockito.times(1)).setSupplies(900);
	}

	@Test
	public void shouldNotLoadAnythingBecauseNothingOnPlanet() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship.setSupplies(100);
		ship.setMineral1(900);

		Planet planet = Mockito.spy(new Planet());

		int max = 1000;
		int shipTotal = 1000;
		int shipRes = 100;
		int planetRes = 0;

		subject.load(Ship::setSupplies, ship, max, shipTotal, shipRes, planetRes, planet, Planet::setSupplies);

		assertEquals(100, ship.getSupplies());
		assertEquals(900, ship.getMineral1());

		assertEquals(0, planet.getSupplies());

		Mockito.verify(ship, Mockito.times(1)).setSupplies(Mockito.anyInt());
		Mockito.verify(ship, Mockito.times(1)).setSupplies(100);
		Mockito.verify(planet, Mockito.times(0)).setSupplies(Mockito.anyInt());
	}

	@Test
	public void shouldLoadEverything() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship.setSupplies(100);

		Planet planet = Mockito.spy(new Planet());
		planet.setSupplies(200);

		int max = 1000;
		int shipTotal = 100;
		int shipRes = 100;
		int planetRes = 200;

		subject.load(Ship::setSupplies, ship, max, shipTotal, shipRes, planetRes, planet, Planet::setSupplies);

		assertEquals(300, ship.getSupplies());

		assertEquals(0, planet.getSupplies());

		Mockito.verify(ship, Mockito.times(2)).setSupplies(Mockito.anyInt());
		Mockito.verify(ship, Mockito.times(1)).setSupplies(100);
		Mockito.verify(ship, Mockito.times(1)).setSupplies(300);
		Mockito.verify(planet, Mockito.times(2)).setSupplies(Mockito.anyInt());
		Mockito.verify(planet, Mockito.times(1)).setSupplies(0);
	}

	@Test
	public void shouldOnlyLoadTillMax() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship.setSupplies(100);

		Planet planet = Mockito.spy(new Planet());
		planet.setSupplies(1000);

		int max = 1000;
		int shipTotal = 100;
		int shipRes = 100;
		int planetRes = 1000;

		subject.load(Ship::setSupplies, ship, max, shipTotal, shipRes, planetRes, planet, Planet::setSupplies);

		assertEquals(1000, ship.getSupplies());

		assertEquals(100, planet.getSupplies());

		Mockito.verify(ship, Mockito.times(2)).setSupplies(Mockito.anyInt());
		Mockito.verify(ship, Mockito.times(1)).setSupplies(100);
		Mockito.verify(ship, Mockito.times(1)).setSupplies(1000);
		Mockito.verify(planet, Mockito.times(2)).setSupplies(Mockito.anyInt());
		Mockito.verify(planet, Mockito.times(1)).setSupplies(100);
	}
}