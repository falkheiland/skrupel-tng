package org.skrupeltng.modules.ingame.service.round.wincondition;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.testcontainers.shaded.com.google.common.collect.Lists;

public class SurviveWinConditionHandlerUnitTest {

	@Spy
	@InjectMocks
	private final SurviveWinConditionHandler subject = new SurviveWinConditionHandler();

	@Mock
	private GameRepository gameRepository;

	@Mock
	private PlayerRepository playerRepository;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldNotFinishGame() {
		Player player1 = new Player(1L);
		Player player2 = new Player(2L);
		Mockito.when(playerRepository.getNotLostPlayers(1L)).thenReturn(Lists.newArrayList(player1, player2));

		subject.checkWinCondition(1L);

		Mockito.verify(gameRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void shouldFinishGameBecauseOfOneSurvivor() {
		Player player1 = new Player(1L);
		Mockito.when(playerRepository.getNotLostPlayers(1L)).thenReturn(Lists.newArrayList(player1));

		Game game = new Game();
		Mockito.when(gameRepository.getOne(1L)).thenReturn(game);

		subject.checkWinCondition(1L);

		Mockito.verify(gameRepository).save(game);
		assertEquals(true, game.isFinished());
	}

	@Test
	public void shouldFinishGameBecauseOfNoSurvivor() {
		Mockito.when(playerRepository.getNotLostPlayers(1L)).thenReturn(Lists.newArrayList());

		Game game = new Game();
		Mockito.when(gameRepository.getOne(1L)).thenReturn(game);

		subject.checkWinCondition(1L);

		Mockito.verify(gameRepository).save(game);
		assertEquals(true, game.isFinished());
	}
}