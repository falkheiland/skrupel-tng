package org.skrupeltng.modules.ingame.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Collections;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.modules.overview.controller.PlayerRelationChangeRequest;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationAction;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRequest;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRequestRepository;
import org.skrupeltng.modules.ingame.modules.politics.service.PlayerRelationChange;
import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;

import com.google.common.collect.Lists;

public class PoliticsServiceUnitTest {

	@Spy
	@InjectMocks
	private final PoliticsService subject = new PoliticsService();

	@Mock
	private PlayerRelationRepository playerRelationRepository;

	@Mock
	private PlayerRelationRequestRepository playerRelationRequestRepository;

	@Mock
	private PlayerRelationChange playerRelationChange;

	@Mock
	private GameRepository gameRepository;

	private Game game;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		game = new Game();
		Mockito.when(gameRepository.getByPlayerId(Mockito.anyLong())).thenReturn(game);
	}

	@Test
	public void shouldCheckExistingRequest() {
		PlayerRelationChangeRequest request = new PlayerRelationChangeRequest();
		request.setPlayerId(2L);
		request.setGameId(1L);
		request.setAction(PlayerRelationAction.OFFER_ALLIANCE.name());

		PlayerRelationRequest existing = new PlayerRelationRequest();
		Mockito.when(playerRelationRequestRepository.findByPlayerIds(1L, 2L)).thenReturn(Optional.of(existing));

		try {
			subject.changeRelation(1L, request);
			fail("IllegalArgumentException expected!");
		} catch (IllegalArgumentException e) {
			assertEquals("There already is a change request!", e.getMessage());
		}

		Mockito.verify(playerRelationChange, Mockito.never()).checkExistingRelation(Mockito.anyLong(), Mockito.anyLong(), Mockito.any(), Mockito.any(),
				Mockito.any());
		Mockito.verify(playerRelationChange, Mockito.never()).createNewRelation(Mockito.anyLong(), Mockito.any(), Mockito.anyLong(), Mockito.any());
	}

	@Test
	public void shouldCallCheckExistingRelation() {
		PlayerRelationChangeRequest request = new PlayerRelationChangeRequest();
		request.setPlayerId(2L);
		request.setGameId(1L);
		request.setAction(PlayerRelationAction.OFFER_ALLIANCE.name());

		Mockito.when(playerRelationRequestRepository.findByPlayerIds(1L, 2L)).thenReturn(Optional.empty());

		PlayerRelation existingRelation = new PlayerRelation();
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Lists.newArrayList(existingRelation));

		subject.changeRelation(1L, request);

		Mockito.verify(playerRelationChange).checkExistingRelation(1L, 2L, PlayerRelationAction.OFFER_ALLIANCE, existingRelation, game);
		Mockito.verify(playerRelationChange, Mockito.never()).createNewRelation(Mockito.anyLong(), Mockito.any(), Mockito.anyLong(), Mockito.any());
	}

	@Test
	public void shouldCallCreateNewRelation() {
		PlayerRelationChangeRequest request = new PlayerRelationChangeRequest();
		request.setPlayerId(2L);
		request.setGameId(1L);
		request.setAction(PlayerRelationAction.OFFER_ALLIANCE.name());

		Mockito.when(playerRelationRequestRepository.findByPlayerIds(1L, 2L)).thenReturn(Optional.empty());

		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Collections.emptyList());

		subject.changeRelation(1L, request);

		Mockito.verify(playerRelationChange, Mockito.never()).checkExistingRelation(Mockito.anyLong(), Mockito.anyLong(), Mockito.any(), Mockito.any(),
				Mockito.any());
		Mockito.verify(playerRelationChange).createNewRelation(1L, PlayerRelationAction.OFFER_ALLIANCE, 2L, game);
	}

	@Test
	public void shouldDeleteRelation() {
		PlayerRelationRequest request = new PlayerRelationRequest();
		request.setId(1L);
		Player requestingPlayer = new Player(1L);
		requestingPlayer.setGame(new Game(1L));
		request.setRequestingPlayer(requestingPlayer);
		request.setOtherPlayer(new Player(2L));
		Mockito.when(playerRelationRequestRepository.getOne(1L)).thenReturn(request);

		PlayerRelation relation = new PlayerRelation();
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Lists.newArrayList(relation));

		long gameId = subject.acceptRelationRequest(1L);
		assertEquals(1L, gameId);

		Mockito.verify(playerRelationRepository).delete(relation);
		Mockito.verify(playerRelationRequestRepository).delete(request);

		Mockito.verify(playerRelationRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void shouldUseExistingRelation() {
		PlayerRelationRequest request = new PlayerRelationRequest();
		request.setType(PlayerRelationType.ALLIANCE);
		request.setId(1L);
		Player requestingPlayer = new Player(1L);
		requestingPlayer.setGame(new Game(1L));
		request.setRequestingPlayer(requestingPlayer);
		request.setOtherPlayer(new Player(2L));
		Mockito.when(playerRelationRequestRepository.getOne(1L)).thenReturn(request);

		PlayerRelation relation = new PlayerRelation();
		relation.setPlayer1(requestingPlayer);
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Lists.newArrayList(relation));

		long gameId = subject.acceptRelationRequest(1L);
		assertEquals(1L, gameId);

		Mockito.verify(playerRelationRepository, Mockito.never()).delete(Mockito.any());
		Mockito.verify(playerRelationRequestRepository).delete(request);

		Mockito.verify(playerRelationRepository).save(relation);
		assertEquals(PlayerRelationType.ALLIANCE, relation.getType());
	}

	@Test
	public void shouldCreateNewRelation() {
		PlayerRelationRequest request = new PlayerRelationRequest();
		request.setType(PlayerRelationType.ALLIANCE);
		request.setId(1L);
		Player requestingPlayer = new Player(1L);
		requestingPlayer.setGame(new Game(1L));
		request.setRequestingPlayer(requestingPlayer);
		request.setOtherPlayer(new Player(2L));
		Mockito.when(playerRelationRequestRepository.getOne(1L)).thenReturn(request);

		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Collections.emptyList());

		long gameId = subject.acceptRelationRequest(1L);
		assertEquals(1L, gameId);

		Mockito.verify(playerRelationRepository, Mockito.never()).delete(Mockito.any());
		Mockito.verify(playerRelationRequestRepository).delete(request);

		ArgumentCaptor<PlayerRelation> arg = ArgumentCaptor.forClass(PlayerRelation.class);
		Mockito.verify(playerRelationRepository).save(arg.capture());
		assertEquals(PlayerRelationType.ALLIANCE, arg.getValue().getType());
	}

	@Test
	public void shouldDeleteRelationRequest() {
		PlayerRelationRequest request = new PlayerRelationRequest();
		Player requestingPlayer = new Player(1L);
		requestingPlayer.setGame(new Game(1L));
		request.setRequestingPlayer(requestingPlayer);
		Mockito.when(playerRelationRequestRepository.getOne(1L)).thenReturn(request);

		long gameId = subject.deleteRelationRequest(1L);
		assertEquals(1L, gameId);

		Mockito.verify(playerRelationRequestRepository).delete(request);
	}

	@Test
	public void shouldCheckPresenseOfRelation() {
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Collections.emptyList());

		boolean result = subject.isAlly(1L, 2L);
		assertEquals(false, result);
	}

	@Test
	public void shouldCheckAllRelationTypes() {
		PlayerRelationType[] values = PlayerRelationType.values();

		for (PlayerRelationType type : values) {
			switch (type) {
				case ALLIANCE:
					assertRelation(type, true);
					break;
				case NON_AGGRESSION_TREATY:
					assertRelation(type, true);
					break;
				case TRADE_AGREEMENT:
					assertRelation(type, false);
					break;
				case WAR:
					assertRelation(type, false);
					break;
			}
		}
	}

	private void assertRelation(PlayerRelationType type, boolean expected) {
		PlayerRelation relation = new PlayerRelation();
		relation.setType(type);
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Lists.newArrayList(relation));

		boolean result = subject.isAlly(1L, 2L);
		assertEquals(expected, result);
	}
}