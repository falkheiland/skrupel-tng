package org.skrupeltng.modules.ingame.service.round;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.PlasmaStorm;
import org.skrupeltng.modules.ingame.modules.anomaly.database.PlasmaStormRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.testcontainers.shaded.com.google.common.collect.Lists;

public class PlasmaStormRoundCalculatorUnitTest {

	private PlasmaStormRoundCalculator subject;
	private PlasmaStormRepository plasmaStormRepository;
	private GameRepository gameRepository;
	private ShipRepository shipRepository;

	@Before
	public void setup() {
		MasterDataService.RANDOM.setSeed(1L);

		subject = Mockito.spy(new PlasmaStormRoundCalculator());

		plasmaStormRepository = Mockito.mock(PlasmaStormRepository.class);
		subject.setPlasmaStormRepository(plasmaStormRepository);

		gameRepository = Mockito.mock(GameRepository.class);
		subject.setGameRepository(gameRepository);

		shipRepository = Mockito.mock(ShipRepository.class);
		subject.setShipRepository(shipRepository);
	}

	@Test
	public void shouldCountDownRoundsLeft() {
		PlasmaStorm storm = new PlasmaStorm();
		storm.setRoundsLeft(3);

		Mockito.when(plasmaStormRepository.findByGameId(1L)).thenReturn(Lists.newArrayList(storm));

		subject.processPlasmaStormDepletion(1L);

		assertEquals(2, storm.getRoundsLeft());
		Mockito.verify(plasmaStormRepository).save(storm);
		Mockito.verify(plasmaStormRepository, Mockito.never()).delete(Mockito.any());
	}

	@Test
	public void shouldDeleteStorms() {
		PlasmaStorm storm = new PlasmaStorm();
		storm.setRoundsLeft(1);

		Mockito.when(plasmaStormRepository.findByGameId(1L)).thenReturn(Lists.newArrayList(storm));

		subject.processPlasmaStormDepletion(1L);

		Mockito.verify(plasmaStormRepository, Mockito.never()).save(Mockito.any());
		Mockito.verify(plasmaStormRepository).delete(storm);
	}

	@Test
	public void shouldCheckActiveStormCount() {
		Mockito.when(plasmaStormRepository.getActiveStormCount(1L)).thenReturn(2L);

		Game game = new Game(1L);
		game.setMaxConcurrentPlasmaStormCount(2);
		Mockito.when(gameRepository.getOne(1L)).thenReturn(game);

		subject.processPlasmaStormCreation(1L);

		Mockito.verify(subject, Mockito.never()).createNewStorm(Mockito.any());
	}

	@Test
	public void shouldCheckCreationProbability() {
		Mockito.when(plasmaStormRepository.getActiveStormCount(1L)).thenReturn(1L);

		Game game = new Game(1L);
		game.setMaxConcurrentPlasmaStormCount(2);
		game.setPlasmaStormProbability(1f);
		Mockito.when(gameRepository.getOne(1L)).thenReturn(game);

		subject.processPlasmaStormCreation(1L);

		Mockito.verify(subject, Mockito.never()).createNewStorm(Mockito.any());
	}

	@Test
	public void shouldCreatePlasmaStorm() {
		Mockito.when(plasmaStormRepository.getActiveStormCount(1L)).thenReturn(1L);

		Game game = new Game(1L);
		game.setMaxConcurrentPlasmaStormCount(2);
		game.setPlasmaStormProbability(100f);
		game.setGalaxySize(250);
		game.setPlasmaStormRounds(3);
		Mockito.when(gameRepository.getOne(1L)).thenReturn(game);

		subject.processPlasmaStormCreation(1L);

		Mockito.verify(subject).createNewStorm(game);

		Mockito.verify(plasmaStormRepository).getMaxStormId(1L);

		ArgumentCaptor<PlasmaStorm> arg = ArgumentCaptor.forClass(PlasmaStorm.class);
		Mockito.verify(plasmaStormRepository, Mockito.atLeastOnce()).save(arg.capture());

		List<PlasmaStorm> storms = arg.getAllValues();
		assertEquals(276, storms.size());

		for (PlasmaStorm storm : storms) {
			assertEquals(1L, storm.getStormId());
			assertEquals(game, storm.getGame());
			assertTrue(storm.getX() >= 0 && storm.getX() <= 250);
			assertTrue(storm.getY() >= 0 && storm.getY() <= 250);
			assertTrue(storm.getRoundsLeft() >= 3 && storm.getRoundsLeft() <= game.getPlasmaStormRounds());
		}
	}

	@Test
	public void shouldReduceWarpSpeed() {
		subject.processShips(1L);
		Mockito.verify(shipRepository).processShipsInPlasmaStorms(1L);
	}
}