package org.skrupeltng.modules.ingame.service.round.wincondition;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerDeathFoeRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.testcontainers.shaded.com.google.common.collect.Lists;

public class DeathFoeWinConditionHandlerUnitTest {

	@Spy
	@InjectMocks
	private final DeathFoeWinConditionHandler subject = new DeathFoeWinConditionHandler();

	@Mock
	private GameRepository gameRepository;

	@Mock
	private PlayerDeathFoeRepository playerDeathFoeRepository;

	@Mock
	private PlayerRepository playerRepository;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldCheckLosersPresent() {
		subject.checkWinCondition(1L);

		Mockito.verify(playerRepository, Mockito.never()).save(Mockito.any());
		Mockito.verify(gameRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void shouldDetectAllWinners() {
		Player player3 = new Player(3L);
		Player player4 = new Player(4L);
		Mockito.when(playerRepository.getLostPlayers(1L)).thenReturn(Lists.newArrayList(player3, player4));

		Player player1 = new Player(1L);
		Mockito.when(playerDeathFoeRepository.getPlayerByDeathFoe(3L)).thenReturn(player1);

		Player player2 = new Player(2L);
		Mockito.when(playerDeathFoeRepository.getPlayerByDeathFoe(4L)).thenReturn(player2);

		Game game = new Game(1L);
		game.setPlayers(Lists.newArrayList(player1, player2, player3, player4));
		Mockito.when(gameRepository.getOne(1L)).thenReturn(game);

		subject.checkWinCondition(1L);

		Mockito.verify(playerRepository, Mockito.times(2)).save(Mockito.any());
		Mockito.verify(playerRepository).save(player3);
		Mockito.verify(playerRepository).save(player4);
		Mockito.verify(gameRepository).save(game);
		assertEquals(true, game.isFinished());
	}
}