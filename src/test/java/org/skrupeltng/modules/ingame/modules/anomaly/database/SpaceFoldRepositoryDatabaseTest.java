package org.skrupeltng.modules.ingame.modules.anomaly.database;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.skrupeltng.AbstractDbTest;
import org.springframework.beans.factory.annotation.Autowired;

public class SpaceFoldRepositoryDatabaseTest extends AbstractDbTest {

	@Autowired
	private SpaceFoldRepository spaceFoldRepository;

	@Test
	public void shouldFindSpaceFoldsInRadius() {
		List<Long> ids = spaceFoldRepository.getSpaceFoldIdsInRadius(1L, 100, 100, 50);
		assertEquals(1, ids.size());
		assertEquals(1L, ids.get(0).longValue());
	}
}