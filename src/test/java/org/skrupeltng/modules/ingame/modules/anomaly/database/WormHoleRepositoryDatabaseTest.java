package org.skrupeltng.modules.ingame.modules.anomaly.database;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.skrupeltng.AbstractDbTest;
import org.springframework.beans.factory.annotation.Autowired;

public class WormHoleRepositoryDatabaseTest extends AbstractDbTest {

	@Autowired
	private WormHoleRepository wormHoleRepository;

	@Test
	public void shouldFindWormHolesInRadius() {
		List<Long> ids = wormHoleRepository.getWormHoleIdsInRadius(1L, 200, 200, 50);
		assertEquals(1, ids.size());
		assertEquals(1L, ids.get(0).longValue());
	}
}