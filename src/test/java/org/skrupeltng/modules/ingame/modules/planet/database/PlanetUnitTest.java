package org.skrupeltng.modules.ingame.modules.planet.database;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PlanetUnitTest {

	@Test
	public void shouldUseFastGrowthLimit() {
		Planet planet = new Planet();
		planet.setColonists(10000);

		int result = planet.retrieveColonistBuildingFactor(0, 1f, 200);

		assertEquals(100, result);
	}

	@Test
	public void shouldNotUseFastGrowthLimit() {
		Planet planet = new Planet();
		planet.setColonists(20001);

		int result = planet.retrieveColonistBuildingFactor(0, 1f, 200);

		assertEquals(214, result);

		planet.setColonists(30000);

		result = planet.retrieveColonistBuildingFactor(0, 1f, 200);

		assertEquals(217, result);
	}
}