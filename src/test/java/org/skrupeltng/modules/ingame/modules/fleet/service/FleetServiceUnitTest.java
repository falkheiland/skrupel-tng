package org.skrupeltng.modules.ingame.modules.fleet.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.fleet.controller.FleetResourceData;
import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;

public class FleetServiceUnitTest {

	@Spy
	@InjectMocks
	private FleetService subject;

	@Mock
	private FleetRepository fleetRepository;

	@Mock
	private PlayerRepository playerRepository;

	@Mock
	private ShipRepository shipRepository;

	@Mock
	private ShipService shipService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldCreateValidFleetResourceData() {
		Fleet fleet = new Fleet();
		fleet.setPlayer(new Player(1L));

		List<Ship> ships = new ArrayList<>();
		fleet.setShips(ships);

		Ship ship1 = ShipTestFactory.createShip(100, 100, 1, 10, 1, 1L);
		ship1.getShipTemplate().setFuelCapacity(100);
		ship1.setFuel(50);
		ship1.setProjectiles(25);
		ships.add(ship1);

		Ship ship2 = ShipTestFactory.createShip(100, 100, 1, 10, 1, 1L);
		ship2.getShipTemplate().setFuelCapacity(100);
		ship2.setFuel(50);
		ship2.setProjectiles(25);

		Planet planet = new Planet();
		planet.setFuel(25);
		planet.setMoney(875);
		planet.setMineral1(50);
		planet.setMineral2(25);
		ship2.setPlanet(planet);
		ships.add(ship2);

		Mockito.when(fleetRepository.getOne(1L)).thenReturn(fleet);

		FleetResourceData result = subject.getFleetResourceData(1L);

		assertNotNull(result);
		assertEquals(50, result.getCurrentTankPercentage());
		assertEquals(63, result.getPossibleTankPercentage());

		assertEquals(50, result.getCurrentProjectilePercentage());
		assertEquals(75, result.getPossibleProjectilePercentage());
	}
}