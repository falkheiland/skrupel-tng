package org.skrupeltng.modules.ingame.modules.planet.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.skrupeltng.AbstractDbTest;
import org.skrupeltng.modules.ingame.controller.PlanetEntry;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class PlanetRepositoryDatabaseTest extends AbstractDbTest {

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Test
	public void shouldReturnAllExistingPlanets() {
		int count = planetRepository.findByGameId(1L).size();
		List<PlanetEntry> planets = planetRepository.getPlanetsForGalaxy(1L);
		assertEquals(count, planets.size());

		for (PlanetEntry planet : planets) {
			if (planet.getPlayerId() != null) {
				assertNotNull(planet.getPlayerColor());
			} else {
				assertNull(planet.getPlayerColor());
				assertNull(planet.getStarbaseId());
			}
		}
	}

	@Test
	public void shouldReturnNoPlanets() {
		List<PlanetEntry> planets = planetRepository.getPlanetsForGalaxy(4L);
		assertEquals(0, planets.size());
	}

	@Test
	public void shouldAcceptHomePlanetAsOwnedByPlayer() {
		Player player = playerRepository.getOne(1L);
		Planet homePlanet = player.getHomePlanet();
		boolean loginOwnsPlanet = planetRepository.loginOwnsPlanet(homePlanet.getId(), player.getLogin().getId());
		assertTrue(loginOwnsPlanet);
	}

	@Test
	public void shouldNotAcceptEnemyPlanetAsOwnedByPlayer() {
		Player player = playerRepository.getOne(1L);
		Player other = playerRepository.getOne(2L);
		Planet otherHomePlanet = other.getHomePlanet();
		boolean loginOwnsPlanet = planetRepository.loginOwnsPlanet(otherHomePlanet.getId(), player.getLogin().getId());
		assertFalse(loginOwnsPlanet);
	}

	@Test
	public void shouldNotAcceptUninhabitedPlanetAsOwnedByPlayer() {
		Player player = playerRepository.getOne(1L);
		Planet uninhabitedPlanet = planetRepository.findAll().stream().filter(p -> p.getPlayer() == null).findFirst().get();
		boolean loginOwnsPlanet = planetRepository.loginOwnsPlanet(uninhabitedPlanet.getId(), player.getLogin().getId());
		assertFalse(loginOwnsPlanet);
	}

	@Test
	public void shouldReturnPlanetsInRadiusColonizedOnly() {
		List<Long> planetIds = planetRepository.getPlanetIdsInRadius(1L, 469, 71, 20, true);
		assertEquals(1, planetIds.size());
	}

	@Test
	public void shouldReturnPlanetsInRadius() {
		List<Long> planetIds = planetRepository.getPlanetIdsInRadius(1L, 139, 950, 20, false);
		assertEquals(1, planetIds.size());
	}

	@Test
	public void shouldReturnNoPlanetsInRadius() {
		List<Long> planetIds = planetRepository.getPlanetIdsInRadius(1L, 180, 100, 2, true);
		assertEquals(0, planetIds.size());
	}

	@Test
	public void shouldCheckRevealingFaction() {
		boolean result = planetRepository.hasRevealingNativeSpeciesPlanets(1L);
		assertTrue(result);

		result = planetRepository.hasRevealingNativeSpeciesPlanets(4L);
		assertFalse(result);
	}

	@Test
	public void shouldResetScanRadius() {
		planetRepository.resetScanRadius(2L);

		List<Planet> planets = planetRepository.findByGameId(2L);

		for (Planet planet : planets) {
			assertEquals(53, planet.getScanRadius());
		}
	}

	@Test
	public void shouldSetExtendedScanRadius() {
		planetRepository.updateExtendedScanRadius(1L);

		Planet planet = planetRepository.getOne(18L);
		assertEquals(116, planet.getScanRadius());
	}

	@Test
	public void shouldSetPsyCorpsScanRadius() {
		planetRepository.updatePsyCorpsScanRadius(1L);

		Planet planet = planetRepository.getOne(1L);
		assertEquals(90, planet.getScanRadius());
	}

	@Test
	public void shouldReturnPlanetsWithoutRoute() {
		List<Long> result = planetRepository.findOwnedPlanetIdsWithoutRoute(3L);
		assertEquals(1, result.size());
		assertTrue(result.contains(80L));

		result = planetRepository.findOwnedPlanetIdsWithoutRoute(1L);
		assertEquals(1, result.size());
		assertTrue(result.contains(3L));
	}

	@Test
	public void shouldReturnRandomPlanetId() {
		long result = planetRepository.getRandomPlanetId(1L);
		assertTrue(result > 0L);
	}
}