package org.skrupeltng.modules.ingame.modules.overview.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.skrupeltng.AbstractDbTest;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.overview.service.RoundSummary;
import org.springframework.beans.factory.annotation.Autowired;

public class NewsEntryRepositoryDatabaseTest extends AbstractDbTest {

	@Autowired
	private NewsEntryRepository newsEntryRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Test
	public void shouldDeleteNewsEntries() {
		newsEntryRepository.deleteByGameId(1L);

		List<Player> players = playerRepository.findByGameId(1L);

		for (Player player : players) {
			List<NewsEntry> list = newsEntryRepository.findByGameIdAndLoginId(1L, player.getLogin().getId());
			assertEquals(0, list.size());
		}
	}

	@Test
	public void shouldReturnSummary() {
		RoundSummary summary = newsEntryRepository.getRoundSummary(1L);
		assertNotNull(summary);

		assertEquals(0, summary.getNewShips());
		assertEquals(0, summary.getDestroyedShips());
		assertEquals(0, summary.getLostShips());
		assertEquals(0, summary.getNewColonies());
		assertEquals(0, summary.getNewStarbases());
		assertEquals(WinCondition.SURVIVE.name(), summary.getWinCondition());
	}
}