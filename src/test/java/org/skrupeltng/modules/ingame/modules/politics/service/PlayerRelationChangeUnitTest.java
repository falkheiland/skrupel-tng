package org.skrupeltng.modules.ingame.modules.politics.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationAction;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRequest;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRequestRepository;

public class PlayerRelationChangeUnitTest {

	@Spy
	@InjectMocks
	private final PlayerRelationChange subject = new PlayerRelationChange();

	@Mock
	private PlayerRelationRepository playerRelationRepository;

	@Mock
	private PlayerRelationRequestRepository playerRelationRequestRepository;

	@Mock
	private PlayerRepository playerRepository;

	private Game game;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		game = new Game();
	}

	@Test
	public void shouldCheckExistingRelationType() {
		checkExistingRelationType(PlayerRelationType.NON_AGGRESSION_TREATY, PlayerRelationAction.CANCEL_ALLIANCE, "Cannot cancel non-existing alliance!");
		checkExistingRelationType(PlayerRelationType.ALLIANCE, PlayerRelationAction.CANCEL_NON_AGGRESSION_TREATY,
				"Cannot cancel non-existing non-aggression treaty!");
		checkExistingRelationType(PlayerRelationType.NON_AGGRESSION_TREATY, PlayerRelationAction.CANCEL_TRADE_AGREEMENT,
				"Cannot cancel non-existing trade agreement!");
		checkExistingRelationType(PlayerRelationType.WAR, PlayerRelationAction.DELCARE_WAR, "War already has been declared!");
		checkExistingRelationType(PlayerRelationType.WAR, PlayerRelationAction.OFFER_ALLIANCE, "There already exists a relation!");
		checkExistingRelationType(PlayerRelationType.WAR, PlayerRelationAction.OFFER_NON_AGGRESSION_TREATY, "There already exists a relation!");
		checkExistingRelationType(PlayerRelationType.NON_AGGRESSION_TREATY, PlayerRelationAction.OFFER_PEACE, "Can only request peace in war!");
		checkExistingRelationType(PlayerRelationType.WAR, PlayerRelationAction.OFFER_TRADE_AGREEMENT, "There already exists a relation!");

		game.setWinCondition(WinCondition.TEAM_DEATH_FOE);
		checkExistingRelationType(null, PlayerRelationAction.OFFER_ALLIANCE, "Cannot offer alliance in Team Death Foe!");
		checkExistingRelationType(null, PlayerRelationAction.CANCEL_ALLIANCE, "Cannot cancel alliance in Team Death Foe!");
	}

	private void checkExistingRelationType(PlayerRelationType relationType, PlayerRelationAction action, String expectedMessage) {
		PlayerRelation relation = new PlayerRelation();
		relation.setType(relationType);

		try {
			subject.checkExistingRelation(1L, 2L, action, relation, game);
			fail("IllegalArgumentException expected");
		} catch (IllegalArgumentException e) {
			assertEquals(expectedMessage, e.getMessage());
		}

		Mockito.verify(playerRelationRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void shouldCancelCorrectly() {
		cancel(PlayerRelationType.ALLIANCE, PlayerRelationAction.CANCEL_ALLIANCE, 12);
		cancel(PlayerRelationType.NON_AGGRESSION_TREATY, PlayerRelationAction.CANCEL_NON_AGGRESSION_TREATY, 6);
		cancel(PlayerRelationType.TRADE_AGREEMENT, PlayerRelationAction.CANCEL_TRADE_AGREEMENT, 3);
		cancel(PlayerRelationType.TRADE_AGREEMENT, PlayerRelationAction.DELCARE_WAR, -1);
	}

	private void cancel(PlayerRelationType type, PlayerRelationAction action, int expectedRoundsLeft) {
		PlayerRelation relation = new PlayerRelation();
		relation.setType(type);

		subject.checkExistingRelation(1L, 2L, action, relation, game);

		assertEquals(expectedRoundsLeft, relation.getRoundsLeft());

		Mockito.verify(playerRelationRepository).save(relation);
	}

	@Test
	public void shouldAddRelationRequest() {
		addRelationRequest(PlayerRelationAction.OFFER_ALLIANCE, PlayerRelationType.ALLIANCE);
		addRelationRequest(PlayerRelationAction.OFFER_NON_AGGRESSION_TREATY, PlayerRelationType.NON_AGGRESSION_TREATY);
		addRelationRequest(PlayerRelationAction.OFFER_TRADE_AGREEMENT, PlayerRelationType.TRADE_AGREEMENT);
	}

	private void addRelationRequest(PlayerRelationAction action, PlayerRelationType expectedType) {
		PlayerRelation relation = new PlayerRelation();

		subject.checkExistingRelation(1L, 2L, action, relation, game);

		Mockito.verify(subject).addRelationRequest(1L, 2L, expectedType);
	}

	@Test
	public void shouldOfferPeace() {
		PlayerRelation relation = new PlayerRelation();
		relation.setType(PlayerRelationType.WAR);

		subject.checkExistingRelation(1L, 2L, PlayerRelationAction.OFFER_PEACE, relation, game);

		Mockito.verify(subject).addRelationRequest(1L, 2L, null);
	}

	@Test
	public void shouldCreateNewRelation() {
		subject.createNewRelation(1L, PlayerRelationAction.DELCARE_WAR, 2L, game);

		Mockito.verify(subject, Mockito.never()).addRelationRequest(Mockito.anyLong(), Mockito.anyLong(), Mockito.any());
		Mockito.verify(playerRelationRepository).save(Mockito.any());

		createNewRelation(PlayerRelationAction.OFFER_ALLIANCE, PlayerRelationType.ALLIANCE);
		createNewRelation(PlayerRelationAction.OFFER_NON_AGGRESSION_TREATY, PlayerRelationType.NON_AGGRESSION_TREATY);
		createNewRelation(PlayerRelationAction.OFFER_TRADE_AGREEMENT, PlayerRelationType.TRADE_AGREEMENT);
	}

	private void createNewRelation(PlayerRelationAction action, PlayerRelationType type) {
		subject.createNewRelation(1L, action, 2L, game);
		Mockito.verify(subject).addRelationRequest(1L, 2L, type);
	}

	@Test
	public void shouldNotCreateNewRelation() {
		dontCreateNewRelation(PlayerRelationAction.CANCEL_ALLIANCE, "Cannot cancel non-existing alliance!");
		dontCreateNewRelation(PlayerRelationAction.CANCEL_NON_AGGRESSION_TREATY, "Cannot cancel non-existing non-aggression treaty!");
		dontCreateNewRelation(PlayerRelationAction.OFFER_PEACE, "Can only offer peace in war!");
		dontCreateNewRelation(PlayerRelationAction.CANCEL_TRADE_AGREEMENT, "Cannot cancel non-existing trade agreement!");

		game.setWinCondition(WinCondition.TEAM_DEATH_FOE);
		dontCreateNewRelation(PlayerRelationAction.OFFER_ALLIANCE, "Cannot offer alliance in Team Death Foe!");
	}

	private void dontCreateNewRelation(PlayerRelationAction action, String expectedMessage) {
		try {
			subject.createNewRelation(1L, action, 2L, game);
			fail("IllegalArgumentException expected!");
		} catch (IllegalArgumentException e) {
			assertEquals(expectedMessage, e.getMessage());
		}
	}

	@Test
	public void shouldAddRelationRequestCorrectly() {
		Mockito.when(playerRepository.getOne(1L)).thenReturn(new Player(1L));
		Mockito.when(playerRepository.getOne(2L)).thenReturn(new Player(2L));

		subject.addRelationRequest(1L, 2L, PlayerRelationType.ALLIANCE);

		ArgumentCaptor<PlayerRelationRequest> arg = ArgumentCaptor.forClass(PlayerRelationRequest.class);
		Mockito.verify(playerRelationRequestRepository).save(arg.capture());

		PlayerRelationRequest request = arg.getValue();
		assertEquals(1L, request.getRequestingPlayer().getId());
		assertEquals(2L, request.getOtherPlayer().getId());
		assertEquals(PlayerRelationType.ALLIANCE, request.getType());
	}
}