package org.skrupeltng.modules.ingame.modules.anomaly.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.skrupeltng.AbstractDbTest;
import org.skrupeltng.modules.ingame.controller.MineFieldEntry;
import org.springframework.beans.factory.annotation.Autowired;

public class MineFieldRepositoryDatabaseTest extends AbstractDbTest {

	@Autowired
	private MineFieldRepository mineFieldRepository;

	@Test
	public void shouldNotReturnAnyMineFields() {
		List<MineFieldEntry> result = mineFieldRepository.getMineFieldEntries(4L, 10L);
		assertEquals(0, result.size());
	}

	@Test
	public void shouldReturnMineFields() {
		List<MineFieldEntry> result = mineFieldRepository.getMineFieldEntries(1L, 5L);
		assertEquals(3, result.size());

		boolean found1 = false;
		boolean found2 = false;
		boolean found3 = false;

		for (MineFieldEntry entry : result) {
			long id = entry.getId();

			if (id == 1L) {
				found1 = true;
				assertEquals(false, entry.isOwn());
				assertEquals(false, entry.isFriendly());
			} else if (id == 2L) {
				found2 = true;
				assertEquals(false, entry.isOwn());
				assertEquals(true, entry.isFriendly());
			} else if (id == 3L) {
				found3 = true;
				assertEquals(true, entry.isOwn());
				assertEquals(true, entry.isFriendly());
			}
		}

		assertEquals(true, found1);
		assertEquals(true, found2);
		assertEquals(true, found3);
	}

	@Test
	public void shouldReturnMineFieldIdsInRadius() {
		List<Long> result = mineFieldRepository.getMineFieldIdsInRadius(1L, 50, 100, 85);
		assertEquals(1, result.size());
		assertEquals(1L, result.get(0).longValue());
	}

	@Test
	public void shouldReturnAllMineFields() {
		List<Long> result = mineFieldRepository.getMineFieldIdsInRadius(1L, 500, 500, 10000);
		assertEquals(3, result.size());
		assertTrue(result.contains(1L));
		assertTrue(result.contains(2L));
		assertTrue(result.contains(3L));
	}
}