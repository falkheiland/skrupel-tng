package org.skrupeltng.modules.ingame.modules.ship.database;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.skrupeltng.AbstractDbTest;
import org.springframework.beans.factory.annotation.Autowired;

public class ShipRouteEntryRepositoryDatabaseTest extends AbstractDbTest {

	@Autowired
	private ShipRouteEntryRepository shipRouteEntryRepository;

	@Test
	public void shouldAcceptOwnedRouteAsOwnedByPlayer() {
		boolean loginOwnsShip = shipRouteEntryRepository.loginOwnsRoute(1L, 1L);
		assertTrue(loginOwnsShip);
	}

	@Test
	public void shouldNotAcceptEnemyRouteAsOwnedByPlayer() {
		boolean loginOwnsShip = shipRouteEntryRepository.loginOwnsRoute(1L, 2L);
		assertFalse(loginOwnsShip);
	}
}