package org.skrupeltng.modules.ingame.modules.overview.service;

import static org.junit.Assert.assertEquals;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.overview.controller.SendPlayerMessageRequest;
import org.skrupeltng.modules.ingame.modules.overview.database.PlayerMessage;
import org.skrupeltng.modules.ingame.modules.overview.database.PlayerMessageRepository;

public class PlayerMessageServiceUnitTest {

	@Spy
	@InjectMocks
	private final PlayerMessageService subject = new PlayerMessageService();

	@Mock
	private PlayerMessageRepository playerMessageRepository;

	@Mock
	private UserDetailServiceImpl userService;

	@Mock
	private PlayerRepository playerRepository;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldCheckRecipientPresent() {
		SendPlayerMessageRequest request = new SendPlayerMessageRequest();
		request.setGameId(1L);
		request.setRecipientId(2L);

		Mockito.when(userService.getLoginId()).thenReturn(3L);

		Mockito.when(playerRepository.findById(2L)).thenReturn(Optional.empty());

		subject.sendMessage(request);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldCheckRecipientInSameGame() {
		SendPlayerMessageRequest request = new SendPlayerMessageRequest();
		request.setGameId(1L);
		request.setRecipientId(2L);

		Mockito.when(userService.getLoginId()).thenReturn(3L);

		Player recipient = new Player(2L);
		recipient.setGame(new Game(2L));
		Mockito.when(playerRepository.findById(2L)).thenReturn(Optional.of(recipient));

		subject.sendMessage(request);
	}

	@Test
	public void shouldSendMessage() {
		SendPlayerMessageRequest request = new SendPlayerMessageRequest();
		request.setGameId(1L);
		request.setRecipientId(2L);
		request.setMessage("message1");
		request.setSubject("subject1");

		Mockito.when(userService.getLoginId()).thenReturn(3L);

		Player recipient = new Player(2L);
		recipient.setGame(new Game(1L));
		Mockito.when(playerRepository.findById(2L)).thenReturn(Optional.of(recipient));

		Player sender = new Player(4L);
		Login login = new Login();
		login.setUsername("sendername");
		sender.setLogin(login);
		Mockito.when(playerRepository.findByGameIdAndLoginId(1L, 3L)).thenReturn(Optional.of(sender));

		subject.sendMessage(request);

		ArgumentCaptor<PlayerMessage> arg = ArgumentCaptor.forClass(PlayerMessage.class);
		Mockito.verify(playerMessageRepository).save(arg.capture());

		PlayerMessage result = arg.getValue();
		assertEquals(sender, result.getFrom());
		assertEquals(recipient, result.getTo());
		assertEquals("message1", result.getMessage());
		assertEquals("subject1", result.getSubject());
	}
}