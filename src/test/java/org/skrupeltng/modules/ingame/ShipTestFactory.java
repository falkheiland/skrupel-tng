package org.skrupeltng.modules.ingame;

import java.util.ArrayList;

import org.mockito.Mockito;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.database.WeaponTemplate;
import org.testcontainers.shaded.com.google.common.collect.Lists;

public class ShipTestFactory {

	public static Ship createShip(int shield, int mass, int energyWeapons, int projectileWeapons, int crew, long playerId) {
		Ship ship = Mockito.spy(new Ship());
		ship.setId(3L);
		ship.setShield(shield);
		ship.setName("test" + playerId);
		ship.setCrew(crew);

		Player player = createPlayer(playerId);
		ship.setPlayer(player);

		Game game = new Game(1L);
		game.setGalaxySize(1000);
		player.setGame(game);

		ShipTemplate shipTemplate = new ShipTemplate("1");
		shipTemplate.setEnergyWeaponsCount(energyWeapons);
		shipTemplate.setProjectileWeaponsCount(projectileWeapons);
		shipTemplate.setHangarCapacity(3);
		shipTemplate.setMass(mass);
		shipTemplate.setCrew(crew);
		shipTemplate.setFaction(new Faction("testfaction"));
		shipTemplate.setShipAbilities(Lists.newArrayList());
		shipTemplate.setFuelCapacity(50);
		shipTemplate.setStorageSpace(100);
		ship.setShipTemplate(shipTemplate);
		ship.setMoney(projectileWeapons * 5 * 35);
		ship.setMineral1(projectileWeapons * 5 * 2);
		ship.setMineral2(projectileWeapons * 5);
		ship.setProjectiles(5 * projectileWeapons);

		ship.setPropulsionSystemTemplate(new PropulsionSystemTemplate());

		WeaponTemplate energyWeaponTemplate = new WeaponTemplate();
		energyWeaponTemplate.setDamageIndex(4);
		energyWeaponTemplate.setTechLevel(4);
		energyWeaponTemplate.setUsesProjectiles(false);
		ship.setEnergyWeaponTemplate(energyWeaponTemplate);

		WeaponTemplate projectileWeaponTemplate = new WeaponTemplate();
		projectileWeaponTemplate.setTechLevel(2);
		projectileWeaponTemplate.setDamageIndex(2);
		projectileWeaponTemplate.setUsesProjectiles(true);
		ship.setProjectileWeaponTemplate(projectileWeaponTemplate);

		return ship;
	}

	public static Player createPlayer(long playerId) {
		Player player = new Player(playerId);
		player.setGame(new Game(1111L));
		Faction faction = new Faction("test_faction");
		player.setFaction(faction);
		faction.setShips(new ArrayList<>());
		return player;
	}
}