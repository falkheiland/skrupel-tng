package org.skrupeltng.modules.ingame.database.player;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.skrupeltng.AbstractDbTest;
import org.skrupeltng.modules.ingame.modules.overview.service.PlayerSummaryEntry;
import org.springframework.beans.factory.annotation.Autowired;

public class PlayerRepositoryDatabaseTest extends AbstractDbTest {

	@Autowired
	private PlayerRepository playerRepository;

	@Test
	public void shouldUpdateTurnValues() {
		playerRepository.setupTurnValues(1L);
		assertTrue(playerRepository.getOne(2L).isTurnFinished());
	}

	@Test
	public void shouldReturnSummaries() {
		List<PlayerSummaryEntry> summaries = playerRepository.getSummaries(1L);
		assertEquals(9, summaries.size());
	}

	@Test
	public void shouldCheckTurnDoneForPlanet() {
		boolean turnNotDone = playerRepository.playersTurnNotDoneForPlanet(92L, 1L);
		assertEquals(true, turnNotDone);

		turnNotDone = playerRepository.playersTurnNotDoneForPlanet(1L, 1L);
		assertEquals(false, turnNotDone);

		turnNotDone = playerRepository.playersTurnNotDoneForPlanet(31L, 3L);
		assertEquals(false, turnNotDone);
	}

	@Test
	public void shouldCheckTurnDoneForShip() {
		boolean turnNotDone = playerRepository.playersTurnNotDoneForShip(8L, 1L);
		assertEquals(true, turnNotDone);

		turnNotDone = playerRepository.playersTurnNotDoneForShip(2L, 1L);
		assertEquals(false, turnNotDone);

		turnNotDone = playerRepository.playersTurnNotDoneForShip(1L, 3L);
		assertEquals(false, turnNotDone);
	}

	@Test
	public void shouldCheckTurnDoneForStarbase() {
		boolean turnNotDone = playerRepository.playersTurnNotDoneForStarbase(1L, 1L);
		assertEquals(true, turnNotDone);

		turnNotDone = playerRepository.playersTurnNotDoneForStarbase(2L, 1L);
		assertEquals(false, turnNotDone);

		turnNotDone = playerRepository.playersTurnNotDoneForStarbase(3L, 3L);
		assertEquals(false, turnNotDone);
	}

	@Test
	public void shouldCheckTurnDoneForRoute() {
		boolean turnNotDone = playerRepository.playersTurnNotDoneForRoute(1L, 1L);
		assertEquals(true, turnNotDone);

		turnNotDone = playerRepository.playersTurnNotDoneForRoute(1L, 3L);
		assertEquals(false, turnNotDone);
	}

	@Test
	public void shouldCheckTurnDoneForOrbitalSystem() {
		boolean turnNotDone = playerRepository.playersTurnNotDoneForOrbitalSystem(647L, 1L);
		assertEquals(true, turnNotDone);

		turnNotDone = playerRepository.playersTurnNotDoneForOrbitalSystem(1L, 1L);
		assertEquals(false, turnNotDone);

		turnNotDone = playerRepository.playersTurnNotDoneForOrbitalSystem(636L, 3L);
		assertEquals(false, turnNotDone);
	}
}