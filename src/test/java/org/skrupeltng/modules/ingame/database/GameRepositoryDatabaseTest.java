package org.skrupeltng.modules.ingame.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.skrupeltng.AbstractDbTest;
import org.skrupeltng.modules.dashboard.GameListResult;
import org.skrupeltng.modules.dashboard.GameSearchParameters;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public class GameRepositoryDatabaseTest extends AbstractDbTest {

	@Autowired
	private GameRepository gameRepository;

	@Test
	public void shouldReturnAllGames() {
		GameSearchParameters params = new GameSearchParameters();
		params.setCurrentLoginId(1L);

		Page<GameListResult> result = gameRepository.searchGames(params, PageRequest.of(0, 10));

		assertNotNull(result);
		assertEquals(3, result.getNumberOfElements());

		List<GameListResult> content = result.getContent();

		GameListResult gameListResult = content.get(0);
		assertEquals(4L, gameListResult.getId());
		assertEquals(true, gameListResult.isPlayerOfGame());

		gameListResult = content.get(1);
		assertEquals(3L, gameListResult.getId());
		assertEquals(true, gameListResult.isPlayerOfGame());

		gameListResult = content.get(2);
		assertEquals(1L, gameListResult.getId());
		assertEquals(true, gameListResult.isPlayerOfGame());
	}

	@Test
	public void shouldReturnAllStartedGames() {
		GameSearchParameters params = new GameSearchParameters();
		params.setCurrentLoginId(1L);
		params.setStarted(true);

		Page<GameListResult> result = gameRepository.searchGames(params, PageRequest.of(0, 10));

		assertNotNull(result);
		assertEquals(2, result.getNumberOfElements());

		List<GameListResult> content = result.getContent();
		assertEquals(3L, content.get(0).getId());
		assertEquals(1L, content.get(1).getId());
	}

	@Test
	public void shouldReturnAllGamesWithLoginOne() {
		GameSearchParameters params = new GameSearchParameters();
		params.setCurrentLoginId(1L);
		params.setOnlyOwnGames(true);

		Page<GameListResult> result = gameRepository.searchGames(params, PageRequest.of(0, 10));

		assertNotNull(result);
		assertEquals(3, result.getNumberOfElements());

		List<GameListResult> content = result.getContent();
		assertEquals(4L, content.get(0).getId());
		assertEquals(3L, content.get(1).getId());
		assertEquals(1L, content.get(2).getId());
	}

	@Test
	public void shouldReturnAllNotStartedGames() {
		GameSearchParameters params = new GameSearchParameters();
		params.setCurrentLoginId(1L);
		params.setStarted(false);

		Page<GameListResult> result = gameRepository.searchGames(params, PageRequest.of(0, 10));

		assertNotNull(result);
		assertEquals(1, result.getNumberOfElements());

		List<GameListResult> content = result.getContent();
		assertEquals(4L, content.get(0).getId());
	}

	@Test
	public void shouldReturnGamesByWinCondition() {
		GameSearchParameters params = new GameSearchParameters();
		params.setCurrentLoginId(1L);
		params.setWinCondition(WinCondition.SURVIVE);

		Page<GameListResult> result = gameRepository.searchGames(params, PageRequest.of(0, 10));

		assertNotNull(result);
		assertEquals(1, result.getNumberOfElements());

		params.setWinCondition(WinCondition.NONE);
		result = gameRepository.searchGames(params, PageRequest.of(0, 10));

		assertNotNull(result);
		assertEquals(2, result.getNumberOfElements());
	}

	@Test
	public void shouldReturnGamesByName() {
		GameSearchParameters params = new GameSearchParameters();
		params.setCurrentLoginId(1L);
		params.setName("debug");

		Page<GameListResult> result = gameRepository.searchGames(params, PageRequest.of(0, 10));

		assertNotNull(result);
		assertEquals(1, result.getNumberOfElements());

		params.setName("de");
		result = gameRepository.searchGames(params, PageRequest.of(0, 10));

		assertNotNull(result);
		assertEquals(1, result.getNumberOfElements());

		params.setName("test");
		result = gameRepository.searchGames(params, PageRequest.of(0, 10));

		assertNotNull(result);
		assertEquals(2, result.getNumberOfElements());

		params.setName("test3");
		result = gameRepository.searchGames(params, PageRequest.of(0, 10));

		assertNotNull(result);
		assertEquals(1, result.getNumberOfElements());

		params.setName("abcd");
		result = gameRepository.searchGames(params, PageRequest.of(0, 10));

		assertNotNull(result);
		assertEquals(0, result.getNumberOfElements());
	}

	@Test
	public void shouldReturnNoGames() {
		GameSearchParameters params = new GameSearchParameters();
		params.setCurrentLoginId(3L);
		params.setOnlyOwnGames(true);

		Page<GameListResult> result = gameRepository.searchGames(params, PageRequest.of(0, 10));

		assertNotNull(result);
		assertEquals(0, result.getNumberOfElements());
	}

	@Test
	public void shouldReturnDifferentVisibilityCoordinates() {
		Set<CoordinateImpl> results1 = gameRepository.getVisibilityCoordinates(1L);
		assertNotNull(results1);
		assertTrue(results1.size() > 0);

		Set<CoordinateImpl> results2 = gameRepository.getVisibilityCoordinates(2L);
		assertNotNull(results2);
		assertTrue(results2.size() > 0);

		assertNotEquals(results1, results2);
	}

	@Test
	public void shouldReturnGamesOfPlayer() {
		List<PlayerGameTurnInfoItem> results = gameRepository.getTurnInfos(1L, 0L);
		assertNotNull(results);
		assertEquals(2, results.size());

		results = gameRepository.getTurnInfos(1L, 1L);
		assertNotNull(results);
		assertEquals(1, results.size());
	}
}