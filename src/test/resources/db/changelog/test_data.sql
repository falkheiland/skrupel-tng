--
-- PostgreSQL database dump
--

-- Dumped from database version 10.10 (Ubuntu 10.10-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.10 (Ubuntu 10.10-0ubuntu0.18.04.1)

-- Started on 2019-09-18 09:28:16 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3185 (class 0 OID 265070)
-- Dependencies: 199
-- Data for Name: login; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE public.login DISABLE TRIGGER ALL;

INSERT INTO public.login VALUES (2, 'AI_EASY', '$2a$10$ntmR4x8mDfsM9KPzgCdj4.QsYojDVRfNmrD0loucn3d0VrxXQcSxi', false);
INSERT INTO public.login VALUES (3, 'AI_MEDIUM', '$2a$10$q6lzsA37Mhc2uFJKCbWPDue2lxqyVVVJPagMRKgBlCpxXPKwpYIUq', false);
INSERT INTO public.login VALUES (4, 'AI_HARD', '$2a$10$6nmbWLj9hAxmTyeOgJpnNumSp1hNI/HLXFoWtk2jsLmJIsDhdNT9.', false);
INSERT INTO public.login VALUES (1, 'admin', '$2a$10$VSusUzaJSB0RZFTtjP4BZ.m8F46IxRHdynYj8/f//cgCf.LcAxtpC', true);


ALTER TABLE public.login ENABLE TRIGGER ALL;

--
-- TOC entry 3189 (class 0 OID 265107)
-- Dependencies: 206
-- Data for Name: game; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.game DISABLE TRIGGER ALL;

INSERT INTO public.game VALUES (1, '2019-09-18 09:26:53.313', 1, 'Debug', 'SURVIVE', true, false, 1, '2019-09-18 09:26:53.485', true, true, true, false, 'EQUAL_DISTANCE', '1', 'LOSE_HOME_PLANET', 8, 1000, 15000, 0.25, '1', '1', 0, '0', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, false, 0, 'VERY_HIGH');
INSERT INTO public.game VALUES (3, '2019-09-18 09:28:04.254', 1, 'test3', 'NONE', true, false, 1, '2019-09-18 09:28:05.049', true, true, true, false, 'EQUAL_DISTANCE', '1', 'LOSE_HOME_PLANET', 2, 1000, 15000, 0.25, '1', '1', 40, '0', 'gala_1', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, false, 0, 'VERY_HIGH');
INSERT INTO public.game VALUES (4, '2019-09-18 09:28:06.867', 1, 'test4', 'NONE', false, false, 1, '2019-09-18 09:28:06.867', true, true, true, false, 'EQUAL_DISTANCE', '1', 'LOSE_HOME_PLANET', 2, 1000, 15000, 0.25, '1', '1', 40, '0', 'gala_1', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, false, 0, 'VERY_HIGH');


ALTER TABLE public.game ENABLE TRIGGER ALL;

--
-- TOC entry 3187 (class 0 OID 265080)
-- Dependencies: 201
-- Data for Name: login_role; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.login_role DISABLE TRIGGER ALL;

INSERT INTO public.login_role VALUES (1, 1, 'ROLE_ADMIN');
INSERT INTO public.login_role VALUES (2, 1, 'ROLE_PLAYER');
INSERT INTO public.login_role VALUES (3, 2, 'ROLE_PLAYER');
INSERT INTO public.login_role VALUES (4, 2, 'ROLE_AI');
INSERT INTO public.login_role VALUES (5, 3, 'ROLE_PLAYER');
INSERT INTO public.login_role VALUES (6, 3, 'ROLE_AI');
INSERT INTO public.login_role VALUES (7, 4, 'ROLE_PLAYER');
INSERT INTO public.login_role VALUES (8, 4, 'ROLE_AI');


ALTER TABLE public.login_role ENABLE TRIGGER ALL;

--
-- TOC entry 3190 (class 0 OID 265129)
-- Dependencies: 207
-- Data for Name: faction; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.faction DISABLE TRIGGER ALL;

INSERT INTO public.faction VALUES ('orion', 'I', 32, 1.35000002384185791, 1.04999995231628418, 1.14999997615814209, 0.75, 0.800000011920928955, 'Beteigeuze', 0, NULL, NULL, NULL);


ALTER TABLE public.faction ENABLE TRIGGER ALL;

--
-- TOC entry 3192 (class 0 OID 265139)
-- Dependencies: 209
-- Data for Name: player; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.player DISABLE TRIGGER ALL;

INSERT INTO public.player VALUES (3, true, 2, 'orion', '0000ff', 1, 'AI_EASY', 80, false, false, false);
INSERT INTO public.player VALUES (4, true, 2, 'orion', '00aaff', 1, 'AI_EASY', 18, false, false, false);
INSERT INTO public.player VALUES (6, true, 2, 'orion', '00ff00', 1, 'AI_EASY', 89, false, false, false);
INSERT INTO public.player VALUES (5, true, 2, 'orion', '00ffaa', 1, 'AI_EASY', 139, false, false, false);
INSERT INTO public.player VALUES (2, true, 2, 'orion', 'aa00ff', 1, 'AI_EASY', 97, false, false, false);
INSERT INTO public.player VALUES (7, true, 2, 'orion', 'aaff00', 1, 'AI_EASY', 133, false, false, false);
INSERT INTO public.player VALUES (9, true, 2, 'orion', 'ff0000', 1, 'AI_EASY', 90, false, false, false);
INSERT INTO public.player VALUES (8, true, 2, 'orion', 'ffaa00', 1, 'AI_EASY', 171, false, false, false);
INSERT INTO public.player VALUES (1, false, 1, 'orion', 'ff00aa', 1, NULL, 92, false, true, false);
INSERT INTO public.player VALUES (12, false, 1, 'orion', 'ff0000', 3, NULL, 267, false, true, false);
INSERT INTO public.player VALUES (13, false, 1, NULL, NULL, 4, NULL, NULL, false, false, false);


ALTER TABLE public.player ENABLE TRIGGER ALL;

--
-- TOC entry 3213 (class 0 OID 265436)
-- Dependencies: 234
-- Data for Name: news_entry; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.news_entry DISABLE TRIGGER ALL;



ALTER TABLE public.news_entry ENABLE TRIGGER ALL;

--
-- TOC entry 3194 (class 0 OID 265168)
-- Dependencies: 211
-- Data for Name: starbase; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.starbase DISABLE TRIGGER ALL;

INSERT INTO public.starbase VALUES (1, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (2, 'STAR_BASE', 'Starbase 1', 'Freighter', 0, 6, 6, 6, 6);
INSERT INTO public.starbase VALUES (3, 'STAR_BASE', 'Starbase 1', 'Freighter', 0, 6, 6, 6, 6);
INSERT INTO public.starbase VALUES (4, 'STAR_BASE', 'Starbase 1', 'Freighter', 0, 6, 6, 6, 6);
INSERT INTO public.starbase VALUES (5, 'STAR_BASE', 'Starbase 1', 'Freighter', 0, 6, 6, 6, 6);
INSERT INTO public.starbase VALUES (6, 'STAR_BASE', 'Starbase 1', 'Freighter', 0, 6, 6, 6, 6);
INSERT INTO public.starbase VALUES (7, 'STAR_BASE', 'Starbase 1', 'Freighter', 0, 6, 6, 6, 6);
INSERT INTO public.starbase VALUES (8, 'STAR_BASE', 'Starbase 1', 'Freighter', 0, 6, 6, 6, 6);
INSERT INTO public.starbase VALUES (9, 'STAR_BASE', 'Starbase 1', 'Freighter', 0, 6, 6, 6, 6);
INSERT INTO public.starbase VALUES (12, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);


ALTER TABLE public.starbase ENABLE TRIGGER ALL;

--
-- TOC entry 3196 (class 0 OID 265181)
-- Dependencies: 213
-- Data for Name: planet; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.planet DISABLE TRIGGER ALL;

INSERT INTO public.planet VALUES (1, 1, 'Brental', 109, 580, 'F', 0, 0, 48, 48, 68, 53, '9_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4820, 4088, 2700, 3157, 4, 2, 6, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2, 1, 'Cardassia', 169, 344, 'M', 36, 0, 24, 69, 7, 39, '1_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1914, 2665, 1528, 1059, 6, 10, 2, 4, 0, 0, 0, NULL, 31, 7231, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (3, 1, 'Ligos VII', 948, 902, 'L', 75, 0, 30, 66, 60, 31, '4_14', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4817, 3926, 2374, 4620, 2, 10, 6, 6, 0, 0, 0, NULL, 29, 12674, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (4, 1, 'KrePain', 895, 434, 'M', 8, 0, 58, 68, 61, 42, '1_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4633, 2657, 3966, 2377, 10, 10, 1, 2, 0, 0, 0, NULL, 14, 5576, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (5, 1, 'Lapolis', 921, 839, 'I', 73, 0, 51, 16, 6, 57, '6_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3064, 3491, 3416, 4836, 2, 2, 6, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (6, 1, 'Metaline', 595, 72, 'F', -3, 0, 68, 62, 56, 8, '9_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3664, 907, 4557, 1849, 6, 4, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (7, 1, 'Trillion', 928, 695, 'G', 93, 0, 57, 41, 41, 10, '5_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4495, 2708, 1040, 2740, 10, 6, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (8, 1, 'BetaNiobe', 59, 851, 'M', 58, 0, 13, 62, 26, 20, '1_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4266, 4196, 3807, 2483, 2, 4, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (9, 1, 'Lavinius', 914, 331, 'L', 58, 0, 14, 44, 55, 61, '4_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4354, 1889, 4169, 3172, 10, 2, 10, 6, 0, 0, 0, NULL, 23, 4166, NULL, NULL, 53, 0, 0, 'MINERAL2');
INSERT INTO public.planet VALUES (10, 1, 'Antares', 50, 706, 'I', 68, 0, 47, 52, 40, 59, '6_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2069, 3897, 2848, 958, 4, 6, 4, 1, 0, 0, 0, NULL, 7, 3598, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (11, 1, 'Norpin', 681, 222, 'F', 5, 0, 67, 51, 32, 6, '9_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3898, 2417, 1586, 2399, 2, 4, 10, 6, 0, 0, 0, NULL, 18, 2905, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (12, 1, 'Sluis Van', 849, 66, 'C', 111, 0, 24, 66, 40, 57, '7_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1158, 2641, 1208, 2364, 2, 6, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (13, 1, 'Chalna', 634, 470, 'I', 72, 0, 52, 11, 45, 64, '6_14', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3895, 2324, 1629, 4333, 2, 2, 6, 6, 0, 0, 0, NULL, 4, 8916, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (14, 1, 'Ithor', 950, 623, 'J', -26, 0, 40, 63, 68, 64, '3_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4719, 3209, 2389, 866, 4, 10, 1, 10, 0, 0, 0, NULL, 21, 11998, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (15, 1, 'Boreal', 950, 494, 'C', 103, 0, 64, 1, 20, 10, '7_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1155, 2797, 1009, 3784, 10, 2, 4, 6, 0, 0, 0, NULL, 38, 13768, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (16, 1, 'Quazulu', 50, 575, 'N', 10, 0, 8, 42, 52, 7, '2_16', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1453, 1094, 2479, 3406, 1, 2, 4, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (17, 1, 'Sphinx', 390, 149, 'G', 55, 0, 14, 24, 60, 1, '5_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1961, 3873, 4508, 1604, 2, 1, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (19, 1, 'Kaelon', 563, 784, 'F', 3, 0, 19, 69, 50, 38, '9_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4953, 1046, 2736, 1775, 10, 10, 1, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, 'COLONY');
INSERT INTO public.planet VALUES (20, 1, 'Paradeen', 632, 950, 'M', 45, 0, 36, 35, 37, 30, '1_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 890, 895, 3851, 2480, 4, 6, 6, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (21, 1, 'Galor', 514, 294, 'G', 114, 0, 49, 2, 35, 29, '5_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4704, 4483, 872, 1247, 10, 1, 4, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (22, 1, 'Curayo', 846, 152, 'M', 46, 0, 48, 19, 26, 55, '1_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3334, 3406, 2867, 4216, 10, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (23, 1, 'Cardassia (2)', 819, 949, 'C', 96, 0, 40, 66, 40, 24, '7_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1125, 3450, 2463, 1933, 10, 1, 4, 2, 0, 0, 0, NULL, 9, 15959, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (24, 1, 'Cheron', 601, 608, 'L', 43, 0, 26, 49, 3, 38, '4_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1769, 1366, 3677, 4609, 4, 10, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (25, 1, 'Manu', 171, 258, 'K', 8, 0, 6, 44, 50, 42, '8_30', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1784, 3874, 4467, 3249, 6, 10, 6, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (26, 1, 'Roon', 153, 950, 'F', 28, 0, 12, 29, 68, 16, '9_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2585, 1284, 824, 1771, 10, 4, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (27, 1, 'Archer', 498, 923, 'J', -35, 0, 15, 7, 13, 67, '3_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4408, 3047, 2825, 2271, 10, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (28, 1, 'Scalos', 342, 950, 'J', -32, 0, 40, 33, 62, 46, '3_16', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 870, 960, 1494, 968, 4, 10, 6, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (29, 1, 'Mon Calamari', 626, 354, 'I', 78, 0, 18, 66, 21, 1, '6_14', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4326, 2564, 3201, 2547, 1, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (30, 1, 'Rhinnal', 158, 65, 'N', 41, 0, 37, 1, 24, 51, '2_24', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2853, 3579, 2513, 2271, 6, 1, 2, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (31, 1, 'Drema', 297, 215, 'G', 97, 0, 70, 1, 50, 18, '5_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4138, 1750, 4223, 4273, 10, 6, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (32, 1, 'Harod', 310, 475, 'C', 99, 0, 12, 4, 19, 41, '7_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1178, 1629, 4484, 3068, 10, 6, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (33, 1, 'Fabrina', 712, 616, 'C', 92, 0, 25, 4, 35, 60, '7_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4412, 1154, 1092, 866, 1, 2, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (34, 1, 'Mab-Bu VI', 136, 731, 'J', -27, 0, 29, 54, 11, 16, '3_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2692, 3004, 2538, 2933, 2, 2, 4, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (35, 1, 'Ferrol', 639, 532, 'C', 104, 0, 68, 24, 50, 15, '7_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 870, 1314, 919, 4284, 1, 2, 10, 1, 0, 0, 0, NULL, 22, 11083, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (36, 1, 'BetaAurigae', 404, 559, 'G', 142, 0, 57, 20, 43, 54, '5_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3031, 1902, 4149, 4151, 1, 4, 6, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (37, 1, 'Ertrus', 621, 200, 'M', 49, 0, 51, 41, 53, 52, '1_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4195, 3246, 873, 2882, 4, 4, 10, 6, 0, 0, 0, NULL, 23, 14767, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (38, 1, 'ChamraVortex', 261, 425, 'L', 65, 0, 17, 29, 22, 68, '4_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 832, 2554, 1248, 3340, 6, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (39, 1, 'Celtis', 472, 497, 'C', 83, 0, 34, 14, 20, 7, '7_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3689, 3429, 1462, 3653, 4, 4, 2, 4, 0, 0, 0, NULL, 16, 8653, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (40, 1, 'Browder', 839, 871, 'K', 17, 0, 27, 3, 67, 23, '8_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4985, 2161, 2796, 2326, 1, 10, 2, 2, 0, 0, 0, NULL, 33, 9022, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (41, 1, 'Archer (2)', 50, 404, 'M', 37, 0, 40, 63, 55, 51, '1_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4174, 3537, 1615, 2483, 6, 2, 6, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (42, 1, 'Mariah', 663, 698, 'C', 117, 0, 6, 4, 28, 53, '7_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3795, 2684, 1454, 2081, 4, 1, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (43, 1, 'Deneb', 901, 108, 'N', 1, 0, 37, 20, 70, 43, '2_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3954, 1070, 3455, 1013, 4, 6, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (44, 1, 'Kaelon (2)', 724, 364, 'G', 60, 0, 18, 21, 20, 1, '5_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1325, 3385, 4563, 3736, 10, 2, 4, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (45, 1, 'Ryloth', 392, 345, 'G', 97, 0, 12, 19, 6, 7, '5_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3934, 1306, 3680, 3592, 10, 6, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (46, 1, 'Plophos', 670, 388, 'G', 138, 0, 55, 47, 59, 14, '5_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1850, 829, 1011, 1429, 2, 1, 4, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (47, 1, 'Idran', 334, 577, 'I', 128, 0, 19, 29, 47, 51, '6_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3001, 4124, 1091, 4961, 10, 2, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (48, 1, 'Bracas', 745, 914, 'N', 12, 0, 49, 5, 40, 43, '2_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3775, 1844, 2279, 4054, 4, 1, 10, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, 'MINERAL2');
INSERT INTO public.planet VALUES (49, 1, 'Morikin', 805, 310, 'L', 45, 0, 21, 32, 16, 45, '4_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4110, 4713, 2712, 1846, 4, 2, 1, 4, 0, 0, 0, NULL, 22, 14750, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (50, 1, 'Topsid', 568, 694, 'C', 90, 0, 6, 55, 60, 47, '7_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4908, 964, 3886, 1826, 1, 6, 10, 4, 0, 0, 0, NULL, 22, 6690, NULL, NULL, 53, 0, 0, 'WEATHER_CONTROL_STATION');
INSERT INTO public.planet VALUES (51, 1, 'Aldebaran III', 762, 662, 'C', 75, 0, 11, 18, 4, 6, '7_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3347, 3922, 2445, 2232, 4, 6, 10, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (52, 1, 'Qualor', 50, 123, 'K', 8, 0, 34, 35, 13, 33, '8_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1545, 1734, 3800, 1732, 4, 2, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (53, 1, 'Epsilo Indi', 336, 50, 'J', -32, 0, 5, 13, 50, 66, '3_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2949, 4845, 2527, 3677, 1, 2, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (54, 1, 'Metaline (2)', 189, 545, 'M', 34, 0, 20, 63, 12, 48, '1_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2796, 3561, 1516, 982, 4, 4, 4, 4, 0, 0, 0, NULL, 35, 7228, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (55, 1, 'Lavinius (2)', 483, 835, 'G', 94, 0, 49, 58, 55, 34, '5_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3996, 4978, 810, 1458, 4, 4, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (56, 1, 'Ariannus', 752, 90, 'J', -28, 0, 6, 30, 69, 4, '3_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 817, 1109, 2713, 1384, 1, 10, 4, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (57, 1, 'Tarsus', 50, 950, 'M', 63, 0, 40, 3, 58, 31, '1_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3720, 3688, 865, 3369, 4, 10, 1, 4, 0, 0, 0, NULL, 21, 8516, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (58, 1, 'Kessel', 272, 270, 'L', 25, 0, 67, 41, 64, 17, '4_14', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2332, 3747, 1522, 3241, 1, 4, 6, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (59, 1, 'Tramp', 532, 158, 'I', 101, 0, 38, 2, 52, 34, '6_13', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3071, 3275, 3768, 1761, 6, 6, 4, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (60, 1, 'SigmaDracon', 153, 413, 'G', 52, 0, 17, 50, 45, 68, '5_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2665, 1546, 1827, 2140, 1, 10, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (61, 1, 'Denkiri', 301, 689, 'C', 94, 0, 39, 39, 3, 27, '7_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 814, 3530, 1597, 4435, 6, 2, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (62, 1, 'Mab-Bu VI (2)', 510, 569, 'G', 116, 0, 32, 50, 37, 53, '5_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3797, 3846, 2366, 4769, 1, 10, 1, 4, 0, 0, 0, NULL, 24, 3177, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (63, 1, 'Fendaus', 482, 652, 'M', 56, 0, 49, 34, 11, 25, '1_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1181, 4818, 1949, 2869, 2, 10, 10, 1, 0, 0, 0, NULL, 14, 5478, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (64, 1, 'Iota', 878, 578, 'C', 79, 0, 68, 19, 51, 54, '7_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3644, 3723, 2497, 2605, 10, 4, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (65, 1, 'Kea IV', 240, 364, 'M', 60, 0, 7, 67, 63, 2, '1_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2550, 862, 2689, 4980, 4, 2, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (66, 1, 'Alawanir', 371, 728, 'L', 64, 0, 45, 69, 62, 18, '4_13', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3592, 2942, 4910, 3989, 1, 4, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (67, 1, 'Canops', 881, 950, 'F', -2, 0, 63, 38, 6, 9, '9_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4099, 3138, 1912, 3943, 10, 6, 10, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (68, 1, 'BetaAurigae (2)', 722, 507, 'M', 55, 0, 1, 16, 47, 28, '1_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4635, 963, 4261, 2471, 2, 4, 1, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (69, 1, 'Helska', 70, 50, 'M', 49, 0, 19, 50, 8, 18, '1_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2910, 2768, 945, 4070, 6, 10, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (70, 1, 'Thalotin', 867, 702, 'C', 71, 0, 65, 68, 63, 49, '7_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1617, 4696, 2099, 1295, 10, 6, 10, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (71, 1, 'Sirius', 569, 341, 'C', 69, 0, 34, 7, 60, 44, '7_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 810, 2737, 1398, 4609, 1, 2, 10, 2, 0, 0, 0, NULL, 40, 3168, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (72, 1, 'Elas', 240, 950, 'I', 77, 0, 31, 16, 5, 35, '6_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2089, 3329, 4704, 3871, 6, 10, 2, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (73, 1, 'Catullan', 420, 895, 'I', 84, 0, 53, 5, 68, 25, '6_16', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4479, 2890, 4558, 4169, 6, 2, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (74, 1, 'Minos', 371, 794, 'N', 40, 0, 2, 23, 2, 3, '2_14', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4223, 1089, 895, 3884, 1, 1, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (75, 1, 'Fabrina (2)', 851, 474, 'I', 60, 0, 26, 24, 9, 17, '6_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3731, 2940, 4020, 2509, 1, 10, 2, 2, 0, 0, 0, NULL, 19, 13004, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (76, 1, 'Holberg 917-G', 364, 409, 'I', 40, 0, 25, 46, 3, 31, '6_22', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4427, 1530, 3841, 906, 10, 4, 10, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (77, 1, 'Boreth', 778, 734, 'F', 0, 0, 70, 13, 19, 52, '9_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4007, 998, 4723, 2403, 6, 2, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (78, 1, 'Gothos', 243, 651, 'I', 116, 0, 50, 56, 38, 54, '6_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1362, 3042, 4032, 4279, 6, 1, 10, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (79, 1, 'Prakat', 897, 763, 'K', 11, 0, 28, 33, 37, 21, '8_33', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2232, 2887, 3286, 4054, 4, 10, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (81, 1, 'Curayo (2)', 226, 814, 'I', 95, 0, 16, 70, 37, 1, '6_17', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4605, 4166, 1896, 4130, 1, 6, 10, 2, 0, 0, 0, NULL, 5, 16130, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (82, 1, 'Chandra', 50, 486, 'N', 7, 0, 48, 39, 46, 65, '2_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1204, 3275, 2586, 3374, 1, 2, 2, 2, 0, 0, 0, NULL, 6, 8488, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (83, 1, 'Gamma II (2)', 50, 285, 'C', 124, 0, 29, 27, 1, 56, '7_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3708, 4531, 3273, 3588, 2, 10, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (84, 1, 'Ertrus (2)', 189, 204, 'K', 10, 0, 14, 33, 16, 6, '8_16', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4741, 3414, 2178, 3118, 6, 4, 1, 10, 0, 0, 0, NULL, 6, 6902, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (85, 1, 'Halee', 950, 557, 'M', 7, 0, 64, 43, 57, 38, '1_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1367, 1118, 2284, 3790, 6, 1, 1, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (86, 1, 'Ingraham B', 741, 844, 'L', 76, 0, 26, 59, 29, 11, '4_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1158, 4781, 4957, 3753, 1, 2, 2, 4, 0, 0, 0, NULL, 24, 2861, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (87, 1, 'Nacht-Acht', 243, 151, 'C', 114, 0, 1, 64, 30, 17, '7_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1838, 1114, 4449, 4090, 10, 10, 4, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (88, 1, 'Galvin', 108, 299, 'F', 18, 0, 36, 58, 42, 10, '9_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3518, 1107, 4535, 1048, 6, 6, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (91, 1, 'Krios', 431, 765, 'L', 48, 0, 2, 15, 27, 46, '4_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2121, 891, 1974, 2031, 1, 10, 4, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (93, 1, 'Kataan', 950, 206, 'G', 138, 0, 29, 45, 65, 26, '5_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2818, 926, 2233, 2865, 6, 6, 10, 10, 0, 0, 0, NULL, 29, 3002, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (94, 1, 'Barzan', 196, 761, 'I', 129, 0, 59, 14, 4, 4, '6_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3319, 3646, 3435, 1619, 4, 2, 1, 1, 0, 0, 0, NULL, 40, 15848, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (95, 1, 'Gideon', 657, 79, 'N', 8, 0, 56, 28, 42, 68, '2_22', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1949, 3562, 1612, 2371, 1, 10, 1, 1, 0, 0, 0, NULL, 18, 10287, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (96, 1, 'Orllyndie', 87, 906, 'M', 47, 0, 13, 52, 50, 62, '1_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4263, 2456, 2155, 3507, 1, 1, 2, 1, 0, 0, 0, NULL, 13, 10803, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (98, 1, 'Kurkos', 652, 291, 'L', 87, 0, 19, 24, 63, 41, '4_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4239, 1848, 4354, 4105, 4, 6, 10, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, 'COLONY');
INSERT INTO public.planet VALUES (99, 1, 'Oxtorne', 132, 654, 'I', 108, 0, 13, 33, 12, 56, '6_17', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2164, 3142, 3910, 2447, 10, 4, 10, 10, 0, 0, 0, NULL, 18, 17191, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (100, 1, 'Delta Rana', 533, 629, 'I', 56, 0, 14, 66, 10, 39, '6_13', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1695, 3348, 4936, 2894, 4, 6, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (101, 1, 'Roon (2)', 426, 192, 'I', 44, 0, 30, 54, 19, 18, '6_18', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2845, 1987, 4726, 2569, 4, 6, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (102, 1, 'Tarsus (2)', 50, 229, 'C', 104, 0, 19, 69, 60, 57, '7_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2773, 4510, 3818, 2842, 10, 2, 2, 10, 0, 0, 0, NULL, 4, 16949, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (103, 1, 'Celtis (2)', 432, 614, 'G', 106, 0, 70, 15, 20, 49, '5_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4798, 3515, 2642, 1858, 2, 4, 6, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (104, 1, 'Delos', 519, 381, 'J', -26, 0, 65, 4, 3, 45, '3_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1463, 4450, 983, 4402, 6, 4, 2, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (105, 1, 'Jeraddo', 574, 270, 'G', 117, 0, 69, 54, 9, 64, '5_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1355, 1419, 909, 2801, 6, 10, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (106, 1, 'Wayland', 950, 389, 'C', 67, 0, 25, 25, 14, 7, '7_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3894, 3097, 3030, 4186, 2, 1, 6, 6, 0, 0, 0, NULL, 31, 14294, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (107, 1, 'Erladu', 118, 160, 'K', 5, 0, 22, 41, 70, 12, '8_16', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4283, 3760, 4727, 4914, 2, 1, 2, 4, 0, 0, 0, NULL, 16, 13916, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (108, 1, 'Gortavor', 459, 259, 'F', -9, 0, 37, 7, 55, 13, '9_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4945, 1547, 2536, 3521, 4, 2, 10, 1, 0, 0, 0, NULL, 27, 2306, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (109, 1, 'Benecia', 744, 185, 'M', 62, 0, 31, 19, 61, 64, '1_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4292, 4725, 3381, 811, 1, 6, 1, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (110, 1, 'Mordan', 51, 783, 'F', -10, 0, 18, 30, 47, 58, '9_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2177, 4714, 1488, 2896, 4, 10, 10, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (111, 1, 'Arvada', 185, 140, 'J', -26, 0, 12, 27, 5, 24, '3_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3093, 4497, 2280, 1946, 10, 2, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (112, 1, 'Corulag', 950, 73, 'J', -33, 0, 36, 11, 16, 24, '3_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2887, 3270, 4158, 2239, 6, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (113, 1, 'Zhar', 627, 831, 'K', 16, 0, 3, 67, 69, 26, '8_20', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2017, 2792, 1948, 1628, 4, 2, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (114, 1, 'Sempidal', 377, 612, 'K', 16, 0, 29, 35, 9, 40, '8_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4893, 4910, 1207, 3857, 4, 4, 10, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (115, 1, 'Obroa Skal', 182, 859, 'J', -34, 0, 6, 61, 49, 62, '3_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1167, 4720, 2285, 1103, 4, 6, 10, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (116, 1, 'Carraya', 848, 810, 'F', 8, 0, 3, 66, 34, 47, '9_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4741, 3284, 3933, 1653, 4, 4, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (117, 1, 'Tatooine', 836, 223, 'J', -32, 0, 64, 59, 33, 37, '3_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3961, 2190, 3127, 4100, 2, 10, 6, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (118, 1, 'Rodia', 268, 50, 'M', 58, 0, 52, 17, 24, 53, '1_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2816, 4054, 2460, 1535, 10, 6, 10, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (119, 1, 'Sahndara', 309, 838, 'K', 17, 0, 61, 39, 42, 58, '8_25', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4010, 4523, 3618, 4926, 4, 6, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (120, 1, 'Koris', 326, 110, 'K', -6, 0, 4, 43, 25, 53, '8_29', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3412, 3293, 2037, 4159, 4, 10, 6, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (121, 1, 'Argelius', 474, 344, 'M', 15, 0, 37, 65, 47, 31, '1_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4989, 2665, 3417, 4562, 4, 2, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (122, 1, 'Archer (3)', 661, 899, 'J', -30, 0, 48, 15, 32, 15, '3_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4135, 1764, 2185, 4917, 1, 6, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (123, 1, 'Cerebus', 678, 786, 'G', 123, 0, 7, 67, 16, 15, '5_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3244, 3145, 3046, 882, 6, 10, 6, 1, 0, 0, 0, NULL, 23, 6470, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (124, 1, 'Agamar', 556, 905, 'J', -29, 0, 67, 5, 22, 35, '3_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4117, 1294, 1373, 2418, 10, 2, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (125, 1, 'Ogus', 826, 604, 'J', -26, 0, 33, 21, 16, 20, '3_13', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2593, 3808, 1761, 2506, 2, 4, 4, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (126, 1, 'Curayo (3)', 692, 146, 'I', 63, 0, 41, 45, 47, 15, '6_21', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3016, 2025, 1975, 4852, 4, 1, 6, 10, 0, 0, 0, NULL, 26, 10671, NULL, NULL, 53, 0, 0, 'COLONY');
INSERT INTO public.planet VALUES (127, 1, 'Auroch-Maxo-55', 761, 468, 'F', 32, 0, 44, 56, 13, 50, '9_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3183, 4277, 2372, 1844, 1, 1, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (128, 1, 'Klavdia III', 328, 282, 'I', 42, 0, 58, 54, 38, 42, '6_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1510, 3521, 1007, 3085, 1, 4, 6, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (129, 1, 'Bespin', 589, 419, 'G', 102, 0, 58, 60, 25, 14, '5_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1193, 4660, 4918, 2232, 6, 1, 10, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (130, 1, 'Lixos', 793, 535, 'G', 100, 0, 43, 66, 35, 66, '5_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4016, 4705, 4343, 2301, 1, 10, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (131, 1, 'Norpin (2)', 367, 857, 'I', 72, 0, 29, 22, 13, 53, '6_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3130, 4573, 879, 2416, 6, 4, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (132, 1, 'Browder (2)', 702, 950, 'I', 83, 0, 25, 67, 44, 54, '6_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2421, 4547, 2089, 925, 10, 10, 6, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (134, 1, 'Andevian', 208, 703, 'F', 29, 0, 29, 3, 4, 37, '9_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 903, 3097, 3071, 1485, 2, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (135, 1, 'Benzar', 299, 354, 'M', 42, 0, 67, 6, 17, 27, '1_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1059, 4477, 2794, 2782, 6, 10, 4, 1, 0, 0, 0, NULL, 7, 3944, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (136, 1, 'Clarus', 488, 741, 'C', 127, 0, 23, 58, 38, 60, '7_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3278, 3439, 2889, 1871, 4, 4, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (137, 1, 'Holberg 917-G (2)', 559, 841, 'L', 15, 0, 12, 65, 43, 46, '4_13', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4485, 1514, 3374, 4828, 1, 4, 6, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (138, 1, 'Pspopta', 879, 284, 'I', 78, 0, 17, 65, 65, 25, '6_19', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2741, 4254, 4913, 3503, 4, 2, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (140, 1, 'Talos', 903, 176, 'J', -27, 0, 7, 25, 28, 36, '3_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2799, 2897, 4329, 2036, 10, 6, 10, 4, 0, 0, 0, NULL, 42, 6728, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (141, 1, 'Omicron Ceti', 228, 893, 'F', 17, 0, 20, 16, 33, 52, '9_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4527, 835, 4700, 3517, 4, 6, 6, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (142, 1, 'Carillon', 50, 648, 'F', 2, 0, 15, 68, 36, 54, '9_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3691, 4152, 3432, 2021, 4, 10, 10, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (143, 1, 'Scalos (2)', 196, 458, 'N', -3, 0, 70, 36, 30, 54, '2_19', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2279, 2469, 2673, 2476, 4, 6, 10, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (144, 1, 'Duro', 352, 517, 'L', 39, 0, 37, 26, 30, 1, '4_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4967, 2100, 1741, 4244, 6, 10, 6, 6, 0, 0, 0, NULL, 13, 13484, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (145, 1, 'Trokan', 793, 819, 'L', 74, 0, 45, 28, 6, 21, '4_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1701, 4361, 3133, 2919, 4, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (146, 1, 'Minos (2)', 416, 950, 'G', 132, 0, 62, 44, 69, 68, '5_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2045, 1211, 893, 3780, 2, 4, 4, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (147, 1, 'KrePain (2)', 226, 97, 'K', 1, 0, 11, 35, 48, 67, '8_27', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3326, 2661, 3864, 2736, 2, 1, 1, 2, 0, 0, 0, NULL, 8, 9147, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (148, 1, 'SigmaDracon (2)', 183, 630, 'I', 43, 0, 52, 24, 59, 62, '6_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 876, 2703, 4401, 1581, 2, 6, 2, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (149, 1, 'Alfa', 528, 451, 'F', 7, 0, 35, 26, 48, 27, '9_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4530, 1419, 4476, 1565, 6, 2, 10, 10, 0, 0, 0, NULL, 18, 9963, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (150, 1, 'Epsal', 408, 50, 'F', 18, 0, 58, 19, 21, 20, '9_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2094, 3038, 1063, 2986, 10, 4, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (151, 1, 'Ryloth (2)', 524, 50, 'F', -3, 0, 11, 67, 64, 21, '9_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3723, 4564, 1515, 2839, 10, 1, 1, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (152, 1, 'Nimbus III', 762, 586, 'F', 3, 0, 8, 24, 57, 7, '9_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4230, 3339, 1143, 3415, 10, 2, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (153, 1, 'Lemma II', 727, 255, 'N', 21, 0, 32, 59, 26, 15, '2_24', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1245, 4204, 2035, 2984, 10, 1, 2, 1, 0, 0, 0, NULL, 3, 2909, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (154, 1, 'Brental (2)', 444, 429, 'J', -29, 0, 19, 40, 1, 62, '3_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3435, 2274, 4846, 4919, 4, 2, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (155, 1, 'Arcybite', 705, 50, 'N', 26, 0, 52, 41, 32, 2, '2_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1076, 2440, 841, 3436, 6, 4, 10, 2, 0, 0, 0, NULL, 36, 2888, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (156, 1, 'Tynna', 233, 499, 'M', 34, 0, 69, 68, 68, 51, '1_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1446, 1440, 815, 1760, 4, 6, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (157, 1, 'Argelius (2)', 376, 466, 'I', 46, 0, 35, 65, 37, 64, '6_20', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1776, 2929, 3360, 1998, 2, 1, 6, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (158, 1, 'Kelrabi', 284, 528, 'L', 28, 0, 32, 35, 30, 14, '4_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3727, 4815, 2274, 2817, 1, 6, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (159, 1, 'Naboo', 594, 142, 'L', 65, 0, 3, 4, 49, 69, '4_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1685, 3408, 1337, 4282, 6, 10, 6, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (160, 1, 'Drema (2)', 842, 374, 'K', 15, 0, 13, 22, 58, 24, '8_27', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4613, 2599, 821, 4778, 10, 4, 4, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (161, 1, 'Lavinius (3)', 120, 795, 'K', 19, 0, 60, 7, 58, 36, '8_13', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1753, 3022, 3400, 4265, 4, 4, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (162, 1, 'Krios (2)', 950, 282, 'C', 69, 0, 62, 69, 8, 69, '7_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1960, 3518, 4778, 2594, 6, 10, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (163, 1, 'Garth', 367, 671, 'C', 116, 0, 59, 35, 61, 38, '7_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1851, 1990, 4755, 2940, 6, 1, 4, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (164, 1, 'Arkon', 950, 789, 'J', -34, 0, 32, 10, 40, 21, '3_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3786, 4603, 4092, 3392, 2, 2, 6, 1, 0, 0, 0, NULL, 36, 8853, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (165, 1, 'Gatas', 472, 158, 'K', 18, 0, 55, 55, 33, 2, '8_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3993, 4324, 4623, 4193, 6, 1, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (166, 1, 'Nequencia', 705, 310, 'J', -34, 0, 11, 40, 41, 32, '3_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1268, 4939, 951, 3257, 2, 6, 10, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (167, 1, 'Prakat (2)', 424, 831, 'L', 74, 0, 54, 46, 39, 10, '4_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3562, 2702, 1599, 2961, 1, 10, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (168, 1, 'Klaestron', 610, 747, 'L', 75, 0, 40, 53, 31, 47, '4_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2058, 1949, 2777, 2686, 4, 4, 6, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (169, 1, 'Donatu', 544, 222, 'N', -2, 0, 61, 21, 36, 41, '2_16', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4343, 4966, 1504, 3671, 1, 1, 10, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (170, 1, 'Balosnee', 658, 633, 'K', -6, 0, 2, 12, 16, 60, '8_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4327, 3809, 2474, 955, 4, 4, 1, 4, 0, 0, 0, NULL, 32, 11200, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (172, 1, 'Legara', 283, 902, 'L', 60, 0, 50, 64, 36, 36, '4_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2576, 4768, 1924, 1200, 2, 4, 10, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (173, 1, 'Epsal (2)', 573, 557, 'M', 29, 0, 10, 1, 11, 56, '1_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3988, 857, 1317, 4604, 6, 6, 6, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (174, 1, 'Cirrus', 106, 383, 'N', 26, 0, 9, 56, 52, 7, '2_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2387, 3035, 3904, 3012, 10, 2, 6, 1, 0, 0, 0, NULL, 11, 9092, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (175, 1, 'Nelvana', 721, 717, 'J', -26, 0, 49, 18, 70, 11, '3_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1169, 1797, 2046, 4858, 6, 4, 6, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (176, 1, 'MemoryAlpha', 99, 444, 'L', 65, 0, 27, 29, 31, 56, '4_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3451, 2875, 1524, 3319, 10, 2, 4, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (177, 1, 'Minos (3)', 835, 750, 'C', 106, 0, 7, 42, 11, 51, '7_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4646, 2704, 2941, 4472, 4, 1, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (178, 1, 'Brental (3)', 684, 564, 'J', -30, 0, 54, 28, 67, 22, '3_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3975, 3442, 4842, 3999, 2, 6, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (179, 1, 'Myrkr', 433, 687, 'I', 92, 0, 47, 3, 47, 23, '6_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4886, 1220, 3652, 4474, 1, 10, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (180, 1, 'Kraus IV', 751, 413, 'G', 66, 0, 2, 33, 7, 8, '5_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1402, 946, 1409, 2970, 4, 6, 2, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (92, 1, 'Beteigeuze', 563, 500, 'I', 32, 15000, 54, 59, 53, 54, '6_4', 1, NULL, 1, 54382, 5, 5, false, 5, false, false, 5, false, 0, 0, 2116, 2171, 2000, 1505, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (97, 1, 'Beteigeuze', 950, 137, 'I', 32, 11385, 52, 55, 55, 51, '6_18', 2, NULL, 2, 52128, 0, 5, false, 10, false, false, 5, false, 0, 0, 2285, 2116, 2007, 2118, 2, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (80, 1, 'Beteigeuze', 301, 767, 'I', 32, 11385, 59, 50, 55, 61, '6_19', 3, NULL, 3, 52385, 0, 5, false, 10, false, false, 5, false, 0, 0, 1954, 2113, 2022, 2184, 1, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (18, 1, 'Beteigeuze', 469, 71, 'I', 32, 11385, 68, 52, 60, 65, '6_14', 4, NULL, 4, 51483, 0, 5, false, 10, false, false, 5, false, 0, 0, 1741, 1697, 2232, 2187, 2, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (139, 1, 'Beteigeuze', 379, 247, 'I', 32, 11385, 62, 62, 57, 51, '6_4', 5, NULL, 5, 50721, 0, 5, false, 10, false, false, 5, false, 0, 0, 2108, 2409, 2494, 2314, 1, 2, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (89, 1, 'Beteigeuze', 268, 582, 'I', 32, 11385, 62, 55, 57, 50, '6_15', 6, NULL, 6, 53534, 0, 5, false, 10, false, false, 5, false, 0, 0, 1777, 1692, 2296, 2094, 1, 2, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (133, 1, 'Beteigeuze', 117, 501, 'I', 32, 11385, 67, 64, 61, 66, '6_19', 7, NULL, 7, 54964, 0, 5, false, 10, false, false, 5, false, 0, 0, 2220, 2353, 2212, 2229, 2, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (171, 1, 'Beteigeuze', 50, 341, 'I', 32, 11385, 67, 67, 64, 62, '6_12', 8, NULL, 8, 53017, 0, 5, false, 10, false, false, 5, false, 0, 0, 2439, 1523, 2088, 2463, 1, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (90, 1, 'Beteigeuze', 807, 421, 'I', 32, 11385, 63, 52, 51, 53, '6_20', 9, NULL, 9, 55758, 0, 5, false, 10, false, false, 5, false, 0, 0, 2221, 1732, 2211, 1831, 1, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (187, 3, 'Arvada', 655, 449, 'L', 22, 0, 51, 50, 35, 23, '4_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4153, 2153, 4467, 4307, 2, 6, 1, 6, 0, 0, 0, NULL, 30, 12395, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (188, 3, 'Norpin', 728, 559, 'G', 61, 0, 25, 64, 4, 11, '5_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4652, 4943, 3812, 4496, 4, 6, 1, 4, 0, 0, 0, NULL, 28, 5446, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (189, 3, 'Ginnick', 210, 309, 'L', 66, 0, 51, 12, 52, 36, '4_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4079, 1593, 3985, 2523, 2, 1, 4, 6, 0, 0, 0, NULL, 36, 8851, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (190, 3, 'M24-Alpha', 289, 290, 'L', 34, 0, 31, 52, 53, 16, '4_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4710, 1070, 1479, 2345, 2, 2, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (191, 3, 'Harod', 650, 183, 'F', -10, 0, 3, 32, 49, 29, '9_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1936, 1796, 1558, 1542, 10, 2, 10, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (192, 3, 'Arcybite', 339, 387, 'G', 133, 0, 3, 57, 65, 30, '5_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4039, 3314, 2383, 3742, 1, 6, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (193, 3, 'Minara', 461, 654, 'M', 36, 0, 13, 65, 22, 26, '1_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1543, 4796, 3162, 1371, 4, 4, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (194, 3, 'Nacht-Acht', 307, 223, 'C', 67, 0, 14, 7, 25, 13, '7_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2185, 1186, 2526, 1366, 6, 4, 6, 4, 0, 0, 0, NULL, 12, 14723, NULL, NULL, 53, 0, 0, 'MINERAL1');
INSERT INTO public.planet VALUES (195, 3, 'Browder', 739, 396, 'C', 104, 0, 36, 26, 64, 52, '7_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3516, 3488, 1802, 1893, 6, 1, 10, 1, 0, 0, 0, NULL, 20, 3275, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (196, 3, 'Ligon II', 327, 522, 'I', 108, 0, 54, 59, 54, 5, '6_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3000, 1976, 3077, 2185, 1, 2, 1, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (197, 3, 'Legara', 675, 604, 'I', 110, 0, 52, 24, 30, 22, '6_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2713, 1853, 2223, 3645, 2, 6, 10, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (198, 3, 'Flusswelt', 452, 364, 'F', 13, 0, 54, 2, 26, 69, '9_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 819, 2319, 4093, 4934, 6, 6, 6, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (199, 3, 'Loren III', 576, 443, 'N', 10, 0, 25, 46, 20, 1, '2_23', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1384, 2974, 2249, 2984, 1, 6, 6, 2, 0, 0, 0, NULL, 7, 6579, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (200, 3, 'Melnos', 449, 276, 'L', 19, 0, 16, 66, 34, 31, '4_14', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2624, 1832, 1467, 1921, 2, 10, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (201, 3, 'Idran', 527, 549, 'K', -10, 0, 37, 58, 30, 65, '8_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2590, 4957, 1278, 3988, 1, 4, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (202, 3, 'Antares', 898, 601, 'N', 8, 0, 56, 2, 16, 21, '2_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3852, 4092, 1723, 1080, 4, 1, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (203, 3, 'Babel', 387, 641, 'I', 97, 0, 53, 5, 57, 46, '6_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4955, 4560, 4056, 1235, 2, 6, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (204, 3, 'Mempa', 325, 824, 'I', 39, 0, 19, 17, 57, 26, '6_16', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4248, 3610, 2870, 4536, 1, 6, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (205, 3, 'Ligos VII', 495, 500, 'N', -1, 0, 47, 70, 20, 47, '2_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4558, 1134, 4880, 4972, 2, 4, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (206, 3, 'Myrkr', 621, 392, 'L', 21, 0, 7, 31, 18, 67, '4_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4208, 3109, 923, 4259, 6, 2, 6, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (207, 3, 'Denkiri', 686, 497, 'F', 2, 0, 53, 23, 66, 38, '9_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4090, 2849, 2633, 2811, 2, 6, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, 'COLONY');
INSERT INTO public.planet VALUES (208, 3, 'Mariah', 810, 676, 'K', 10, 0, 21, 6, 18, 61, '8_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2491, 4164, 3671, 3194, 2, 6, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, 'MINERAL3');
INSERT INTO public.planet VALUES (209, 3, 'Lok', 950, 488, 'F', 22, 0, 30, 1, 62, 66, '9_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1796, 2226, 1389, 4948, 10, 2, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (210, 3, 'Carema', 538, 259, 'M', 63, 0, 35, 7, 49, 13, '1_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2419, 1793, 2086, 1665, 1, 2, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (211, 3, 'Agamar', 173, 353, 'C', 60, 0, 27, 10, 35, 37, '7_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4724, 4595, 1350, 3325, 6, 1, 10, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (212, 3, 'Epsal', 761, 308, 'G', 112, 0, 32, 63, 70, 4, '5_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3746, 1080, 4683, 4679, 1, 4, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (213, 3, 'YagDul', 294, 442, 'G', 60, 0, 69, 4, 52, 2, '5_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1425, 1261, 1708, 2685, 1, 2, 1, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (214, 3, 'Celkar', 418, 709, 'K', -4, 0, 45, 1, 48, 38, '8_14', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2905, 3001, 2278, 4552, 4, 2, 2, 1, 0, 0, 0, NULL, 35, 7295, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (215, 3, 'BetaNiobe', 673, 737, 'J', -28, 0, 28, 24, 14, 5, '3_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4740, 4392, 2323, 1515, 6, 4, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (216, 3, 'Garon', 527, 399, 'K', 1, 0, 6, 27, 51, 42, '8_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4585, 4295, 2392, 2046, 4, 4, 4, 6, 0, 0, 0, NULL, 10, 7773, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (217, 3, 'Ohniaka', 554, 317, 'M', 13, 0, 3, 5, 57, 25, '1_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2279, 2800, 2470, 2705, 2, 4, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (218, 3, 'Ardana', 557, 50, 'J', -34, 0, 45, 43, 59, 69, '3_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1156, 2728, 1511, 3787, 4, 10, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (219, 3, 'Lokvorth', 417, 221, 'F', 30, 0, 32, 35, 4, 4, '9_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1158, 1560, 4922, 4538, 10, 1, 2, 10, 0, 0, 0, NULL, 36, 11974, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (220, 3, 'Luna V', 74, 470, 'F', 4, 0, 20, 37, 43, 35, '9_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2068, 1731, 1455, 3906, 1, 2, 1, 4, 0, 0, 0, NULL, 29, 13995, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (221, 3, 'Doraf', 592, 542, 'J', -29, 0, 54, 66, 30, 2, '3_13', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2556, 1161, 4917, 3887, 10, 6, 10, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (222, 3, 'Delb', 243, 689, 'I', 89, 0, 64, 16, 43, 61, '6_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1992, 2510, 3953, 3082, 2, 1, 10, 10, 0, 0, 0, NULL, 42, 15854, NULL, NULL, 53, 0, 0, 'MINERAL3');
INSERT INTO public.planet VALUES (223, 3, 'Nar Shaddaa', 444, 950, 'K', -14, 0, 9, 60, 13, 40, '8_31', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1998, 3226, 2515, 2454, 10, 4, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (224, 3, 'Platonius', 704, 218, 'I', 74, 0, 4, 30, 46, 34, '6_13', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3737, 4810, 1172, 4230, 4, 2, 10, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (225, 3, 'Psi2000', 554, 496, 'C', 89, 0, 68, 26, 55, 23, '7_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3242, 987, 1793, 4907, 2, 10, 4, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (226, 3, 'Garth', 737, 735, 'M', 33, 0, 70, 40, 39, 12, '1_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3659, 4421, 1235, 1465, 2, 4, 1, 6, 0, 0, 0, NULL, 23, 7518, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (227, 3, 'Gamoray', 488, 589, 'G', 139, 0, 11, 65, 29, 23, '5_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3114, 4792, 2171, 943, 1, 2, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (228, 3, 'Draken', 502, 736, 'M', 19, 0, 69, 31, 61, 4, '1_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4282, 3811, 3328, 2642, 1, 4, 4, 4, 0, 0, 0, NULL, 20, 5549, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (229, 3, 'Krios', 580, 645, 'C', 71, 0, 39, 53, 25, 60, '7_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3637, 1179, 4292, 3039, 6, 6, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (230, 3, 'Reese', 277, 627, 'I', 54, 0, 40, 22, 30, 67, '6_22', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2173, 4509, 4110, 4191, 10, 6, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (231, 3, 'Quazulu', 427, 564, 'G', 69, 0, 55, 13, 69, 12, '5_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1115, 1940, 1357, 1330, 10, 4, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, 'COLONY');
INSERT INTO public.planet VALUES (232, 3, 'AlphaCentauri', 257, 560, 'I', 91, 0, 19, 9, 3, 30, '6_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4336, 3158, 3304, 1991, 6, 6, 4, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (233, 3, 'Iota', 929, 541, 'F', 19, 0, 8, 9, 42, 67, '9_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1485, 3483, 3329, 2744, 6, 1, 1, 4, 0, 0, 0, NULL, 10, 17086, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (234, 3, 'Antares (2)', 392, 339, 'L', 27, 0, 66, 37, 56, 61, '4_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1091, 1563, 925, 1580, 10, 10, 4, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (235, 3, 'Milika', 381, 495, 'G', 121, 0, 25, 45, 46, 70, '5_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4197, 961, 3506, 4750, 10, 2, 6, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (236, 3, 'Parliament', 206, 598, 'L', 31, 0, 57, 53, 11, 21, '4_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4777, 4335, 2993, 4920, 6, 1, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (237, 3, 'Angosia', 319, 748, 'K', 14, 0, 24, 61, 25, 29, '8_19', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1181, 4903, 4302, 4973, 2, 2, 1, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (238, 3, 'Scalos', 411, 410, 'K', 5, 0, 64, 58, 37, 64, '8_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2531, 3509, 3216, 2661, 6, 2, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (239, 3, 'Koris', 571, 785, 'G', 56, 0, 67, 44, 10, 62, '5_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4497, 2082, 2968, 2602, 10, 10, 4, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (240, 3, 'Tiburon', 519, 670, 'C', 106, 0, 8, 9, 24, 56, '7_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3912, 3647, 2465, 4615, 6, 1, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (241, 3, 'Kenda II', 746, 507, 'F', 24, 0, 54, 57, 59, 49, '9_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1832, 4343, 909, 3281, 1, 4, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (242, 3, 'Helska', 632, 108, 'N', 13, 0, 47, 1, 48, 13, '2_13', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4071, 2861, 2492, 1126, 6, 10, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (243, 3, 'Bolarus', 799, 385, 'N', -1, 0, 56, 2, 61, 55, '2_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4714, 2301, 2800, 1475, 10, 4, 4, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (244, 3, 'Delinia', 656, 662, 'L', 86, 0, 7, 45, 27, 38, '4_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1166, 2224, 1013, 3450, 2, 6, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (245, 3, 'Canops', 846, 631, 'K', 5, 0, 23, 49, 17, 60, '8_16', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3092, 3499, 1640, 2006, 10, 10, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (246, 3, 'Nemptox', 619, 320, 'F', 27, 0, 60, 55, 39, 18, '9_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3805, 4667, 4655, 1594, 6, 10, 2, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, 'FUEL');
INSERT INTO public.planet VALUES (247, 3, 'Ophicus', 117, 366, 'J', -33, 0, 64, 60, 8, 3, '3_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2965, 1633, 975, 1237, 10, 4, 6, 4, 0, 0, 0, NULL, 20, 13229, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (248, 3, 'Gamelan', 483, 232, 'K', 18, 0, 61, 66, 50, 28, '8_19', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1424, 3771, 4190, 3171, 2, 2, 2, 4, 0, 0, 0, NULL, 28, 9202, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (249, 3, 'Pollux', 500, 335, 'F', 30, 0, 70, 59, 52, 33, '9_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1347, 3539, 4742, 2240, 4, 10, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (250, 3, 'Acamar', 403, 906, 'M', 27, 0, 47, 41, 41, 31, '1_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3414, 1330, 4060, 3875, 10, 10, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (251, 3, 'Wayland', 794, 469, 'N', 2, 0, 66, 57, 40, 19, '2_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1619, 2102, 1194, 2268, 2, 4, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (252, 3, 'Kobol', 357, 274, 'K', -1, 0, 62, 39, 28, 7, '8_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4962, 1098, 1695, 2567, 4, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (253, 3, 'Stiftermann', 502, 950, 'I', 92, 0, 32, 3, 45, 69, '6_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2087, 3375, 1270, 2333, 1, 4, 6, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (254, 3, 'Dephi Ardu', 240, 476, 'N', 38, 0, 67, 54, 38, 64, '2_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1606, 3851, 4643, 1057, 1, 4, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (255, 3, 'Hoek', 317, 588, 'G', 138, 0, 8, 18, 29, 9, '5_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 998, 1288, 4820, 3150, 4, 10, 6, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (256, 3, 'Iconia', 617, 744, 'C', 48, 0, 58, 51, 57, 13, '7_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2694, 1448, 3010, 3553, 2, 1, 4, 10, 0, 0, 0, NULL, 16, 15027, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (257, 3, 'Stiftermann (2)', 441, 465, 'N', 12, 0, 22, 43, 2, 63, '2_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3305, 4413, 3347, 1660, 2, 4, 6, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (258, 3, 'Fondor', 263, 751, 'N', 24, 0, 64, 68, 7, 23, '2_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4453, 4593, 4110, 2764, 2, 1, 10, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (259, 3, 'Argos', 114, 422, 'I', 98, 0, 8, 15, 46, 12, '6_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3409, 4582, 2210, 4292, 2, 6, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (260, 3, 'Traversan', 651, 550, 'J', -27, 0, 25, 3, 18, 54, '3_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4297, 3032, 2470, 3495, 4, 6, 6, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (261, 3, 'Carema (2)', 396, 851, 'I', 89, 0, 30, 41, 69, 6, '6_22', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4386, 3852, 4270, 2098, 6, 1, 4, 4, 0, 0, 0, NULL, 19, 11996, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (262, 3, 'Ingraham B', 491, 50, 'M', 10, 0, 61, 34, 31, 14, '1_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4675, 4415, 4609, 2143, 10, 10, 6, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (263, 3, 'Phelan', 243, 421, 'F', 16, 0, 35, 47, 16, 50, '9_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3393, 4404, 4868, 4504, 10, 2, 6, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (264, 3, 'Cygnet', 595, 260, 'C', 126, 0, 1, 46, 22, 65, '7_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2448, 2748, 3689, 2310, 6, 4, 1, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (265, 3, 'Nemptox (2)', 702, 287, 'I', 112, 0, 6, 61, 55, 10, '6_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1099, 1311, 2710, 2625, 10, 2, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (266, 3, 'Bothawul', 559, 731, 'K', -11, 0, 26, 25, 63, 30, '8_17', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3033, 2471, 3232, 912, 6, 6, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (268, 3, 'Galen', 372, 578, 'L', 76, 0, 28, 50, 15, 5, '4_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2335, 3591, 4522, 2588, 10, 2, 1, 10, 0, 0, 0, NULL, 8, 12602, NULL, NULL, 53, 0, 0, 'MINERAL1');
INSERT INTO public.planet VALUES (269, 3, 'Luna V (2)', 612, 597, 'F', 21, 0, 25, 35, 13, 62, '9_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 906, 2444, 1112, 1587, 4, 2, 4, 2, 0, 0, 0, NULL, 18, 9421, NULL, NULL, 53, 0, 0, 'MINERAL1');
INSERT INTO public.planet VALUES (270, 3, 'Kuat', 465, 777, 'K', 19, 0, 31, 7, 44, 16, '8_21', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3205, 2432, 4737, 2687, 2, 10, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (271, 3, 'Qualor', 609, 489, 'K', 8, 0, 26, 13, 60, 56, '8_14', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4795, 4828, 1709, 3049, 6, 6, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (272, 3, 'Ogus', 349, 445, 'F', -2, 0, 11, 2, 27, 47, '9_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4184, 3766, 4848, 4194, 6, 1, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (273, 3, 'Oxtorne', 562, 108, 'N', 25, 0, 30, 35, 70, 55, '2_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1889, 1515, 4697, 2125, 1, 10, 6, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (274, 3, 'Vortex', 545, 602, 'K', 7, 0, 15, 8, 48, 52, '8_29', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3615, 1647, 2772, 1343, 2, 1, 6, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (267, 3, 'Beteigeuze', 674, 366, 'I', 32, 15000, 58, 50, 67, 68, '6_20', 12, NULL, 12, 55376, 5, 5, false, 5, false, false, 5, false, 0, 0, 1876, 1615, 2175, 1981, 2, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);


ALTER TABLE public.planet ENABLE TRIGGER ALL;

--
-- TOC entry 3211 (class 0 OID 265422)
-- Dependencies: 232
-- Data for Name: orbital_system; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.orbital_system DISABLE TRIGGER ALL;

INSERT INTO public.orbital_system VALUES (1, 1, NULL);
INSERT INTO public.orbital_system VALUES (2, 1, NULL);
INSERT INTO public.orbital_system VALUES (3, 1, NULL);
INSERT INTO public.orbital_system VALUES (4, 2, NULL);
INSERT INTO public.orbital_system VALUES (5, 2, NULL);
INSERT INTO public.orbital_system VALUES (6, 2, NULL);
INSERT INTO public.orbital_system VALUES (7, 3, NULL);
INSERT INTO public.orbital_system VALUES (8, 3, NULL);
INSERT INTO public.orbital_system VALUES (9, 3, NULL);
INSERT INTO public.orbital_system VALUES (10, 3, NULL);
INSERT INTO public.orbital_system VALUES (11, 3, NULL);
INSERT INTO public.orbital_system VALUES (12, 3, NULL);
INSERT INTO public.orbital_system VALUES (13, 4, NULL);
INSERT INTO public.orbital_system VALUES (14, 4, NULL);
INSERT INTO public.orbital_system VALUES (15, 4, NULL);
INSERT INTO public.orbital_system VALUES (16, 5, NULL);
INSERT INTO public.orbital_system VALUES (17, 6, NULL);
INSERT INTO public.orbital_system VALUES (18, 7, NULL);
INSERT INTO public.orbital_system VALUES (19, 7, NULL);
INSERT INTO public.orbital_system VALUES (20, 7, NULL);
INSERT INTO public.orbital_system VALUES (21, 8, NULL);
INSERT INTO public.orbital_system VALUES (22, 8, NULL);
INSERT INTO public.orbital_system VALUES (23, 8, NULL);
INSERT INTO public.orbital_system VALUES (24, 8, NULL);
INSERT INTO public.orbital_system VALUES (25, 9, NULL);
INSERT INTO public.orbital_system VALUES (26, 9, NULL);
INSERT INTO public.orbital_system VALUES (27, 9, NULL);
INSERT INTO public.orbital_system VALUES (28, 9, NULL);
INSERT INTO public.orbital_system VALUES (29, 9, NULL);
INSERT INTO public.orbital_system VALUES (30, 10, NULL);
INSERT INTO public.orbital_system VALUES (31, 10, NULL);
INSERT INTO public.orbital_system VALUES (32, 10, NULL);
INSERT INTO public.orbital_system VALUES (33, 10, NULL);
INSERT INTO public.orbital_system VALUES (34, 11, NULL);
INSERT INTO public.orbital_system VALUES (35, 11, NULL);
INSERT INTO public.orbital_system VALUES (36, 11, NULL);
INSERT INTO public.orbital_system VALUES (37, 11, NULL);
INSERT INTO public.orbital_system VALUES (38, 11, NULL);
INSERT INTO public.orbital_system VALUES (39, 11, NULL);
INSERT INTO public.orbital_system VALUES (40, 12, NULL);
INSERT INTO public.orbital_system VALUES (41, 12, NULL);
INSERT INTO public.orbital_system VALUES (42, 12, NULL);
INSERT INTO public.orbital_system VALUES (43, 12, NULL);
INSERT INTO public.orbital_system VALUES (44, 12, NULL);
INSERT INTO public.orbital_system VALUES (45, 12, NULL);
INSERT INTO public.orbital_system VALUES (46, 13, NULL);
INSERT INTO public.orbital_system VALUES (47, 13, NULL);
INSERT INTO public.orbital_system VALUES (48, 13, NULL);
INSERT INTO public.orbital_system VALUES (49, 13, NULL);
INSERT INTO public.orbital_system VALUES (50, 13, NULL);
INSERT INTO public.orbital_system VALUES (51, 13, NULL);
INSERT INTO public.orbital_system VALUES (52, 14, NULL);
INSERT INTO public.orbital_system VALUES (53, 15, NULL);
INSERT INTO public.orbital_system VALUES (54, 15, NULL);
INSERT INTO public.orbital_system VALUES (55, 15, NULL);
INSERT INTO public.orbital_system VALUES (56, 15, NULL);
INSERT INTO public.orbital_system VALUES (57, 15, NULL);
INSERT INTO public.orbital_system VALUES (58, 15, NULL);
INSERT INTO public.orbital_system VALUES (59, 16, NULL);
INSERT INTO public.orbital_system VALUES (60, 16, NULL);
INSERT INTO public.orbital_system VALUES (61, 16, NULL);
INSERT INTO public.orbital_system VALUES (62, 17, NULL);
INSERT INTO public.orbital_system VALUES (63, 17, NULL);
INSERT INTO public.orbital_system VALUES (64, 17, NULL);
INSERT INTO public.orbital_system VALUES (65, 17, NULL);
INSERT INTO public.orbital_system VALUES (66, 17, NULL);
INSERT INTO public.orbital_system VALUES (69, 19, NULL);
INSERT INTO public.orbital_system VALUES (70, 19, NULL);
INSERT INTO public.orbital_system VALUES (71, 19, NULL);
INSERT INTO public.orbital_system VALUES (72, 20, NULL);
INSERT INTO public.orbital_system VALUES (73, 20, NULL);
INSERT INTO public.orbital_system VALUES (74, 21, NULL);
INSERT INTO public.orbital_system VALUES (75, 22, NULL);
INSERT INTO public.orbital_system VALUES (76, 22, NULL);
INSERT INTO public.orbital_system VALUES (77, 22, NULL);
INSERT INTO public.orbital_system VALUES (78, 22, NULL);
INSERT INTO public.orbital_system VALUES (79, 22, NULL);
INSERT INTO public.orbital_system VALUES (80, 23, NULL);
INSERT INTO public.orbital_system VALUES (81, 23, NULL);
INSERT INTO public.orbital_system VALUES (82, 23, NULL);
INSERT INTO public.orbital_system VALUES (83, 23, NULL);
INSERT INTO public.orbital_system VALUES (84, 23, NULL);
INSERT INTO public.orbital_system VALUES (85, 23, NULL);
INSERT INTO public.orbital_system VALUES (86, 24, NULL);
INSERT INTO public.orbital_system VALUES (87, 24, NULL);
INSERT INTO public.orbital_system VALUES (88, 24, NULL);
INSERT INTO public.orbital_system VALUES (89, 25, NULL);
INSERT INTO public.orbital_system VALUES (90, 26, NULL);
INSERT INTO public.orbital_system VALUES (91, 26, NULL);
INSERT INTO public.orbital_system VALUES (92, 26, NULL);
INSERT INTO public.orbital_system VALUES (93, 26, NULL);
INSERT INTO public.orbital_system VALUES (94, 26, NULL);
INSERT INTO public.orbital_system VALUES (95, 26, NULL);
INSERT INTO public.orbital_system VALUES (96, 27, NULL);
INSERT INTO public.orbital_system VALUES (97, 27, NULL);
INSERT INTO public.orbital_system VALUES (98, 27, NULL);
INSERT INTO public.orbital_system VALUES (99, 28, NULL);
INSERT INTO public.orbital_system VALUES (100, 28, NULL);
INSERT INTO public.orbital_system VALUES (101, 28, NULL);
INSERT INTO public.orbital_system VALUES (102, 28, NULL);
INSERT INTO public.orbital_system VALUES (103, 28, NULL);
INSERT INTO public.orbital_system VALUES (104, 29, NULL);
INSERT INTO public.orbital_system VALUES (105, 29, NULL);
INSERT INTO public.orbital_system VALUES (106, 30, NULL);
INSERT INTO public.orbital_system VALUES (107, 31, NULL);
INSERT INTO public.orbital_system VALUES (108, 31, NULL);
INSERT INTO public.orbital_system VALUES (109, 31, NULL);
INSERT INTO public.orbital_system VALUES (110, 31, NULL);
INSERT INTO public.orbital_system VALUES (111, 31, NULL);
INSERT INTO public.orbital_system VALUES (112, 32, NULL);
INSERT INTO public.orbital_system VALUES (113, 32, NULL);
INSERT INTO public.orbital_system VALUES (114, 32, NULL);
INSERT INTO public.orbital_system VALUES (115, 33, NULL);
INSERT INTO public.orbital_system VALUES (116, 33, NULL);
INSERT INTO public.orbital_system VALUES (117, 33, NULL);
INSERT INTO public.orbital_system VALUES (118, 33, NULL);
INSERT INTO public.orbital_system VALUES (119, 33, NULL);
INSERT INTO public.orbital_system VALUES (120, 34, NULL);
INSERT INTO public.orbital_system VALUES (121, 34, NULL);
INSERT INTO public.orbital_system VALUES (122, 34, NULL);
INSERT INTO public.orbital_system VALUES (123, 35, NULL);
INSERT INTO public.orbital_system VALUES (124, 35, NULL);
INSERT INTO public.orbital_system VALUES (125, 35, NULL);
INSERT INTO public.orbital_system VALUES (126, 35, NULL);
INSERT INTO public.orbital_system VALUES (127, 35, NULL);
INSERT INTO public.orbital_system VALUES (128, 36, NULL);
INSERT INTO public.orbital_system VALUES (129, 36, NULL);
INSERT INTO public.orbital_system VALUES (130, 36, NULL);
INSERT INTO public.orbital_system VALUES (131, 36, NULL);
INSERT INTO public.orbital_system VALUES (132, 36, NULL);
INSERT INTO public.orbital_system VALUES (133, 37, NULL);
INSERT INTO public.orbital_system VALUES (134, 37, NULL);
INSERT INTO public.orbital_system VALUES (135, 38, NULL);
INSERT INTO public.orbital_system VALUES (136, 38, NULL);
INSERT INTO public.orbital_system VALUES (137, 38, NULL);
INSERT INTO public.orbital_system VALUES (138, 39, NULL);
INSERT INTO public.orbital_system VALUES (139, 39, NULL);
INSERT INTO public.orbital_system VALUES (140, 39, NULL);
INSERT INTO public.orbital_system VALUES (141, 39, NULL);
INSERT INTO public.orbital_system VALUES (142, 39, NULL);
INSERT INTO public.orbital_system VALUES (143, 39, NULL);
INSERT INTO public.orbital_system VALUES (144, 40, NULL);
INSERT INTO public.orbital_system VALUES (145, 40, NULL);
INSERT INTO public.orbital_system VALUES (146, 40, NULL);
INSERT INTO public.orbital_system VALUES (147, 40, NULL);
INSERT INTO public.orbital_system VALUES (148, 40, NULL);
INSERT INTO public.orbital_system VALUES (149, 40, NULL);
INSERT INTO public.orbital_system VALUES (150, 41, NULL);
INSERT INTO public.orbital_system VALUES (151, 41, NULL);
INSERT INTO public.orbital_system VALUES (152, 41, NULL);
INSERT INTO public.orbital_system VALUES (153, 41, NULL);
INSERT INTO public.orbital_system VALUES (154, 42, NULL);
INSERT INTO public.orbital_system VALUES (155, 42, NULL);
INSERT INTO public.orbital_system VALUES (156, 42, NULL);
INSERT INTO public.orbital_system VALUES (157, 42, NULL);
INSERT INTO public.orbital_system VALUES (158, 43, NULL);
INSERT INTO public.orbital_system VALUES (159, 43, NULL);
INSERT INTO public.orbital_system VALUES (160, 44, NULL);
INSERT INTO public.orbital_system VALUES (161, 44, NULL);
INSERT INTO public.orbital_system VALUES (162, 44, NULL);
INSERT INTO public.orbital_system VALUES (163, 44, NULL);
INSERT INTO public.orbital_system VALUES (164, 45, NULL);
INSERT INTO public.orbital_system VALUES (165, 45, NULL);
INSERT INTO public.orbital_system VALUES (166, 45, NULL);
INSERT INTO public.orbital_system VALUES (167, 45, NULL);
INSERT INTO public.orbital_system VALUES (168, 45, NULL);
INSERT INTO public.orbital_system VALUES (169, 46, NULL);
INSERT INTO public.orbital_system VALUES (170, 46, NULL);
INSERT INTO public.orbital_system VALUES (171, 46, NULL);
INSERT INTO public.orbital_system VALUES (172, 46, NULL);
INSERT INTO public.orbital_system VALUES (173, 46, NULL);
INSERT INTO public.orbital_system VALUES (174, 47, NULL);
INSERT INTO public.orbital_system VALUES (175, 47, NULL);
INSERT INTO public.orbital_system VALUES (176, 48, NULL);
INSERT INTO public.orbital_system VALUES (177, 48, NULL);
INSERT INTO public.orbital_system VALUES (178, 48, NULL);
INSERT INTO public.orbital_system VALUES (179, 49, NULL);
INSERT INTO public.orbital_system VALUES (180, 49, NULL);
INSERT INTO public.orbital_system VALUES (181, 49, NULL);
INSERT INTO public.orbital_system VALUES (182, 49, NULL);
INSERT INTO public.orbital_system VALUES (183, 49, NULL);
INSERT INTO public.orbital_system VALUES (184, 49, NULL);
INSERT INTO public.orbital_system VALUES (185, 50, NULL);
INSERT INTO public.orbital_system VALUES (186, 50, NULL);
INSERT INTO public.orbital_system VALUES (187, 50, NULL);
INSERT INTO public.orbital_system VALUES (188, 50, NULL);
INSERT INTO public.orbital_system VALUES (189, 50, NULL);
INSERT INTO public.orbital_system VALUES (190, 50, NULL);
INSERT INTO public.orbital_system VALUES (191, 51, NULL);
INSERT INTO public.orbital_system VALUES (192, 51, NULL);
INSERT INTO public.orbital_system VALUES (193, 51, NULL);
INSERT INTO public.orbital_system VALUES (194, 51, NULL);
INSERT INTO public.orbital_system VALUES (195, 51, NULL);
INSERT INTO public.orbital_system VALUES (196, 52, NULL);
INSERT INTO public.orbital_system VALUES (197, 52, NULL);
INSERT INTO public.orbital_system VALUES (198, 52, NULL);
INSERT INTO public.orbital_system VALUES (199, 53, NULL);
INSERT INTO public.orbital_system VALUES (200, 53, NULL);
INSERT INTO public.orbital_system VALUES (201, 53, NULL);
INSERT INTO public.orbital_system VALUES (202, 53, NULL);
INSERT INTO public.orbital_system VALUES (203, 53, NULL);
INSERT INTO public.orbital_system VALUES (204, 53, NULL);
INSERT INTO public.orbital_system VALUES (205, 54, NULL);
INSERT INTO public.orbital_system VALUES (206, 54, NULL);
INSERT INTO public.orbital_system VALUES (207, 54, NULL);
INSERT INTO public.orbital_system VALUES (208, 54, NULL);
INSERT INTO public.orbital_system VALUES (209, 54, NULL);
INSERT INTO public.orbital_system VALUES (210, 55, NULL);
INSERT INTO public.orbital_system VALUES (211, 55, NULL);
INSERT INTO public.orbital_system VALUES (212, 55, NULL);
INSERT INTO public.orbital_system VALUES (213, 56, NULL);
INSERT INTO public.orbital_system VALUES (214, 56, NULL);
INSERT INTO public.orbital_system VALUES (215, 56, NULL);
INSERT INTO public.orbital_system VALUES (216, 57, NULL);
INSERT INTO public.orbital_system VALUES (217, 57, NULL);
INSERT INTO public.orbital_system VALUES (218, 57, NULL);
INSERT INTO public.orbital_system VALUES (219, 57, NULL);
INSERT INTO public.orbital_system VALUES (220, 57, NULL);
INSERT INTO public.orbital_system VALUES (221, 57, NULL);
INSERT INTO public.orbital_system VALUES (222, 58, NULL);
INSERT INTO public.orbital_system VALUES (223, 58, NULL);
INSERT INTO public.orbital_system VALUES (224, 58, NULL);
INSERT INTO public.orbital_system VALUES (225, 59, NULL);
INSERT INTO public.orbital_system VALUES (226, 60, NULL);
INSERT INTO public.orbital_system VALUES (227, 60, NULL);
INSERT INTO public.orbital_system VALUES (228, 60, NULL);
INSERT INTO public.orbital_system VALUES (229, 60, NULL);
INSERT INTO public.orbital_system VALUES (230, 60, NULL);
INSERT INTO public.orbital_system VALUES (231, 60, NULL);
INSERT INTO public.orbital_system VALUES (232, 61, NULL);
INSERT INTO public.orbital_system VALUES (233, 61, NULL);
INSERT INTO public.orbital_system VALUES (234, 62, NULL);
INSERT INTO public.orbital_system VALUES (235, 62, NULL);
INSERT INTO public.orbital_system VALUES (236, 63, NULL);
INSERT INTO public.orbital_system VALUES (237, 63, NULL);
INSERT INTO public.orbital_system VALUES (238, 64, NULL);
INSERT INTO public.orbital_system VALUES (239, 64, NULL);
INSERT INTO public.orbital_system VALUES (240, 64, NULL);
INSERT INTO public.orbital_system VALUES (241, 64, NULL);
INSERT INTO public.orbital_system VALUES (242, 64, NULL);
INSERT INTO public.orbital_system VALUES (243, 64, NULL);
INSERT INTO public.orbital_system VALUES (244, 65, NULL);
INSERT INTO public.orbital_system VALUES (245, 66, NULL);
INSERT INTO public.orbital_system VALUES (246, 66, NULL);
INSERT INTO public.orbital_system VALUES (247, 66, NULL);
INSERT INTO public.orbital_system VALUES (248, 67, NULL);
INSERT INTO public.orbital_system VALUES (249, 67, NULL);
INSERT INTO public.orbital_system VALUES (250, 67, NULL);
INSERT INTO public.orbital_system VALUES (251, 67, NULL);
INSERT INTO public.orbital_system VALUES (252, 68, NULL);
INSERT INTO public.orbital_system VALUES (253, 68, NULL);
INSERT INTO public.orbital_system VALUES (254, 68, NULL);
INSERT INTO public.orbital_system VALUES (255, 68, NULL);
INSERT INTO public.orbital_system VALUES (256, 68, NULL);
INSERT INTO public.orbital_system VALUES (257, 69, NULL);
INSERT INTO public.orbital_system VALUES (258, 69, NULL);
INSERT INTO public.orbital_system VALUES (259, 69, NULL);
INSERT INTO public.orbital_system VALUES (260, 70, NULL);
INSERT INTO public.orbital_system VALUES (261, 70, NULL);
INSERT INTO public.orbital_system VALUES (262, 70, NULL);
INSERT INTO public.orbital_system VALUES (263, 71, NULL);
INSERT INTO public.orbital_system VALUES (264, 72, NULL);
INSERT INTO public.orbital_system VALUES (265, 72, NULL);
INSERT INTO public.orbital_system VALUES (266, 73, NULL);
INSERT INTO public.orbital_system VALUES (267, 73, NULL);
INSERT INTO public.orbital_system VALUES (268, 73, NULL);
INSERT INTO public.orbital_system VALUES (269, 74, NULL);
INSERT INTO public.orbital_system VALUES (270, 74, NULL);
INSERT INTO public.orbital_system VALUES (271, 74, NULL);
INSERT INTO public.orbital_system VALUES (272, 75, NULL);
INSERT INTO public.orbital_system VALUES (273, 75, NULL);
INSERT INTO public.orbital_system VALUES (274, 76, NULL);
INSERT INTO public.orbital_system VALUES (275, 76, NULL);
INSERT INTO public.orbital_system VALUES (276, 76, NULL);
INSERT INTO public.orbital_system VALUES (277, 76, NULL);
INSERT INTO public.orbital_system VALUES (278, 77, NULL);
INSERT INTO public.orbital_system VALUES (279, 77, NULL);
INSERT INTO public.orbital_system VALUES (280, 77, NULL);
INSERT INTO public.orbital_system VALUES (281, 77, NULL);
INSERT INTO public.orbital_system VALUES (282, 77, NULL);
INSERT INTO public.orbital_system VALUES (283, 77, NULL);
INSERT INTO public.orbital_system VALUES (284, 78, NULL);
INSERT INTO public.orbital_system VALUES (285, 78, NULL);
INSERT INTO public.orbital_system VALUES (286, 78, NULL);
INSERT INTO public.orbital_system VALUES (287, 78, NULL);
INSERT INTO public.orbital_system VALUES (288, 79, NULL);
INSERT INTO public.orbital_system VALUES (289, 79, NULL);
INSERT INTO public.orbital_system VALUES (290, 79, NULL);
INSERT INTO public.orbital_system VALUES (291, 79, NULL);
INSERT INTO public.orbital_system VALUES (294, 81, NULL);
INSERT INTO public.orbital_system VALUES (295, 81, NULL);
INSERT INTO public.orbital_system VALUES (296, 81, NULL);
INSERT INTO public.orbital_system VALUES (297, 81, NULL);
INSERT INTO public.orbital_system VALUES (298, 82, NULL);
INSERT INTO public.orbital_system VALUES (299, 82, NULL);
INSERT INTO public.orbital_system VALUES (300, 82, NULL);
INSERT INTO public.orbital_system VALUES (301, 82, NULL);
INSERT INTO public.orbital_system VALUES (302, 82, NULL);
INSERT INTO public.orbital_system VALUES (303, 82, NULL);
INSERT INTO public.orbital_system VALUES (304, 83, NULL);
INSERT INTO public.orbital_system VALUES (305, 83, NULL);
INSERT INTO public.orbital_system VALUES (306, 84, NULL);
INSERT INTO public.orbital_system VALUES (307, 85, NULL);
INSERT INTO public.orbital_system VALUES (308, 85, NULL);
INSERT INTO public.orbital_system VALUES (309, 85, NULL);
INSERT INTO public.orbital_system VALUES (310, 86, NULL);
INSERT INTO public.orbital_system VALUES (311, 87, NULL);
INSERT INTO public.orbital_system VALUES (312, 87, NULL);
INSERT INTO public.orbital_system VALUES (313, 88, NULL);
INSERT INTO public.orbital_system VALUES (319, 91, NULL);
INSERT INTO public.orbital_system VALUES (320, 91, NULL);
INSERT INTO public.orbital_system VALUES (321, 91, NULL);
INSERT INTO public.orbital_system VALUES (322, 91, NULL);
INSERT INTO public.orbital_system VALUES (323, 91, NULL);
INSERT INTO public.orbital_system VALUES (324, 91, NULL);
INSERT INTO public.orbital_system VALUES (327, 93, NULL);
INSERT INTO public.orbital_system VALUES (328, 93, NULL);
INSERT INTO public.orbital_system VALUES (329, 93, NULL);
INSERT INTO public.orbital_system VALUES (330, 94, NULL);
INSERT INTO public.orbital_system VALUES (331, 94, NULL);
INSERT INTO public.orbital_system VALUES (332, 94, NULL);
INSERT INTO public.orbital_system VALUES (333, 94, NULL);
INSERT INTO public.orbital_system VALUES (334, 95, NULL);
INSERT INTO public.orbital_system VALUES (335, 95, NULL);
INSERT INTO public.orbital_system VALUES (336, 95, NULL);
INSERT INTO public.orbital_system VALUES (337, 95, NULL);
INSERT INTO public.orbital_system VALUES (338, 96, NULL);
INSERT INTO public.orbital_system VALUES (339, 96, NULL);
INSERT INTO public.orbital_system VALUES (340, 96, NULL);
INSERT INTO public.orbital_system VALUES (341, 96, NULL);
INSERT INTO public.orbital_system VALUES (342, 96, NULL);
INSERT INTO public.orbital_system VALUES (349, 98, NULL);
INSERT INTO public.orbital_system VALUES (350, 98, NULL);
INSERT INTO public.orbital_system VALUES (351, 98, NULL);
INSERT INTO public.orbital_system VALUES (352, 98, NULL);
INSERT INTO public.orbital_system VALUES (353, 99, NULL);
INSERT INTO public.orbital_system VALUES (354, 99, NULL);
INSERT INTO public.orbital_system VALUES (355, 99, NULL);
INSERT INTO public.orbital_system VALUES (356, 100, NULL);
INSERT INTO public.orbital_system VALUES (357, 100, NULL);
INSERT INTO public.orbital_system VALUES (358, 101, NULL);
INSERT INTO public.orbital_system VALUES (359, 101, NULL);
INSERT INTO public.orbital_system VALUES (360, 101, NULL);
INSERT INTO public.orbital_system VALUES (361, 101, NULL);
INSERT INTO public.orbital_system VALUES (362, 101, NULL);
INSERT INTO public.orbital_system VALUES (363, 102, NULL);
INSERT INTO public.orbital_system VALUES (364, 102, NULL);
INSERT INTO public.orbital_system VALUES (365, 102, NULL);
INSERT INTO public.orbital_system VALUES (366, 103, NULL);
INSERT INTO public.orbital_system VALUES (367, 103, NULL);
INSERT INTO public.orbital_system VALUES (368, 103, NULL);
INSERT INTO public.orbital_system VALUES (369, 103, NULL);
INSERT INTO public.orbital_system VALUES (370, 104, NULL);
INSERT INTO public.orbital_system VALUES (371, 105, NULL);
INSERT INTO public.orbital_system VALUES (372, 105, NULL);
INSERT INTO public.orbital_system VALUES (373, 105, NULL);
INSERT INTO public.orbital_system VALUES (374, 105, NULL);
INSERT INTO public.orbital_system VALUES (375, 105, NULL);
INSERT INTO public.orbital_system VALUES (376, 105, NULL);
INSERT INTO public.orbital_system VALUES (377, 106, NULL);
INSERT INTO public.orbital_system VALUES (378, 106, NULL);
INSERT INTO public.orbital_system VALUES (379, 106, NULL);
INSERT INTO public.orbital_system VALUES (380, 106, NULL);
INSERT INTO public.orbital_system VALUES (381, 106, NULL);
INSERT INTO public.orbital_system VALUES (382, 106, NULL);
INSERT INTO public.orbital_system VALUES (383, 107, NULL);
INSERT INTO public.orbital_system VALUES (384, 107, NULL);
INSERT INTO public.orbital_system VALUES (385, 107, NULL);
INSERT INTO public.orbital_system VALUES (386, 107, NULL);
INSERT INTO public.orbital_system VALUES (387, 108, NULL);
INSERT INTO public.orbital_system VALUES (388, 108, NULL);
INSERT INTO public.orbital_system VALUES (389, 109, NULL);
INSERT INTO public.orbital_system VALUES (390, 109, NULL);
INSERT INTO public.orbital_system VALUES (391, 109, NULL);
INSERT INTO public.orbital_system VALUES (392, 110, NULL);
INSERT INTO public.orbital_system VALUES (393, 111, NULL);
INSERT INTO public.orbital_system VALUES (394, 111, NULL);
INSERT INTO public.orbital_system VALUES (395, 112, NULL);
INSERT INTO public.orbital_system VALUES (396, 112, NULL);
INSERT INTO public.orbital_system VALUES (397, 112, NULL);
INSERT INTO public.orbital_system VALUES (398, 112, NULL);
INSERT INTO public.orbital_system VALUES (399, 112, NULL);
INSERT INTO public.orbital_system VALUES (400, 112, NULL);
INSERT INTO public.orbital_system VALUES (401, 113, NULL);
INSERT INTO public.orbital_system VALUES (402, 113, NULL);
INSERT INTO public.orbital_system VALUES (403, 114, NULL);
INSERT INTO public.orbital_system VALUES (404, 114, NULL);
INSERT INTO public.orbital_system VALUES (405, 114, NULL);
INSERT INTO public.orbital_system VALUES (406, 115, NULL);
INSERT INTO public.orbital_system VALUES (407, 115, NULL);
INSERT INTO public.orbital_system VALUES (408, 116, NULL);
INSERT INTO public.orbital_system VALUES (409, 116, NULL);
INSERT INTO public.orbital_system VALUES (410, 116, NULL);
INSERT INTO public.orbital_system VALUES (411, 116, NULL);
INSERT INTO public.orbital_system VALUES (412, 117, NULL);
INSERT INTO public.orbital_system VALUES (413, 117, NULL);
INSERT INTO public.orbital_system VALUES (414, 117, NULL);
INSERT INTO public.orbital_system VALUES (415, 118, NULL);
INSERT INTO public.orbital_system VALUES (416, 118, NULL);
INSERT INTO public.orbital_system VALUES (417, 119, NULL);
INSERT INTO public.orbital_system VALUES (418, 119, NULL);
INSERT INTO public.orbital_system VALUES (419, 119, NULL);
INSERT INTO public.orbital_system VALUES (420, 119, NULL);
INSERT INTO public.orbital_system VALUES (421, 120, NULL);
INSERT INTO public.orbital_system VALUES (422, 120, NULL);
INSERT INTO public.orbital_system VALUES (423, 120, NULL);
INSERT INTO public.orbital_system VALUES (424, 120, NULL);
INSERT INTO public.orbital_system VALUES (425, 120, NULL);
INSERT INTO public.orbital_system VALUES (426, 120, NULL);
INSERT INTO public.orbital_system VALUES (427, 121, NULL);
INSERT INTO public.orbital_system VALUES (428, 121, NULL);
INSERT INTO public.orbital_system VALUES (429, 121, NULL);
INSERT INTO public.orbital_system VALUES (430, 121, NULL);
INSERT INTO public.orbital_system VALUES (431, 121, NULL);
INSERT INTO public.orbital_system VALUES (432, 121, NULL);
INSERT INTO public.orbital_system VALUES (433, 122, NULL);
INSERT INTO public.orbital_system VALUES (434, 122, NULL);
INSERT INTO public.orbital_system VALUES (435, 122, NULL);
INSERT INTO public.orbital_system VALUES (436, 122, NULL);
INSERT INTO public.orbital_system VALUES (437, 123, NULL);
INSERT INTO public.orbital_system VALUES (438, 123, NULL);
INSERT INTO public.orbital_system VALUES (439, 123, NULL);
INSERT INTO public.orbital_system VALUES (440, 123, NULL);
INSERT INTO public.orbital_system VALUES (441, 123, NULL);
INSERT INTO public.orbital_system VALUES (442, 123, NULL);
INSERT INTO public.orbital_system VALUES (443, 124, NULL);
INSERT INTO public.orbital_system VALUES (444, 124, NULL);
INSERT INTO public.orbital_system VALUES (445, 124, NULL);
INSERT INTO public.orbital_system VALUES (446, 124, NULL);
INSERT INTO public.orbital_system VALUES (447, 124, NULL);
INSERT INTO public.orbital_system VALUES (448, 125, NULL);
INSERT INTO public.orbital_system VALUES (449, 126, NULL);
INSERT INTO public.orbital_system VALUES (450, 127, NULL);
INSERT INTO public.orbital_system VALUES (451, 127, NULL);
INSERT INTO public.orbital_system VALUES (452, 127, NULL);
INSERT INTO public.orbital_system VALUES (453, 127, NULL);
INSERT INTO public.orbital_system VALUES (454, 127, NULL);
INSERT INTO public.orbital_system VALUES (455, 128, NULL);
INSERT INTO public.orbital_system VALUES (456, 128, NULL);
INSERT INTO public.orbital_system VALUES (457, 128, NULL);
INSERT INTO public.orbital_system VALUES (458, 128, NULL);
INSERT INTO public.orbital_system VALUES (459, 129, NULL);
INSERT INTO public.orbital_system VALUES (460, 129, NULL);
INSERT INTO public.orbital_system VALUES (461, 129, NULL);
INSERT INTO public.orbital_system VALUES (462, 130, NULL);
INSERT INTO public.orbital_system VALUES (463, 130, NULL);
INSERT INTO public.orbital_system VALUES (464, 130, NULL);
INSERT INTO public.orbital_system VALUES (465, 130, NULL);
INSERT INTO public.orbital_system VALUES (466, 131, NULL);
INSERT INTO public.orbital_system VALUES (467, 131, NULL);
INSERT INTO public.orbital_system VALUES (468, 131, NULL);
INSERT INTO public.orbital_system VALUES (469, 132, NULL);
INSERT INTO public.orbital_system VALUES (470, 132, NULL);
INSERT INTO public.orbital_system VALUES (475, 134, NULL);
INSERT INTO public.orbital_system VALUES (476, 134, NULL);
INSERT INTO public.orbital_system VALUES (477, 134, NULL);
INSERT INTO public.orbital_system VALUES (478, 135, NULL);
INSERT INTO public.orbital_system VALUES (479, 135, NULL);
INSERT INTO public.orbital_system VALUES (480, 135, NULL);
INSERT INTO public.orbital_system VALUES (481, 135, NULL);
INSERT INTO public.orbital_system VALUES (482, 135, NULL);
INSERT INTO public.orbital_system VALUES (483, 135, NULL);
INSERT INTO public.orbital_system VALUES (484, 136, NULL);
INSERT INTO public.orbital_system VALUES (485, 136, NULL);
INSERT INTO public.orbital_system VALUES (486, 136, NULL);
INSERT INTO public.orbital_system VALUES (487, 136, NULL);
INSERT INTO public.orbital_system VALUES (488, 136, NULL);
INSERT INTO public.orbital_system VALUES (489, 137, NULL);
INSERT INTO public.orbital_system VALUES (490, 137, NULL);
INSERT INTO public.orbital_system VALUES (491, 137, NULL);
INSERT INTO public.orbital_system VALUES (492, 137, NULL);
INSERT INTO public.orbital_system VALUES (493, 137, NULL);
INSERT INTO public.orbital_system VALUES (494, 137, NULL);
INSERT INTO public.orbital_system VALUES (495, 138, NULL);
INSERT INTO public.orbital_system VALUES (502, 140, NULL);
INSERT INTO public.orbital_system VALUES (503, 140, NULL);
INSERT INTO public.orbital_system VALUES (504, 140, NULL);
INSERT INTO public.orbital_system VALUES (505, 141, NULL);
INSERT INTO public.orbital_system VALUES (506, 141, NULL);
INSERT INTO public.orbital_system VALUES (507, 141, NULL);
INSERT INTO public.orbital_system VALUES (508, 142, NULL);
INSERT INTO public.orbital_system VALUES (509, 142, NULL);
INSERT INTO public.orbital_system VALUES (510, 142, NULL);
INSERT INTO public.orbital_system VALUES (511, 142, NULL);
INSERT INTO public.orbital_system VALUES (512, 142, NULL);
INSERT INTO public.orbital_system VALUES (513, 142, NULL);
INSERT INTO public.orbital_system VALUES (514, 143, NULL);
INSERT INTO public.orbital_system VALUES (515, 143, NULL);
INSERT INTO public.orbital_system VALUES (516, 143, NULL);
INSERT INTO public.orbital_system VALUES (517, 144, NULL);
INSERT INTO public.orbital_system VALUES (518, 144, NULL);
INSERT INTO public.orbital_system VALUES (519, 144, NULL);
INSERT INTO public.orbital_system VALUES (520, 144, NULL);
INSERT INTO public.orbital_system VALUES (521, 144, NULL);
INSERT INTO public.orbital_system VALUES (522, 144, NULL);
INSERT INTO public.orbital_system VALUES (523, 145, NULL);
INSERT INTO public.orbital_system VALUES (524, 145, NULL);
INSERT INTO public.orbital_system VALUES (525, 146, NULL);
INSERT INTO public.orbital_system VALUES (526, 146, NULL);
INSERT INTO public.orbital_system VALUES (527, 147, NULL);
INSERT INTO public.orbital_system VALUES (528, 147, NULL);
INSERT INTO public.orbital_system VALUES (529, 147, NULL);
INSERT INTO public.orbital_system VALUES (530, 148, NULL);
INSERT INTO public.orbital_system VALUES (531, 148, NULL);
INSERT INTO public.orbital_system VALUES (532, 148, NULL);
INSERT INTO public.orbital_system VALUES (533, 148, NULL);
INSERT INTO public.orbital_system VALUES (534, 148, NULL);
INSERT INTO public.orbital_system VALUES (535, 148, NULL);
INSERT INTO public.orbital_system VALUES (536, 149, NULL);
INSERT INTO public.orbital_system VALUES (537, 149, NULL);
INSERT INTO public.orbital_system VALUES (538, 150, NULL);
INSERT INTO public.orbital_system VALUES (539, 150, NULL);
INSERT INTO public.orbital_system VALUES (540, 150, NULL);
INSERT INTO public.orbital_system VALUES (541, 150, NULL);
INSERT INTO public.orbital_system VALUES (542, 150, NULL);
INSERT INTO public.orbital_system VALUES (543, 150, NULL);
INSERT INTO public.orbital_system VALUES (544, 151, NULL);
INSERT INTO public.orbital_system VALUES (545, 151, NULL);
INSERT INTO public.orbital_system VALUES (546, 151, NULL);
INSERT INTO public.orbital_system VALUES (547, 151, NULL);
INSERT INTO public.orbital_system VALUES (548, 151, NULL);
INSERT INTO public.orbital_system VALUES (549, 152, NULL);
INSERT INTO public.orbital_system VALUES (550, 153, NULL);
INSERT INTO public.orbital_system VALUES (551, 153, NULL);
INSERT INTO public.orbital_system VALUES (552, 153, NULL);
INSERT INTO public.orbital_system VALUES (553, 153, NULL);
INSERT INTO public.orbital_system VALUES (554, 154, NULL);
INSERT INTO public.orbital_system VALUES (555, 155, NULL);
INSERT INTO public.orbital_system VALUES (556, 156, NULL);
INSERT INTO public.orbital_system VALUES (557, 157, NULL);
INSERT INTO public.orbital_system VALUES (558, 158, NULL);
INSERT INTO public.orbital_system VALUES (559, 158, NULL);
INSERT INTO public.orbital_system VALUES (560, 158, NULL);
INSERT INTO public.orbital_system VALUES (561, 159, NULL);
INSERT INTO public.orbital_system VALUES (562, 160, NULL);
INSERT INTO public.orbital_system VALUES (563, 160, NULL);
INSERT INTO public.orbital_system VALUES (564, 160, NULL);
INSERT INTO public.orbital_system VALUES (565, 160, NULL);
INSERT INTO public.orbital_system VALUES (566, 160, NULL);
INSERT INTO public.orbital_system VALUES (567, 160, NULL);
INSERT INTO public.orbital_system VALUES (568, 161, NULL);
INSERT INTO public.orbital_system VALUES (569, 161, NULL);
INSERT INTO public.orbital_system VALUES (570, 161, NULL);
INSERT INTO public.orbital_system VALUES (571, 161, NULL);
INSERT INTO public.orbital_system VALUES (572, 162, NULL);
INSERT INTO public.orbital_system VALUES (573, 162, NULL);
INSERT INTO public.orbital_system VALUES (574, 162, NULL);
INSERT INTO public.orbital_system VALUES (575, 162, NULL);
INSERT INTO public.orbital_system VALUES (576, 162, NULL);
INSERT INTO public.orbital_system VALUES (577, 162, NULL);
INSERT INTO public.orbital_system VALUES (578, 163, NULL);
INSERT INTO public.orbital_system VALUES (579, 163, NULL);
INSERT INTO public.orbital_system VALUES (580, 163, NULL);
INSERT INTO public.orbital_system VALUES (581, 163, NULL);
INSERT INTO public.orbital_system VALUES (582, 163, NULL);
INSERT INTO public.orbital_system VALUES (583, 163, NULL);
INSERT INTO public.orbital_system VALUES (584, 164, NULL);
INSERT INTO public.orbital_system VALUES (585, 165, NULL);
INSERT INTO public.orbital_system VALUES (586, 165, NULL);
INSERT INTO public.orbital_system VALUES (587, 166, NULL);
INSERT INTO public.orbital_system VALUES (588, 166, NULL);
INSERT INTO public.orbital_system VALUES (589, 167, NULL);
INSERT INTO public.orbital_system VALUES (590, 167, NULL);
INSERT INTO public.orbital_system VALUES (591, 167, NULL);
INSERT INTO public.orbital_system VALUES (592, 167, NULL);
INSERT INTO public.orbital_system VALUES (593, 167, NULL);
INSERT INTO public.orbital_system VALUES (594, 167, NULL);
INSERT INTO public.orbital_system VALUES (595, 168, NULL);
INSERT INTO public.orbital_system VALUES (596, 168, NULL);
INSERT INTO public.orbital_system VALUES (597, 168, NULL);
INSERT INTO public.orbital_system VALUES (598, 168, NULL);
INSERT INTO public.orbital_system VALUES (599, 168, NULL);
INSERT INTO public.orbital_system VALUES (600, 168, NULL);
INSERT INTO public.orbital_system VALUES (601, 169, NULL);
INSERT INTO public.orbital_system VALUES (602, 169, NULL);
INSERT INTO public.orbital_system VALUES (603, 169, NULL);
INSERT INTO public.orbital_system VALUES (604, 169, NULL);
INSERT INTO public.orbital_system VALUES (605, 170, NULL);
INSERT INTO public.orbital_system VALUES (606, 170, NULL);
INSERT INTO public.orbital_system VALUES (607, 170, NULL);
INSERT INTO public.orbital_system VALUES (608, 170, NULL);
INSERT INTO public.orbital_system VALUES (609, 170, NULL);
INSERT INTO public.orbital_system VALUES (612, 172, NULL);
INSERT INTO public.orbital_system VALUES (613, 173, NULL);
INSERT INTO public.orbital_system VALUES (614, 173, NULL);
INSERT INTO public.orbital_system VALUES (615, 173, NULL);
INSERT INTO public.orbital_system VALUES (616, 173, NULL);
INSERT INTO public.orbital_system VALUES (617, 173, NULL);
INSERT INTO public.orbital_system VALUES (618, 173, NULL);
INSERT INTO public.orbital_system VALUES (619, 174, NULL);
INSERT INTO public.orbital_system VALUES (620, 174, NULL);
INSERT INTO public.orbital_system VALUES (621, 174, NULL);
INSERT INTO public.orbital_system VALUES (622, 174, NULL);
INSERT INTO public.orbital_system VALUES (623, 174, NULL);
INSERT INTO public.orbital_system VALUES (624, 174, NULL);
INSERT INTO public.orbital_system VALUES (625, 175, NULL);
INSERT INTO public.orbital_system VALUES (626, 175, NULL);
INSERT INTO public.orbital_system VALUES (627, 175, NULL);
INSERT INTO public.orbital_system VALUES (628, 175, NULL);
INSERT INTO public.orbital_system VALUES (629, 176, NULL);
INSERT INTO public.orbital_system VALUES (630, 176, NULL);
INSERT INTO public.orbital_system VALUES (631, 176, NULL);
INSERT INTO public.orbital_system VALUES (632, 176, NULL);
INSERT INTO public.orbital_system VALUES (633, 176, NULL);
INSERT INTO public.orbital_system VALUES (634, 177, NULL);
INSERT INTO public.orbital_system VALUES (635, 177, NULL);
INSERT INTO public.orbital_system VALUES (636, 177, NULL);
INSERT INTO public.orbital_system VALUES (637, 177, NULL);
INSERT INTO public.orbital_system VALUES (638, 178, NULL);
INSERT INTO public.orbital_system VALUES (639, 178, NULL);
INSERT INTO public.orbital_system VALUES (640, 179, NULL);
INSERT INTO public.orbital_system VALUES (641, 179, NULL);
INSERT INTO public.orbital_system VALUES (642, 179, NULL);
INSERT INTO public.orbital_system VALUES (643, 180, NULL);
INSERT INTO public.orbital_system VALUES (644, 92, NULL);
INSERT INTO public.orbital_system VALUES (645, 92, NULL);
INSERT INTO public.orbital_system VALUES (646, 92, NULL);
INSERT INTO public.orbital_system VALUES (647, 92, NULL);
INSERT INTO public.orbital_system VALUES (648, 97, NULL);
INSERT INTO public.orbital_system VALUES (649, 97, NULL);
INSERT INTO public.orbital_system VALUES (650, 97, NULL);
INSERT INTO public.orbital_system VALUES (651, 97, NULL);
INSERT INTO public.orbital_system VALUES (652, 80, NULL);
INSERT INTO public.orbital_system VALUES (653, 80, NULL);
INSERT INTO public.orbital_system VALUES (654, 80, NULL);
INSERT INTO public.orbital_system VALUES (655, 80, NULL);
INSERT INTO public.orbital_system VALUES (656, 18, NULL);
INSERT INTO public.orbital_system VALUES (657, 18, NULL);
INSERT INTO public.orbital_system VALUES (658, 18, NULL);
INSERT INTO public.orbital_system VALUES (659, 18, NULL);
INSERT INTO public.orbital_system VALUES (660, 139, NULL);
INSERT INTO public.orbital_system VALUES (661, 139, NULL);
INSERT INTO public.orbital_system VALUES (662, 139, NULL);
INSERT INTO public.orbital_system VALUES (663, 139, NULL);
INSERT INTO public.orbital_system VALUES (664, 89, NULL);
INSERT INTO public.orbital_system VALUES (665, 89, NULL);
INSERT INTO public.orbital_system VALUES (666, 89, NULL);
INSERT INTO public.orbital_system VALUES (667, 89, NULL);
INSERT INTO public.orbital_system VALUES (668, 133, NULL);
INSERT INTO public.orbital_system VALUES (669, 133, NULL);
INSERT INTO public.orbital_system VALUES (670, 133, NULL);
INSERT INTO public.orbital_system VALUES (671, 133, NULL);
INSERT INTO public.orbital_system VALUES (672, 171, NULL);
INSERT INTO public.orbital_system VALUES (673, 171, NULL);
INSERT INTO public.orbital_system VALUES (674, 171, NULL);
INSERT INTO public.orbital_system VALUES (675, 171, NULL);
INSERT INTO public.orbital_system VALUES (676, 90, NULL);
INSERT INTO public.orbital_system VALUES (677, 90, NULL);
INSERT INTO public.orbital_system VALUES (678, 90, NULL);
INSERT INTO public.orbital_system VALUES (679, 90, NULL);
INSERT INTO public.orbital_system VALUES (700, 187, NULL);
INSERT INTO public.orbital_system VALUES (701, 187, NULL);
INSERT INTO public.orbital_system VALUES (702, 187, NULL);
INSERT INTO public.orbital_system VALUES (703, 187, NULL);
INSERT INTO public.orbital_system VALUES (704, 188, NULL);
INSERT INTO public.orbital_system VALUES (705, 188, NULL);
INSERT INTO public.orbital_system VALUES (706, 189, NULL);
INSERT INTO public.orbital_system VALUES (707, 189, NULL);
INSERT INTO public.orbital_system VALUES (708, 189, NULL);
INSERT INTO public.orbital_system VALUES (709, 189, NULL);
INSERT INTO public.orbital_system VALUES (710, 190, NULL);
INSERT INTO public.orbital_system VALUES (711, 190, NULL);
INSERT INTO public.orbital_system VALUES (712, 190, NULL);
INSERT INTO public.orbital_system VALUES (713, 191, NULL);
INSERT INTO public.orbital_system VALUES (714, 191, NULL);
INSERT INTO public.orbital_system VALUES (715, 191, NULL);
INSERT INTO public.orbital_system VALUES (716, 191, NULL);
INSERT INTO public.orbital_system VALUES (717, 192, NULL);
INSERT INTO public.orbital_system VALUES (718, 192, NULL);
INSERT INTO public.orbital_system VALUES (719, 192, NULL);
INSERT INTO public.orbital_system VALUES (720, 192, NULL);
INSERT INTO public.orbital_system VALUES (721, 193, NULL);
INSERT INTO public.orbital_system VALUES (722, 193, NULL);
INSERT INTO public.orbital_system VALUES (723, 193, NULL);
INSERT INTO public.orbital_system VALUES (724, 193, NULL);
INSERT INTO public.orbital_system VALUES (725, 193, NULL);
INSERT INTO public.orbital_system VALUES (726, 194, NULL);
INSERT INTO public.orbital_system VALUES (727, 194, NULL);
INSERT INTO public.orbital_system VALUES (728, 195, NULL);
INSERT INTO public.orbital_system VALUES (729, 195, NULL);
INSERT INTO public.orbital_system VALUES (730, 196, NULL);
INSERT INTO public.orbital_system VALUES (731, 196, NULL);
INSERT INTO public.orbital_system VALUES (732, 197, NULL);
INSERT INTO public.orbital_system VALUES (733, 198, NULL);
INSERT INTO public.orbital_system VALUES (734, 198, NULL);
INSERT INTO public.orbital_system VALUES (735, 198, NULL);
INSERT INTO public.orbital_system VALUES (736, 199, NULL);
INSERT INTO public.orbital_system VALUES (737, 199, NULL);
INSERT INTO public.orbital_system VALUES (738, 199, NULL);
INSERT INTO public.orbital_system VALUES (739, 200, NULL);
INSERT INTO public.orbital_system VALUES (740, 200, NULL);
INSERT INTO public.orbital_system VALUES (741, 200, NULL);
INSERT INTO public.orbital_system VALUES (742, 200, NULL);
INSERT INTO public.orbital_system VALUES (743, 200, NULL);
INSERT INTO public.orbital_system VALUES (744, 201, NULL);
INSERT INTO public.orbital_system VALUES (745, 201, NULL);
INSERT INTO public.orbital_system VALUES (746, 201, NULL);
INSERT INTO public.orbital_system VALUES (747, 201, NULL);
INSERT INTO public.orbital_system VALUES (748, 201, NULL);
INSERT INTO public.orbital_system VALUES (749, 202, NULL);
INSERT INTO public.orbital_system VALUES (750, 202, NULL);
INSERT INTO public.orbital_system VALUES (751, 203, NULL);
INSERT INTO public.orbital_system VALUES (752, 203, NULL);
INSERT INTO public.orbital_system VALUES (753, 203, NULL);
INSERT INTO public.orbital_system VALUES (754, 203, NULL);
INSERT INTO public.orbital_system VALUES (755, 204, NULL);
INSERT INTO public.orbital_system VALUES (756, 204, NULL);
INSERT INTO public.orbital_system VALUES (757, 204, NULL);
INSERT INTO public.orbital_system VALUES (758, 204, NULL);
INSERT INTO public.orbital_system VALUES (759, 204, NULL);
INSERT INTO public.orbital_system VALUES (760, 204, NULL);
INSERT INTO public.orbital_system VALUES (761, 205, NULL);
INSERT INTO public.orbital_system VALUES (762, 205, NULL);
INSERT INTO public.orbital_system VALUES (763, 205, NULL);
INSERT INTO public.orbital_system VALUES (764, 205, NULL);
INSERT INTO public.orbital_system VALUES (765, 205, NULL);
INSERT INTO public.orbital_system VALUES (766, 205, NULL);
INSERT INTO public.orbital_system VALUES (767, 206, NULL);
INSERT INTO public.orbital_system VALUES (768, 207, NULL);
INSERT INTO public.orbital_system VALUES (769, 207, NULL);
INSERT INTO public.orbital_system VALUES (770, 207, NULL);
INSERT INTO public.orbital_system VALUES (771, 207, NULL);
INSERT INTO public.orbital_system VALUES (772, 207, NULL);
INSERT INTO public.orbital_system VALUES (773, 208, NULL);
INSERT INTO public.orbital_system VALUES (774, 208, NULL);
INSERT INTO public.orbital_system VALUES (775, 208, NULL);
INSERT INTO public.orbital_system VALUES (776, 208, NULL);
INSERT INTO public.orbital_system VALUES (777, 209, NULL);
INSERT INTO public.orbital_system VALUES (778, 209, NULL);
INSERT INTO public.orbital_system VALUES (779, 209, NULL);
INSERT INTO public.orbital_system VALUES (780, 209, NULL);
INSERT INTO public.orbital_system VALUES (781, 210, NULL);
INSERT INTO public.orbital_system VALUES (782, 210, NULL);
INSERT INTO public.orbital_system VALUES (783, 210, NULL);
INSERT INTO public.orbital_system VALUES (784, 210, NULL);
INSERT INTO public.orbital_system VALUES (785, 210, NULL);
INSERT INTO public.orbital_system VALUES (786, 211, NULL);
INSERT INTO public.orbital_system VALUES (787, 212, NULL);
INSERT INTO public.orbital_system VALUES (788, 212, NULL);
INSERT INTO public.orbital_system VALUES (789, 213, NULL);
INSERT INTO public.orbital_system VALUES (790, 213, NULL);
INSERT INTO public.orbital_system VALUES (791, 213, NULL);
INSERT INTO public.orbital_system VALUES (792, 213, NULL);
INSERT INTO public.orbital_system VALUES (793, 214, NULL);
INSERT INTO public.orbital_system VALUES (794, 214, NULL);
INSERT INTO public.orbital_system VALUES (795, 214, NULL);
INSERT INTO public.orbital_system VALUES (796, 215, NULL);
INSERT INTO public.orbital_system VALUES (797, 216, NULL);
INSERT INTO public.orbital_system VALUES (798, 217, NULL);
INSERT INTO public.orbital_system VALUES (799, 217, NULL);
INSERT INTO public.orbital_system VALUES (800, 217, NULL);
INSERT INTO public.orbital_system VALUES (801, 217, NULL);
INSERT INTO public.orbital_system VALUES (802, 218, NULL);
INSERT INTO public.orbital_system VALUES (803, 218, NULL);
INSERT INTO public.orbital_system VALUES (804, 218, NULL);
INSERT INTO public.orbital_system VALUES (805, 219, NULL);
INSERT INTO public.orbital_system VALUES (806, 219, NULL);
INSERT INTO public.orbital_system VALUES (807, 219, NULL);
INSERT INTO public.orbital_system VALUES (808, 219, NULL);
INSERT INTO public.orbital_system VALUES (809, 219, NULL);
INSERT INTO public.orbital_system VALUES (810, 220, NULL);
INSERT INTO public.orbital_system VALUES (811, 220, NULL);
INSERT INTO public.orbital_system VALUES (812, 221, NULL);
INSERT INTO public.orbital_system VALUES (813, 221, NULL);
INSERT INTO public.orbital_system VALUES (814, 222, NULL);
INSERT INTO public.orbital_system VALUES (815, 222, NULL);
INSERT INTO public.orbital_system VALUES (816, 222, NULL);
INSERT INTO public.orbital_system VALUES (817, 222, NULL);
INSERT INTO public.orbital_system VALUES (818, 222, NULL);
INSERT INTO public.orbital_system VALUES (819, 223, NULL);
INSERT INTO public.orbital_system VALUES (820, 224, NULL);
INSERT INTO public.orbital_system VALUES (821, 224, NULL);
INSERT INTO public.orbital_system VALUES (822, 225, NULL);
INSERT INTO public.orbital_system VALUES (823, 225, NULL);
INSERT INTO public.orbital_system VALUES (824, 225, NULL);
INSERT INTO public.orbital_system VALUES (825, 225, NULL);
INSERT INTO public.orbital_system VALUES (826, 225, NULL);
INSERT INTO public.orbital_system VALUES (827, 225, NULL);
INSERT INTO public.orbital_system VALUES (828, 226, NULL);
INSERT INTO public.orbital_system VALUES (829, 226, NULL);
INSERT INTO public.orbital_system VALUES (830, 227, NULL);
INSERT INTO public.orbital_system VALUES (831, 227, NULL);
INSERT INTO public.orbital_system VALUES (832, 227, NULL);
INSERT INTO public.orbital_system VALUES (833, 227, NULL);
INSERT INTO public.orbital_system VALUES (834, 228, NULL);
INSERT INTO public.orbital_system VALUES (835, 229, NULL);
INSERT INTO public.orbital_system VALUES (836, 229, NULL);
INSERT INTO public.orbital_system VALUES (837, 229, NULL);
INSERT INTO public.orbital_system VALUES (838, 230, NULL);
INSERT INTO public.orbital_system VALUES (839, 230, NULL);
INSERT INTO public.orbital_system VALUES (840, 230, NULL);
INSERT INTO public.orbital_system VALUES (841, 230, NULL);
INSERT INTO public.orbital_system VALUES (842, 231, NULL);
INSERT INTO public.orbital_system VALUES (843, 231, NULL);
INSERT INTO public.orbital_system VALUES (844, 231, NULL);
INSERT INTO public.orbital_system VALUES (845, 231, NULL);
INSERT INTO public.orbital_system VALUES (846, 231, NULL);
INSERT INTO public.orbital_system VALUES (847, 231, NULL);
INSERT INTO public.orbital_system VALUES (848, 232, NULL);
INSERT INTO public.orbital_system VALUES (849, 232, NULL);
INSERT INTO public.orbital_system VALUES (850, 232, NULL);
INSERT INTO public.orbital_system VALUES (851, 232, NULL);
INSERT INTO public.orbital_system VALUES (852, 232, NULL);
INSERT INTO public.orbital_system VALUES (853, 233, NULL);
INSERT INTO public.orbital_system VALUES (854, 233, NULL);
INSERT INTO public.orbital_system VALUES (855, 233, NULL);
INSERT INTO public.orbital_system VALUES (856, 234, NULL);
INSERT INTO public.orbital_system VALUES (857, 234, NULL);
INSERT INTO public.orbital_system VALUES (858, 234, NULL);
INSERT INTO public.orbital_system VALUES (859, 234, NULL);
INSERT INTO public.orbital_system VALUES (860, 235, NULL);
INSERT INTO public.orbital_system VALUES (861, 235, NULL);
INSERT INTO public.orbital_system VALUES (862, 235, NULL);
INSERT INTO public.orbital_system VALUES (863, 236, NULL);
INSERT INTO public.orbital_system VALUES (864, 236, NULL);
INSERT INTO public.orbital_system VALUES (865, 236, NULL);
INSERT INTO public.orbital_system VALUES (866, 236, NULL);
INSERT INTO public.orbital_system VALUES (867, 236, NULL);
INSERT INTO public.orbital_system VALUES (868, 236, NULL);
INSERT INTO public.orbital_system VALUES (869, 237, NULL);
INSERT INTO public.orbital_system VALUES (870, 237, NULL);
INSERT INTO public.orbital_system VALUES (871, 237, NULL);
INSERT INTO public.orbital_system VALUES (872, 237, NULL);
INSERT INTO public.orbital_system VALUES (873, 237, NULL);
INSERT INTO public.orbital_system VALUES (874, 237, NULL);
INSERT INTO public.orbital_system VALUES (875, 238, NULL);
INSERT INTO public.orbital_system VALUES (876, 238, NULL);
INSERT INTO public.orbital_system VALUES (877, 238, NULL);
INSERT INTO public.orbital_system VALUES (878, 238, NULL);
INSERT INTO public.orbital_system VALUES (879, 238, NULL);
INSERT INTO public.orbital_system VALUES (880, 238, NULL);
INSERT INTO public.orbital_system VALUES (881, 239, NULL);
INSERT INTO public.orbital_system VALUES (882, 239, NULL);
INSERT INTO public.orbital_system VALUES (883, 240, NULL);
INSERT INTO public.orbital_system VALUES (884, 240, NULL);
INSERT INTO public.orbital_system VALUES (885, 240, NULL);
INSERT INTO public.orbital_system VALUES (886, 240, NULL);
INSERT INTO public.orbital_system VALUES (887, 241, NULL);
INSERT INTO public.orbital_system VALUES (888, 241, NULL);
INSERT INTO public.orbital_system VALUES (889, 242, NULL);
INSERT INTO public.orbital_system VALUES (890, 242, NULL);
INSERT INTO public.orbital_system VALUES (891, 243, NULL);
INSERT INTO public.orbital_system VALUES (892, 243, NULL);
INSERT INTO public.orbital_system VALUES (893, 243, NULL);
INSERT INTO public.orbital_system VALUES (894, 243, NULL);
INSERT INTO public.orbital_system VALUES (895, 243, NULL);
INSERT INTO public.orbital_system VALUES (896, 244, NULL);
INSERT INTO public.orbital_system VALUES (897, 245, NULL);
INSERT INTO public.orbital_system VALUES (898, 245, NULL);
INSERT INTO public.orbital_system VALUES (899, 245, NULL);
INSERT INTO public.orbital_system VALUES (900, 245, NULL);
INSERT INTO public.orbital_system VALUES (901, 245, NULL);
INSERT INTO public.orbital_system VALUES (902, 246, NULL);
INSERT INTO public.orbital_system VALUES (903, 246, NULL);
INSERT INTO public.orbital_system VALUES (904, 246, NULL);
INSERT INTO public.orbital_system VALUES (905, 246, NULL);
INSERT INTO public.orbital_system VALUES (906, 246, NULL);
INSERT INTO public.orbital_system VALUES (907, 247, NULL);
INSERT INTO public.orbital_system VALUES (908, 248, NULL);
INSERT INTO public.orbital_system VALUES (909, 249, NULL);
INSERT INTO public.orbital_system VALUES (910, 249, NULL);
INSERT INTO public.orbital_system VALUES (911, 250, NULL);
INSERT INTO public.orbital_system VALUES (912, 250, NULL);
INSERT INTO public.orbital_system VALUES (913, 250, NULL);
INSERT INTO public.orbital_system VALUES (914, 250, NULL);
INSERT INTO public.orbital_system VALUES (915, 250, NULL);
INSERT INTO public.orbital_system VALUES (916, 250, NULL);
INSERT INTO public.orbital_system VALUES (917, 251, NULL);
INSERT INTO public.orbital_system VALUES (918, 251, NULL);
INSERT INTO public.orbital_system VALUES (919, 252, NULL);
INSERT INTO public.orbital_system VALUES (920, 253, NULL);
INSERT INTO public.orbital_system VALUES (921, 253, NULL);
INSERT INTO public.orbital_system VALUES (922, 254, NULL);
INSERT INTO public.orbital_system VALUES (923, 254, NULL);
INSERT INTO public.orbital_system VALUES (924, 254, NULL);
INSERT INTO public.orbital_system VALUES (925, 254, NULL);
INSERT INTO public.orbital_system VALUES (926, 254, NULL);
INSERT INTO public.orbital_system VALUES (927, 254, NULL);
INSERT INTO public.orbital_system VALUES (928, 255, NULL);
INSERT INTO public.orbital_system VALUES (929, 256, NULL);
INSERT INTO public.orbital_system VALUES (930, 256, NULL);
INSERT INTO public.orbital_system VALUES (931, 256, NULL);
INSERT INTO public.orbital_system VALUES (932, 257, NULL);
INSERT INTO public.orbital_system VALUES (933, 257, NULL);
INSERT INTO public.orbital_system VALUES (934, 257, NULL);
INSERT INTO public.orbital_system VALUES (935, 258, NULL);
INSERT INTO public.orbital_system VALUES (936, 259, NULL);
INSERT INTO public.orbital_system VALUES (937, 259, NULL);
INSERT INTO public.orbital_system VALUES (938, 259, NULL);
INSERT INTO public.orbital_system VALUES (939, 259, NULL);
INSERT INTO public.orbital_system VALUES (940, 259, NULL);
INSERT INTO public.orbital_system VALUES (941, 260, NULL);
INSERT INTO public.orbital_system VALUES (942, 260, NULL);
INSERT INTO public.orbital_system VALUES (943, 260, NULL);
INSERT INTO public.orbital_system VALUES (944, 260, NULL);
INSERT INTO public.orbital_system VALUES (945, 260, NULL);
INSERT INTO public.orbital_system VALUES (946, 260, NULL);
INSERT INTO public.orbital_system VALUES (947, 261, NULL);
INSERT INTO public.orbital_system VALUES (948, 262, NULL);
INSERT INTO public.orbital_system VALUES (949, 262, NULL);
INSERT INTO public.orbital_system VALUES (950, 263, NULL);
INSERT INTO public.orbital_system VALUES (951, 263, NULL);
INSERT INTO public.orbital_system VALUES (952, 263, NULL);
INSERT INTO public.orbital_system VALUES (953, 263, NULL);
INSERT INTO public.orbital_system VALUES (954, 263, NULL);
INSERT INTO public.orbital_system VALUES (955, 263, NULL);
INSERT INTO public.orbital_system VALUES (956, 264, NULL);
INSERT INTO public.orbital_system VALUES (957, 264, NULL);
INSERT INTO public.orbital_system VALUES (958, 264, NULL);
INSERT INTO public.orbital_system VALUES (959, 265, NULL);
INSERT INTO public.orbital_system VALUES (960, 266, NULL);
INSERT INTO public.orbital_system VALUES (961, 266, NULL);
INSERT INTO public.orbital_system VALUES (962, 266, NULL);
INSERT INTO public.orbital_system VALUES (963, 266, NULL);
INSERT INTO public.orbital_system VALUES (964, 266, NULL);
INSERT INTO public.orbital_system VALUES (969, 268, NULL);
INSERT INTO public.orbital_system VALUES (970, 268, NULL);
INSERT INTO public.orbital_system VALUES (971, 269, NULL);
INSERT INTO public.orbital_system VALUES (972, 269, NULL);
INSERT INTO public.orbital_system VALUES (973, 269, NULL);
INSERT INTO public.orbital_system VALUES (974, 269, NULL);
INSERT INTO public.orbital_system VALUES (975, 269, NULL);
INSERT INTO public.orbital_system VALUES (976, 270, NULL);
INSERT INTO public.orbital_system VALUES (977, 270, NULL);
INSERT INTO public.orbital_system VALUES (978, 270, NULL);
INSERT INTO public.orbital_system VALUES (979, 270, NULL);
INSERT INTO public.orbital_system VALUES (980, 270, NULL);
INSERT INTO public.orbital_system VALUES (981, 270, NULL);
INSERT INTO public.orbital_system VALUES (982, 271, NULL);
INSERT INTO public.orbital_system VALUES (983, 271, NULL);
INSERT INTO public.orbital_system VALUES (984, 271, NULL);
INSERT INTO public.orbital_system VALUES (985, 271, NULL);
INSERT INTO public.orbital_system VALUES (986, 272, NULL);
INSERT INTO public.orbital_system VALUES (987, 272, NULL);
INSERT INTO public.orbital_system VALUES (988, 272, NULL);
INSERT INTO public.orbital_system VALUES (989, 272, NULL);
INSERT INTO public.orbital_system VALUES (990, 273, NULL);
INSERT INTO public.orbital_system VALUES (991, 273, NULL);
INSERT INTO public.orbital_system VALUES (992, 273, NULL);
INSERT INTO public.orbital_system VALUES (993, 273, NULL);
INSERT INTO public.orbital_system VALUES (994, 274, NULL);
INSERT INTO public.orbital_system VALUES (995, 274, NULL);
INSERT INTO public.orbital_system VALUES (996, 274, NULL);
INSERT INTO public.orbital_system VALUES (997, 274, NULL);
INSERT INTO public.orbital_system VALUES (998, 274, NULL);
INSERT INTO public.orbital_system VALUES (999, 274, NULL);
INSERT INTO public.orbital_system VALUES (1000, 267, NULL);
INSERT INTO public.orbital_system VALUES (1001, 267, NULL);
INSERT INTO public.orbital_system VALUES (1002, 267, NULL);
INSERT INTO public.orbital_system VALUES (1003, 267, NULL);


ALTER TABLE public.orbital_system ENABLE TRIGGER ALL;

--
-- TOC entry 3209 (class 0 OID 265402)
-- Dependencies: 230
-- Data for Name: player_planet_scan_log; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.player_planet_scan_log DISABLE TRIGGER ALL;

INSERT INTO public.player_planet_scan_log VALUES (1, 8, 52, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (2, 8, 102, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (3, 8, 83, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (4, 8, 171, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (5, 8, 41, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (6, 8, 82, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (7, 8, 16, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (8, 8, 176, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (9, 8, 174, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (10, 8, 88, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (11, 8, 1, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (12, 8, 133, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (13, 8, 107, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (14, 8, 60, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (15, 8, 2, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (16, 8, 25, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (17, 8, 111, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (18, 8, 84, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (19, 8, 54, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (20, 8, 143, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (21, 8, 156, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (22, 8, 65, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (23, 8, 38, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (24, 8, 58, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (25, 8, 135, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (26, 7, 83, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (27, 7, 171, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (28, 7, 41, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (29, 7, 82, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (30, 7, 16, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (31, 7, 142, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (32, 7, 10, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (33, 7, 176, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (34, 7, 174, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (35, 7, 88, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (36, 7, 1, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (37, 7, 133, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (38, 7, 99, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (39, 7, 34, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (40, 7, 60, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (41, 7, 2, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (42, 7, 25, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (43, 7, 148, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (44, 7, 54, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (45, 7, 143, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (46, 7, 134, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (47, 7, 156, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (48, 7, 65, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (49, 7, 78, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (50, 7, 38, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (51, 7, 89, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (52, 7, 158, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (53, 7, 135, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (54, 7, 32, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (55, 7, 47, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (56, 7, 144, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (57, 6, 82, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (58, 6, 16, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (59, 6, 142, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (60, 6, 176, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (61, 6, 1, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (62, 6, 133, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (63, 6, 99, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (64, 6, 34, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (65, 6, 60, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (66, 6, 148, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (67, 6, 54, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (68, 6, 143, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (69, 6, 94, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (70, 6, 134, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (71, 6, 81, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (72, 6, 156, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (73, 6, 65, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (74, 6, 78, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (75, 6, 38, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (76, 6, 89, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (77, 6, 158, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (78, 6, 135, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (79, 6, 61, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (80, 6, 80, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (81, 6, 32, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (82, 6, 47, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (83, 6, 144, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (84, 6, 76, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (85, 6, 163, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (86, 6, 66, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (87, 6, 74, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (88, 6, 157, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (89, 6, 114, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (90, 6, 36, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (91, 6, 91, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (92, 6, 103, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (93, 6, 179, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (94, 6, 154, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (95, 6, 39, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (96, 6, 63, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (97, 6, 62, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (98, 3, 161, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (99, 3, 99, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (100, 3, 34, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (101, 3, 26, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (102, 3, 115, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (103, 3, 148, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (104, 3, 54, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (105, 3, 94, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (106, 3, 134, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (107, 3, 81, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (108, 3, 141, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (109, 3, 72, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (110, 3, 78, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (111, 3, 89, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (112, 3, 172, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (113, 3, 158, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (114, 3, 61, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (115, 3, 80, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (116, 3, 119, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (117, 3, 47, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (118, 3, 28, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (119, 3, 163, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (120, 3, 131, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (121, 3, 66, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (122, 3, 74, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (123, 3, 114, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (124, 3, 36, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (125, 3, 146, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (126, 3, 73, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (127, 3, 167, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (128, 3, 91, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (129, 3, 103, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (130, 3, 179, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (131, 3, 63, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (132, 3, 55, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (133, 3, 136, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (134, 5, 2, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (135, 5, 25, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (136, 5, 111, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (137, 5, 84, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (138, 5, 147, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (139, 5, 65, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (140, 5, 87, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (141, 5, 38, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (142, 5, 118, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (143, 5, 58, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (144, 5, 31, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (145, 5, 135, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (146, 5, 32, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (147, 5, 120, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (148, 5, 128, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (149, 5, 53, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (150, 5, 76, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (151, 5, 157, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (152, 5, 139, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (153, 5, 17, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (154, 5, 45, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (155, 5, 150, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (156, 5, 101, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (157, 5, 154, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (158, 5, 108, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (159, 5, 18, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (160, 5, 165, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (161, 5, 121, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (162, 5, 21, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (163, 5, 104, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (164, 5, 151, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (165, 5, 59, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (166, 5, 169, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (167, 5, 71, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (168, 5, 105, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (169, 5, 159, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (170, 5, 37, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (171, 4, 147, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (172, 4, 87, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (173, 4, 118, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (174, 4, 31, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (175, 4, 120, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (176, 4, 53, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (177, 4, 139, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (178, 4, 17, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (179, 4, 150, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (180, 4, 101, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (181, 4, 108, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (182, 4, 18, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (183, 4, 165, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (184, 4, 21, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (185, 4, 151, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (186, 4, 59, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (187, 4, 169, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (188, 4, 105, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (189, 4, 159, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (190, 4, 6, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (191, 4, 37, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (192, 4, 95, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (193, 4, 126, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (194, 4, 155, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (195, 1, 47, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (196, 1, 144, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (197, 1, 76, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (198, 1, 157, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (199, 1, 114, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (200, 1, 45, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (201, 1, 36, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (202, 1, 103, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (203, 1, 179, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (204, 1, 154, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (205, 1, 39, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (206, 1, 121, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (207, 1, 63, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (208, 1, 62, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (209, 1, 21, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (210, 1, 104, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (211, 1, 149, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (212, 1, 100, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (213, 1, 92, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (214, 1, 50, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (215, 1, 71, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (216, 1, 173, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (217, 1, 105, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (218, 1, 129, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (219, 1, 24, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (220, 1, 29, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (221, 1, 13, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (222, 1, 35, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (223, 1, 98, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (224, 1, 170, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (225, 1, 42, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (226, 1, 46, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (227, 1, 178, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (228, 1, 166, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (229, 1, 33, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (230, 1, 68, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (231, 1, 44, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (232, 1, 180, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (233, 1, 127, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (234, 1, 152, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (235, 1, 130, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (236, 9, 129, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (237, 9, 29, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (238, 9, 13, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (239, 9, 35, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (240, 9, 98, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (241, 9, 46, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (242, 9, 11, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (243, 9, 178, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (244, 9, 166, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (245, 9, 33, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (246, 9, 68, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (247, 9, 44, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (248, 9, 153, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (249, 9, 109, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (250, 9, 180, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (251, 9, 127, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (252, 9, 152, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (253, 9, 51, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (254, 9, 130, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (255, 9, 49, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (256, 9, 90, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (257, 9, 125, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (258, 9, 117, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (259, 9, 160, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (260, 9, 75, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (261, 9, 64, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (262, 9, 138, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (263, 9, 4, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (264, 9, 9, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (265, 9, 162, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (266, 9, 106, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (267, 9, 15, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (268, 9, 85, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (269, 9, 14, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (270, 2, 109, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (271, 2, 56, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (272, 2, 49, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (273, 2, 117, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (274, 2, 22, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (275, 2, 12, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (276, 2, 138, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (277, 2, 43, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (278, 2, 140, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (279, 2, 9, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (280, 2, 112, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (281, 2, 97, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (282, 2, 93, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (283, 2, 162, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (295, 12, 200, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (296, 12, 198, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (297, 12, 248, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (298, 12, 205, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (299, 12, 249, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (300, 12, 216, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (301, 12, 201, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (302, 12, 210, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (303, 12, 217, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (304, 12, 225, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (305, 12, 199, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (306, 12, 221, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (307, 12, 264, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (308, 12, 271, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (309, 12, 269, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (310, 12, 246, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (311, 12, 206, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (312, 12, 191, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (313, 12, 260, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (314, 12, 187, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (315, 12, 267, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (316, 12, 197, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (317, 12, 207, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (318, 12, 265, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (319, 12, 224, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (320, 12, 188, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (321, 12, 195, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (322, 12, 241, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (323, 12, 212, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (324, 12, 251, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (325, 12, 243, NULL, false);


ALTER TABLE public.player_planet_scan_log ENABLE TRIGGER ALL;

--
-- TOC entry 3215 (class 0 OID 265457)
-- Dependencies: 236
-- Data for Name: player_relation; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.player_relation DISABLE TRIGGER ALL;

INSERT INTO public.player_relation VALUES (1, 4, 3, 'WAR', -1);
INSERT INTO public.player_relation VALUES (2, 5, 4, 'NON_AGGRESSION_TREATY', -1);
INSERT INTO public.player_relation VALUES (3, 6, 2, 'TRADE_AGREEMENT', -1);
INSERT INTO public.player_relation VALUES (4, 7, 5, 'TRADE_AGREEMENT', -1);
INSERT INTO public.player_relation VALUES (5, 9, 7, 'WAR', -1);


ALTER TABLE public.player_relation ENABLE TRIGGER ALL;

--
-- TOC entry 3197 (class 0 OID 265238)
-- Dependencies: 216
-- Data for Name: ship_template; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.ship_template DISABLE TRIGGER ALL;

INSERT INTO public.ship_template VALUES ('orion_1', 'orion', 1, '1.jpg', 2, 30, 200, 70, 1, 0, 0, 0, 10, 2, 2, 3);
INSERT INTO public.ship_template VALUES ('orion_5', 'orion', 2, '5.jpg', 6, 30, 80, 15, 1, 2, 0, 0, 30, 12, 17, 7);
INSERT INTO public.ship_template VALUES ('orion_10', 'orion', 3, '10.jpg', 70, 32, 180, 30, 2, 2, 0, 0, 190, 10, 20, 3);
INSERT INTO public.ship_template VALUES ('orion_15', 'orion', 3, '15.jpg', 190, 90, 180, 50, 1, 4, 2, 0, 70, 25, 50, 7);
INSERT INTO public.ship_template VALUES ('orion_7', 'orion', 3, '7.jpg', 2, 80, 40, 60, 1, 0, 4, 0, 55, 25, 10, 20);
INSERT INTO public.ship_template VALUES ('orion_2', 'orion', 3, '2.jpg', 6, 60, 250, 200, 1, 0, 0, 0, 65, 4, 4, 6);
INSERT INTO public.ship_template VALUES ('orion_16', 'orion', 4, '16.jpg', 78, 35, 110, 30, 2, 2, 2, 0, 30, 4, 3, 13);
INSERT INTO public.ship_template VALUES ('orion_6', 'orion', 4, '6.jpg', 286, 115, 450, 250, 2, 4, 2, 0, 150, 32, 37, 23);
INSERT INTO public.ship_template VALUES ('orion_17', 'orion', 5, '17.jpg', 79, 100, 140, 30, 2, 4, 4, 0, 170, 12, 23, 57);
INSERT INTO public.ship_template VALUES ('orion_14', 'orion', 5, '14.jpg', 162, 90, 140, 110, 2, 4, 0, 0, 100, 5, 45, 35);
INSERT INTO public.ship_template VALUES ('orion_18', 'orion', 6, '18.jpg', 430, 180, 470, 350, 2, 4, 0, 4, 390, 42, 61, 73);
INSERT INTO public.ship_template VALUES ('orion_3', 'orion', 6, '3.jpg', 102, 130, 600, 1200, 2, 0, 0, 0, 160, 85, 7, 8);
INSERT INTO public.ship_template VALUES ('orion_13', 'orion', 8, '13.jpg', 328, 210, 260, 170, 2, 7, 5, 0, 510, 120, 113, 105);
INSERT INTO public.ship_template VALUES ('orion_8', 'orion', 9, '8.jpg', 190, 712, 800, 1050, 10, 6, 0, 0, 970, 125, 150, 527);
INSERT INTO public.ship_template VALUES ('orion_12', 'orion', 9, '12.jpg', 1870, 820, 1400, 300, 6, 4, 0, 9, 990, 422, 184, 165);
INSERT INTO public.ship_template VALUES ('orion_4', 'orion', 10, '4.jpg', 202, 160, 1200, 2600, 4, 0, 0, 0, 220, 125, 13, 18);
INSERT INTO public.ship_template VALUES ('orion_11', 'orion', 10, '11.jpg', 1010, 650, 560, 320, 4, 10, 10, 0, 810, 240, 343, 350);
INSERT INTO public.ship_template VALUES ('orion_9', 'orion', 10, '9.jpg', 120, 920, 450, 2700, 10, 8, 0, 0, 840, 625, 250, 134);


ALTER TABLE public.ship_template ENABLE TRIGGER ALL;

--
-- TOC entry 3207 (class 0 OID 265351)
-- Dependencies: 228
-- Data for Name: ship; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.ship DISABLE TRIGGER ALL;

INSERT INTO public.ship VALUES (1, 'Debug ship', 301, 767, 3, 80, 'orion_1', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 301, 767, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL);
INSERT INTO public.ship VALUES (2, 'Debug ship', 469, 71, 4, 18, 'orion_1', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 469, 71, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL);
INSERT INTO public.ship VALUES (3, 'Debug ship', 268, 582, 6, 89, 'orion_1', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 268, 582, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL);
INSERT INTO public.ship VALUES (4, 'Debug ship', 379, 247, 5, 139, 'orion_1', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 379, 247, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL);
INSERT INTO public.ship VALUES (5, 'Debug ship', 950, 137, 2, 97, 'orion_1', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 950, 137, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL);
INSERT INTO public.ship VALUES (6, 'Debug ship', 117, 501, 7, 133, 'orion_1', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 117, 501, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL);
INSERT INTO public.ship VALUES (7, 'Debug ship', 807, 421, 9, 90, 'orion_1', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 807, 421, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL);
INSERT INTO public.ship VALUES (8, 'Debug ship', 563, 500, 1, 92, 'orion_1', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 563, 500, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL);
INSERT INTO public.ship VALUES (9, 'Debug ship', 50, 341, 8, 171, 'orion_1', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 341, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL);


ALTER TABLE public.ship ENABLE TRIGGER ALL;

--
-- TOC entry 3199 (class 0 OID 265261)
-- Dependencies: 220
-- Data for Name: starbase_hull_stock; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.starbase_hull_stock DISABLE TRIGGER ALL;



ALTER TABLE public.starbase_hull_stock ENABLE TRIGGER ALL;

--
-- TOC entry 3203 (class 0 OID 265297)
-- Dependencies: 224
-- Data for Name: starbase_propulsion_stock; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.starbase_propulsion_stock DISABLE TRIGGER ALL;



ALTER TABLE public.starbase_propulsion_stock ENABLE TRIGGER ALL;

--
-- TOC entry 3205 (class 0 OID 265315)
-- Dependencies: 226
-- Data for Name: starbase_ship_construction_job; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.starbase_ship_construction_job DISABLE TRIGGER ALL;



ALTER TABLE public.starbase_ship_construction_job ENABLE TRIGGER ALL;

--
-- TOC entry 3201 (class 0 OID 265279)
-- Dependencies: 222
-- Data for Name: starbase_weapon_stock; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.starbase_weapon_stock DISABLE TRIGGER ALL;



ALTER TABLE public.starbase_weapon_stock ENABLE TRIGGER ALL;

--
-- TOC entry 3221 (class 0 OID 0)
-- Dependencies: 205
-- Name: game_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.game_id_seq', 5, true);


--
-- TOC entry 3222 (class 0 OID 0)
-- Dependencies: 198
-- Name: login_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.login_id_seq', 4, true);


--
-- TOC entry 3223 (class 0 OID 0)
-- Dependencies: 200
-- Name: login_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.login_role_id_seq', 8, true);


--
-- TOC entry 3224 (class 0 OID 0)
-- Dependencies: 233
-- Name: news_entry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.news_entry_id_seq', 9, true);


--
-- TOC entry 3225 (class 0 OID 0)
-- Dependencies: 231
-- Name: orbital_system_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.orbital_system_id_seq', 1306, true);


--
-- TOC entry 3226 (class 0 OID 0)
-- Dependencies: 212
-- Name: planet_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.planet_id_seq', 361, true);


--
-- TOC entry 3227 (class 0 OID 0)
-- Dependencies: 208
-- Name: player_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.player_id_seq', 14, true);


--
-- TOC entry 3228 (class 0 OID 0)
-- Dependencies: 229
-- Name: player_planet_scan_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.player_planet_scan_log_id_seq', 342, true);


--
-- TOC entry 3229 (class 0 OID 0)
-- Dependencies: 235
-- Name: player_relation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.player_relation_id_seq', 5, true);


--
-- TOC entry 3230 (class 0 OID 0)
-- Dependencies: 227
-- Name: ship_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.ship_id_seq', 11, true);


--
-- TOC entry 3231 (class 0 OID 0)
-- Dependencies: 219
-- Name: starbase_hull_stock_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.starbase_hull_stock_id_seq', 2, true);


--
-- TOC entry 3232 (class 0 OID 0)
-- Dependencies: 210
-- Name: starbase_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.starbase_id_seq', 13, true);


--
-- TOC entry 3233 (class 0 OID 0)
-- Dependencies: 223
-- Name: starbase_propulsion_stock_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.starbase_propulsion_stock_id_seq', 2, true);


--
-- TOC entry 3234 (class 0 OID 0)
-- Dependencies: 225
-- Name: starbase_ship_construction_job_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.starbase_ship_construction_job_id_seq', 2, true);


--
-- TOC entry 3235 (class 0 OID 0)
-- Dependencies: 221
-- Name: starbase_weapon_stock_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.starbase_weapon_stock_id_seq', 1, true);


-- Completed on 2019-09-18 09:28:17 CEST

--
-- PostgreSQL database dump complete
--

