package org.skrupeltng.modules;

import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("themeService")
public class ThemeService {

	public static final String LIGHT_MODE = "light_mode";
	public static final String DARK_MODE = "dark_mode";

	@Autowired
	private UserDetailServiceImpl userService;

	@Autowired
	private LoginRepository loginRepository;

	public String getTheme() {
		if (userService.isLoggedIn()) {
			return loginRepository.getTheme(userService.getLoginId());
		}

		return LIGHT_MODE;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void toggleTheme() {
		Login login = loginRepository.getOne(userService.getLoginId());
		login.setTheme(login.getTheme().equals(LIGHT_MODE) ? DARK_MODE : LIGHT_MODE);
		loginRepository.save(login);
	}
}
