package org.skrupeltng.modules.ingame.modules.anomaly.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface PlasmaStormRepository extends JpaRepository<PlasmaStorm, Long> {

	List<PlasmaStorm> findByGameId(long gameId);

	@Query("SELECT " +
			"	COUNT(DISTINCT s.stormId) " +
			"FROM " +
			"	PlasmaStorm s " +
			"WHERE " +
			"	s.game.id = ?1")
	long getActiveStormCount(long gameId);

	@Query("SELECT " +
			"	COALESCE(MAX(COALESCE(s.stormId, 0))) " +
			"FROM " +
			"	PlasmaStorm s " +
			"WHERE " +
			"	s.game.id = ?1")
	Long getMaxStormId(long gameId);

	@Modifying
	@Query("DELETE FROM PlasmaStorm p WHERE p.game.id = ?1")
	void deleteByGameId(long id);
}