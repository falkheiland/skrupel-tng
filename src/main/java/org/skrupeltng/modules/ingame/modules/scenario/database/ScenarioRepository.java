package org.skrupeltng.modules.ingame.modules.scenario.database;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface ScenarioRepository extends JpaRepository<Scenario, Long> {

	@Query("SELECT s FROM Scenario s INNER JOIN s.player p WHERE p.login.id = ?1 AND s.type = ?2")
	Optional<Scenario> findByLoginIdAndType(long loginId, ScenarioType type);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Scenario s " +
			"WHERE " +
			"	s.game.id = ?1")
	Optional<Scenario> findScenarioByGameId(long gameId);

	@Query("SELECT " +
			"	s.stage " +
			"FROM " +
			"	Scenario s " +
			"WHERE " +
			"	s.game.id = ?1")
	String getStageByGameId(long gameId);

	@Query("SELECT s FROM Scenario s INNER JOIN s.player p WHERE p.login.id = ?1 AND s.finished = true")
	Optional<Scenario> getFinishedScenario(long loginId);

	@Query("SELECT s.finished FROM Scenario s WHERE s.game.id = ?1")
	boolean isFinished(long gameId);

	@Modifying
	@Query("DELETE FROM Scenario s WHERE s.game.id = ?1")
	void deleteByGameId(long gameId);

	@Query("SELECT g.scenario FROM Player p INNER JOIN p.game g WHERE p.id = ?1")
	boolean isScenario(long playerId);

	@Query("SELECT " +
			"	s.type " +
			"FROM " +
			"	Scenario s " +
			"WHERE " +
			"	s.game.id = ?1")
	ScenarioType getScenarioType(long gameId);
}