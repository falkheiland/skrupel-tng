package org.skrupeltng.modules.ingame.modules.scenario.service.generate;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ai.AIConstants;
import org.skrupeltng.modules.ingame.CoordinateDistanceComparator;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHole;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleType;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpeciesEffect;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLogRepository;
import org.skrupeltng.modules.ingame.modules.scenario.service.ScenarioPlanets;
import org.skrupeltng.modules.ingame.modules.scenario.service.ScenarioPlayers;
import org.skrupeltng.modules.ingame.modules.scenario.service.ScenarioShips;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplateRepository;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.WeaponTemplateRepository;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class LateGameScenario {

	@Autowired
	private ScenarioPlayers scenarioPlayers;

	@Autowired
	private ScenarioPlanets scenarioPlanets;

	@Autowired
	private ScenarioShips scenarioShips;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private OrbitalSystemRepository orbitalSystemRepository;

	@Autowired
	private PlayerPlanetScanLogRepository planetScanLogRepository;

	@Autowired
	private StarbaseRepository starbaseRepository;

	@Autowired
	private WeaponTemplateRepository weaponTemplateRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PropulsionSystemTemplateRepository propulsionSystemTemplateRepository;

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private WormHoleRepository wormHoleRepository;

	@Autowired
	private ConfigProperties configProperties;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void initPlayerPlanets(long gameId) {
		Pair<Player, Player> players = scenarioPlayers.getPlayers(gameId);
		Player player = players.getLeft();

		Planet playerHomePlanet = initHomePlanet(player);
		int x = playerHomePlanet.getX();
		int y = playerHomePlanet.getY();
		initPlanetsForPlayer(player, x, y, 200);

		List<Long> planetIds = planetRepository.getPlanetIdsInRadius(gameId, x, y, 250, false);

		List<Planet> planets = planetRepository.findByIds(planetIds);
		int pref = player.getFaction().getPreferredTemperature();

		Planet warbasePlanet = planets.stream().filter(p -> p.getDistance(playerHomePlanet) > 200)
				.min((a, b) -> Integer.compare(Math.abs(pref - a.getTemperature()), Math.abs(pref - b.getTemperature()))).get();

		Starbase warbase = new Starbase();
		warbase.setName("Warbase I");
		warbase.setPlanet(warbasePlanet);
		warbase.setType(StarbaseType.WAR_BASE);
		warbase.setHullLevel(6);
		warbase.setPropulsionLevel(7);
		warbase.setEnergyLevel(4);
		warbase.setProjectileLevel(3);

		warbase = starbaseRepository.save(warbase);
		warbasePlanet.setStarbase(warbase);
		warbasePlanet.setScanRadius(116);

		initPlanetsForPlayer(player, warbasePlanet.getX(), warbasePlanet.getY(), 250);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void relocateAiHomePlanet(long gameId) {
		Pair<Player, Player> players = scenarioPlayers.getPlayers(gameId);
		Player player = players.getLeft();
		Player ai = players.getRight();

		Set<Long> allPlanetIds = player.getGame().getPlanets().stream().map(Planet::getId).collect(Collectors.toSet());

		Planet farthestPlanet = scenarioPlanets.getFarthestPlanet(player.getHomePlanet().getX(), player.getHomePlanet().getY(), allPlanetIds);
		Planet aiHomePlanet = ai.getHomePlanet();

		if (farthestPlanet.getId() != aiHomePlanet.getId()) {
			int x = farthestPlanet.getX();
			int y = farthestPlanet.getY();

			orbitalSystemRepository.deleteByPlanetId(farthestPlanet.getId());
			planetScanLogRepository.deleteByPlanetId(farthestPlanet.getId());
			planetRepository.delete(farthestPlanet);

			aiHomePlanet.setX(x);
			aiHomePlanet.setY(y + 1);
			planetRepository.save(aiHomePlanet);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void initAiPlanets(long gameId) {
		Pair<Player, Player> players = scenarioPlayers.getPlayers(gameId);
		Player ai = players.getRight();

		Planet aiHomePlanet = initHomePlanet(ai);
		initPlanetsForPlayer(ai, aiHomePlanet.getX(), aiHomePlanet.getY(), 200);
	}

	private Planet initHomePlanet(Player player) {
		Planet homePlanet = player.getHomePlanet();
		homePlanet.setUntappedFuel(100 + MasterDataService.RANDOM.nextInt(50));
		homePlanet.setUntappedMineral1(100 + MasterDataService.RANDOM.nextInt(50));
		homePlanet.setUntappedMineral2(100 + MasterDataService.RANDOM.nextInt(50));
		homePlanet.setUntappedMineral3(100 + MasterDataService.RANDOM.nextInt(50));

		homePlanet.setColonists(80000 + MasterDataService.RANDOM.nextInt(2000));
		homePlanet.setMoney(10000 + MasterDataService.RANDOM.nextInt(1000));
		homePlanet.setSupplies(900 + MasterDataService.RANDOM.nextInt(100));
		homePlanet.setFuel(800 + MasterDataService.RANDOM.nextInt(150));
		homePlanet.setMineral1(800 + MasterDataService.RANDOM.nextInt(150));
		homePlanet.setMineral2(800 + MasterDataService.RANDOM.nextInt(150));
		homePlanet.setMineral3(800 + MasterDataService.RANDOM.nextInt(150));

		homePlanet.setMines(homePlanet.retrieveMaxMines());
		homePlanet.setAutoBuildMines(true);

		homePlanet.setFactories(homePlanet.retrieveMaxFactories());
		homePlanet.setAutoBuildFactories(true);

		homePlanet.setPlanetaryDefense(homePlanet.retrieveMaxPlanetaryDefense());
		homePlanet.setAutoBuildPlanetaryDefense(true);

		homePlanet = planetRepository.save(homePlanet);

		Starbase starbase = homePlanet.getStarbase();
		starbase.setHullLevel(10);
		starbase.setPropulsionLevel(9);
		starbase.setEnergyLevel(8);
		starbase.setProjectileLevel(7);
		starbaseRepository.save(starbase);

		return homePlanet;
	}

	protected void initPlanetsForPlayer(Player player, int x, int y, int radius) {
		long gameId = player.getGame().getId();

		List<Long> planetIdsInRadius = planetRepository.getPlanetIdsInRadius(gameId, x, y, radius, false);

		for (Long planetId : planetIdsInRadius) {
			Planet planet = planetRepository.getOne(planetId);

			if (planet.getPlayer() == null) {
				planet.setPlayer(player);
				planet.setColonists(1500 + MasterDataService.RANDOM.nextInt(1500));
				planet.setMoney(200 + MasterDataService.RANDOM.nextInt(600));
				planet.setSupplies(50 + MasterDataService.RANDOM.nextInt(90));

				if (planet.retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.ATTACK).isPresent()) {
					planet.setNativeSpecies(null);
					planet.setNativeSpeciesCount(0);
				}

				planet.setMines(planet.retrieveMaxMines());
				planet.setAutoBuildMines(true);

				planet.setFactories(planet.retrieveMaxFactories());
				planet.setAutoBuildFactories(true);

				planet.setPlanetaryDefense(planet.retrieveMaxPlanetaryDefense());
				planet.setAutoBuildPlanetaryDefense(true);

				planetRepository.save(planet);
			}
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void initRoutesForPlayerHomePlanet(long gameId) {
		Pair<Player, Player> players = scenarioPlayers.getPlayers(gameId);
		Player player = players.getLeft();

		scenarioShips.initRoutes(player.getHomePlanet(), 200, "silverstarag_10");
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void initRoutesForPlayerWarbasePlanet(long gameId) {
		Pair<Player, Player> players = scenarioPlayers.getPlayers(gameId);
		Player player = players.getLeft();

		Planet warbasePlanet = starbaseRepository.findByPlayerId(player.getId()).stream().filter(s -> s.getType() == StarbaseType.WAR_BASE).findAny().get()
				.getPlanet();

		scenarioShips.initRoutes(warbasePlanet, 200, "silverstarag_10");
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void initRoutesForAI(long gameId) {
		Pair<Player, Player> players = scenarioPlayers.getPlayers(gameId);
		Player ai = players.getRight();

		scenarioShips.initRoutes(ai.getHomePlanet(), 200, "nightmare_13");
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void initWormhole(long gameId) {
		Pair<Player, Player> players = scenarioPlayers.getPlayers(gameId);
		Player player = players.getLeft();
		Player ai = players.getRight();

		List<Planet> starbasePlanets = player.getPlanets().stream().filter(p -> p.getStarbase() != null).collect(Collectors.toList());

		Planet starbasePlanet1 = starbasePlanets.get(0);
		Planet starbasePlanet2 = starbasePlanets.get(1);
		int playerWormholeX = (starbasePlanet1.getX() + starbasePlanet2.getX()) / 2;
		int playerWormholeY = (starbasePlanet1.getY() + starbasePlanet2.getY()) / 2;

		int radius = (int)(starbasePlanet1.getDistance(starbasePlanet2) * 0.25);

		WormHole wormhole1 = addWormhole(gameId, playerWormholeX, playerWormholeY, radius);

		Planet aiPlanet = ai.getHomePlanet();
		Planet farAIPlanet = scenarioPlanets.getFarthestPlanet(gameId, aiPlanet.getX(), aiPlanet.getY(), 200);

		Vector2D aiPlanetVector = new Vector2D(aiPlanet.getX(), aiPlanet.getY());
		Vector2D farAIPlanetVector = new Vector2D(farAIPlanet.getX(), farAIPlanet.getY());
		Vector2D direction = farAIPlanetVector.subtract(aiPlanetVector);
		Vector2D aiWormholeLocation = aiPlanetVector.add(direction.normalize().scalarMultiply(Vector2D.distance(aiPlanetVector, farAIPlanetVector) * 1.1));

		WormHole wormhole2 = addWormhole(gameId, (int)aiWormholeLocation.getX(), (int)aiWormholeLocation.getY(), 50);
		wormhole2.setConnection(wormhole1);
		wormhole2 = wormHoleRepository.save(wormhole2);

		wormhole1.setConnection(wormhole2);
		wormHoleRepository.save(wormhole1);
	}

	private WormHole addWormhole(long gameId, int x, int y, int radius) {
		WormHole wormhole = new WormHole();
		wormhole.setGame(gameRepository.getOne(gameId));
		wormhole.setType(WormHoleType.STABLE_WORMHOLE);

		int minAnomalyDistance = configProperties.getMinAnomalyDistance();
		int diameter = radius * 2;
		int min = configProperties.getMinGalaxyMapBorder();
		int max = wormhole.getGame().getGalaxySize() - min;

		while (true) {
			int wormHoleX = x - radius + MasterDataService.RANDOM.nextInt(diameter);
			int wormHoleY = y - radius + MasterDataService.RANDOM.nextInt(diameter);

			if (wormHoleX < min) {
				wormHoleY = min;
			} else if (wormHoleX > max) {
				wormHoleX = max;
			}

			if (wormHoleY < min) {
				wormHoleY = min;
			} else if (wormHoleY > max) {
				wormHoleY = max;
			}

			List<Long> planets = planetRepository.getPlanetIdsInRadius(gameId, wormHoleX, wormHoleY, minAnomalyDistance, false);

			if (planets.isEmpty()) {
				wormhole.setX(wormHoleX);
				wormhole.setY(wormHoleY);
				break;
			}
		}

		return wormHoleRepository.save(wormhole);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void initShips(long gameId) {
		Pair<Player, Player> players = scenarioPlayers.getPlayers(gameId);
		initPlayerShips(players.getLeft(), players.getRight());
		initAIShips(players.getRight());
	}

	private void initPlayerShips(Player player, Player ai) {
		initPlayerScout(player, ai);
		initPlayerClusterShip(player);
		initPlayerAttackShips(player);
		initPlayerScannerShip(player, ai);
	}

	private void initPlayerScout(Player player, Player ai) {
		Planet aiHomePlanet = ai.getHomePlanet();

		Planet farAIPlanet = scenarioPlanets.getFarthestPlanet(aiHomePlanet.getX(), aiHomePlanet.getY(),
				ai.getPlanets().stream().map(Planet::getId).collect(Collectors.toList()));

		Ship scout = scenarioShips.createShip("silverstarag_2", "Scout", player, farAIPlanet.getX() + 20, farAIPlanet.getY() + 25);
		scout.setPropulsionSystemTemplate(propulsionSystemTemplateRepository.getOne("solarisplasmotan"));
		scout.setEnergyWeaponTemplate(weaponTemplateRepository.getOne("laser"));
		scout.setFuel(34);
		shipRepository.save(scout);
	}

	private void initPlayerClusterShip(Player player) {
		Planet homePlanet = player.getHomePlanet();
		Ship cluster = scenarioShips.createShip("silverstarag_18", "Subparticlecluster1", player, homePlanet.getX(), homePlanet.getY());
		cluster.setPropulsionSystemTemplate(propulsionSystemTemplateRepository.getOne("sun_sail"));
		cluster.setEnergyWeaponTemplate(weaponTemplateRepository.getOne("laser"));
		cluster.setProjectileWeaponTemplate(weaponTemplateRepository.getOne("fusion_rockets"));
		cluster.setTaskType(ShipTaskType.ACTIVE_ABILITY);
		cluster.setActiveAbility(cluster.getAbility(ShipAbilityType.SUB_PARTICLE_CLUSTER).get());
		cluster.setTaskValue("true");
		cluster.setPlanet(homePlanet);
		shipRepository.save(cluster);
	}

	private void initPlayerAttackShips(Player player) {
		Planet homePlanet = player.getHomePlanet();

		Ship birdWing = scenarioShips.createShip("silverstarag_11", "Birdwing1", player, homePlanet.getX(), homePlanet.getY());
		birdWing.setPropulsionSystemTemplate(propulsionSystemTemplateRepository.getOne("psion_flow_net"));
		birdWing.setEnergyWeaponTemplate(weaponTemplateRepository.getOne("desintegrator"));
		birdWing.setProjectileWeaponTemplate(weaponTemplateRepository.getOne("gamma_bombs"));
		birdWing.setPlanet(homePlanet);
		shipRepository.save(birdWing);

		Ship canonBoat = scenarioShips.createShip("silverstarag_12", "Canonboat1", player, homePlanet.getX(), homePlanet.getY());
		canonBoat.setPropulsionSystemTemplate(propulsionSystemTemplateRepository.getOne("psion_flow_net"));
		canonBoat.setEnergyWeaponTemplate(weaponTemplateRepository.getOne("gravitractor"));
		canonBoat.setProjectileWeaponTemplate(weaponTemplateRepository.getOne("fission_torpedos"));
		canonBoat.setPlanet(homePlanet);
		shipRepository.save(canonBoat);

		Ship liberator = scenarioShips.createShip("silverstarag_13", "Liberator1", player, homePlanet.getX(), homePlanet.getY());
		liberator.setPropulsionSystemTemplate(propulsionSystemTemplateRepository.getOne("psion_flow_net"));
		liberator.setEnergyWeaponTemplate(weaponTemplateRepository.getOne("potential_compressor"));
		liberator.setProjectileWeaponTemplate(weaponTemplateRepository.getOne("mun_catapult"));
		liberator.setPlanet(homePlanet);
		shipRepository.save(liberator);
	}

	private void initPlayerScannerShip(Player player, Player ai) {
		Planet homePlanet = player.getHomePlanet();

		Planet nearestAiPlanetToPlayer = ai.getPlanets().stream().min(new CoordinateDistanceComparator(homePlanet)).get();
		Planet nearestPlayerPlanetToAI = player.getPlanets().stream().min(new CoordinateDistanceComparator(nearestAiPlanetToPlayer)).get();

		Ship scanner = scenarioShips.createShip("silverstarag_17", "Scanner1", player, nearestPlayerPlanetToAI.getX(), nearestPlayerPlanetToAI.getY());
		scanner.setPropulsionSystemTemplate(propulsionSystemTemplateRepository.getOne("psion_flow_net"));
		scanner.setEnergyWeaponTemplate(weaponTemplateRepository.getOne("tachion_beam"));
		scanner.setProjectileWeaponTemplate(weaponTemplateRepository.getOne("transform_canon"));
		scanner.setTaskType(ShipTaskType.ACTIVE_ABILITY);
		scanner.setActiveAbility(scanner.getAbility(ShipAbilityType.ASTRO_PHYSICS_LAB).get());
		scanner.setScanRadius(116);
		scanner.setPlanet(nearestPlayerPlanetToAI);
		scanner.setFuel(34);
		shipRepository.save(scanner);
	}

	private void initAIShips(Player ai) {
		initAIClusterShip(ai);
		initAIScout(ai);
		initAIAttackShips(ai);
	}

	private void initAIClusterShip(Player ai) {
		Planet homePlanet = ai.getHomePlanet();
		Ship cluster = scenarioShips.createShip("nightmare_18", AIConstants.AI_SUBPARTICLECLUSTER_NAME, ai, homePlanet.getX(), homePlanet.getY());
		cluster.setPropulsionSystemTemplate(propulsionSystemTemplateRepository.getOne("sun_sail"));
		cluster.setEnergyWeaponTemplate(weaponTemplateRepository.getOne("laser"));
		cluster.setTaskType(ShipTaskType.ACTIVE_ABILITY);
		cluster.setActiveAbility(cluster.getAbility(ShipAbilityType.SUB_PARTICLE_CLUSTER).get());
		cluster.setTaskValue("true");
		cluster.setPlanet(homePlanet);
		shipRepository.save(cluster);
	}

	private void initAIScout(Player ai) {
		Planet homePlanet = ai.getHomePlanet();

		Planet farPlanet = scenarioPlanets.getFarthestPlanet(homePlanet.getX(), homePlanet.getY(),
				ai.getPlanets().stream().map(Planet::getId).collect(Collectors.toList()));

		Ship scout = scenarioShips.createShip("nightmare_2", AIConstants.AI_SCOUT_NAME, ai, farPlanet.getX(), farPlanet.getY());
		scout.setPropulsionSystemTemplate(propulsionSystemTemplateRepository.getOne("solarisplasmotan"));
		scout.setEnergyWeaponTemplate(weaponTemplateRepository.getOne("laser"));
		scout.setFuel(54);
		shipRepository.save(scout);
	}

	private void initAIAttackShips(Player ai) {
		Planet homePlanet = ai.getHomePlanet();

		Ship ambusher = scenarioShips.createShip("nightmare_6", AIConstants.AI_AMBUSHER_NAME, ai, homePlanet.getX(), homePlanet.getY());
		ambusher.setPropulsionSystemTemplate(propulsionSystemTemplateRepository.getOne("psion_flow_net"));
		ambusher.setEnergyWeaponTemplate(weaponTemplateRepository.getOne("desintegrator"));
		ambusher.setProjectileWeaponTemplate(weaponTemplateRepository.getOne("gamma_bombs"));
		ambusher.setPlanet(homePlanet);
		ambusher.setFuel(ambusher.getShipTemplate().getFuelCapacity());
		shipRepository.save(ambusher);

		Ship bomber = scenarioShips.createShip("nightmare_9", AIConstants.AI_BOMBER_NAME, ai, homePlanet.getX(), homePlanet.getY());
		bomber.setPropulsionSystemTemplate(propulsionSystemTemplateRepository.getOne("psion_flow_net"));
		bomber.setEnergyWeaponTemplate(weaponTemplateRepository.getOne("desintegrator"));
		bomber.setProjectileWeaponTemplate(weaponTemplateRepository.getOne("gamma_bombs"));
		bomber.setPlanet(homePlanet);
		bomber.setFuel(bomber.getShipTemplate().getFuelCapacity());
		shipRepository.save(bomber);
	}
}