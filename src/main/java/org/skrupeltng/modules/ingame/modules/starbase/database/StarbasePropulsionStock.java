package org.skrupeltng.modules.ingame.modules.starbase.database;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;

@Entity
@Table(name = "starbase_propulsion_stock")
public class StarbasePropulsionStock implements Serializable {

	private static final long serialVersionUID = -6389567837825383002L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Starbase starbase;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "propulsion_system_template_name")
	private PropulsionSystemTemplate propulsionSystemTemplate;

	private int stock;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Starbase getStarbase() {
		return starbase;
	}

	public void setStarbase(Starbase starbase) {
		this.starbase = starbase;
	}

	public PropulsionSystemTemplate getPropulsionSystemTemplate() {
		return propulsionSystemTemplate;
	}

	public void setPropulsionSystemTemplate(PropulsionSystemTemplate propulsionSystemTemplate) {
		this.propulsionSystemTemplate = propulsionSystemTemplate;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}
}