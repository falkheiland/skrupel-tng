package org.skrupeltng.modules.ingame.modules.politics.database;

public enum PlayerRelationAction {

	DELCARE_WAR,

	OFFER_PEACE,

	OFFER_TRADE_AGREEMENT,

	OFFER_NON_AGGRESSION_TREATY,

	OFFER_ALLIANCE,

	CANCEL_TRADE_AGREEMENT,

	CANCEL_NON_AGGRESSION_TREATY,

	CANCEL_ALLIANCE
}