package org.skrupeltng.modules.ingame.modules.scenario.service.generate;

import org.skrupeltng.modules.dashboard.NewGameRequest;
import org.skrupeltng.modules.ingame.database.LoseCondition;
import org.skrupeltng.modules.ingame.modules.scenario.database.MidGameScenarioStage;
import org.skrupeltng.modules.ingame.modules.scenario.database.ScenarioType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component("GENERATOR_MID_GAME")
public class MidGameScenarioGenerator extends AbstractScenarioGenerator {

	@Autowired
	private MidGameScenario midGameScenario;

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public long generateScenario(long loginId) {
		long gameId = super.generateScenario(loginId);

		midGameScenario.initPlanets(gameId);
		midGameScenario.initShips(gameId);

		return gameId;
	}

	@Override
	protected NewGameRequest createNewGameRequest() {
		NewGameRequest request = super.createNewGameRequest();
		request.setLoseCondition(LoseCondition.LOSE_PLANETS.name());
		return request;
	}

	@Override
	protected String getInitStage() {
		return MidGameScenarioStage.MID_GAME_WELCOME.name();
	}

	@Override
	protected ScenarioType getType() {
		return ScenarioType.MID_GAME;
	}

	@Override
	public String getPlayerFactionName() {
		return "nightmare";
	}

	@Override
	protected String getBotPlayerFaction() {
		return "silverstarag";
	}
}