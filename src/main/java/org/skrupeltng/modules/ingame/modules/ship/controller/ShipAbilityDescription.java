package org.skrupeltng.modules.ingame.modules.ship.controller;

public class ShipAbilityDescription {

	private long abilityId;
	private String name;
	private String description;

	public ShipAbilityDescription(long abilityId, String name, String description) {
		this.abilityId = abilityId;
		this.name = name;
		this.description = description;
	}

	public long getAbilityId() {
		return abilityId;
	}

	public void setAbilityId(long abilityId) {
		this.abilityId = abilityId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}