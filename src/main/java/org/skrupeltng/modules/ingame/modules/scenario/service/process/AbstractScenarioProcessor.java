package org.skrupeltng.modules.ingame.modules.scenario.service.process;

import org.skrupeltng.modules.dashboard.database.Achievement;
import org.skrupeltng.modules.dashboard.database.AchievementRepository;
import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.scenario.database.Scenario;
import org.skrupeltng.modules.ingame.modules.scenario.database.ScenarioRepository;
import org.skrupeltng.modules.ingame.modules.scenario.database.ScenarioStage;
import org.skrupeltng.modules.ingame.modules.scenario.service.ScenarioPlanets;
import org.skrupeltng.modules.ingame.modules.scenario.service.ScenarioPlayers;
import org.skrupeltng.modules.ingame.modules.scenario.service.ScenarioShips;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplateRepository;
import org.skrupeltng.modules.masterdata.database.WeaponTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractScenarioProcessor {

	@Autowired
	protected ScenarioRepository scenarioRepository;

	@Autowired
	protected AchievementRepository achievementRepository;

	@Autowired
	protected ShipRepository shipRepository;

	@Autowired
	protected PropulsionSystemTemplateRepository propulsionSystemTemplateRepository;

	@Autowired
	protected WeaponTemplateRepository weaponTemplateRepository;

	@Autowired
	protected ScenarioPlayers scenarioPlayers;

	@Autowired
	protected ScenarioPlanets scenarioPlanets;

	@Autowired
	protected ScenarioShips scenarioShips;

	@Autowired
	protected PlanetRepository planetRepository;

	public abstract void process(Scenario scenario);

	public abstract AchievementType getAchievementType();

	protected Player getPlayer(Scenario scenario) {
		return scenarioPlayers.getPlayers(scenario.getGame().getId()).getLeft();
	}

	protected void changeStage(Scenario scenario, ScenarioStage stage) {
		scenario.setStage(stage.toString());
		scenario.setNewStage(true);
		scenarioRepository.save(scenario);
	}

	protected void finishScenario(Scenario scenario) {
		Login login = scenario.getPlayer().getLogin();
		AchievementType type = getAchievementType();

		if (achievementRepository.findByLoginIdAndType(login.getId(), type).isEmpty()) {
			Achievement achievement = new Achievement(login, type);
			achievementRepository.save(achievement);
		}

		scenario.setFinished(true);
		scenarioRepository.save(scenario);
	}
}