package org.skrupeltng.modules.ingame.modules.scenario.database;

public enum EarlyGameScenarioStage implements ScenarioStage {

	EARLY_GAME_WELCOME,

	EARLY_GAME_SHIP_BUILT,

	EARLY_GAME_SHIP_ARRIVED,

	EARLY_GAME_NEW_COLONY,

	EARLY_GAME_THREE_COLONIES,

	EARLY_GAME_ENEMY_SCOUT
}