package org.skrupeltng.modules.ingame.modules.overview.service;

import java.io.Serializable;
import java.time.Instant;

public class RoundSummary implements Serializable {

	private static final long serialVersionUID = -3464234632954604032L;

	private int round;
	private Instant date;
	private String winCondition;
	private int newColonies;
	private int newShips;
	private int newStarbases;
	private int destroyedShips;
	private int lostShips;

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public Instant getDate() {
		return date;
	}

	public void setDate(Instant date) {
		this.date = date;
	}

	public String getWinCondition() {
		return winCondition;
	}

	public void setWinCondition(String winCondition) {
		this.winCondition = winCondition;
	}

	public int getNewColonies() {
		return newColonies;
	}

	public void setNewColonies(int newColonies) {
		this.newColonies = newColonies;
	}

	public int getNewShips() {
		return newShips;
	}

	public void setNewShips(int newShips) {
		this.newShips = newShips;
	}

	public int getNewStarbases() {
		return newStarbases;
	}

	public void setNewStarbases(int newStarbases) {
		this.newStarbases = newStarbases;
	}

	public int getDestroyedShips() {
		return destroyedShips;
	}

	public void setDestroyedShips(int destroyedShips) {
		this.destroyedShips = destroyedShips;
	}

	public int getLostShips() {
		return lostShips;
	}

	public void setLostShips(int lostShips) {
		this.lostShips = lostShips;
	}
}