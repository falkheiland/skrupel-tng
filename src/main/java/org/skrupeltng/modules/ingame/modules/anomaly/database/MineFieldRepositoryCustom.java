package org.skrupeltng.modules.ingame.modules.anomaly.database;

import java.util.List;

import org.skrupeltng.modules.ingame.controller.MineFieldEntry;

public interface MineFieldRepositoryCustom {

	List<MineFieldEntry> getMineFieldEntries(long gameId, long currentPlayerId);

	List<Long> getMineFieldIdsInRadius(long gameId, int x, int y, int distance);
}