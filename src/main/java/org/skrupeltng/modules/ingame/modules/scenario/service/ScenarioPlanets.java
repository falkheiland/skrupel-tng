package org.skrupeltng.modules.ingame.modules.scenario.service;

import java.util.Collection;
import java.util.List;

import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ScenarioPlanets {

	@Autowired
	private PlanetRepository planetRepository;

	public Planet getFarthestPlanet(long gameId, int x, int y, int radius) {
		List<Long> planetIdsInRadius = planetRepository.getPlanetIdsInRadius(gameId, x, y, radius, false);
		return getFarthestPlanet(x, y, planetIdsInRadius);
	}

	public Planet getFarthestPlanet(int x, int y, Collection<Long> planetIdsInRadius) {
		double maxDist = 0;
		Planet maxDistPlanet = null;
		CoordinateImpl center = new CoordinateImpl(x, y, 0);

		for (Long planetId : planetIdsInRadius) {
			Planet planet = planetRepository.getOne(planetId);
			double dist = center.getDistance(planet);

			if (dist > maxDist) {
				maxDist = dist;
				maxDistPlanet = planet;
			}
		}

		return maxDistPlanet;
	}
}
