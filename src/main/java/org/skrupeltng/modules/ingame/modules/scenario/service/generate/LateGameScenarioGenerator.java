package org.skrupeltng.modules.ingame.modules.scenario.service.generate;

import org.skrupeltng.modules.dashboard.NewGameRequest;
import org.skrupeltng.modules.ingame.modules.scenario.database.LateGameScenarioStage;
import org.skrupeltng.modules.ingame.modules.scenario.database.ScenarioType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component("GENERATOR_LATE_GAME")
public class LateGameScenarioGenerator extends AbstractScenarioGenerator {

	@Autowired
	private LateGameScenario lateGameScenario;

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public long generateScenario(long loginId) {
		long gameId = super.generateScenario(loginId);

		lateGameScenario.initPlayerPlanets(gameId);
		lateGameScenario.relocateAiHomePlanet(gameId);
		lateGameScenario.initAiPlanets(gameId);
		lateGameScenario.initRoutesForPlayerHomePlanet(gameId);
		lateGameScenario.initRoutesForPlayerWarbasePlanet(gameId);
		lateGameScenario.initRoutesForAI(gameId);
		lateGameScenario.initWormhole(gameId);
		lateGameScenario.initShips(gameId);

		return gameId;
	}

	@Override
	protected NewGameRequest createNewGameRequest() {
		NewGameRequest request = super.createNewGameRequest();
		request.setGalaxyConfigId("gala_6");
		return request;
	}

	@Override
	protected String getInitStage() {
		return LateGameScenarioStage.LATE_GAME_WELCOME.name();
	}

	@Override
	protected ScenarioType getType() {
		return ScenarioType.LATE_GAME;
	}

	@Override
	public String getPlayerFactionName() {
		return "silverstarag";
	}

	@Override
	protected String getBotPlayerFaction() {
		return "nightmare";
	}
}
