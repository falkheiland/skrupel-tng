package org.skrupeltng.modules.ingame.modules.scenario.database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;

@Entity
@Table(name = "scenario")
public class Scenario implements Serializable {

	private static final long serialVersionUID = -3762160207992297278L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Player player;

	@ManyToOne(fetch = FetchType.LAZY)
	private Game game;

	@Enumerated(EnumType.STRING)
	private ScenarioType type;

	private String stage;

	private boolean finished;

	@Column(name = "new_stage")
	private boolean newStage;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public ScenarioType getType() {
		return type;
	}

	public void setType(ScenarioType type) {
		this.type = type;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public boolean isNewStage() {
		return newStage;
	}

	public void setNewStage(boolean newStage) {
		this.newStage = newStage;
	}

	@Override
	public String toString() {
		return "Scenario [id=" + id + ", type=" + type + ", stage=" + stage + "]";
	}
}