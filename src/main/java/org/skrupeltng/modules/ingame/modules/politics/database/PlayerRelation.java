package org.skrupeltng.modules.ingame.modules.politics.database;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;

@Entity
@Table(name = "player_relation")
public class PlayerRelation implements Serializable {

	private static final long serialVersionUID = 5504952865753231859L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "player1_id")
	private Player player1;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "player2_id")
	private Player player2;

	@Enumerated(EnumType.STRING)
	private PlayerRelationType type;

	@JoinColumn(name = "rounds_left")
	private int roundsLeft = -1;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Player getPlayer1() {
		return player1;
	}

	public void setPlayer1(Player player1) {
		this.player1 = player1;
	}

	public Player getPlayer2() {
		return player2;
	}

	public void setPlayer2(Player player2) {
		this.player2 = player2;
	}

	public PlayerRelationType getType() {
		return type;
	}

	public void setType(PlayerRelationType type) {
		this.type = type;
	}

	public int getRoundsLeft() {
		return roundsLeft;
	}

	public void setRoundsLeft(int roundsLeft) {
		this.roundsLeft = roundsLeft;
	}

	public String retrieveOtherPlayerName(long currentLoginId) {
		if (player2.getLogin().getId() == currentLoginId) {
			return player1.getLogin().getUsername();
		}

		return player2.getLogin().getUsername();
	}
}