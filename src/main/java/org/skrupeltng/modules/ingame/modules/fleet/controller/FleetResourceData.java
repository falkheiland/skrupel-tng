package org.skrupeltng.modules.ingame.modules.fleet.controller;

import java.io.Serializable;

public class FleetResourceData implements Serializable {

	private static final long serialVersionUID = -6753989213362422134L;

	private int currentTankPercentage;
	private int currentProjectilePercentage;

	private int possibleTankPercentage;
	private int possibleProjectilePercentage;

	private int weaponCount;
	private int shipCount;

	private String availableSpecialAbilities;

	private int averageAggressiveness;
	private boolean hasDifferentAggressivenesses;

	public int getCurrentTankPercentage() {
		return currentTankPercentage;
	}

	public int getCurrentProjectilePercentage() {
		return currentProjectilePercentage;
	}

	public void setCurrentProjectilePercentage(int currentProjectilePercentage) {
		this.currentProjectilePercentage = currentProjectilePercentage;
	}

	public int getPossibleTankPercentage() {
		return possibleTankPercentage;
	}

	public void setPossibleTankPercentage(int possibleTankPercentage) {
		this.possibleTankPercentage = possibleTankPercentage;
	}

	public int getPossibleProjectilePercentage() {
		return possibleProjectilePercentage;
	}

	public void setPossibleProjectilePercentage(int possibleProjectilePercentage) {
		this.possibleProjectilePercentage = possibleProjectilePercentage;
	}

	public void setCurrentTankPercentage(int currentTankPercentage) {
		this.currentTankPercentage = currentTankPercentage;
	}

	public int getWeaponCount() {
		return weaponCount;
	}

	public void setWeaponCount(int weaponCount) {
		this.weaponCount = weaponCount;
	}

	public int getShipCount() {
		return shipCount;
	}

	public void setShipCount(int shipCount) {
		this.shipCount = shipCount;
	}

	public String getAvailableSpecialAbilities() {
		return availableSpecialAbilities;
	}

	public void setAvailableSpecialAbilities(String availableSpecialAbilities) {
		this.availableSpecialAbilities = availableSpecialAbilities;
	}

	public int getAverageAggressiveness() {
		return averageAggressiveness;
	}

	public void setAverageAggressiveness(int averageAggressiveness) {
		this.averageAggressiveness = averageAggressiveness;
	}

	public boolean isHasDifferentAggressivenesses() {
		return hasDifferentAggressivenesses;
	}

	public void setHasDifferentAggressivenesses(boolean hasDifferentAggressivenesses) {
		this.hasDifferentAggressivenesses = hasDifferentAggressivenesses;
	}
}