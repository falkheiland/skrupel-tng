package org.skrupeltng.modules.ingame.modules.scenario.service.process;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.scenario.database.EarlyGameScenarioStage;
import org.skrupeltng.modules.ingame.modules.scenario.database.Scenario;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntry;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteResourceAction;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component("PROCESSOR_EARLY_GAME")
public class EarlyGameScenarioProcessor extends AbstractScenarioProcessor {

	@Override
	public AchievementType getAchievementType() {
		return AchievementType.SCENARIO_FINISHED_EARLY_GAME;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void process(Scenario scenario) {
		String stage = scenario.getStage();

		if (stage.equals(EarlyGameScenarioStage.EARLY_GAME_WELCOME.name())) {
			checkStageShipBuilt(scenario);
		} else if (stage.equals(EarlyGameScenarioStage.EARLY_GAME_SHIP_BUILT.name())) {
			checkStageShipArrived(scenario);
		} else if (stage.equals(EarlyGameScenarioStage.EARLY_GAME_SHIP_ARRIVED.name())) {
			checkStageNewColony(scenario);
		} else if (stage.equals(EarlyGameScenarioStage.EARLY_GAME_NEW_COLONY.name())) {
			checkStageThreeColonies(scenario);
		} else if (stage.equals(EarlyGameScenarioStage.EARLY_GAME_THREE_COLONIES.name())) {
			checkStageEnemyScout(scenario);
		} else if (stage.equals(EarlyGameScenarioStage.EARLY_GAME_ENEMY_SCOUT.name())) {
			checkStageFinal(scenario);
		}
	}

	private void checkStageShipBuilt(Scenario scenario) {
		Player player = getPlayer(scenario);
		Set<Ship> ships = player.getShips();

		if (!CollectionUtils.isEmpty(ships)) {
			changeStage(scenario, EarlyGameScenarioStage.EARLY_GAME_SHIP_BUILT);
		}
	}

	private void checkStageShipArrived(Scenario scenario) {
		Player player = getPlayer(scenario);
		Set<Ship> ships = player.getShips();

		if (!CollectionUtils.isEmpty(ships)) {
			for (Ship ship : ships) {
				Planet planet = ship.getPlanet();

				if (planet != null && planet.getPlayer() == null && ship.getColonists() > 0) {
					changeStage(scenario, EarlyGameScenarioStage.EARLY_GAME_SHIP_ARRIVED);
				}
			}
		}
	}

	private void checkStageNewColony(Scenario scenario) {
		Player player = getPlayer(scenario);

		if (player.getPlanets().size() > 1) {
			changeStage(scenario, EarlyGameScenarioStage.EARLY_GAME_NEW_COLONY);
		}
	}

	private void checkStageThreeColonies(Scenario scenario) {
		Player player = getPlayer(scenario);

		if (player.getPlanets().size() > 2) {
			changeStage(scenario, EarlyGameScenarioStage.EARLY_GAME_THREE_COLONIES);
		}
	}

	private void checkStageEnemyScout(Scenario scenario) {
		Player player = getPlayer(scenario);
		Set<Ship> ships = player.getShips();

		if (!CollectionUtils.isEmpty(ships)) {
			for (Ship ship : ships) {
				List<ShipRouteEntry> route = ship.getRoute();

				if (!CollectionUtils.isEmpty(route) && ship.getRouteTravelSpeed() > 0) {
					Planet homePlanet = player.getHomePlanet();

					boolean correctRouteFound = true;
					boolean homePlanetFound = false;
					int otherPlanetCount = 0;

					for (ShipRouteEntry routeEntry : route) {
						if (routeEntry.getPlanet() == homePlanet) {
							if (allRouteActionsMatch(routeEntry, ShipRouteResourceAction.LEAVE)) {
								homePlanetFound = true;
							} else {
								correctRouteFound = false;
								break;
							}
						} else {
							if (allRouteActionsMatch(routeEntry, ShipRouteResourceAction.TAKE)) {
								otherPlanetCount++;
							} else {
								correctRouteFound = false;
								break;
							}
						}
					}

					if (correctRouteFound && homePlanetFound && otherPlanetCount >= 2) {
						addEnemyShip(scenario.getGame().getId());
						changeStage(scenario, EarlyGameScenarioStage.EARLY_GAME_ENEMY_SCOUT);
						break;
					}
				}
			}
		}
	}

	private boolean allRouteActionsMatch(ShipRouteEntry entry, ShipRouteResourceAction expected) {
		if (entry.getFuelAction() != expected) {
			return false;
		}
		if (entry.getSupplyAction() != expected) {
			return false;
		}
		if (entry.getMoneyAction() != expected) {
			return false;
		}
		if (entry.getMineral1Action() != expected) {
			return false;
		}
		if (entry.getMineral2Action() != expected) {
			return false;
		}
		if (entry.getMineral3Action() != expected) {
			return false;
		}

		return true;
	}

	private void addEnemyShip(long gameId) {
		Pair<Player, Player> players = scenarioPlayers.getPlayers(gameId);
		Player player = players.getLeft();
		Player ai = players.getRight();

		Set<Planet> planets = player.getPlanets();
		Set<Long> planetIds = new HashSet<>();

		for (Planet planet : planets) {
			planetIds.addAll(planetRepository.getPlanetIdsInRadius(gameId, planet.getX(), planet.getY(), 125, false));
		}

		Planet homePlanet = player.getHomePlanet();
		Planet planet = scenarioPlanets.getFarthestPlanet(homePlanet.getX(), homePlanet.getY(), planetIds);

		int x = planet.getX();
		int y = planet.getY();

		Ship ship = scenarioShips.createShip("nightmare_6", "Ambusher", ai, x, y);
		ship.setPropulsionSystemTemplate(propulsionSystemTemplateRepository.getOne("micro_grav"));
		ship.setEnergyWeaponTemplate(weaponTemplateRepository.getOne("plasma_blaster"));
		ship.setProjectileWeaponTemplate(weaponTemplateRepository.getOne("photon_torpedos"));

		ship.setPlanet(planet);
		ship.setFuel(60);
		ship.setTravelSpeed(6);
		ship.setDestinationX(homePlanet.getX() + (20 * randomDirection()));
		ship.setDestinationY(homePlanet.getY() + (20 * randomDirection()));

		shipRepository.save(ship);
	}

	private int randomDirection() {
		return MasterDataService.RANDOM.nextBoolean() ? 1 : -1;
	}

	private void checkStageFinal(Scenario scenario) {
		long gameId = scenario.getGame().getId();

		Pair<Player, Player> players = scenarioPlayers.getPlayers(gameId);
		Player ai = players.getRight();

		if (CollectionUtils.isEmpty(ai.getShips())) {
			finishScenario(scenario);
		}
	}
}