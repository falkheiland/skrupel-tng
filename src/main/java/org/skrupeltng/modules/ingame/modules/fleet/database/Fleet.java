package org.skrupeltng.modules.ingame.modules.fleet.database;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

@Entity
@Table(name = "fleet")
public class Fleet implements Serializable {

	private static final long serialVersionUID = 9100070696671938761L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String name;

	@ManyToOne(fetch = FetchType.LAZY)
	private Player player;

	@ManyToOne(fetch = FetchType.LAZY)
	private Ship leader;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "fleet")
	private List<Ship> ships;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Ship getLeader() {
		return leader;
	}

	public void setLeader(Ship leader) {
		this.leader = leader;
	}

	public List<Ship> getShips() {
		return ships;
	}

	public void setShips(List<Ship> ships) {
		this.ships = ships;
	}

	public List<Ship> retrieveSortedShips() {
		if (ships == null) {
			return ships;
		}

		ships.sort(new SortFleetShipsComparator());
		return ships;
	}
}