package org.skrupeltng.modules.ingame.modules.ship;

import org.skrupeltng.modules.masterdata.database.Resource;
import org.skrupeltng.modules.masterdata.service.MasterDataConstants;

public interface GoodsContainer {

	int getFuel();

	int getMineral1();

	int getMineral2();

	int getMineral3();

	int getColonists();

	int getMoney();

	int getSupplies();

	int getLightGroundUnits();

	int getHeavyGroundUnits();

	default int retrieveTotalGoods() {
		int mineral1 = getMineral1();
		int mineral2 = getMineral2();
		int mineral3 = getMineral3();
		int supplies = getSupplies();
		int colonists = getColonists();
		int lightGroundUnits = getLightGroundUnits();
		int heavyGroundUnits = getHeavyGroundUnits();

		return Math.round(mineral1 + mineral2 + mineral3 + supplies + (colonists / MasterDataConstants.COLONIST_STORAGE_FACTOR) +
				(lightGroundUnits * MasterDataConstants.LIGHT_GROUND_UNITS_FACTOR) + (heavyGroundUnits * MasterDataConstants.HEAVY_GROUND_UNITS_FACTOR));
	}

	default int retrieveResourceQuantity(Resource type) {
		switch (type) {
			case FUEL:
				return getFuel();
			case MINERAL1:
				return getMineral1();
			case MINERAL2:
				return getMineral2();
			case MINERAL3:
				return getMineral3();
			case MONEY:
				return getMoney();
			case SUPPLIES:
				return getSupplies();
		}

		return 0;
	}
}