package org.skrupeltng.modules.ingame.modules.overview.database;

import org.skrupeltng.modules.ingame.modules.overview.service.RoundSummary;

public interface NewsEntryRepositoryCustom {

	void deleteByGameId(long gameId);

	RoundSummary getRoundSummary(long gameId);
}