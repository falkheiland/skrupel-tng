package org.skrupeltng.modules.ingame.modules.overview.service;

import java.util.List;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerDeathFoeRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationContainer;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OverviewService {

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private PlayerRelationRepository playerRelationRepository;

	@Autowired
	private PlayerDeathFoeRepository playerDeathFoeRepository;

	public List<PlayerSummaryEntry> getPlayerSummaries(long gameId) {
		return playerRepository.getSummaries(gameId);
	}

	public List<PlayerRelation> getOtherPlayerRelations(long currentPlayerId, long gameId) {
		return playerRelationRepository.findOtherRelations(currentPlayerId, gameId);
	}

	public List<PlayerRelationContainer> getContainers(long currentPlayerId, long gameId) {
		return playerRelationRepository.getContainers(currentPlayerId, gameId);
	}

	public List<Player> getDeathFoes(long gameId, long loginId) {
		return playerDeathFoeRepository.getDeathFoes(gameId, loginId);
	}
}