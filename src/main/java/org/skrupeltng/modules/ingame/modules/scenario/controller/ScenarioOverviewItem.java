package org.skrupeltng.modules.ingame.modules.scenario.controller;

import org.skrupeltng.modules.ingame.modules.scenario.database.ScenarioType;

public class ScenarioOverviewItem {

	private ScenarioType type;
	private String playerFaction;
	private boolean unlocked;
	private boolean finished;
	private Long gameId;

	public ScenarioType getType() {
		return type;
	}

	public void setType(ScenarioType type) {
		this.type = type;
	}

	public String getPlayerFaction() {
		return playerFaction;
	}

	public void setPlayerFaction(String playerFaction) {
		this.playerFaction = playerFaction;
	}

	public boolean isUnlocked() {
		return unlocked;
	}

	public void setUnlocked(boolean unlocked) {
		this.unlocked = unlocked;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public Long getGameId() {
		return gameId;
	}

	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}
}