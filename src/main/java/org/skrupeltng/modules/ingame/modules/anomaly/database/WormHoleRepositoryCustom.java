package org.skrupeltng.modules.ingame.modules.anomaly.database;

import java.util.List;

public interface WormHoleRepositoryCustom {

	List<Long> getWormHoleIdsInRadius(long gameId, int x, int y, int distance);
}