package org.skrupeltng.modules.ingame.modules.politics.database;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.skrupeltng.modules.RepositoryCustomBase;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

public class PlayerRelationRepositoryImpl extends RepositoryCustomBase implements PlayerRelationRepositoryCustom {

	@Override
	public List<PlayerRelationContainer> getContainers(long currentPlayerId, long gameId) {
		String sql = "" +
				"SELECT \n" +
				"	p.id as \"playerId\", \n" +
				"	l.username as \"playerName\", \n" +
				"	p.color as \"playerColor\", \n" +
				"	r.type as \"relationType\", \n" +
				"	(g.win_condition = '" + WinCondition.TEAM_DEATH_FOE.name() + "') as \"teamDeathFoe\", \n" +
				"	COALESCE(r.rounds_left, -1) as \"roundsLeft\", \n" +
				"	rr.type as \"requestedRelationType\", \n" +
				"	rr.id as \"requestId\", \n" +
				"	(rr.id IS NOT NULL AND rr.requesting_player_id != :currentPlayerId) as \"toBeAccepted\" \n" +
				"FROM \n" +
				"	player p \n" +
				"	INNER JOIN login l \n" +
				"		ON l.id = p.login_id AND p.game_id = :gameId AND p.id != :currentPlayerId \n" +
				"	INNER JOIN game g \n" +
				"		ON g.id = p.game_id \n" +
				"	LEFT OUTER JOIN player_relation r \n" +
				"		ON (r.player1_id = p.id OR r.player2_id = p.id) AND (r.player1_id = :currentPlayerId OR r.player2_id = :currentPlayerId) \n" +
				"	LEFT OUTER JOIN player_relation_request rr \n" +
				"		ON (rr.requesting_player_id = p.id OR rr.other_player_id = p.id) AND (rr.requesting_player_id = :currentPlayerId OR rr.other_player_id = :currentPlayerId) \n" +
				"GROUP BY \n" +
				"	p.id, \n" +
				"	l.username, \n" +
				"	p.color, \n" +
				"	r.type, \n" +
				"	r.rounds_left, \n" +
				"	g.win_condition, \n" +
				"	rr.id, \n" +
				"	rr.type, \n" +
				"	rr.requesting_player_id \n" +
				"ORDER BY \n" +
				"	p.id ASC";

		Map<String, Object> params = new HashMap<>(2);
		params.put("currentPlayerId", currentPlayerId);
		params.put("gameId", gameId);

		RowMapper<PlayerRelationContainer> rowMapper = new BeanPropertyRowMapper<>(PlayerRelationContainer.class);
		return jdbcTemplate.query(sql, params, rowMapper);
	}
}