package org.skrupeltng.modules.ingame.modules.scenario.service.generate;

import org.skrupeltng.modules.dashboard.NewGameRequest;
import org.skrupeltng.modules.ingame.modules.scenario.database.EarlyGameScenarioStage;
import org.skrupeltng.modules.ingame.modules.scenario.database.ScenarioType;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component("GENERATOR_EARLY_GAME")
public class EarlyGameScenarioGenerator extends AbstractScenarioGenerator {

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public long generateScenario(long loginId) {
		long gameId = super.generateScenario(loginId);
		return gameId;
	}

	@Override
	protected NewGameRequest createNewGameRequest() {
		NewGameRequest request = super.createNewGameRequest();
		request.setInhabitedPlanetsPercentage(0f);
		return request;
	}

	@Override
	protected String getInitStage() {
		return EarlyGameScenarioStage.EARLY_GAME_WELCOME.name();
	}

	@Override
	protected ScenarioType getType() {
		return ScenarioType.EARLY_GAME;
	}

	@Override
	public String getPlayerFactionName() {
		return "trodan";
	}

	@Override
	protected String getBotPlayerFaction() {
		return "nightmare";
	}
}