package org.skrupeltng.modules.ingame.modules.scenario.database;

public enum MidGameScenarioStage implements ScenarioStage {

	MID_GAME_WELCOME,

	MID_GAME_SHIPS_BUILT,

	MID_GAME_PLANET_BOMBARDMENT
}