package org.skrupeltng.modules.ingame.modules.overview.controller;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.SortField;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntry;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntryArgument;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.overview.service.OverviewService;
import org.skrupeltng.modules.ingame.modules.overview.service.PlayerMessageService;
import org.skrupeltng.modules.ingame.modules.overview.service.PlayerSummaryEntry;
import org.skrupeltng.modules.ingame.modules.overview.service.RoundSummary;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationContainer;
import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;
import org.skrupeltng.modules.ingame.service.IngameService;
import org.skrupeltng.modules.ingame.service.VisiblePlanetsAndShips;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/ingame/overview")
public class OverviewController extends AbstractController {

	@Autowired
	private NewsService newsService;

	@Autowired
	private OverviewService overviewService;

	@Autowired
	private IngameService ingameService;

	@Autowired
	private PoliticsService politicsService;

	@Autowired
	private PlayerMessageService playerMessageService;

	@GetMapping("")
	public String getOverview(@RequestParam long gameId, Model model) {
		RoundSummary summary = newsService.getRoundSummary(gameId);
		model.addAttribute("summary", summary);

		long loginId = userDetailService.getLoginId();

		if (summary.getWinCondition().equals(WinCondition.DEATH_FOE.name())) {
			Player deathFoe = overviewService.getDeathFoes(gameId, loginId).get(0);
			model.addAttribute("deathFoe", deathFoe);
		} else if (summary.getWinCondition().equals(WinCondition.TEAM_DEATH_FOE.name())) {
			List<Player> deathFoes = overviewService.getDeathFoes(gameId, loginId);
			model.addAttribute("deathFoes", deathFoes);
		}

		List<PlayerSummaryEntry> playerSummaries = overviewService.getPlayerSummaries(gameId);
		model.addAttribute("players", playerSummaries);

		model.addAttribute("currentLoginId", loginId);

		long currentPlayerId = ingameService.getPlayerId(loginId, gameId);
		model.addAttribute("playerId", currentPlayerId);

		List<NewsEntry> news = newsService.findNews(gameId, loginId);

		for (NewsEntry newsEntry : news) {
			List<NewsEntryArgument> arguments = newsEntry.getArguments();

			Object[] args = null;

			if (CollectionUtils.isNotEmpty(arguments)) {
				args = arguments.stream().sorted().map(NewsEntryArgument::getValue).toArray();
			}

			String text = messageSource.getMessage(newsEntry.getTemplate(), args, newsEntry.getTemplate(), LocaleContextHolder.getLocale());
			newsEntry.setTemplate(text);
		}

		model.addAttribute("news", news);

		boolean newsViewed = news.isEmpty() ? true : news.get(0).getPlayer().isNewsViewed();
		model.addAttribute("newsViewed", newsViewed);

		PlayerMessageSearchRequestDTO request = new PlayerMessageSearchRequestDTO();
		request.setGameId(gameId);
		request.setPage(0);
		request.setPageSize(15);
		request.setSortAscending(true);
		request.setSortField(SortField.PLAYER_MESSAGES_DATE.name());
		Page<PlayerMessageDTO> playerMessages = playerMessageService.getMessages(request);
		model.addAttribute("messages", playerMessages);

		model.addAttribute("unreadPlayerMessageCount", playerMessageService.getUnreadMessageCount(gameId));

		addPoliticsData(gameId, model, loginId);

		Optional<Game> game = ingameService.getGame(gameId);
		model.addAttribute("game", game.get());

		Set<CoordinateImpl> visibilityCoordinates = ingameService.getVisibilityCoordinates(gameId, loginId);
		model.addAttribute("visibilityCoordinates", visibilityCoordinates);

		VisiblePlanetsAndShips visiblePlanetsAndShips = ingameService.getVisiblePlanetsAndShips(gameId, loginId, visibilityCoordinates, false);
		model.addAttribute("planets", visiblePlanetsAndShips.getPlanets());
		model.addAttribute("singleShips", visiblePlanetsAndShips.getSingleShips());
		model.addAttribute("shipClusters", visiblePlanetsAndShips.getShipClusters());

		return "ingame/overview/overview::content";
	}

	private void addPoliticsData(long gameId, Model model, long loginId) {
		long currentPlayerId = ingameService.getPlayerId(loginId, gameId);

		List<PlayerRelation> otherRelations = overviewService.getOtherPlayerRelations(currentPlayerId, gameId);
		model.addAttribute("otherRelations", otherRelations);

		List<PlayerRelationContainer> ownRelations = overviewService.getContainers(currentPlayerId, gameId);
		ownRelations.forEach(c -> c.offerMessageSource(messageSource));
		model.addAttribute("ownRelations", ownRelations);

		long toBeAcceptedCount = ownRelations.stream().filter(PlayerRelationContainer::isToBeAccepted).count();
		model.addAttribute("toBeAcceptedCount", toBeAcceptedCount);

		model.addAttribute("messageSource", messageSource);
	}

	@PostMapping("/toggle-news-delete")
	@ResponseBody
	public boolean toggleDeleteNews(@RequestParam long newsEntryId) {
		return newsService.toggleDeleteNews(newsEntryId);
	}

	@PostMapping("/player-relation")
	public String performPlayerRelationAction(@RequestBody PlayerRelationChangeRequest request, Model model) {
		long loginId = userDetailService.getLoginId();

		long gameId = request.getGameId();
		long currentPlayerId = ingameService.getPlayerId(loginId, gameId);
		politicsService.changeRelation(currentPlayerId, request);

		addPoliticsData(gameId, model, loginId);

		return "ingame/overview/politics::content";
	}

	@PutMapping("/player-relation")
	public String acceptPlayerRelationRequest(@RequestParam long id, Model model) {
		long gameId = politicsService.acceptRelationRequest(id);
		addPoliticsData(gameId, model, userDetailService.getLoginId());
		return "ingame/overview/politics::content";
	}

	@GetMapping("/undecided-politics-request-count")
	@ResponseBody
	public long getUndecidedPoliticsRequestCount(@RequestParam long gameId) {
		List<PlayerRelationContainer> ownRelations = overviewService.getContainers(userDetailService.getLoginId(), gameId);
		return ownRelations.stream().filter(PlayerRelationContainer::isToBeAccepted).count();
	}

	@DeleteMapping("/player-relation")
	public String rejectPlayerRelationRequest(@RequestParam long id, Model model) {
		long gameId = politicsService.deleteRelationRequest(id);
		addPoliticsData(gameId, model, userDetailService.getLoginId());
		return "ingame/overview/politics::content";
	}

	@PostMapping("/messages")
	public String getMessages(@RequestBody PlayerMessageSearchRequestDTO request, Model model) {
		Page<PlayerMessageDTO> messages = playerMessageService.getMessages(request);
		model.addAttribute("messages", messages);
		return "ingame/overview/messages::table-content";
	}

	@PostMapping("/message")
	@ResponseBody
	public void sendMessageToPlayer(@RequestBody SendPlayerMessageRequest request) {
		playerMessageService.sendMessage(request);
	}

	@DeleteMapping("/message")
	@ResponseBody
	public long deleteMessage(@RequestParam long messageId, @RequestParam long gameId) {
		playerMessageService.deleteMessage(messageId);
		return playerMessageService.getUnreadMessageCount(gameId);
	}

	@PutMapping("/message")
	@ResponseBody
	public long markMessageAsRead(@RequestParam long messageId, @RequestParam long gameId) {
		playerMessageService.markMessageAsRead(messageId);
		return playerMessageService.getUnreadMessageCount(gameId);
	}
}