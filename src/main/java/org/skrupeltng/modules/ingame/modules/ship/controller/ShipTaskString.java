package org.skrupeltng.modules.ingame.modules.ship.controller;

import java.util.Locale;
import java.util.Optional;

import org.apache.commons.collections4.CollectionUtils;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component("shipTaskString")
public class ShipTaskString {

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private MessageSource messageSource;

	public String retrieveTaskString(long shipId) {
		Ship ship = shipRepository.getOne(shipId);
		return retrieveTaskString(ship);
	}

	public String retrieveTaskString(Ship ship) {
		String taskString = createTaskString(ship);

		Locale locale = LocaleContextHolder.getLocale();
		taskString = messageSource.getMessage(taskString, null, taskString, locale);

		if (ship.getFleet() != null && ship.getFleet().getLeader() != null && ship.getFleet().getLeader().getId() != ship.getId() &&
				ship.getDestinationShip() == ship.getFleet().getLeader()) {
			return taskString + ", " + messageSource.getMessage("follows_fleet_leader", null, "Follows Fleet Leader", locale);
		}

		if ((CollectionUtils.isEmpty(ship.getRoute()) || ship.isRouteDisabled()) && ship.getTravelSpeed() > 0) {
			Ship destinationShip = ship.getDestinationShip();

			taskString += ", ";

			if (destinationShip != null) {
				if (destinationShip.getPlayer().getId() == ship.getPlayer().getId()) {
					taskString += messageSource.getMessage("escorts", new Object[] { destinationShip.getName() }, "Escorts " + destinationShip.getName(),
							locale);
				} else {
					taskString += messageSource.getMessage("interceps_alien_ship", null, "Intercepts alien ship", locale);
				}
			} else {
				int x = ship.getDestinationX();
				int y = ship.getDestinationY();
				Optional<Planet> planetOpt = planetRepository.findByGameIdAndXAndY(ship.getPlayer().getGame().getId(), x, y);

				if (planetOpt.isPresent()) {
					Planet planet = planetOpt.get();
					taskString += messageSource.getMessage("travels_to_planet", new Object[] { planet.getName() }, "Travels to " + planet.getName(), locale);
				} else {
					taskString += messageSource.getMessage("travels_to_location", new Object[] { x, y }, "Travels to  " + x + ", " + y, locale);
				}
			}
		} else if (CollectionUtils.isNotEmpty(ship.getRoute()) && !ship.isRouteDisabled() && ship.getTravelSpeed() == 0 && ship.getDestinationX() == -1 &&
				ship.getDestinationY() == -1 && ship.getFuel() < 10) {
			taskString += ", " + messageSource.getMessage("not_enough_fuel", null, "Not enough fuel on board!", locale);
		}

		return taskString;
	}

	private String createTaskString(Ship ship) {
		ShipTaskType taskType = ship.getTaskType();

		if (taskType == ShipTaskType.ACTIVE_ABILITY) {
			return "ship_ability_" + ship.getActiveAbility().getType().name();
		}

		if (CollectionUtils.isNotEmpty(ship.getRoute()) && !ship.isRouteDisabled()) {
			return "routing";
		}

		return "task_" + taskType.name().toLowerCase();
	}
}