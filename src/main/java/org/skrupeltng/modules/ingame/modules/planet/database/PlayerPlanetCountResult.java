package org.skrupeltng.modules.ingame.modules.planet.database;

public class PlayerPlanetCountResult {

	private long playerId;
	private long planetCount;

	public PlayerPlanetCountResult() {

	}

	public PlayerPlanetCountResult(long playerId, long planetCount) {
		this.playerId = playerId;
		this.planetCount = planetCount;
	}

	public long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

	public long getPlanetCount() {
		return planetCount;
	}

	public void setPlanetCount(long planetCount) {
		this.planetCount = planetCount;
	}
}