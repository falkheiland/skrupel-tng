package org.skrupeltng.modules.ingame.modules.scenario.service.generate;

import org.skrupeltng.modules.dashboard.NewGameRequest;
import org.skrupeltng.modules.dashboard.service.DashboardService;
import org.skrupeltng.modules.dashboard.service.GameFullException;
import org.skrupeltng.modules.dashboard.service.GameStartingHelper;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.scenario.database.Scenario;
import org.skrupeltng.modules.ingame.modules.scenario.database.ScenarioRepository;
import org.skrupeltng.modules.ingame.modules.scenario.database.ScenarioType;
import org.skrupeltng.modules.ingame.modules.scenario.service.ScenarioPlanets;
import org.skrupeltng.modules.ingame.modules.scenario.service.ScenarioShips;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.service.round.RoundCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public abstract class AbstractScenarioGenerator {

	@Autowired
	protected ScenarioRepository scenarioRepository;

	@Autowired
	protected GameRepository gameRepository;

	@Autowired
	protected PlayerRepository playerRepository;

	@Autowired
	protected DashboardService dashboardService;

	@Autowired
	protected GameStartingHelper gameStartingHelper;

	@Autowired
	protected ShipRepository shipRepository;

	@Autowired
	protected RoundCalculationService roundCalculationService;

	@Autowired
	protected ScenarioPlanets scenarioPlanets;

	@Autowired
	protected ScenarioShips scenarioShips;

	@Autowired
	protected MessageSource messageSource;

	protected abstract String getInitStage();

	protected abstract ScenarioType getType();

	public abstract String getPlayerFactionName();

	protected abstract String getBotPlayerFaction();

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public long generateScenario(long loginId) {
		NewGameRequest newGameRequest = createNewGameRequest();

		long gameId = dashboardService.createdNewGameWithNewTransaction(newGameRequest, loginId);
		Game game = gameRepository.getOne(gameId);

		Player player = game.getPlayers().get(0);

		String botPlayerFaction = getBotPlayerFaction();

		if (botPlayerFaction != null) {
			long aiPlayerId = 0L;

			try {
				aiPlayerId = dashboardService.addPlayerWithNewTransaction(gameId, 3L, null).getId();
			} catch (GameFullException e) {
				e.printStackTrace();
				throw new IllegalStateException("Something went wrong that could not have!");
			}

			dashboardService.selectFactionForPlayerWithNewTransaction(botPlayerFaction, aiPlayerId);
		}

		dashboardService.selectFactionForPlayerWithNewTransaction(getPlayerFactionName(), player.getId());

		gameStartingHelper.startGame(gameId);
		roundCalculationService.setupTurnValues(gameId);

		game.setScenario(true);
		game = gameRepository.save(game);

		player = game.getPlayers().get(0);

		Scenario scenario = new Scenario();
		scenario.setNewStage(true);
		scenario.setGame(game);
		scenario.setPlayer(player);
		scenario.setType(getType());
		scenario.setStage(getInitStage());
		scenarioRepository.save(scenario);

		return gameId;
	}

	protected NewGameRequest createNewGameRequest() {
		NewGameRequest newGameRequest = NewGameRequest.createDefaultRequest();
		newGameRequest.setName(messageSource.getMessage("scenario_title_" + getType(), null, "Scenario", LocaleContextHolder.getLocale()));
		newGameRequest.setWinCondition(WinCondition.NONE.name());
		return newGameRequest;
	}
}