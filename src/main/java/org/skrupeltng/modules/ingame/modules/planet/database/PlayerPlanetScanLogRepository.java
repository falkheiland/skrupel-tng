package org.skrupeltng.modules.ingame.modules.planet.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface PlayerPlanetScanLogRepository extends JpaRepository<PlayerPlanetScanLog, Long> {

	List<PlayerPlanetScanLog> findByPlayerId(long playerId);

	@Query("SELECT l FROM PlayerPlanetScanLog l INNER JOIN FETCH l.planet p WHERE p.game.id = ?1")
	List<PlayerPlanetScanLog> findByGameId(long gameId);

	@Modifying
	@Query("DELETE FROM PlayerPlanetScanLog p WHERE p.planet.id = ?1")
	void deleteByPlanetId(long planetId);

	@Modifying
	@Query("DELETE FROM PlayerPlanetScanLog p WHERE p.player.id = ?1")
	void deleteByPlayerId(long playerId);
}