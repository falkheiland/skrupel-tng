package org.skrupeltng.modules.ingame.modules.ship.database;

public enum ShipRouteResourceAction {

	TAKE("+"), IGNORE("0"), LEAVE("-");

	private final String displayText;

	private ShipRouteResourceAction(String displayText) {
		this.displayText = displayText;
	}

	public String getDisplayText() {
		return displayText;
	}
}