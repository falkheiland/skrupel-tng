package org.skrupeltng.modules.ingame.modules.ship.database;

public enum ShipTactics {

	DEFENSIVE,

	STANDARD,

	OFFENSIVE
}