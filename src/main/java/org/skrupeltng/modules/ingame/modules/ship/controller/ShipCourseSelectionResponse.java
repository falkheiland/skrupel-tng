package org.skrupeltng.modules.ingame.modules.ship.controller;

import java.io.Serializable;

public class ShipCourseSelectionResponse implements Serializable {

	private static final long serialVersionUID = -6258408719275911889L;

	private boolean cycleDetected;

	public boolean isCycleDetected() {
		return cycleDetected;
	}

	public void setCycleDetected(boolean cycleDetected) {
		this.cycleDetected = cycleDetected;
	}
}