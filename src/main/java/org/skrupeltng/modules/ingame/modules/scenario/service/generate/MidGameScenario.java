package org.skrupeltng.modules.ingame.modules.scenario.service.generate;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.skrupeltng.modules.ai.AIConstants;
import org.skrupeltng.modules.ingame.CoordinateDistanceComparator;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.anomaly.database.MineField;
import org.skrupeltng.modules.ingame.modules.anomaly.database.MineFieldRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpeciesEffect;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.scenario.service.ScenarioPlanets;
import org.skrupeltng.modules.ingame.modules.scenario.service.ScenarioPlayers;
import org.skrupeltng.modules.ingame.modules.scenario.service.ScenarioShips;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplateRepository;
import org.skrupeltng.modules.masterdata.database.WeaponTemplateRepository;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class MidGameScenario {

	@Autowired
	private ScenarioPlayers scenarioPlayers;

	@Autowired
	private ScenarioPlanets scenarioPlanets;

	@Autowired
	private ScenarioShips scenarioShips;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private StarbaseRepository starbaseRepository;

	@Autowired
	private WeaponTemplateRepository weaponTemplateRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PropulsionSystemTemplateRepository propulsionSystemTemplateRepository;

	@Autowired
	private MineFieldRepository mineFieldRepository;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void initPlanets(long gameId) {
		Pair<Player, Player> players = scenarioPlayers.getPlayers(gameId);
		Player player = players.getLeft();
		Player ai = players.getRight();

		Planet homePlanet = player.getHomePlanet();
		homePlanet.setMoney(3000 + MasterDataService.RANDOM.nextInt(1000));
		homePlanet.setSupplies(300 + MasterDataService.RANDOM.nextInt(100));
		homePlanet.setFuel(100 + MasterDataService.RANDOM.nextInt(50));
		homePlanet.setMineral1(100 + MasterDataService.RANDOM.nextInt(50));
		homePlanet.setMineral2(100 + MasterDataService.RANDOM.nextInt(50));
		homePlanet.setMineral3(100 + MasterDataService.RANDOM.nextInt(50));

		homePlanet.setMines(homePlanet.retrieveMaxMines());
		homePlanet.setAutoBuildMines(true);

		homePlanet.setFactories(homePlanet.retrieveMaxFactories());
		homePlanet.setAutoBuildFactories(true);
		homePlanet = planetRepository.save(homePlanet);

		Starbase starbase = homePlanet.getStarbase();
		starbase.setHullLevel(6);
		starbase.setPropulsionLevel(7);
		starbase.setEnergyLevel(1);
		starbaseRepository.save(starbase);

		initPlanetsForPlayer(player, homePlanet.getX(), homePlanet.getY(), 200);

		initAIPlanets(ai, homePlanet);
	}

	private void initAIPlanets(Player ai, Planet playerHomePlanet) {
		Planet homePlanet = ai.getHomePlanet();
		Starbase starbase = homePlanet.getStarbase();
		homePlanet.setStarbase(null);

		starbaseRepository.delete(starbase);

		homePlanet.setColonists(0);
		homePlanet.setPlayer(null);
		planetRepository.save(homePlanet);

		long gameId = playerHomePlanet.getGame().getId();
		Planet maxDistPlanet = scenarioPlanets.getFarthestPlanet(gameId, playerHomePlanet.getX(), playerHomePlanet.getY(), 350);

		if (maxDistPlanet == null) {
			throw new IllegalStateException("No planet for AI found!");
		}

		List<Long> planetIdsInRadius = planetRepository.getPlanetIdsInRadius(gameId, maxDistPlanet.getX(), maxDistPlanet.getY(), 150, false);
		List<Planet> nearPlanets = planetRepository.findByIds(planetIdsInRadius);
		nearPlanets.sort(new CoordinateDistanceComparator(maxDistPlanet));

		prepareAIPlanet(maxDistPlanet, ai);

		int centerX = maxDistPlanet.getX();
		int centerY = maxDistPlanet.getY();

		MineField mineField = new MineField();
		mineField.setGame(ai.getGame());
		mineField.setLevel(3);
		mineField.setMines(10);
		mineField.setPlayer(ai);
		mineField.setX(centerX);
		mineField.setY(centerY);
		mineFieldRepository.save(mineField);
	}

	private void prepareAIPlanet(Planet planet, Player ai) {
		planet.setPlayer(ai);
		planet.setColonists(2000 + MasterDataService.RANDOM.nextInt(100));
		planet.setMoney(500 + MasterDataService.RANDOM.nextInt(50));
		planet.setSupplies(60 + MasterDataService.RANDOM.nextInt(50));
		planet.setMines(20);
		planet.setFactories(20);
		planet.setPlanetaryDefense(20);
		planetRepository.save(planet);
	}

	protected void initPlanetsForPlayer(Player player, int x, int y, int radius) {
		long gameId = player.getGame().getId();

		List<Long> planetIdsInRadius = planetRepository.getPlanetIdsInRadius(gameId, x, y, radius, false);

		for (Long planetId : planetIdsInRadius) {
			Planet planet = planetRepository.getOne(planetId);

			if (planet.retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.ATTACK).isEmpty() && planet != player.getHomePlanet()) {
				planet.setPlayer(player);
				planet.setColonists(1000 + MasterDataService.RANDOM.nextInt(500));
				planet.setMoney(200 + MasterDataService.RANDOM.nextInt(200));
				planet.setSupplies(30 + MasterDataService.RANDOM.nextInt(20));

				planet.setMines(planet.retrieveMaxMines());
				planet.setAutoBuildMines(true);

				planet.setFactories(planet.retrieveMaxFactories());
				planet.setAutoBuildFactories(true);

				planetRepository.save(planet);
			}
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void initShips(long gameId) {
		Pair<Player, Player> players = scenarioPlayers.getPlayers(gameId);
		Player player = players.getLeft();
		Player ai = players.getRight();

		scenarioShips.initRoutes(player.getHomePlanet(), 200, "nightmare_13");
		initPlayerScout(player, ai);
		initAIShips(ai);
	}

	private void initPlayerScout(Player player, Player ai) {
		Planet homePlanet = player.getHomePlanet();

		MineField mineField = mineFieldRepository.findByGameId(player.getGame().getId()).get(0);

		Vector2D mineFieldLoc = new Vector2D(mineField.getX(), mineField.getY());
		Vector2D homePlanetLoc = new Vector2D(homePlanet.getX(), homePlanet.getY());
		Vector2D direction = homePlanetLoc.subtract(mineFieldLoc).normalize();
		Vector2D shipPos = mineFieldLoc.add(direction.scalarMultiply(100));

		Ship ship = scenarioShips.createShip("nightmare_2", "Scout", player, (int)shipPos.getX(), (int)shipPos.getY());
		ship.setPropulsionSystemTemplate(propulsionSystemTemplateRepository.getOne("solarisplasmotan"));
		ship.setEnergyWeaponTemplate(weaponTemplateRepository.getOne("laser"));
		ship.setFuel(50);
		shipRepository.save(ship);
	}

	private void initAIShips(Player ai) {
		List<Planet> planets = new ArrayList<>(ai.getPlanets());

		addAiShip(planets.get(0), AIConstants.AI_SCOUT_NAME, "silverstarag_3", ai, "plasma_blaster");
		addAiShip(planets.get(0), AIConstants.AI_SCOUT_NAME, "silverstarag_5", ai, "tachion_beam");

		addAiShip(planets.get(0), AIConstants.AI_AMBUSHER_NAME, "silverstarag_6", ai, "disruptor");
		addAiShip(planets.get(0), AIConstants.AI_SCOUT_NAME, "silverstarag_2", ai, "laser");

		addAiShip(planets.get(0), AIConstants.AI_BOMBER_NAME, "silverstarag_9", ai, "desintegrator");
	}

	private void addAiShip(Planet planet, String name, String templateId, Player ai, String energyWeaponName) {
		int galaxySize = ai.getGame().getGalaxySize();
		int x = planet.getX() + (randomDirection() * MasterDataService.RANDOM.nextInt(60));
		int y = planet.getY() + (randomDirection() * MasterDataService.RANDOM.nextInt(60));

		x = Math.min(Math.max(x, 20), galaxySize - 20);
		y = Math.min(Math.max(y, 20), galaxySize - 20);

		Ship ship = scenarioShips.createShip(templateId, name, ai, x, y);
		ship.setPropulsionSystemTemplate(propulsionSystemTemplateRepository.getOne("micro_grav"));
		ship.setEnergyWeaponTemplate(weaponTemplateRepository.getOne(energyWeaponName));
		ship.setFuel(70);
		ship.setTravelSpeed(6);
		ship.setDestinationX(100 + MasterDataService.RANDOM.nextInt(galaxySize - 200));
		ship.setDestinationY(100 + MasterDataService.RANDOM.nextInt(galaxySize - 200));
		shipRepository.save(ship);
	}

	private int randomDirection() {
		return MasterDataService.RANDOM.nextBoolean() ? 1 : -1;
	}
}