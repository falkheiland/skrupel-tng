package org.skrupeltng.modules.ingame.modules.scenario.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.skrupeltng.modules.dashboard.database.AchievementRepository;
import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.dashboard.service.GameRemoval;
import org.skrupeltng.modules.ingame.modules.scenario.controller.ScenarioOverviewItem;
import org.skrupeltng.modules.ingame.modules.scenario.database.Scenario;
import org.skrupeltng.modules.ingame.modules.scenario.database.ScenarioRepository;
import org.skrupeltng.modules.ingame.modules.scenario.database.ScenarioType;
import org.skrupeltng.modules.ingame.modules.scenario.service.generate.AbstractScenarioGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ScenarioService {

	@Autowired
	private ScenarioRepository scenarioRepository;

	@Autowired
	private AchievementRepository achievementRepository;

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private GameRemoval gameRemoval;

	@Autowired
	private Map<String, AbstractScenarioGenerator> scenarioGenerators;

	public List<ScenarioOverviewItem> getOverviewItems(long loginId) {
		List<ScenarioOverviewItem> items = new ArrayList<>(3);
		items.add(createOverviewItem(loginId, ScenarioType.EARLY_GAME));
		items.add(createOverviewItem(loginId, ScenarioType.MID_GAME));
		items.add(createOverviewItem(loginId, ScenarioType.LATE_GAME));
		return items;
	}

	private ScenarioOverviewItem createOverviewItem(long loginId, ScenarioType type) {
		ScenarioOverviewItem item = new ScenarioOverviewItem();
		item.setType(type);
		item.setFinished(hasFinishedScenario(loginId, type));

		Optional<Scenario> result = scenarioRepository.findByLoginIdAndType(loginId, type);

		if (result.isPresent()) {
			Scenario scenario = result.get();

			if (!scenario.isFinished()) {
				item.setGameId(scenario.getGame().getId());
			}
		}

		switch (type) {
			case EARLY_GAME:
				item.setUnlocked(true);
				break;
			case MID_GAME:
				item.setUnlocked(hasFinishedScenario(loginId, ScenarioType.EARLY_GAME));
				break;
			case LATE_GAME:
				item.setUnlocked(hasFinishedScenario(loginId, ScenarioType.MID_GAME));
				break;
		}

		item.setPlayerFaction(scenarioGenerators.get("GENERATOR_" + type).getPlayerFactionName());

		return item;
	}

	public boolean isHideScenarioTutorialHint(long loginId) {
		return loginRepository.isHideScenarioTutorialHint(loginId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void hideScenarioTutorialHint(long loginId) {
		Login login = loginRepository.getOne(loginId);
		login.setHideScenarioTutorialHint(true);
		loginRepository.save(login);
	}

	public boolean hasFinishedScenario(long loginId, ScenarioType type) {
		AchievementType achievementType = AchievementType.valueOf("SCENARIO_FINISHED_" + type.name());
		return achievementRepository.findByLoginIdAndType(loginId, achievementType).isPresent();
	}

	public Long getScenarioGameId(long loginId, ScenarioType type) {
		Optional<Scenario> result = scenarioRepository.findByLoginIdAndType(loginId, type);

		if (result.isPresent()) {
			return result.get().getGame().getId();
		}

		return null;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public long startScenario(long loginId, ScenarioType type) {
		Long existingId = getScenarioGameId(loginId, type);

		if (existingId != null) {
			throw new IllegalArgumentException("Login " + loginId + " already started a scenario of type " + type + "!");
		}

		AbstractScenarioGenerator generator = scenarioGenerators.get("GENERATOR_" + type.name());
		return generator.generateScenario(loginId);
	}

	public String getScenarioStage(long gameId) {
		return scenarioRepository.getStageByGameId(gameId);
	}

	public ScenarioType getScenarioType(long gameId) {
		return scenarioRepository.getScenarioType(gameId);
	}

	public boolean scenarioFinished(long gameId) {
		return scenarioRepository.isFinished(gameId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Optional<ScenarioType> deleteFinishedScenario(long loginId) {
		Optional<Scenario> result = scenarioRepository.getFinishedScenario(loginId);

		if (result.isPresent()) {
			Scenario scenario = result.get();
			ScenarioType type = scenario.getType();

			gameRemoval.delete(scenario.getGame());

			return Optional.of(type);
		}

		return Optional.empty();
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public long restartScenario(long loginId, long gameId) {
		Optional<Scenario> scenarioOpt = scenarioRepository.findScenarioByGameId(gameId);

		if (scenarioOpt.isEmpty()) {
			throw new IllegalArgumentException("Unable to find Scenario with gameId " + gameId + "!");
		}

		Scenario scenario = scenarioOpt.get();
		ScenarioType type = scenario.getType();
		gameRemoval.delete(scenario.getGame());

		return startScenario(loginId, type);
	}

	public boolean scenarioHasNewStage(long gameId) {
		Optional<Scenario> scenarioOpt = scenarioRepository.findScenarioByGameId(gameId);

		if (scenarioOpt.isEmpty()) {
			throw new IllegalArgumentException("Unable to find Scenario with gameId " + gameId + "!");
		}

		return scenarioOpt.get().isNewStage();
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void markScenarioStageAsSeen(long gameId) {
		Optional<Scenario> scenarioOpt = scenarioRepository.findScenarioByGameId(gameId);

		if (scenarioOpt.isEmpty()) {
			throw new IllegalArgumentException("Unable to find Scenario with gameId " + gameId + "!");
		}

		Scenario scenario = scenarioOpt.get();
		scenario.setNewStage(false);
		scenarioRepository.save(scenario);
	}
}