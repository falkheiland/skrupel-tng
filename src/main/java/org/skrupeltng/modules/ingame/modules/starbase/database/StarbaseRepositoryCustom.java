package org.skrupeltng.modules.ingame.modules.starbase.database;

import org.skrupeltng.modules.ingame.controller.StarbaseListResultDTO;
import org.skrupeltng.modules.ingame.controller.StarbaseOverviewRequest;
import org.springframework.data.domain.Page;

public interface StarbaseRepositoryCustom {

	boolean loginOwnsStarbase(long starbaseId, long loginId);

	Page<StarbaseListResultDTO> searchStarbases(StarbaseOverviewRequest request, long loginId);
}