package org.skrupeltng.modules.ingame.modules.anomaly.database;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

@Entity
@Table(name = "worm_hole")
public class WormHole implements Serializable, Coordinate {

	private static final long serialVersionUID = -7870846957849005120L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private int x;

	private int y;

	@Enumerated(EnumType.STRING)
	private WormHoleType type;

	@ManyToOne(fetch = FetchType.LAZY)
	private Game game;

	@ManyToOne(fetch = FetchType.LAZY)
	private WormHole connection;

	@ManyToOne(fetch = FetchType.LAZY)
	private Ship ship;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	@Override
	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public WormHoleType getType() {
		return type;
	}

	public void setType(WormHoleType type) {
		this.type = type;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public WormHole getConnection() {
		return connection;
	}

	public void setConnection(WormHole connection) {
		this.connection = connection;
	}

	public Ship getShip() {
		return ship;
	}

	public void setShip(Ship ship) {
		this.ship = ship;
	}

	@Override
	public String toString() {
		return "WormHole [id=" + id + ", x=" + x + ", y=" + y + ", type=" + type + "]";
	}

	@Override
	public int getScanRadius() {
		return 0;
	}

	public String retrieveGalaxyMapStyle() {
		int size = 0;

		switch (type) {
			case JUMP_PORTAL:
				size = 20;
				break;
			case STABLE_WORMHOLE:
				size = 40;
				break;
			case UNSTABLE_WORMHOLE:
				size = 40;
				break;
		}

		int xy = size / 2;

		return "right: " + xy + "px; bottom: " + xy + "px; width: " + size + "px; height: " + size + "px;";
	}
}