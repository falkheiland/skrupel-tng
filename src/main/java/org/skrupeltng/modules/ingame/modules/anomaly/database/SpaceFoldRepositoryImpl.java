package org.skrupeltng.modules.ingame.modules.anomaly.database;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.skrupeltng.modules.RepositoryCustomBase;

public class SpaceFoldRepositoryImpl extends RepositoryCustomBase implements SpaceFoldRepositoryCustom {

	@Override
	public List<Long> getSpaceFoldIdsInRadius(long gameId, int x, int y, int distance) {
		String sql = "" +
				"SELECT \n" +
				"	s.id \n" +
				"FROM \n" +
				"	space_fold s \n" +
				"WHERE \n" +
				"	s.game_id = :gameId \n" +
				"	AND sqrt(((s.x - :x) * (s.x - :x)) + ((s.y - :y) * (s.y - :y))) <= :distance";

		Map<String, Object> params = new HashMap<>(4);
		params.put("gameId", gameId);
		params.put("x", x);
		params.put("y", y);
		params.put("distance", distance);

		return jdbcTemplate.queryForList(sql, params, Long.class);
	}
}