package org.skrupeltng.modules.ingame.modules.planet.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface OrbitalSystemRepository extends JpaRepository<OrbitalSystem, Long>, OrbitalSystemRepositoryCustom {

	@Query("SELECT o FROM OrbitalSystem o WHERE o.planet.id = ?1 ORDER BY o.id ASC")
	List<OrbitalSystem> findByPlanetId(long planetId);

	@Modifying
	@Query("DELETE FROM OrbitalSystem o WHERE o.planet.id = ?1")
	void deleteByPlanetId(long planetId);
}