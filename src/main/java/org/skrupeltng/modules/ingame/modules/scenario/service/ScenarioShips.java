package org.skrupeltng.modules.ingame.modules.scenario.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.skrupeltng.modules.ai.AIConstants;
import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.CoordinateDistanceComparator;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntry;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteResourceAction;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplateRepository;
import org.skrupeltng.modules.masterdata.database.Resource;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.database.ShipTemplateRepository;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ScenarioShips {

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private ShipTemplateRepository shipTemplateRepository;

	@Autowired
	private ShipRouteEntryRepository shipRouteEntryRepository;

	@Autowired
	private PropulsionSystemTemplateRepository propulsionSystemTemplateRepository;

	@Autowired
	private PlanetRepository planetRepository;

	public Ship createShip(String templateId, String name, Player player, int x, int y) {
		ShipTemplate shipTemplate = shipTemplateRepository.getOne(templateId);

		Ship ship = new Ship();
		ship.setShield(100);
		ship.setCrew(shipTemplate.getCrew());
		ship.setDestinationX(-1);
		ship.setDestinationY(-1);
		ship.setName(name);
		ship.setPlayer(player);
		ship.setShipTemplate(shipTemplate);
		ship.setX(x);
		ship.setY(y);
		ship.setCreationX(x);
		ship.setCreationY(y);

		return shipRepository.save(ship);
	}

	public void initRoutes(Planet mainPlanet, int radius, String shipTemplate) {
		Player player = mainPlanet.getPlayer();

		List<Planet> topLeft = new ArrayList<>();
		List<Planet> topRight = new ArrayList<>();
		List<Planet> bottomRight = new ArrayList<>();
		List<Planet> bottomLeft = new ArrayList<>();

		int xMain = mainPlanet.getX();
		int yMain = mainPlanet.getY();

		long gameId = player.getGame().getId();
		List<Long> planetIdsInRadius = planetRepository.getPlanetIdsInRadius(gameId, xMain, yMain, radius, true);

		List<Planet> planets = planetRepository.findByIds(planetIdsInRadius);

		for (Planet planet : planets) {
			if (planet == mainPlanet || !CollectionUtils.isEmpty(planet.getRouteEntries())) {
				continue;
			}

			int x = planet.getX();
			int y = planet.getY();

			if (x <= xMain && y <= yMain) {
				topLeft.add(planet);
			} else if (x > xMain && y <= yMain) {
				topRight.add(planet);
			} else if (x > xMain && y > yMain) {
				bottomRight.add(planet);
			} else {
				bottomLeft.add(planet);
			}
		}

		initShipWithRoute(mainPlanet, topLeft, shipTemplate);
		initShipWithRoute(mainPlanet, topRight, shipTemplate);
		initShipWithRoute(mainPlanet, bottomRight, shipTemplate);
		initShipWithRoute(mainPlanet, bottomLeft, shipTemplate);
	}

	private void initShipWithRoute(Planet mainPlanet, List<Planet> planets, String shipTemplate) {
		int planetCount = planets.size();

		if (planetCount < 2) {
			return;
		}

		Player player = mainPlanet.getPlayer();
		Planet randomPlanet = planets.get(MasterDataService.RANDOM.nextInt(planetCount - 1));

		Ship ship = createShip(shipTemplate, AIConstants.AI_FREIGHTER_NAME, player, randomPlanet.getX(), randomPlanet.getY());
		PropulsionSystemTemplate propulsionSystemTemplate = propulsionSystemTemplateRepository.getOne("solarisplasmotan");
		ship.setPropulsionSystemTemplate(propulsionSystemTemplate);

		ship.setFuel(40);
		ship.setRouteMinFuel(60);
		ship.setRoutePrimaryResource(Resource.SUPPLIES);
		ship.setRouteTravelSpeed(propulsionSystemTemplate.getWarpSpeed());
		ship.setPlanet(randomPlanet);

		ShipRouteEntry mainEntry = new ShipRouteEntry();
		mainEntry.setShip(ship);
		mainEntry.setPlanet(mainPlanet);
		mainEntry.setFuelAction(ShipRouteResourceAction.LEAVE);
		mainEntry.setMoneyAction(ShipRouteResourceAction.LEAVE);
		mainEntry.setSupplyAction(ShipRouteResourceAction.LEAVE);
		mainEntry.setMineral1Action(ShipRouteResourceAction.LEAVE);
		mainEntry.setMineral2Action(ShipRouteResourceAction.LEAVE);
		mainEntry.setMineral3Action(ShipRouteResourceAction.LEAVE);
		mainEntry = shipRouteEntryRepository.save(mainEntry);
		ship.setCurrentRouteEntry(mainEntry);

		int currentEntryIndex = 0;
		Planet currentPlanet = mainPlanet;

		for (int i = 0; i < planetCount; i++) {
			currentPlanet = getNextPlanet(currentPlanet, planets);
			planets.remove(currentPlanet);

			ShipRouteEntry entry = new ShipRouteEntry();
			entry.setShip(ship);
			entry.setPlanet(currentPlanet);
			entry.setFuelAction(ShipRouteResourceAction.TAKE);
			entry.setMoneyAction(ShipRouteResourceAction.TAKE);
			entry.setSupplyAction(ShipRouteResourceAction.TAKE);
			entry.setMineral1Action(ShipRouteResourceAction.TAKE);
			entry.setMineral2Action(ShipRouteResourceAction.TAKE);
			entry.setMineral3Action(ShipRouteResourceAction.TAKE);
			entry = shipRouteEntryRepository.save(entry);

			if (currentPlanet == randomPlanet) {
				ship.setCurrentRouteEntry(entry);
				currentEntryIndex = i;
			}
		}

		if (currentEntryIndex > 0) {
			ship.setMineral1(MasterDataService.RANDOM.nextInt(100));
			ship.setMineral2(MasterDataService.RANDOM.nextInt(100));
			ship.setMineral3(MasterDataService.RANDOM.nextInt(100));
			ship.setMoney(MasterDataService.RANDOM.nextInt(100));
			ship.setSupplies(MasterDataService.RANDOM.nextInt(100));
		}

		shipRepository.save(ship);
	}

	private Planet getNextPlanet(Coordinate coord, List<Planet> planets) {
		return planets.stream().sorted(new CoordinateDistanceComparator(coord)).findFirst().get();
	}
}