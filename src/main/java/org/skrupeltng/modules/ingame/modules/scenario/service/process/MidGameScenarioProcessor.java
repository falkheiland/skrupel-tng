package org.skrupeltng.modules.ingame.modules.scenario.service.process;

import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.scenario.database.MidGameScenarioStage;
import org.skrupeltng.modules.ingame.modules.scenario.database.Scenario;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.masterdata.database.WeaponTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component("PROCESSOR_MID_GAME")
public class MidGameScenarioProcessor extends AbstractScenarioProcessor {

	@Override
	public AchievementType getAchievementType() {
		return AchievementType.SCENARIO_FINISHED_MID_GAME;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void process(Scenario scenario) {
		String stage = scenario.getStage();

		if (stage.equals(MidGameScenarioStage.MID_GAME_WELCOME.name())) {
			checkStageBuildShips(scenario);
		} else if (stage.equals(MidGameScenarioStage.MID_GAME_SHIPS_BUILT.name())) {
			checkStagePlanetBombardment(scenario);
		} else if (stage.equals(MidGameScenarioStage.MID_GAME_PLANET_BOMBARDMENT.name())) {
			checkStageFinal(scenario);
		}
	}

	private void checkStageBuildShips(Scenario scenario) {
		Set<Ship> ships = scenario.getPlayer().getShips();

		for (Ship ship : ships) {
			WeaponTemplate energyWeapon = ship.getEnergyWeaponTemplate();
			WeaponTemplate projectileWeapon = ship.getProjectileWeaponTemplate();

			if ((energyWeapon != null && energyWeapon.getTechLevel() > 1) || (projectileWeapon != null && projectileWeapon.getTechLevel() > 1)) {
				changeStage(scenario, MidGameScenarioStage.MID_GAME_SHIPS_BUILT);
				return;
			}
		}
	}

	private void checkStagePlanetBombardment(Scenario scenario) {
		Set<Ship> ships = scenario.getPlayer().getShips();

		Player aiPlayer = scenarioPlayers.getPlayers(scenario.getGame().getId()).getRight();
		Set<Planet> planets = aiPlayer.getPlanets();

		for (Ship ship : ships) {
			WeaponTemplate energyWeapon = ship.getEnergyWeaponTemplate();
			WeaponTemplate projectileWeapon = ship.getProjectileWeaponTemplate();

			if ((energyWeapon != null && energyWeapon.getTechLevel() > 1) || (projectileWeapon != null && projectileWeapon.getTechLevel() > 1)) {
				for (Planet planet : planets) {
					if (planet.getDistance(ship) < 100) {
						changeStage(scenario, MidGameScenarioStage.MID_GAME_PLANET_BOMBARDMENT);
						return;
					}
				}
			}
		}
	}

	private void checkStageFinal(Scenario scenario) {
		Player aiPlayer = scenarioPlayers.getPlayers(scenario.getGame().getId()).getRight();
		Set<Planet> planets = aiPlayer.getPlanets();

		if (CollectionUtils.isEmpty(planets)) {
			finishScenario(scenario);
		}
	}
}