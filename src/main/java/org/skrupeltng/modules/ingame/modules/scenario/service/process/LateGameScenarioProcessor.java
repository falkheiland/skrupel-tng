package org.skrupeltng.modules.ingame.modules.scenario.service.process;

import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.scenario.database.LateGameScenarioStage;
import org.skrupeltng.modules.ingame.modules.scenario.database.Scenario;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component("PROCESSOR_LATE_GAME")
public class LateGameScenarioProcessor extends AbstractScenarioProcessor {

	@Override
	public AchievementType getAchievementType() {
		return AchievementType.SCENARIO_FINISHED_LATE_GAME;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void process(Scenario scenario) {
		String stage = scenario.getStage();

		if (stage.equals(LateGameScenarioStage.LATE_GAME_WELCOME.name())) {
			checkFinalStage(scenario);
		}
	}

	private void checkFinalStage(Scenario scenario) {
		Player ai = scenarioPlayers.getPlayers(scenario.getGame().getId()).getRight();

		if (ai.getHomePlanet().getPlayer() != ai) {
			finishScenario(scenario);
		}
	}
}