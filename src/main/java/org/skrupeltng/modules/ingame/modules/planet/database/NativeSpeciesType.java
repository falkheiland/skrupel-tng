package org.skrupeltng.modules.ingame.modules.planet.database;

public enum NativeSpeciesType {

	HUMANOID(1),

	ENERGY(2),

	FORMLESS(3),

	PLASMANOID(4),

	AMPHIBIC(5),

	PARASITE(6),

	ARTIFICIAL(7);

	private final int id;

	private NativeSpeciesType(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
}