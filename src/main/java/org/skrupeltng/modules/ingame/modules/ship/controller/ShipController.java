package org.skrupeltng.modules.ingame.modules.ship.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetService;
import org.skrupeltng.modules.ingame.modules.planet.controller.NativeSpeciesDescription;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntry;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteResourceAction;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTactics;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.masterdata.database.Resource;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityConstants;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.ShipModule;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/ingame/ship")
public class ShipController extends AbstractShipController {

	@Autowired
	private FleetService fleetService;

	@Autowired
	private PoliticsService politicsService;

	@GetMapping("")
	public String getShip(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);
		model.addAttribute("turnDone", turnDoneForShip(shipId) && !configProperties.isDisablePermissionChecks());
		model.addAttribute("fleets", fleetService.getPossibleFleetsForShip(shipId));
		return "ingame/ship/ship::content";
	}

	@GetMapping("/navigation")
	public String getNavigation(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);

		return getNavigation(ship, model);
	}

	@PostMapping("/navigation")
	@ResponseBody
	public ShipCourseSelectionResponse setCourse(@RequestParam long shipId, @RequestBody ShipCourseSelectionRequest request) {
		return shipService.setCourse(shipId, request);
	}

	@DeleteMapping("/navigation")
	@ResponseBody
	public void deleteCourse(@RequestParam long shipId) {
		shipService.deleteCourse(shipId);
	}

	@GetMapping("/transporter")
	public String getTransporter(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		Planet planet = shipService.getNearestPlanetInExtendedTransporterRange(ship);

		if (planet != null) {
			model.addAttribute("ship", ship);
			model.addAttribute("playerId", ship.getPlayer().getId());
			model.addAttribute("planet", planet);

			boolean isOwnPlanet = planet.getPlayer() != null && planet.getPlayer().getId() == ship.getPlayer().getId();
			model.addAttribute("isOwnPlanet", isOwnPlanet);

			model.addAttribute("isInfantryTransporter", ship.getAbility(ShipAbilityType.INFANTRY_TRANSPORT).isPresent());

			return "ingame/ship/transporter::content";
		}

		return "ingame/ship/transporter::cannot-transport";
	}

	@PostMapping("/transporter")
	public String transport(@RequestParam long shipId, @RequestBody ShipTransportRequest request) {
		shipService.transport(shipId, request);
		return "ingame/ship/transporter::transport-successfull";
	}

	@GetMapping("/scanner")
	public String getScannerOverview(@RequestParam long shipId, Model model) {
		List<Ship> ships = shipService.getScannedShips(shipId);
		List<Planet> planets = shipService.getScannedPlanets(shipId);

		if (ships.isEmpty() && planets.isEmpty()) {
			return "ingame/ship/scanner::no-objects";
		}

		model.addAttribute("scanner", true);
		model.addAttribute("ships", ships);
		model.addAttribute("planets", planets);

		NativeSpeciesDescription description = new NativeSpeciesDescription(messageSource);
		model.addAttribute("nativeSpeciesDescription", description);

		return "ingame/ship/scanner::content";
	}

	@GetMapping("/scanner/ship")
	public String getScannedShipDetails(@RequestParam long shipId, @RequestParam long scannedShipId, Model model) {
		Ship ship = shipService.getScannedShip(shipId, scannedShipId);
		model.addAttribute("ship", ship);
		return "ingame/ship/scanner::ship";
	}

	@GetMapping("/scanner/planet")
	public String getScannedPlanetDetails(@RequestParam long shipId, @RequestParam long scannedPlanetId, Model model) {
		Planet planet = shipService.getScannedPlanet(shipId, scannedPlanetId);
		model.addAttribute("planet", planet);
		return "ingame/ship/scanner::planet";
	}

	@GetMapping("/logbook")
	public String getLogbook(@RequestParam long shipId, Model model) {
		model.addAttribute("logbook", shipService.getLogbook(shipId));
		return "ingame/logbook::content";
	}

	@PostMapping("/logbook")
	public String changeLogbook(@RequestParam long shipId, @RequestParam String logbook) {
		shipService.changeLogbook(shipId, logbook);
		return "ingame/logbook::success";
	}

	@GetMapping("/ship-template")
	public String shipTemplateDetails(@RequestParam String shipTemplateId, Model model) {
		ShipTemplate shipTemplate = masterDataService.getShipTemplate(shipTemplateId);
		model.addAttribute("template", shipTemplate);

		List<ShipAbilityDescription> descriptions = shipTemplate.getShipAbilities().stream().map(shipAbilityDescriptionMapper::mapShipAbilityDescription)
				.collect(Collectors.toList());
		model.addAttribute("abilityDescriptions", descriptions);
		model.addAttribute("modal", true);

		return "ship-template-details::content";
	}

	@GetMapping("/task")
	public String getTask(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);

		Game game = ship.getPlayer().getGame();
		model.addAttribute("mineFieldsEnabled", game.isEnableMineFields());

		long loginId = userDetailService.getLoginId();
		List<Ship> allShips = shipService.getShipsOfLogin(game.getId(), loginId, null, null);
		allShips.remove(ship);
		model.addAttribute("allShips", allShips);

		boolean escorting = ship.getDestinationShip() != null && allShips.contains(ship.getDestinationShip());

		if (escorting) {
			model.addAttribute("escortTarget", ship.getDestinationShip());
		}

		ShipAbility activeAbility = ship.getActiveAbility();

		if (activeAbility != null) {
			model.addAttribute("activeAbility", activeAbility.getType());
		}

		List<Ship> towableShips = allShips.stream().filter(s -> ship.canTowShip(s)).collect(Collectors.toList());
		model.addAttribute("towableShips", towableShips);
		model.addAttribute("showTractorBeam", !towableShips.isEmpty() && masterDataService.hasTractorBeam(ship.getPropulsionSystemTemplate().getTechLevel()));

		if (ship.getAbility(ShipAbilityType.SIGNATURE_MASK).isPresent()) {
			model.addAttribute("players", game.getPlayers());
		}

		if (ship.getAbility(ShipAbilityType.OVERDRIVE).isPresent()) {
			int min = ship.getAbilityValue(ShipAbilityType.OVERDRIVE, ShipAbilityConstants.OVERDRIVE_MIN).get().intValue();
			int max = ship.getAbilityValue(ShipAbilityType.OVERDRIVE, ShipAbilityConstants.OVERDRIVE_MAX).get().intValue();

			List<Integer> overDriveSteps = new ArrayList<>();

			for (int step = min; step <= max; step += 10) {
				overDriveSteps.add(step);
			}

			model.addAttribute("overDriveSteps", overDriveSteps);
		}

		model.addAttribute("hasAutoDestruction", ship.getShipModule() == ShipModule.AUTO_DESTRUCTION);

		Planet planet = ship.getPlanet();
		ShipTemplate shipTemplate = ship.getShipTemplate();

		if (planet != null && planet.hasOrbitalSystem(OrbitalSystemType.HUNTER_ACADEMY) && shipTemplate.getMass() < 100) {
			Starbase starbase = planet.getStarbase();

			if (starbase != null && starbase.getType() == StarbaseType.BATTLE_STATION && ship.getExperience() < 5 && ship.getTravelSpeed() == 0) {
				model.addAttribute("canTrain", true);
			}
		}

		Map<Long, ShipAbilityDescription> descriptions = shipTemplate.getShipAbilities().stream().map(shipAbilityDescriptionMapper::mapShipAbilityDescription)
				.collect(Collectors.toMap(s -> s.getAbilityId(), s -> s));
		model.addAttribute("abilityDescriptions", descriptions);

		return "ingame/ship/task::content";
	}

	@PostMapping("/task")
	public String changeTask(@RequestBody ShipTaskChangeRequest request) {
		shipService.changeTask(request.getShipId(), request);
		return "ingame/ship/task::success";
	}

	@GetMapping("/travel-data")
	@ResponseBody
	public ShipTravelData getShipTravelData(@RequestParam long shipId) {
		Ship ship = shipService.getShip(shipId);
		return new ShipTravelData(ship);
	}

	@GetMapping("/route-data")
	public String getShipRouteData(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShipWithRoutesFetched(shipId);
		model.addAttribute("ship", ship);
		return "ingame/galaxymap/galaxy-map::ship-routes";
	}

	@GetMapping("/tactics")
	public String getTactics(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);

		model.addAttribute("ship", ship);
		model.addAttribute("tacticsList", ShipTactics.values());

		boolean enableTactialCombat = ship.getPlayer().getGame().isEnableTactialCombat();
		model.addAttribute("enableTactialCombat", enableTactialCombat);

		Locale locale = LocaleContextHolder.getLocale();
		String helpText = messageSource.getMessage("help_ship_aggressiveness", null, locale);

		if (enableTactialCombat) {
			helpText += "<br/><br/>";
			helpText += messageSource.getMessage("help_ship_tactics", null, locale);
		}

		model.addAttribute("helpText", helpText);

		return "ingame/ship/tactics::content";
	}

	@PostMapping("/tactics")
	public String changeTactics(@RequestBody ShipTacticsChangeRequest request) {
		shipService.changeTactics(request.getShipId(), request);
		return "ingame/ship/tactics::success";
	}

	@GetMapping("/storage")
	public String getStorage(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);
		return "ingame/ship/storage::content";
	}

	@GetMapping("/weapon-systems")
	public String getWeaponSystems(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);
		return "ingame/ship/weapon-systems::content";
	}

	@GetMapping("/propulsion-system")
	public String getPropulsionSystem(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);
		return "ingame/ship/propulsion-system::content";
	}

	@GetMapping("/projectiles")
	public String getProjectiles(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);

		boolean canAutoBuildProjectiles = shipService.getBuildableProjectiles(shipId) > 0;
		model.addAttribute("canAutoBuildProjectiles", canAutoBuildProjectiles);

		return "ingame/ship/projectiles::content";
	}

	@PostMapping("/projectiles")
	@ResponseBody
	public void buildProjectiles(@RequestParam long shipId, @RequestParam int quantity) {
		shipService.buildProjectiles(shipId, quantity);
	}

	@PostMapping("/automated-projectiles")
	@ResponseBody
	public void toggleAutoBuildProjectiles(@RequestParam long shipId) {
		shipService.toggleAutoBuildProjectiles(shipId);
	}

	@PostMapping("/projectiles-auto")
	@ResponseBody
	public void buildAllPossibleProjectilesAutomatically(@RequestParam long shipId) {
		shipService.buildAllPossibleProjectilesAutomatically(shipId);
	}

	@GetMapping("/stats-details")
	public String getStatsDetails(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);
		model.addAttribute("fleets", fleetService.getPossibleFleetsForShip(shipId));
		return "ingame/ship/ship::stats-details";
	}

	@GetMapping("/route")
	public String getRoute(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);

		if (ship.getTravelSpeed() == 0) {
			ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
		}

		model.addAttribute("ship", ship);
		List<Planet> planets = new ArrayList<>(ship.getPlayer().getPlanets());
		Collections.sort(planets, Comparator.comparing(Planet::getName));
		model.addAttribute("planets", planets);

		List<ShipRouteEntry> entries = shipService.getRoute(shipId);
		model.addAttribute("entries", entries);

		model.addAttribute("actions", ShipRouteResourceAction.values());
		model.addAttribute("resources", Resource.values());

		return "ingame/ship/route::content";
	}

	@PutMapping("/route-entry")
	public String addRouteEntry(@RequestParam long shipId, @RequestParam long planetId, @RequestParam String action, Model model) {
		shipService.addRouteEntry(shipId, planetId, ShipRouteResourceAction.valueOf(action));
		return getRoute(shipId, model);
	}

	@PostMapping("/route-entry-action")
	@ResponseBody
	public void changeRouteEntryAction(@RequestParam long entryId, @RequestParam String type, @RequestParam String action) {
		shipService.changeRouteEntryAction(entryId, Resource.valueOf(type), ShipRouteResourceAction.valueOf(action));
	}

	@PostMapping("/route-entry-planet")
	@ResponseBody
	public void changeRouteEntryPlanet(@RequestParam long entryId, @RequestParam long planetId) {
		shipService.changeRouteEntryPlanet(entryId, planetId);
	}

	@PostMapping("/route-entry-wait-for-full-storage")
	@ResponseBody
	public void toggleWaitForFullStorage(@RequestParam long entryId) {
		shipService.toggleWaitForFullStorage(entryId);
	}

	@PostMapping("/move-route-entry-up")
	public String moveRouteEntryUp(@RequestParam long entryId, Model model) {
		long shipId = shipService.moveRouteEntryUp(entryId);
		return getRoute(shipId, model);
	}

	@PostMapping("/move-route-entry-down")
	public String moveRouteEntryDown(@RequestParam long entryId, Model model) {
		long shipId = shipService.moveRouteEntryDown(entryId);
		return getRoute(shipId, model);
	}

	@DeleteMapping("/route-entry")
	public String deleteRouteEntry(@RequestParam long entryId, Model model) {
		long shipId = shipService.deleteRouteEntry(entryId);
		return getRoute(shipId, model);
	}

	@PostMapping("/route-travel-speed")
	@ResponseBody
	public void setRouteTravelSpeed(@RequestParam long shipId, @RequestParam int travelSpeed) {
		shipService.setRouteTravelSpeed(shipId, travelSpeed);
	}

	@PostMapping("/route-disabled")
	@ResponseBody
	public void setRouteDisabled(@RequestParam long shipId, @RequestParam boolean disabled) {
		shipService.setRouteDisabled(shipId, disabled);
	}

	@PostMapping("/route-primary-resource")
	@ResponseBody
	public void setRoutePrimaryResource(@RequestParam long shipId, @RequestParam String primaryResource) {
		shipService.setRouteTravelSpeed(shipId, Resource.valueOf(primaryResource));
	}

	@PostMapping("/route-min-fuel")
	@ResponseBody
	public void setRouteMinFuel(@RequestParam long shipId, @RequestParam int minFuel) {
		shipService.setRouteMinFuel(shipId, minFuel);
	}

	@GetMapping("/options")
	public String getOptions(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);

		List<Player> allies = politicsService.getPlayersWithRelation(ship.getPlayer().getId(), PlayerRelationType.ALLIANCE);
		model.addAttribute("allies", allies);

		return "ingame/ship/settings::content";
	}

	@PostMapping("/options")
	public String changeOptions(@RequestParam long shipId, @RequestBody ShipOptionsChangeRequest request) {
		shipService.changeOptions(shipId, request);
		return "ingame/ship/settings::name-changed";
	}
}