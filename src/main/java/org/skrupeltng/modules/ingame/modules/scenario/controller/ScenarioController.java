package org.skrupeltng.modules.ingame.modules.scenario.controller;

import java.util.List;
import java.util.Optional;

import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.dashboard.service.TutorialService;
import org.skrupeltng.modules.ingame.modules.scenario.database.ScenarioType;
import org.skrupeltng.modules.ingame.modules.scenario.service.ScenarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ScenarioController extends AbstractController {

	@Autowired
	private ScenarioService scenarioService;

	@Autowired
	private TutorialService tutorialService;

	@GetMapping("/scenario-overview")
	public String getScenarioOverview(Model model) {
		long loginId = userDetailService.getLoginId();

		List<ScenarioOverviewItem> items = scenarioService.getOverviewItems(loginId);
		model.addAttribute("items", items);

		ScenarioType maxFinishedType = null;

		for (ScenarioOverviewItem item : items) {
			if (item.isFinished()) {
				maxFinishedType = item.getType();
			}
		}

		model.addAttribute("maxFinishedType", maxFinishedType);
		model.addAttribute("tutorialFinished", tutorialService.tutorialFinished());
		model.addAttribute("hideScenarioTutorialHint", scenarioService.isHideScenarioTutorialHint(loginId));

		Optional<ScenarioType> finishedScenarioType = scenarioService.deleteFinishedScenario(loginId);

		if (finishedScenarioType.isPresent()) {
			model.addAttribute("finishedScenarioType", finishedScenarioType.get());
		}

		return "dashboard/scenario-overview";
	}

	@PostMapping("/hide-scenario-tutorial-hint")
	@ResponseBody
	public void hideScenarioTutorialHint() {
		scenarioService.hideScenarioTutorialHint(userDetailService.getLoginId());
	}

	@PostMapping("/scenario-stage-seen")
	@ResponseBody
	public void markScenarioStageAsSeen(@RequestParam long gameId) {
		scenarioService.markScenarioStageAsSeen(gameId);
	}

	@PostMapping("/scenario")
	@ResponseBody
	public long startScenario(@RequestParam String type) {
		return scenarioService.startScenario(userDetailService.getLoginId(), ScenarioType.valueOf(type));
	}

	@PostMapping("/restart-scenario")
	@ResponseBody
	public long restartScenario(@RequestParam long gameId) {
		return scenarioService.restartScenario(userDetailService.getLoginId(), gameId);
	}
}