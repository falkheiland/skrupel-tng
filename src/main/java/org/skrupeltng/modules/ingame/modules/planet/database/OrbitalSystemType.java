package org.skrupeltng.modules.ingame.modules.planet.database;

public enum OrbitalSystemType {

	MEGA_FACTORY(true, 120, 17, 0, 20, 17, 13),

	EXO_REFINERY(true, 85, 32, 28, 35, 12, 8),

	BANK(true, 250, 10, 10, 36, 12, 12),

	SPY_CENTER(true, 2289, 191, 116, 179, 166, 138),

	CLOAKING_FIELD_GENERATOR(false, 350, 35, 58, 24, 77, 14),

	RECREATIONAL_PARK(true, 275, 156, 12, 50, 76, 43),

	BUNKER(true, 250, 100, 75, 75, 75, 75),

	ECHO_CENTER(true, 190, 13, 8, 25, 42, 33),

	METROPOLIS(true, 200, 25, 50, 20, 30, 20),

	MILITARY_BASE(true, 248, 70, 53, 80, 13, 65),

	PLANETARY_GUN_BATTERY(true, 2530, 117, 38, 230, 51, 77),

	ARMS_COMPLEX(true, 723, 206, 134, 177, 28, 186),

	PSY_CORPS(false, 210, 37, 61, 58, 48, 36),

	WEATHER_CONTROL_STATION(false, 0, 0, 0, 0, 0, 0),

	HUNTER_CONSTRUCTION_YARD(false, 151, 39, 17, 68, 57, 90),

	HUNTER_ACADEMY(false, 350, 20, 0, 75, 150, 100),

	RECYCLING_FACILITY(true, 127, 0, 25, 57, 15, 30),

	HOBAN_BUD(false, 75, 18, 23, 7, 36, 21),

	PLANETARY_SHIELDS(true, 250, 125, 50, 75, 150, 125),

	MEDICAL_CENTER(true, 250, 125, 50, 90, 137, 113),

	CLONE_FACTORY(false, 100, 25, 0, 30, 37, 13),

	BATTLE_SQUARE(false, 100, 25, 0, 15, 43, 31),

	RESERVATION(true, 50, 75, 7, 19, 26, 14),

	ORBITAL_EXTENSION(true, 2998, 549, 224, 293, 253, 268);

	private final boolean allowedByDefault;
	private final int money;
	private final int supplies;
	private final int fuel;
	private final int mineral1;
	private final int mineral2;
	private final int mineral3;

	private OrbitalSystemType(boolean allowedByDefault, int money, int supplies, int fuel, int mineral1, int mineral2, int mineral3) {
		this.allowedByDefault = allowedByDefault;
		this.money = money;
		this.supplies = supplies;
		this.fuel = fuel;
		this.mineral1 = mineral1;
		this.mineral2 = mineral2;
		this.mineral3 = mineral3;
	}

	public boolean isAllowedByDefault() {
		return allowedByDefault;
	}

	public int getMoney() {
		return money;
	}

	public int getSupplies() {
		return supplies;
	}

	public int getFuel() {
		return fuel;
	}

	public int getMineral1() {
		return mineral1;
	}

	public int getMineral2() {
		return mineral2;
	}

	public int getMineral3() {
		return mineral3;
	}

	public boolean canBeBuild(Planet planet) {
		if (planet.getMoney() < money) {
			return false;
		}

		if (planet.getSupplies() < supplies) {
			return false;
		}

		if (planet.getFuel() < fuel) {
			return false;
		}

		if (planet.getMineral1() < mineral1) {
			return false;
		}

		if (planet.getMineral2() < mineral2) {
			return false;
		}

		if (planet.getMineral3() < mineral3) {
			return false;
		}

		return true;
	}
}