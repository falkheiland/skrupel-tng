package org.skrupeltng.modules.ingame.modules.scenario.database;

public enum ScenarioType {

	EARLY_GAME,

	MID_GAME,

	LATE_GAME
}