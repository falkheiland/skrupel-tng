package org.skrupeltng.modules.ingame.modules.overview.service;

import java.time.Instant;
import java.util.Optional;

import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.overview.controller.PlayerMessageDTO;
import org.skrupeltng.modules.ingame.modules.overview.controller.PlayerMessageSearchRequestDTO;
import org.skrupeltng.modules.ingame.modules.overview.controller.SendPlayerMessageRequest;
import org.skrupeltng.modules.ingame.modules.overview.database.PlayerMessage;
import org.skrupeltng.modules.ingame.modules.overview.database.PlayerMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PlayerMessageService {

	@Autowired
	private PlayerMessageRepository playerMessageRepository;

	@Autowired
	private UserDetailServiceImpl userService;

	@Autowired
	private PlayerRepository playerRepository;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void sendMessage(SendPlayerMessageRequest request) {
		Optional<Player> recipientOpt = playerRepository.findById(request.getRecipientId());

		if (!recipientOpt.isPresent()) {
			throw new IllegalArgumentException("Recipient " + request.getRecipientId() + " not found!");
		}

		Player recipient = recipientOpt.get();

		if (recipient.getGame().getId() != request.getGameId()) {
			throw new IllegalArgumentException("Recipient " + request.getRecipientId() + " is not in game " + request.getGameId() + "!");
		}

		Player sender = playerRepository.findByGameIdAndLoginId(request.getGameId(), userService.getLoginId()).get();

		PlayerMessage message = new PlayerMessage();
		message.setTo(recipient);
		message.setFrom(sender);
		message.setDate(Instant.now());
		message.setSubject(request.getSubject());
		message.setMessage(request.getMessage());

		playerMessageRepository.save(message);
	}

	public long getUnreadMessageCount(long gameId) {
		return playerMessageRepository.getUnreadMessageCount(userService.getLoginId(), gameId);
	}

	public Page<PlayerMessageDTO> getMessages(PlayerMessageSearchRequestDTO request) {
		long loginId = userService.getLoginId();
		return playerMessageRepository.searchPlayerMessages(request, loginId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void markMessageAsRead(long messageId) {
		PlayerMessage message = getCheckedMessage(messageId);
		message.setRead(true);
		playerMessageRepository.save(message);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteMessage(long messageId) {
		PlayerMessage message = getCheckedMessage(messageId);
		playerMessageRepository.delete(message);
	}

	private PlayerMessage getCheckedMessage(long messageId) {
		long loginId = userService.getLoginId();

		PlayerMessage message = playerMessageRepository.getOne(messageId);

		if (message.getTo().getLogin().getId() != loginId) {
			throw new IllegalArgumentException("Cannot delete messages that are not sent to you!");
		}

		return message;
	}
}