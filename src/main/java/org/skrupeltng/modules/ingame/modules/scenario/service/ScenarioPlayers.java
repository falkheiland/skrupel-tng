package org.skrupeltng.modules.ingame.modules.scenario.service;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ScenarioPlayers {

	@Autowired
	private PlayerRepository playerRepository;

	public Pair<Player, Player> getPlayers(long gameId) {
		List<Player> players = playerRepository.findByGameId(gameId);

		Player player = null;
		Player ai = null;

		for (Player p : players) {
			if (p.getAiLevel() != null) {
				ai = p;
			} else {
				player = p;
			}
		}

		if (player == null || ai == null) {
			throw new IllegalStateException("Players not found!");
		}

		return Pair.of(player, ai);
	}
}