package org.skrupeltng.modules.ingame.modules.starbase.database;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.skrupeltng.modules.masterdata.database.ShipTemplate;

@Entity
@Table(name = "starbase_hull_stock")
public class StarbaseHullStock implements Serializable {

	private static final long serialVersionUID = -2214208442416385190L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Starbase starbase;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ship_template_id")
	private ShipTemplate shipTemplate;

	private int stock;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Starbase getStarbase() {
		return starbase;
	}

	public void setStarbase(Starbase starbase) {
		this.starbase = starbase;
	}

	public ShipTemplate getShipTemplate() {
		return shipTemplate;
	}

	public void setShipTemplate(ShipTemplate shipTemplate) {
		this.shipTemplate = shipTemplate;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}
}