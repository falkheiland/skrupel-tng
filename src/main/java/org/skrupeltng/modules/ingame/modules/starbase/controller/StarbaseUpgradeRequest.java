package org.skrupeltng.modules.ingame.modules.starbase.controller;

import java.io.Serializable;

public class StarbaseUpgradeRequest implements Serializable {

	private static final long serialVersionUID = 8901833446133072599L;

	private int hullLevel;
	private int propulsionLevel;
	private int energyLevel;
	private int projectileLevel;

	public int getHullLevel() {
		return hullLevel;
	}

	public void setHullLevel(int hullLevel) {
		this.hullLevel = hullLevel;
	}

	public int getPropulsionLevel() {
		return propulsionLevel;
	}

	public void setPropulsionLevel(int propulsionLevel) {
		this.propulsionLevel = propulsionLevel;
	}

	public int getEnergyLevel() {
		return energyLevel;
	}

	public void setEnergyLevel(int energyLevel) {
		this.energyLevel = energyLevel;
	}

	public int getProjectileLevel() {
		return projectileLevel;
	}

	public void setProjectileLevel(int projectileLevel) {
		this.projectileLevel = projectileLevel;
	}
}