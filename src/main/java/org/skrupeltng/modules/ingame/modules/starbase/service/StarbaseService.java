package org.skrupeltng.modules.ingame.modules.starbase.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;
import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.database.LoginStatsFaction;
import org.skrupeltng.modules.ingame.controller.StarbaseListResultDTO;
import org.skrupeltng.modules.ingame.controller.StarbaseOverviewRequest;
import org.skrupeltng.modules.ingame.modules.anomaly.database.SpaceFold;
import org.skrupeltng.modules.ingame.modules.anomaly.database.SpaceFoldRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpeciesEffect;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.starbase.controller.SpaceFoldRequest;
import org.skrupeltng.modules.ingame.modules.starbase.controller.StarbaseProductionRequest;
import org.skrupeltng.modules.ingame.modules.starbase.controller.StarbaseShipConstructionRequest;
import org.skrupeltng.modules.ingame.modules.starbase.controller.StarbaseUpgradeLevel;
import org.skrupeltng.modules.ingame.modules.starbase.controller.StarbaseUpgradeRequest;
import org.skrupeltng.modules.ingame.modules.starbase.database.AquiredShipTemplate;
import org.skrupeltng.modules.ingame.modules.starbase.database.AquiredShipTemplateRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseHullStock;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseHullStockRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbasePropulsionStock;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbasePropulsionStockRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseShipConstructionJob;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseShipConstructionJobRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseWeaponStock;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseWeaponStockRepository;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.skrupeltng.modules.ingame.service.round.combat.ShipDamage;
import org.skrupeltng.modules.masterdata.StarbaseProducable;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplateRepository;
import org.skrupeltng.modules.masterdata.database.ShipModule;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.database.ShipTemplateRepository;
import org.skrupeltng.modules.masterdata.database.WeaponTemplate;
import org.skrupeltng.modules.masterdata.database.WeaponTemplateRepository;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.skrupeltng.modules.masterdata.service.StarbaseProductionEntry;
import org.skrupeltng.modules.masterdata.service.StarbaseUpgradeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

@Service
public class StarbaseService {

	private static final Set<String> TUTORIAL_SHIP_TEMPLATE_IDS = Sets.newHashSet("orion_5", "orion_2", "orion_6", "orion_14", "orion_13");
	private static final Set<String> TUTORIAL_PROPULSION_SYSTEM_IDS = Sets.newHashSet("sun_sail", "linear_engine", "solarisplasmotan", "transwarp");

	@Autowired
	private StarbaseRepository starbaseRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private MasterDataService masterDataService;

	@Autowired
	private StarbaseHullStockRepository starbaseHullStockRepository;

	@Autowired
	private StarbasePropulsionStockRepository starbasePropulsionStockRepository;

	@Autowired
	private StarbaseWeaponStockRepository starbaseWeaponStockRepository;

	@Autowired
	private WeaponTemplateRepository weaponTemplateRepository;

	@Autowired
	private PropulsionSystemTemplateRepository propulsionSystemTemplateRepository;

	@Autowired
	private StarbaseShipConstructionJobRepository starbaseShipConstructionJobRepository;

	@Autowired
	private AquiredShipTemplateRepository aquiredShipTemplateRepository;

	@Autowired
	private ShipTemplateRepository shipTemplateRepository;

	@Autowired
	private SpaceFoldRepository spaceFoldRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private StatsUpdater statsUpdater;

	@PreAuthorize("hasPermission(#starbaseId, 'starbase')")
	public Starbase getStarbase(long starbaseId) {
		return starbaseRepository.getOne(starbaseId);
	}

	@PreAuthorize("hasPermission(#starbaseId, 'starbase') and hasPermission(#starbaseId, 'turnNotDoneStarbase')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void upgradeStarbase(long starbaseId, StarbaseUpgradeRequest request) {
		Starbase starbase = starbaseRepository.getOne(starbaseId);

		if (starbase.fullyUpgraded()) {
			return;
		}

		Planet planet = starbase.getPlanet();

		int hullLevel = request.getHullLevel();
		int propulsionLevel = request.getPropulsionLevel();
		int energyLevel = request.getEnergyLevel();
		int projectileLevel = request.getProjectileLevel();

		if (hullLevel > 0) {
			int finalLevel = upgradeStarbaseProperty(planet, starbase.getHullLevel(), hullLevel, StarbaseUpgradeType.HULL);
			starbase.setHullLevel(finalLevel);
		}

		if (propulsionLevel > 0) {
			int finalLevel = upgradeStarbaseProperty(planet, starbase.getPropulsionLevel(), propulsionLevel, StarbaseUpgradeType.PROPULSION);
			starbase.setPropulsionLevel(finalLevel);
		}

		if (energyLevel > 0) {
			int finalLevel = upgradeStarbaseProperty(planet, starbase.getEnergyLevel(), energyLevel, StarbaseUpgradeType.ENERGY);
			starbase.setEnergyLevel(finalLevel);
		}

		if (projectileLevel > 0) {
			int finalLevel = upgradeStarbaseProperty(planet, starbase.getProjectileLevel(), projectileLevel, StarbaseUpgradeType.PROJECTILE);
			starbase.setProjectileLevel(finalLevel);
		}

		planet = planetRepository.save(planet);
		starbase = starbaseRepository.save(starbase);

		if (starbase.fullyUpgraded()) {
			statsUpdater.incrementStats(planet.getPlayer(), LoginStatsFaction::getStarbasesMaxed, LoginStatsFaction::setStarbasesMaxed,
					AchievementType.STARBASES_MAXED_1, AchievementType.STARBASES_MAXED_10);
		}
	}

	private int upgradeStarbaseProperty(Planet planet, int startLevel, int targetLevel, StarbaseUpgradeType type) {
		List<StarbaseUpgradeLevel> list = masterDataService.getLevelCosts(startLevel, type, planet.getMoney());

		int finalLevel = startLevel;
		int moneyToBeSubtracted = 0;

		for (int i = finalLevel + 1; i <= targetLevel; i++) {
			int index = i - startLevel - 1;

			if (index < list.size()) {
				StarbaseUpgradeLevel level = list.get(index);

				if (planet.getMoney() >= level.getMoney()) {
					finalLevel++;
					moneyToBeSubtracted = level.getMoney();
				} else {
					break;
				}
			} else {
				break;
			}
		}

		if (moneyToBeSubtracted > 0) {
			planet.setMoney(planet.getMoney() - moneyToBeSubtracted);
		}

		return finalLevel;
	}

	@PreAuthorize("hasPermission(#starbaseId, 'starbase') and hasPermission(#starbaseId, 'turnNotDoneStarbase')")
	public List<StarbaseProductionEntry> getStarbaseProductionData(long starbaseId, StarbaseUpgradeType type) {
		Starbase starbase = starbaseRepository.getOne(starbaseId);

		switch (type) {
			case HULL:
				return getHullProductionData(starbase);
			case PROPULSION:
				return getPropulsionProductionData(starbase);
			case ENERGY:
				return getWeaponProductionData(starbase, StarbaseUpgradeType.ENERGY);
			case PROJECTILE:
				return getWeaponProductionData(starbase, StarbaseUpgradeType.PROJECTILE);
		}

		return null;
	}

	private List<StarbaseProductionEntry> getHullProductionData(Starbase starbase) {
		Planet planet = starbase.getPlanet();
		Faction faction = planet.getPlayer().getFaction();
		int techLevel = starbase.getHullLevel();

		List<ShipTemplate> shipTemplates = faction.getShips();
		List<StarbaseProductionEntry> data = new ArrayList<>(shipTemplates.size());

		boolean tutorial = planet.getGame().isTutorial();

		addShipTemplates(starbase, techLevel, shipTemplates, data, tutorial);

		if (!tutorial && faction.getId().equals("kuatoh")) {
			List<AquiredShipTemplate> aquiredShipTemplates = aquiredShipTemplateRepository.findByGameId(planet.getGame().getId());
			addShipTemplates(starbase, techLevel, aquiredShipTemplates.stream().map(a -> a.getShipTemplate()).collect(Collectors.toList()), data, false);
		}

		return data;
	}

	private void addShipTemplates(Starbase starbase, int techLevel, List<ShipTemplate> shipTemplates, List<StarbaseProductionEntry> data, boolean isTutorial) {
		for (ShipTemplate shipTemplate : shipTemplates) {
			String templateId = shipTemplate.getId();

			if (isTutorial && !TUTORIAL_SHIP_TEMPLATE_IDS.contains(templateId)) {
				continue;
			}

			StarbaseProductionEntry entry = new StarbaseProductionEntry();
			data.add(entry);

			entry.setId(templateId);
			entry.setName(masterDataService.getShipTemplateName(shipTemplate));
			entry.setTechLevel(shipTemplate.getTechLevel());
			entry.setType(StarbaseUpgradeType.HULL);
			entry.setCostMoney(shipTemplate.getCostMoney());
			entry.setCostMineral1(shipTemplate.getCostMineral1());
			entry.setCostMineral2(shipTemplate.getCostMineral2());
			entry.setCostMineral3(shipTemplate.getCostMineral3());

			Optional<StarbaseHullStock> stock = starbaseHullStockRepository.findByShipTemplateIdAndStarbaseId(templateId, starbase.getId());

			if (stock.isPresent()) {
				entry.setStock(stock.get().getStock());
			}

			if (techLevel >= shipTemplate.getTechLevel()) {
				entry.setProducableQuantity(calculateProducableQuantity(starbase.getPlanet(), entry));
			}
		}
	}

	private List<StarbaseProductionEntry> getPropulsionProductionData(Starbase starbase) {
		Planet planet = starbase.getPlanet();
		List<PropulsionSystemTemplate> propulsionSystems = propulsionSystemTemplateRepository.findAll();
		int techLevel = starbase.getPropulsionLevel();

		List<StarbaseProductionEntry> data = new ArrayList<>(propulsionSystems.size());

		Optional<Float> opt = planet.retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.UNLIMITED_SUNSAILS);

		boolean isTutorial = planet.getGame().isTutorial();

		for (PropulsionSystemTemplate propulsionSystem : propulsionSystems) {
			String name = propulsionSystem.getName();

			if (isTutorial && !TUTORIAL_PROPULSION_SYSTEM_IDS.contains(name)) {
				continue;
			}

			StarbaseProductionEntry entry = new StarbaseProductionEntry();
			data.add(entry);

			entry.setId(name);
			entry.setName(name);
			entry.setTechLevel(propulsionSystem.getTechLevel());
			entry.setType(StarbaseUpgradeType.PROPULSION);
			entry.setCostMoney(propulsionSystem.getCostMoney());
			entry.setCostMineral1(propulsionSystem.getCostMineral1());
			entry.setCostMineral2(propulsionSystem.getCostMineral2());
			entry.setCostMineral3(propulsionSystem.getCostMineral3());

			Optional<StarbasePropulsionStock> stock = starbasePropulsionStockRepository
					.findByPropulsionSystemTemplateNameAndStarbaseId(name, starbase.getId());

			if (stock.isPresent()) {
				entry.setStock(stock.get().getStock());
			}

			if (techLevel >= propulsionSystem.getTechLevel()) {
				entry.setProducableQuantity(calculateProducableQuantity(planet, entry));
			}

			if (opt.isPresent() && name.equals("sun_sail")) {
				entry.setHasUnlimitedStock(true);
			}
		}

		return data;
	}

	private List<StarbaseProductionEntry> getWeaponProductionData(Starbase starbase, StarbaseUpgradeType type) {
		Planet planet = starbase.getPlanet();
		boolean usesProjectiles = type == StarbaseUpgradeType.PROJECTILE;
		List<WeaponTemplate> weapons = weaponTemplateRepository.findByUsesProjectiles(usesProjectiles);
		int techLevel = usesProjectiles ? starbase.getProjectileLevel() : starbase.getEnergyLevel();

		List<StarbaseProductionEntry> data = new ArrayList<>(weapons.size());

		for (WeaponTemplate weapon : weapons) {
			StarbaseProductionEntry entry = new StarbaseProductionEntry();
			data.add(entry);

			entry.setId(weapon.getName());
			entry.setName(weapon.getName());
			entry.setTechLevel(weapon.getTechLevel());
			entry.setType(type);
			entry.setCostMoney(weapon.getCostMoney());
			entry.setCostMineral1(weapon.getCostMineral1());
			entry.setCostMineral2(weapon.getCostMineral2());
			entry.setCostMineral3(weapon.getCostMineral3());

			if (usesProjectiles) {
				entry.setStandardDamage(ShipDamage.PROJECTILE_WEAPON_DAMAGE_DATA[weapon.getDamageIndex()]);
				entry.setStandardCrewDamage(ShipDamage.PROJECTILE_WEAPON_DAMAGE_CREW_DATA[weapon.getDamageIndex()]);
			} else {
				entry.setStandardDamage(ShipDamage.ENERGY_WEAPON_DAMAGE_DATA[weapon.getDamageIndex()]);
				entry.setStandardCrewDamage(ShipDamage.ENERGY_WEAPON_DAMAGE_CREW_DATA[weapon.getDamageIndex()]);
			}

			entry.setCaptureDamage(Math.round(entry.getStandardDamage() * ShipDamage.CAPTURE_DAMAGE_FACTOR));
			entry.setCaptureCrewDamage(Math.round(entry.getStandardCrewDamage() * ShipDamage.CAPTURE_DAMAGE_CREW_FACTOR));

			Optional<StarbaseWeaponStock> stock = starbaseWeaponStockRepository.findByWeaponTemplateNameAndStarbaseId(weapon.getName(), starbase.getId());

			if (stock.isPresent()) {
				entry.setStock(stock.get().getStock());
			}

			if (techLevel >= weapon.getTechLevel()) {
				entry.setProducableQuantity(calculateProducableQuantity(planet, entry));
			}
		}

		return data;
	}

	private int calculateProducableQuantity(Planet planet, StarbaseProductionEntry entry) {
		int costMoney = entry.getCostMoney();
		int costMineral1 = entry.getCostMineral1();
		int costMineral2 = entry.getCostMineral2();
		int costMineral3 = entry.getCostMineral3();

		int quantityMoney = costMoney > 0 ? planet.getMoney() / costMoney : 10;
		int quantityMin1 = costMineral1 > 0 ? planet.getMineral1() / costMineral1 : 10;
		int quantityMin2 = costMineral2 > 0 ? planet.getMineral2() / costMineral2 : 10;
		int quantityMin3 = costMineral3 > 0 ? planet.getMineral3() / costMineral3 : 10;

		List<Integer> list = Lists.newArrayList(10, quantityMoney, quantityMin1, quantityMin2, quantityMin3);
		return list.stream().sorted().findFirst().get();
	}

	@PreAuthorize("hasPermission(#starbaseId, 'starbase') and hasPermission(#starbaseId, 'turnNotDoneStarbase')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void produce(StarbaseProductionRequest request, long starbaseId) {
		produceWithoutPermissionCheck(request, starbaseId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void produceWithoutPermissionCheck(StarbaseProductionRequest request, long starbaseId) {
		if (request.getQuantity() <= 0) {
			throw new IllegalArgumentException("Quantity must be higher than zero!");
		}

		StarbaseUpgradeType type = StarbaseUpgradeType.valueOf(request.getType());
		Starbase starbase = starbaseRepository.getOne(starbaseId);

		switch (type) {
			case HULL:
				produceHull(request, starbase);
				break;
			case PROPULSION:
				producePropulsionSystem(request, starbase);
				break;
			case ENERGY:
				produceWeaponSystem(request, starbase);
				break;
			case PROJECTILE:
				produceWeaponSystem(request, starbase);
				break;
		}
	}

	private void produceHull(StarbaseProductionRequest request, Starbase starbase) {
		String shipTemplateId = request.getTemplateId();
		ShipTemplate shipTemplate = shipTemplateRepository.getOne(shipTemplateId);

		Planet planet = starbase.getPlanet();
		Faction faction = planet.getPlayer().getFaction();

		if (shipTemplate.getFaction() != faction && !(faction.getId().equals("kuatoh") && aquiredShipTemplateRepository.findByGameIdAndShipTemplateId(planet.getGame().getId(), shipTemplateId).isPresent())) {
			throw new IllegalArgumentException("ShipTemplate " + shipTemplateId + " does not belong to player!");
		}

		if (shipTemplate.getTechLevel() > starbase.getHullLevel()) {
			throw new IllegalArgumentException("Higher techlevel required!");
		}

		Optional<StarbaseHullStock> optional = starbaseHullStockRepository.findByShipTemplateIdAndStarbaseId(shipTemplateId, starbase.getId());
		StarbaseHullStock stock = optional.isPresent() ? optional.get() : new StarbaseHullStock();

		stock.setShipTemplate(shipTemplate);
		stock.setStarbase(starbase);
		stock.setStock(stock.getStock() + request.getQuantity());
		stock = starbaseHullStockRepository.save(stock);
		starbase.getHullStocks().add(stock);

		spendPlanetResources(planet, shipTemplate, request.getQuantity());
	}

	private void producePropulsionSystem(StarbaseProductionRequest request, Starbase starbase) {
		String templateId = request.getTemplateId();
		Optional<StarbasePropulsionStock> optional = starbasePropulsionStockRepository.findByPropulsionSystemTemplateNameAndStarbaseId(templateId,
				starbase.getId());
		StarbasePropulsionStock stock = optional.isPresent() ? optional.get() : new StarbasePropulsionStock();

		PropulsionSystemTemplate template = propulsionSystemTemplateRepository.getOne(templateId);

		if (template.getTechLevel() > starbase.getPropulsionLevel()) {
			throw new IllegalArgumentException("Higher techlevel required!");
		}

		stock.setPropulsionSystemTemplate(template);
		stock.setStarbase(starbase);
		stock.setStock(stock.getStock() + request.getQuantity());
		stock = starbasePropulsionStockRepository.save(stock);
		starbase.getPropulsionStocks().add(stock);

		spendPlanetResources(starbase.getPlanet(), template, request.getQuantity());
	}

	private void produceWeaponSystem(StarbaseProductionRequest request, Starbase starbase) {
		String templateId = request.getTemplateId();
		Optional<StarbaseWeaponStock> optional = starbaseWeaponStockRepository.findByWeaponTemplateNameAndStarbaseId(templateId, starbase.getId());
		StarbaseWeaponStock stock = optional.isPresent() ? optional.get() : new StarbaseWeaponStock();

		WeaponTemplate template = weaponTemplateRepository.getOne(templateId);

		int requiredTechLevel = template.isUsesProjectiles() ? starbase.getProjectileLevel() : starbase.getEnergyLevel();

		if (template.getTechLevel() > requiredTechLevel) {
			throw new IllegalArgumentException("Higher techlevel required!");
		}

		stock.setWeaponTemplate(template);
		stock.setStarbase(starbase);
		stock.setStock(stock.getStock() + request.getQuantity());
		stock = starbaseWeaponStockRepository.save(stock);
		starbase.getWeaponStocks().add(stock);

		spendPlanetResources(starbase.getPlanet(), template, request.getQuantity());
	}

	private void spendPlanetResources(Planet planet, StarbaseProducable producable, int quantity) {
		planet.spendMoney(producable.getCostMoney() * quantity);
		planet.spendMineral1(producable.getCostMineral1() * quantity);
		planet.spendMineral2(producable.getCostMineral2() * quantity);
		planet.spendMineral3(producable.getCostMineral3() * quantity);
	}

	@PreAuthorize("hasPermission(#starbaseId, 'starbase') and hasPermission(#starbaseId, 'turnNotDoneStarbase')")
	public boolean shipConstructionJobExists(long starbaseId) {
		Optional<StarbaseShipConstructionJob> existing = starbaseShipConstructionJobRepository.findByStarbaseId(starbaseId);
		return existing.isPresent();
	}

	@PreAuthorize("hasPermission(#starbaseId, 'starbase') and hasPermission(#starbaseId, 'turnNotDoneStarbase')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void addShipConstructionJob(long starbaseId, StarbaseShipConstructionRequest request) {
		addShipConstructionJobWithoutPermissionCheck(starbaseId, request);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void addShipConstructionJobWithoutPermissionCheck(long starbaseId, StarbaseShipConstructionRequest request) {
		if (shipConstructionJobExists(starbaseId)) {
			throw new IllegalArgumentException("Starbase " + starbaseId + " already has a ship construction job!");
		}

		Starbase starbase = starbaseRepository.getOne(starbaseId);

		if (starbase.getType() == StarbaseType.SHIP_YARD || starbase.getType() == StarbaseType.BATTLE_STATION) {
			throw new IllegalArgumentException("Starbase " + starbaseId + " cannot build ships!");
		}

		String shipModule = request.getShipModule();

		if (starbase.getType() != StarbaseType.WAR_BASE && shipModule != null) {
			throw new IllegalArgumentException("Starbase " + starbaseId + " is not a WarBase and thus cannot add ShipModules!");
		}

		PolicyFactory policy = new HtmlPolicyBuilder().toFactory();
		String name = policy.sanitize(request.getName());

		if (StringUtils.isBlank(name)) {
			throw new IllegalArgumentException("The ship name cannot be blank!");
		}

		ShipTemplate shipTemplate = getShipTemplate(starbaseId, request);
		PropulsionSystemTemplate propulsionSystemTemplate = getPropulsionTemplate(starbaseId, request, shipTemplate);

		StarbaseShipConstructionJob constructionJob = new StarbaseShipConstructionJob();
		constructionJob.setShipName(name);
		constructionJob.setStarbase(starbase);
		constructionJob.setShipTemplate(shipTemplate);
		constructionJob.setPropulsionSystemTemplate(propulsionSystemTemplate);

		if (StringUtils.isNotBlank(shipModule)) {
			constructionJob.setShipModule(ShipModule.valueOf(shipModule));
		}

		if (shipTemplate.getEnergyWeaponsCount() > 0) {
			StarbaseWeaponStock energyStock = getEnergyStock(starbaseId, request, shipTemplate);
			constructionJob.setEnergyWeaponTemplate(energyStock.getWeaponTemplate());
			energyStock.setStock(energyStock.getStock() - shipTemplate.getEnergyWeaponsCount());

			if (energyStock.getStock() == 0) {
				starbaseWeaponStockRepository.delete(energyStock);
			}
		}

		if (shipTemplate.getProjectileWeaponsCount() > 0) {
			StarbaseWeaponStock projectileStock = getProjectileStock(starbaseId, request, shipTemplate);
			constructionJob.setProjectileWeaponTemplate(projectileStock.getWeaponTemplate());
			projectileStock.setStock(projectileStock.getStock() - shipTemplate.getProjectileWeaponsCount());

			if (projectileStock.getStock() == 0) {
				starbaseWeaponStockRepository.delete(projectileStock);
			}
		}

		starbaseShipConstructionJobRepository.save(constructionJob);

		statsUpdater.incrementStats(starbase.getPlanet().getPlayer(), LoginStatsFaction::getShipsBuilt, LoginStatsFaction::setShipsBuilt,
				AchievementType.SHIPS_BUILT_1, AchievementType.SHIPS_BUILT_20, AchievementType.SHIPS_BUILT_100);
	}

	private StarbaseWeaponStock getProjectileStock(long starbaseId, StarbaseShipConstructionRequest request, ShipTemplate shipTemplate) {
		if (request.getProjectileStockId() == null) {
			throw new IllegalArgumentException("No projectile weapon stock was given!");
		}

		Optional<StarbaseWeaponStock> projectileResult = starbaseWeaponStockRepository.findById(request.getProjectileStockId());

		if (!projectileResult.isPresent()) {
			throw new IllegalArgumentException("Projectile weapon stock " + request.getProjectileStockId() + " not found!");
		}

		StarbaseWeaponStock projectileStock = projectileResult.get();

		if (projectileStock.getStock() < shipTemplate.getProjectileWeaponsCount()) {
			throw new IllegalArgumentException("Projectile weapon stock " + request.getProjectileStockId() + " not enough for ship!");
		}

		if (projectileStock.getStarbase().getId() != starbaseId) {
			throw new IllegalArgumentException(
					"Projectile weapon stock " + request.getProjectileStockId() + " does not belong to starbase " + starbaseId + "!");
		}
		return projectileStock;
	}

	private StarbaseWeaponStock getEnergyStock(long starbaseId, StarbaseShipConstructionRequest request, ShipTemplate shipTemplate) {
		if (request.getEnergyStockId() == null) {
			throw new IllegalArgumentException("No energy weapon stock was given!");
		}

		Optional<StarbaseWeaponStock> energyResult = starbaseWeaponStockRepository.findById(request.getEnergyStockId());

		if (!energyResult.isPresent()) {
			throw new IllegalArgumentException("Energy weapon stock " + request.getEnergyStockId() + " not found!");
		}

		StarbaseWeaponStock energyStock = energyResult.get();

		if (energyStock.getStock() < shipTemplate.getEnergyWeaponsCount()) {
			throw new IllegalArgumentException("Energy weapon stock " + request.getEnergyStockId() + " not enough for ship!");
		}

		if (energyStock.getStarbase().getId() != starbaseId) {
			throw new IllegalArgumentException("Energy weapon stock " + request.getEnergyStockId() + " does not belong to starbase " + starbaseId + "!");
		}
		return energyStock;
	}

	private ShipTemplate getShipTemplate(long starbaseId, StarbaseShipConstructionRequest request) {
		Optional<StarbaseHullStock> hullResult = starbaseHullStockRepository.findById(request.getHullStockId());

		if (!hullResult.isPresent()) {
			throw new IllegalArgumentException("Hull stock " + request.getHullStockId() + " not found!");
		}

		StarbaseHullStock hullStock = hullResult.get();

		if (hullStock.getStock() <= 0) {
			throw new IllegalArgumentException("Hull stock " + request.getHullStockId() + " has no stock!");
		}

		ShipTemplate shipTemplate = hullStock.getShipTemplate();

		if (hullStock.getStarbase().getId() != starbaseId) {
			throw new IllegalArgumentException("Hull stock " + request.getHullStockId() + " does not belong to starbase " + starbaseId + "!");
		}

		hullStock.setStock(hullStock.getStock() - 1);

		if (hullStock.getStock() == 0) {
			starbaseHullStockRepository.delete(hullStock);
		}
		return shipTemplate;
	}

	private PropulsionSystemTemplate getPropulsionTemplate(long starbaseId, StarbaseShipConstructionRequest request, ShipTemplate shipTemplate) {
		Optional<StarbasePropulsionStock> propulsionResult = starbasePropulsionStockRepository.findById(request.getPropulsionSystemStockId());

		if (!propulsionResult.isPresent()) {
			throw new IllegalArgumentException("Propulsion system stock " + request.getPropulsionSystemStockId() + " not found!");
		}

		StarbasePropulsionStock propulsionStock = propulsionResult.get();

		if (propulsionStock.getStock() < shipTemplate.getPropulsionSystemsCount()) {
			throw new IllegalArgumentException("Propulsion system stock " + request.getPropulsionSystemStockId() + " not enough for ship!");
		}

		if (propulsionStock.getStarbase().getId() != starbaseId) {
			throw new IllegalArgumentException(
					"Propulsion system stock " + request.getPropulsionSystemStockId() + " does not belong to starbase " + starbaseId + "!");
		}

		Optional<Float> sunSailsOpt = starbaseRepository.getOne(starbaseId).getPlanet()
				.retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.UNLIMITED_SUNSAILS);
		PropulsionSystemTemplate propulsionSystemTemplate = propulsionStock.getPropulsionSystemTemplate();

		if (!sunSailsOpt.isPresent() || !propulsionSystemTemplate.getName().equals("sun_sail")) {
			propulsionStock.setStock(propulsionStock.getStock() - shipTemplate.getPropulsionSystemsCount());

			if (propulsionStock.getStock() == 0) {
				starbasePropulsionStockRepository.delete(propulsionStock);
			}
		}

		return propulsionSystemTemplate;
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public List<Starbase> getStarbasesOfLogin(long gameId, long loginId) {
		return starbaseRepository.findByGameIdAndLoginId(gameId, loginId);
	}

	@PreAuthorize("hasPermission(#starbaseId, 'starbase')")
	public String getLogbook(long starbaseId) {
		Starbase starbase = starbaseRepository.getOne(starbaseId);
		return starbase.getLog();
	}

	@PreAuthorize("hasPermission(#starbaseId, 'starbase')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void changeLogbook(long starbaseId, String logbook) {
		Starbase starbase = starbaseRepository.getOne(starbaseId);
		starbase.setLog(logbook);
		starbaseRepository.save(starbase);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@PreAuthorize("hasPermission(#starbaseId, 'starbase') and hasPermission(#starbaseId, 'turnNotDoneStarbase')")
	public Starbase buildDefense(long starbaseId, int quantity) {
		return buildDefenseWithoutPermissionCheck(starbaseId, quantity);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Starbase buildDefenseWithoutPermissionCheck(long starbaseId, int quantity) {
		if (quantity <= 0) {
			throw new IllegalArgumentException("Quantity must be higher than zero!");
		}

		Starbase starbase = starbaseRepository.getOne(starbaseId);
		Planet planet = starbase.getPlanet();
		int moneyCosts = quantity * 10;

		if (starbase.retrieveMaxDefense() < starbase.getDefense() + quantity || planet.getMineral2() < quantity || moneyCosts > planet.getMoney()) {
			throw new RuntimeException("Cannot build " + quantity + " defense systems on starbase " + starbaseId + "!");
		}

		starbase.setDefense(starbase.getDefense() + quantity);
		planet.spendMineral2(quantity);
		planet.spendMoney(moneyCosts);
		return starbaseRepository.save(starbase);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@PreAuthorize("hasPermission(#starbaseId, 'starbase') and hasPermission(#starbaseId, 'turnNotDoneStarbase')")
	public void initSpaceFold(long starbaseId, SpaceFoldRequest request) {
		initSpaceFoldWithoutPermissionCheck(starbaseId, request);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void initSpaceFoldWithoutPermissionCheck(long starbaseId, SpaceFoldRequest request) {
		Starbase starbase = starbaseRepository.getOne(starbaseId);

		if (!starbase.canSendSpaceFolds()) {
			throw new IllegalArgumentException("Starbase " + starbaseId + " cannot create space folds yet!");
		}

		if (request.getFuel() < 0 || request.getMoney() < 0 || request.getSupplies() < 0 || request.getMineral1() < 0 || request.getMineral2() < 0 || request.getMineral3() < 0 || request.getFuel() < 0) {
			throw new IllegalArgumentException("All values must be higher than zero!");
		}

		Planet planet = starbase.getPlanet();
		int costs = (request.getFuel() + request.getSupplies() + request.getMineral1() + request.getMineral2() + request.getMineral3()) * 8 + 75;

		if (costs > planet.getMoney() - request.getMoney()) {
			throw new IllegalArgumentException("Starbase " + starbaseId + " has not enough money for this space fold!");
		}

		Long shipId = request.getShipId();
		Long planetId = request.getPlanetId();

		if (shipId == null && planetId == null) {
			throw new IllegalArgumentException("No shipId and no planetId were given!");
		}

		SpaceFold spaceFold = new SpaceFold();
		spaceFold.setGame(planet.getPlayer().getGame());
		spaceFold.setX(planet.getX());
		spaceFold.setY(planet.getY());

		if (shipId != null) {
			Ship ship = shipRepository.getOne(shipId);

			if (ship == null) {
				throw new IllegalArgumentException("Ship " + shipId + " does not exist!");
			}

			spaceFold.setShip(ship);
		} else {
			Planet targetPlanet = planetRepository.getOne(planetId);

			if (targetPlanet == null) {
				throw new IllegalArgumentException("Planet " + planetId + " does not exist!");
			}

			spaceFold.setPlanet(targetPlanet);
		}

		planet.spendMoney(request.getMoney() + costs);
		planet.spendSupplies(request.getSupplies());
		planet.spendFuel(request.getFuel());
		planet.spendMineral1(request.getMineral1());
		planet.spendMineral2(request.getMineral2());
		planet.spendMineral3(request.getMineral3());
		planetRepository.save(planet);

		spaceFold.setMoney(request.getMoney());
		spaceFold.setSupplies(request.getSupplies());
		spaceFold.setFuel(request.getFuel());
		spaceFold.setMineral1(request.getMineral1());
		spaceFold.setMineral2(request.getMineral2());
		spaceFold.setMineral3(request.getMineral3());
		spaceFoldRepository.save(spaceFold);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void delete(Starbase starbase) {
		long starbaseId = starbase.getId();
		starbaseHullStockRepository.deleteByStarbaseId(starbaseId);
		starbasePropulsionStockRepository.deleteByStarbaseId(starbaseId);
		starbaseShipConstructionJobRepository.deleteByStarbaseId(starbaseId);
		starbaseWeaponStockRepository.deleteByStarbaseId(starbaseId);

		starbaseRepository.delete(starbase);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public Page<StarbaseListResultDTO> getStarbasesOfLogin(long gameId, StarbaseOverviewRequest request, long loginId) {
		return starbaseRepository.searchStarbases(request, loginId);
	}
}