package org.skrupeltng.modules.ingame;

import org.skrupeltng.modules.masterdata.service.MasterDataService;

public interface Coordinate {

	int getX();

	int getY();

	int getScanRadius();

	default double getDistance(Coordinate other) {
		int diffX = getX() - other.getX();
		int diffY = getY() - other.getY();
		return getDistance(diffX, diffY);
	}

	static double getDistance(int x1, int y1, int x2, int y2) {
		int diffX = x1 - x2;
		int diffY = y1 - y2;
		return getDistance(diffX, diffY);
	}

	static double getDistance(int diffX, int diffY) {
		double distance = Math.sqrt((diffX * diffX) + (diffY * diffY));
		return distance;
	}

	default String getSectorString() {
		String letter = "" + (char)((getX() / 250) + 65);
		int line = (getY() / 250) + 1;
		return letter + line;
	}

	static CoordinateImpl getRandomNearCoordinates(int x, int y, int distance, int galaxySize) {
		double angle = Math.PI * 2 * MasterDataService.RANDOM.nextFloat();
		long deltaX = Math.round(distance * Math.sin(angle));
		long deltaY = Math.round(distance * Math.cos(angle));
		int x2 = (int)Math.max(0, Math.min(galaxySize, x + deltaX));
		int y2 = (int)Math.max(0, Math.min(galaxySize, y + deltaY));
		return new CoordinateImpl(x2, y2, 0);
	}
}