package org.skrupeltng.modules.ingame.database;

import java.util.List;
import java.util.Set;

import org.skrupeltng.modules.dashboard.GameListResult;
import org.skrupeltng.modules.dashboard.GameSearchParameters;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GameRepositoryCustom {

	Page<GameListResult> searchGames(GameSearchParameters params, Pageable page);

	Set<CoordinateImpl> getVisibilityCoordinates(long playerId);

	List<PlayerGameTurnInfoItem> getTurnInfos(long loginId, long excludedGameId);
}