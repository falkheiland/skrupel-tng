package org.skrupeltng.modules.ingame.database.player;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "player_death_foe")
public class PlayerDeathFoe implements Serializable, Comparable<PlayerDeathFoe> {

	private static final long serialVersionUID = 7327826670403555029L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Player player;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "death_foe_id")
	private Player deathFoe;

	public PlayerDeathFoe() {

	}

	public PlayerDeathFoe(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Player getDeathFoe() {
		return deathFoe;
	}

	public void setDeathFoe(Player deathFoe) {
		this.deathFoe = deathFoe;
	}

	@Override
	public int compareTo(PlayerDeathFoe o) {
		return Long.compare(id, o.id);
	}
}