package org.skrupeltng.modules.ingame.database.player;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

@Entity
@Table(name = "player")
public class Player implements Serializable, Comparable<Player> {

	private static final long serialVersionUID = -1384915608373116149L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "turn_finished")
	private boolean turnFinished;

	private String color;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "faction_id")
	private Faction faction;

	@ManyToOne(fetch = FetchType.LAZY)
	private Game game;

	@ManyToOne(fetch = FetchType.LAZY)
	private Login login;

	@Enumerated(EnumType.STRING)
	@Column(name = "ai_level")
	private AILevel aiLevel;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "home_planet_id")
	private Planet homePlanet;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "player")
	private Set<Planet> planets;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "player")
	private Set<Ship> ships;

	@Column(name = "has_lost")
	private boolean hasLost;

	@Column(name = "overview_viewed")
	private boolean overviewViewed;

	@Column(name = "news_viewed")
	private boolean newsViewed;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "player")
	private Set<PlayerDeathFoe> deathFoes;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "player")
	private List<Fleet> fleets;

	public Player() {

	}

	public Player(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Player [id=" + id + ", login=" + login + "]";
	}

	public Player(Game game, Login login) {
		this.game = game;
		this.login = login;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Faction getFaction() {
		return faction;
	}

	public void setFaction(Faction faction) {
		this.faction = faction;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public boolean isTurnFinished() {
		return turnFinished;
	}

	public void setTurnFinished(boolean turnFinished) {
		this.turnFinished = turnFinished;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public AILevel getAiLevel() {
		return aiLevel;
	}

	public void setAiLevel(AILevel aiLevel) {
		this.aiLevel = aiLevel;
	}

	public Planet getHomePlanet() {
		return homePlanet;
	}

	public void setHomePlanet(Planet homePlanet) {
		this.homePlanet = homePlanet;
	}

	public Set<Planet> getPlanets() {
		return planets;
	}

	public void setPlanets(Set<Planet> planets) {
		this.planets = planets;
	}

	public Set<Ship> getShips() {
		return ships;
	}

	public void setShips(Set<Ship> ships) {
		this.ships = ships;
	}

	public boolean isHasLost() {
		return hasLost;
	}

	public void setHasLost(boolean hasLost) {
		this.hasLost = hasLost;
	}

	public boolean isOverviewViewed() {
		return overviewViewed;
	}

	public void setOverviewViewed(boolean overviewViewed) {
		this.overviewViewed = overviewViewed;
	}

	public boolean isNewsViewed() {
		return newsViewed;
	}

	public void setNewsViewed(boolean newsViewed) {
		this.newsViewed = newsViewed;
	}

	public Set<PlayerDeathFoe> getDeathFoes() {
		return deathFoes;
	}

	public void setDeathFoes(Set<PlayerDeathFoe> deathFoes) {
		this.deathFoes = deathFoes;
	}

	public List<Fleet> getFleets() {
		return fleets;
	}

	public void setFleets(List<Fleet> fleets) {
		this.fleets = fleets;
	}

	public String retrieveDisplayName(MessageSource messageSource) {
		if (aiLevel != null) {
			return messageSource.getMessage(aiLevel.name(), null, LocaleContextHolder.getLocale());
		}

		return login.getUsername();
	}

	public String retrieveDisplayNameWithColor(MessageSource messageSource) {
		String name = retrieveDisplayName(messageSource);
		return "<span class=\"player-name\" style=\"color: #" + color + " ;\">" + name + "</span>";
	}

	@Override
	public int compareTo(Player o) {
		return Long.compare(id, o.getId());
	}
}