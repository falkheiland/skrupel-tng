package org.skrupeltng.modules.ingame.database.player;

import java.util.List;
import java.util.Optional;

import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PlayerRepository extends JpaRepository<Player, Long>, PlayerRepositoryCustom {

	Optional<Player> findByGameIdAndLoginId(long gameId, long loginId);

	@Query("SELECT p FROM Player p WHERE p.aiLevel IS NOT NULL AND p.game.id = ?1 ORDER BY p.id ASC")
	List<Player> findAIPlayers(long gameId);

	@Query("SELECT p.id FROM Player p WHERE p.login.id = ?1 AND p.game.id = ?2")
	long getIdByLoginIdAndGameId(long loginId, long gameId);

	@Query("SELECT p FROM Player p WHERE p.login.id = ?1 AND p.game.id = ?2")
	Player findByLoginIdAndGameId(long loginId, long gameId);

	@Query("SELECT p FROM Player p WHERE p.game.id = ?1 AND p.hasLost = false")
	List<Player> getNotLostPlayers(long gameId);

	@Query("SELECT p FROM Player p WHERE p.game.id = ?1 AND p.hasLost = true")
	List<Player> getLostPlayers(long gameId);

	@Query("SELECT p FROM Player p WHERE p.game.id = ?1")
	List<Player> findByGameId(long gameId);

	@Query("SELECT COUNT(p.id) FROM Player p WHERE p.game.id = ?1")
	long getPlayerCount(long id);

	@Query("SELECT p.homePlanet FROM Player p WHERE p.id = ?1")
	Planet getHomePlanet(long playerId);

	@Query("SELECT p.game.id FROM Player p WHERE p.id = ?1")
	long getGameIdByPlayerId(long playerId);

	@Query("SELECT p.color FROM Player p WHERE p.id = ?1")
	String getPlayerColor(long playerId);
}