package org.skrupeltng.modules.ingame.database;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface TutorialRepository extends JpaRepository<Tutorial, Long> {

	Optional<Tutorial> findByLoginId(long loginId);

	@Modifying
	@Query("DELETE FROM Tutorial t WHERE t.game.id = ?1")
	void deleteByGameId(long gameId);

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Modifying
	@Query("UPDATE " +
			"	Player p " +
			"SET " +
			"	p.overviewViewed = true " +
			"WHERE " +
			"	p.login.id IN (" +
			"		SELECT " +
			"			t.login.id " +
			"		FROM " +
			"			Tutorial t " +
			"		WHERE " +
			"			t.id = ?1 " +
			"	)")
	void resetPlayerOverviewViewed(long tutorialId);

	@Modifying
	@Query("DELETE FROM Tutorial t WHERE t.login.id = ?1")
	void deleteByLoginId(long loginId);
}