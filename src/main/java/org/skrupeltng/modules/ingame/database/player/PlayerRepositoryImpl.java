package org.skrupeltng.modules.ingame.database.player;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.skrupeltng.modules.RepositoryCustomBase;
import org.skrupeltng.modules.ingame.modules.overview.service.PlayerSummaryEntry;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class PlayerRepositoryImpl extends RepositoryCustomBase implements PlayerRepositoryCustom {

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void setupTurnValues(long gameId) {
		String sql = "UPDATE player SET overview_viewed = false, news_viewed = false, turn_finished = ai_level IS NOT NULL WHERE game_id = :gameId";
		Map<String, Object> params = new HashMap<>(1);
		params.put("gameId", gameId);
		jdbcTemplate.update(sql, params);
	}

	@Override
	public List<PlayerSummaryEntry> getSummaries(long gameId) {
		String sql = "" +
				"SELECT \n" +
				"	p.id as \"playerId\", \n" +
				"	l.username as \"name\", \n" +
				"	r.id as \"factionId\", \n" +
				"	p.color as \"color\", \n" +
				"	p.ai_level IS NOT NULL as \"ai\", \n" +
				"	COUNT(DISTINCT sb.id) as \"starbaseCount\", \n" +
				"	COUNT(DISTINCT pl.id) as \"planetCount\", \n" +
				"	COUNT(DISTINCT s.id) as \"shipCount\" \n" +
				"FROM \n" +
				"	player p \n" +
				"	INNER JOIN login l \n" +
				"		ON l.id = p.login_id AND p.game_id = :gameId \n" +
				"	INNER JOIN faction r \n" +
				"		ON r.id = p.faction_id \n" +
				"	LEFT OUTER JOIN planet pl \n" +
				"		ON pl.player_id = p.id \n" +
				"	LEFT OUTER JOIN starbase sb \n" +
				"		ON sb.id = pl.starbase_id \n" +
				"	LEFT OUTER JOIN ship s \n" +
				"		ON s.player_id = p.id \n" +
				"GROUP BY \n" +
				"	p.id, \n" +
				"	l.username, \n" +
				"	p.ai_level, \n" +
				"	r.id, \n" +
				"	p.color";

		Map<String, Object> params = new HashMap<>(1);
		params.put("gameId", gameId);

		RowMapper<PlayerSummaryEntry> rowMapper = new BeanPropertyRowMapper<>(PlayerSummaryEntry.class);
		List<PlayerSummaryEntry> list = jdbcTemplate.query(sql, params, rowMapper);

		rankPlayers(list, PlayerSummaryEntry::getStarbaseCount, PlayerSummaryEntry::setStarbaseRank);
		rankPlayers(list, PlayerSummaryEntry::getPlanetCount, PlayerSummaryEntry::setPlanetRank);
		rankPlayers(list, PlayerSummaryEntry::getShipCount, PlayerSummaryEntry::setShipRank);
		rankPlayers(list, PlayerSummaryEntry::retrieveTotalCount, PlayerSummaryEntry::setRank);

		Collections.sort(list, Comparator.comparing(PlayerSummaryEntry::getRank));

		return list;
	}

	private void rankPlayers(List<PlayerSummaryEntry> list, Function<PlayerSummaryEntry, Integer> getter, BiConsumer<PlayerSummaryEntry, Integer> setter) {
		Collections.sort(list, Comparator.comparing(getter).reversed());
		int i = 1;
		int lastValue = getter.apply(list.get(0));

		for (PlayerSummaryEntry playerSummaryEntry : list) {
			Integer value = getter.apply(playerSummaryEntry);

			if (lastValue > value) {
				i++;
			}

			setter.accept(playerSummaryEntry, i);
			lastValue = value;
		}
	}

	@Override
	public boolean playersTurnNotDoneForPlanet(long planetId, long loginId) {
		String sql = "" +
				"SELECT \n" +
				"	pl.turn_finished \n" +
				"FROM \n" +
				"	player pl \n" +
				"	INNER JOIN planet p \n" +
				"		ON p.id = :planetId AND p.player_id = pl.id AND pl.login_id = :loginId";

		Map<String, Object> params = new HashMap<>(2);
		params.put("planetId", planetId);
		params.put("loginId", loginId);

		List<Boolean> results = jdbcTemplate.queryForList(sql, params, Boolean.class);

		if (results.size() == 1) {
			return !results.get(0);
		}

		return false;
	}

	@Override
	public boolean playersTurnNotDoneForShip(long shipId, long loginId) {
		String sql = "" +
				"SELECT \n" +
				"	pl.turn_finished \n" +
				"FROM \n" +
				"	player pl \n" +
				"	INNER JOIN ship s \n" +
				"		ON s.id = :shipId AND s.player_id = pl.id AND pl.login_id = :loginId";

		Map<String, Object> params = new HashMap<>(2);
		params.put("shipId", shipId);
		params.put("loginId", loginId);

		List<Boolean> results = jdbcTemplate.queryForList(sql, params, Boolean.class);

		if (results.size() == 1) {
			return !results.get(0);
		}

		return false;
	}

	@Override
	public boolean playersTurnNotDoneForStarbase(long starbaseId, long loginId) {
		String sql = "" +
				"SELECT \n" +
				"	pl.turn_finished \n" +
				"FROM \n" +
				"	planet p \n" +
				"	INNER JOIN player pl \n" +
				"		ON p.starbase_id = :starbaseId AND p.player_id = pl.id AND pl.login_id = :loginId";

		Map<String, Object> params = new HashMap<>(2);
		params.put("starbaseId", starbaseId);
		params.put("loginId", loginId);

		List<Boolean> results = jdbcTemplate.queryForList(sql, params, Boolean.class);

		if (results.size() == 1) {
			return !results.get(0);
		}

		return false;
	}

	@Override
	public boolean playersTurnNotDoneForRoute(long shipRouteEntryId, long loginId) {
		String sql = "" +
				"SELECT \n" +
				"	pl.turn_finished \n" +
				"FROM \n" +
				"	ship_route_entry e \n" +
				"	INNER JOIN ship s \n" +
				"		ON s.id = e.ship_id \n" +
				"	INNER JOIN player pl \n" +
				"		ON pl.id = s.player_id AND e.id = :shipRouteEntryId AND pl.login_id = :loginId";

		Map<String, Object> params = new HashMap<>(2);
		params.put("shipRouteEntryId", shipRouteEntryId);
		params.put("loginId", loginId);

		List<Boolean> results = jdbcTemplate.queryForList(sql, params, Boolean.class);

		if (results.size() == 1) {
			return !results.get(0);
		}

		return false;
	}

	@Override
	public boolean playersTurnNotDoneForOrbitalSystem(long orbitalSystemId, long loginId) {
		String sql = "" +
				"SELECT \n" +
				"	pl.turn_finished \n" +
				"FROM \n" +
				"	orbital_system o \n" +
				"	INNER JOIN planet p \n" +
				"		ON p.id = o.planet_id \n" +
				"	INNER JOIN player pl \n" +
				"		ON pl.id = p.player_id AND o.id = :orbitalSystemId AND pl.login_id = :loginId";

		Map<String, Object> params = new HashMap<>(2);
		params.put("orbitalSystemId", orbitalSystemId);
		params.put("loginId", loginId);

		List<Boolean> results = jdbcTemplate.queryForList(sql, params, Boolean.class);

		if (results.size() == 1) {
			return !results.get(0);
		}

		return false;
	}

	@Override
	public boolean playersTurnNotDoneForFleet(long fleetId, long loginId) {
		String sql = "" +
				"SELECT \n" +
				"	pl.turn_finished \n" +
				"FROM \n" +
				"	fleet f \n" +
				"	INNER JOIN player pl \n" +
				"		ON pl.id = f.player_id AND f.id = :fleetId AND pl.login_id = :loginId";

		Map<String, Object> params = new HashMap<>(2);
		params.put("fleetId", fleetId);
		params.put("loginId", loginId);

		List<Boolean> results = jdbcTemplate.queryForList(sql, params, Boolean.class);

		if (results.size() == 1) {
			return !results.get(0);
		}

		return false;
	}
}