package org.skrupeltng.modules.ingame.database;

import java.util.List;

import org.skrupeltng.modules.dashboard.database.Login;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface GameRepository extends JpaRepository<Game, Long>, GameRepositoryCustom {

	@Modifying
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	@Query("UPDATE Game g SET g.round = round + 1, g.roundDate = now(), g.turnNotFinishedNotificationsSent = false, g.lastToFinishTurnNotificationsSent = false WHERE g.id = ?1")
	void increaseRound(long gameId);

	@Query("SELECT p.color FROM Player p WHERE p.game.id = ?1")
	List<String> getPlayerColors(long gameId);

	@Modifying
	@Query("UPDATE Game g SET g.creator = NULL WHERE g.creator.id = ?1")
	void clearGameCreator(long loginId);

	@Query("SELECT p.login FROM Player p WHERE p.game.id = ?1 AND p.aiLevel IS NULL AND p.hasLost = false")
	List<Login> getLoginsByGameId(long gameId);

	@Query("SELECT g.galaxySize FROM Game g WHERE g.id = ?1")
	int getGalaxySize(long gameId);

	@Query("SELECT p.game FROM Player p WHERE p.id = ?1")
	Game getByPlayerId(long currentPlayerId);

	@Query("SELECT g.enableMineFields FROM Game g WHERE g.id = ?1")
	boolean mineFieldsEnabled(long gameId);

	@Query("SELECT g.tutorial FROM Game g WHERE g.id = ?1")
	boolean isTutorial(long gameId);

	@Query("SELECT g FROM Game g WHERE g.autoTickSeconds > 0 AND g.started = true AND g.finished = false AND g.roundDate IS NOT NULL")
	List<Game> findAutoTickGames();

	@Query("SELECT DISTINCT(g) FROM Game g INNER JOIN FETCH g.players p INNER JOIN FETCH p.login l WHERE g.turnNotFinishedNotificationSeconds > 0 AND g.turnNotFinishedNotificationsSent = false AND g.started = true AND g.finished = false AND g.roundDate IS NOT NULL")
	List<Game> findGamesWithTurnNotFinishedNotification();

	@Query("SELECT DISTINCT(g) FROM Game g INNER JOIN FETCH g.players p INNER JOIN FETCH p.login l WHERE g.lastToFinishTurnNotificationSeconds > 0 AND g.lastToFinishTurnNotificationsSent = false AND g.started = true AND g.finished = false AND g.roundDate IS NOT NULL")
	List<Game> findGamesWithLastToFinishTurnNotification();

	@Query("SELECT g.fogOfWarType FROM Game g WHERE g.id = ?1")
	FogOfWarType getFogOfWarType(long gameId);

	@Query("SELECT g.round FROM Player p INNER JOIN p.game g WHERE p.id = ?1")
	int getRoundByPlayerId(long playerId);

	@Query("SELECT g.name FROM Game g WHERE g.id = ?1")
	String getName(long gameId);

	@Modifying
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	@Query("UPDATE Game g SET g.turnNotFinishedNotificationsSent = ?2 WHERE id = ?1")
	void setTurnNotFinishedNotificationsSent(long gameId, boolean sent);

	@Modifying
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	@Query("UPDATE Game g SET g.lastToFinishTurnNotificationsSent = ?2 WHERE id = ?1")
	void setLastToFinishTurnNotificationsSent(long gameId, boolean sent);
}