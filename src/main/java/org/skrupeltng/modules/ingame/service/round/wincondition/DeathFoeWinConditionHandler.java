package org.skrupeltng.modules.ingame.service.round.wincondition;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerDeathFoeRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("DEATH_FOE")
public class DeathFoeWinConditionHandler implements WinConditionHandler {

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private PlayerDeathFoeRepository playerDeathFoeRepository;

	@Autowired
	private GameRepository gameRepository;

	@Override
	public void checkWinCondition(long gameId) {
		List<Player> losers = playerRepository.getLostPlayers(gameId);

		if (!losers.isEmpty()) {
			Set<Long> winnerIds = new HashSet<>();

			for (Player loser : losers) {
				Player winner = playerDeathFoeRepository.getPlayerByDeathFoe(loser.getId());
				winnerIds.add(winner.getId());
			}

			Game game = gameRepository.getOne(gameId);
			List<Player> allPlayers = game.getPlayers();

			for (Player player : allPlayers) {
				if (!winnerIds.contains(player.getId())) {
					player.setHasLost(true);
					playerRepository.save(player);
				}
			}

			game.setFinished(true);
			gameRepository.save(game);
		}
	}
}