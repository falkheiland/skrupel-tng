package org.skrupeltng.modules.ingame.service.round.wincondition;

import java.util.List;
import java.util.Map;

import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.database.LoginStatsFaction;
import org.skrupeltng.modules.dashboard.service.AchievementService;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class WinConditionCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private StatsUpdater statsUpdater;

	@Autowired
	private AchievementService achievementService;

	@Autowired
	private Map<String, WinConditionHandler> winConditionHandlers;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void checkWinCondition(long gameId) {
		log.debug("Processing win condition...");

		Game game = gameRepository.getOne(gameId);

		WinCondition winCondition = game.getWinCondition();
		WinConditionHandler handler = winConditionHandlers.get(winCondition.name());

		handler.checkWinCondition(gameId);

		game = gameRepository.getOne(gameId);

		if (game.isFinished()) {
			List<Player> players = game.getPlayers();

			for (Player player : players) {
				if (player.isHasLost()) {
					continue;
				}

				statsUpdater.incrementStats(player, LoginStatsFaction::getGamesWon, LoginStatsFaction::setGamesWon, AchievementType.GAMES_WON_1,
						AchievementType.GAMES_WON_10);

				try {
					String faction = player.getFaction().getId();
					AchievementType type = AchievementType.valueOf("GAMES_WON_" + faction.toUpperCase());
					achievementService.unlockAchievement(player, type);
				} catch (Exception e) {
					log.error("Error while creating win achievement: ", e);
				}
			}
		}

		log.debug("Win condition processed.");
	}
}