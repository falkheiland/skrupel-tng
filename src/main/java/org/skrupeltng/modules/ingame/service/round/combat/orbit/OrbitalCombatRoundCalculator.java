package org.skrupeltng.modules.ingame.service.round.combat.orbit;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class OrbitalCombatRoundCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private ShipService shipService;

	@Autowired
	private OrbitalCombat orbitalCombat;

	@Autowired
	private PoliticsService politicsService;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processCombat(long gameId) {
		log.debug("Processing orbital combat...");

		List<Ship> ships = shipRepository.getShipsOnEnemyPlanet(gameId);
		Set<Ship> destroyedShips = new HashSet<>(ships.size());

		Map<Long, Integer> totalPlanetaryDefenses = orbitalCombat.getTotalPlanetaryDefenses(ships);

		for (Ship ship : ships) {
			processShip(destroyedShips, ship, totalPlanetaryDefenses);
		}

		shipService.deleteShips(destroyedShips);

		log.debug("Orbital combat processed.");
	}

	protected void processShip(Set<Ship> destroyedShips, Ship ship, Map<Long, Integer> totalPlanetaryDefenses) {
		if (ship.getAbility(ShipAbilityType.ORBITAL_SHIELD).isPresent()) {
			return;
		}

		Planet planet = ship.getPlanet();

		if (politicsService.isAlly(ship.getPlayer().getId(), planet.getPlayer().getId())) {
			return;
		}

		long planetId = planet.getId();
		int totalPlanetaryDefense = totalPlanetaryDefenses.get(planetId);

		orbitalCombat.checkStructurScanner(ship);
		int damageToPlanet = orbitalCombat.processCombat(ship, totalPlanetaryDefense);

		orbitalCombat.handlePlanetDamage(planetId, totalPlanetaryDefenses, damageToPlanet);
		orbitalCombat.handleShipDamage(destroyedShips, ship);
	}
}