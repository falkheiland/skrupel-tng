package org.skrupeltng.modules.ingame.service.round.wincondition;

import org.springframework.stereotype.Component;

@Component("NONE")
public class NoneWinConditionHandler implements WinConditionHandler {

	@Override
	public void checkWinCondition(long gameId) {
		// nothing to do here
	}
}