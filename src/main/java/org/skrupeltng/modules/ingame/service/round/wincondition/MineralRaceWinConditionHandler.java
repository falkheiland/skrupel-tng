package org.skrupeltng.modules.ingame.service.round.wincondition;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.MineralRaceStorageType;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("MINERAL_RACE")
public class MineralRaceWinConditionHandler implements WinConditionHandler {

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private GameRepository gameRepository;

	@Override
	public void checkWinCondition(long gameId) {
		Game game = gameRepository.getOne(gameId);

		MineralRaceStorageType storageType = game.getMineralRaceStorageType();

		List<Player> players = game.getPlayers();
		List<Player> winners = null;

		switch (storageType) {
			case HOME_PLANET:
				winners = checkHomePlanet(players, game);
				break;
			case STARBASE_PLANET:
				winners = checkStarbasePlanets(players, game);
				break;
			case PLANET:
				winners = checkPlanets(players, game);
				break;
			case FLEET:
				winners = checkShips(players, game);
				break;
			case FLEET_IN_EMPTY_SPACE:
				winners = checkShipsInEmptySpace(players, game);
				break;
		}

		if (CollectionUtils.isNotEmpty(winners)) {
			List<Player> losers = new ArrayList<>(players);
			losers.removeAll(winners);

			for (Player player : losers) {
				player.setHasLost(true);
			}

			playerRepository.saveAll(losers);

			game.setFinished(true);
			gameRepository.save(game);
		}
	}

	private List<Player> checkHomePlanet(List<Player> players, Game game) {
		List<Player> winners = new ArrayList<>(players.size());

		for (Player player : players) {
			Planet homePlanet = player.getHomePlanet();

			if (homePlanet != null) {
				int quantity = homePlanet.retrieveResourceQuantity(game.getMineralRaceMineralName());

				if (quantity >= game.getMineralRaceQuantity()) {
					winners.add(player);
				}
			}
		}

		return winners;
	}

	private List<Player> checkStarbasePlanets(List<Player> players, Game game) {
		List<Player> winners = new ArrayList<>(players.size());

		for (Player player : players) {
			Set<Planet> planets = player.getPlanets();
			int sum = 0;

			for (Planet planet : planets) {
				if (planet.getStarbase() != null) {
					sum += planet.retrieveResourceQuantity(game.getMineralRaceMineralName());
				}
			}

			if (sum >= game.getMineralRaceQuantity()) {
				winners.add(player);
			}
		}

		return winners;
	}

	private List<Player> checkPlanets(List<Player> players, Game game) {
		List<Player> winners = new ArrayList<>(players.size());

		for (Player player : players) {
			Set<Planet> planets = player.getPlanets();
			int sum = 0;

			for (Planet planet : planets) {
				sum += planet.retrieveResourceQuantity(game.getMineralRaceMineralName());
			}

			if (sum >= game.getMineralRaceQuantity()) {
				winners.add(player);
			}
		}

		return winners;
	}

	private List<Player> checkShips(List<Player> players, Game game) {
		List<Player> winners = new ArrayList<>(players.size());

		for (Player player : players) {
			Set<Ship> ships = player.getShips();
			int sum = 0;

			for (Ship ship : ships) {
				sum += ship.retrieveResourceQuantity(game.getMineralRaceMineralName());
			}

			if (sum >= game.getMineralRaceQuantity()) {
				winners.add(player);
			}
		}

		return winners;
	}

	private List<Player> checkShipsInEmptySpace(List<Player> players, Game game) {
		List<Player> winners = new ArrayList<>(players.size());

		for (Player player : players) {
			Set<Ship> ships = player.getShips();
			int sum = 0;

			for (Ship ship : ships) {
				if (ship.getPlanet() == null) {
					sum += ship.retrieveResourceQuantity(game.getMineralRaceMineralName());
				}
			}

			if (sum >= game.getMineralRaceQuantity()) {
				winners.add(player);
			}
		}

		return winners;
	}
}