package org.skrupeltng.modules.ingame.service.round;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetCountResult;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class PoliticsRoundCalculator {

	@Autowired
	private PlayerRelationRepository playerRelationRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void updateCancelledRelations(long gameId) {
		playerRelationRepository.deleteFinishedRelations(gameId);
		playerRelationRepository.countDownCancelledRelations(gameId);
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Map<Long, Float> getTradeBonusData(long gameId) {
		Map<Long, Float> data = new HashMap<>();

		Set<PlayerRelation> relations = playerRelationRepository.findByGameIdAndType(gameId, PlayerRelationType.TRADE_AGREEMENT);
		Map<Long, Long> playerToPlanetCountMap = planetRepository.getPlanetCountsByGameId(gameId).stream()
				.collect(Collectors.toMap(PlayerPlanetCountResult::getPlayerId, PlayerPlanetCountResult::getPlanetCount, (a, b) -> a));

		for (PlayerRelation relation : relations) {
			Player player1 = relation.getPlayer1();
			Player player2 = relation.getPlayer2();

			computeTradeBonus(player1, player2, data, playerToPlanetCountMap);
			computeTradeBonus(player2, player1, data, playerToPlanetCountMap);
		}

		Map<Long, Float> finalData = new HashMap<>();

		for (Entry<Long, Float> entry : data.entrySet()) {
			finalData.put(entry.getKey(), Math.round(entry.getValue()) / 100f);
		}

		return finalData;
	}

	protected void computeTradeBonus(Player player, Player partner, Map<Long, Float> data, Map<Long, Long> playerToPlanetCountMap) {
		final float bonus = 18f;

		Long planetCount = playerToPlanetCountMap.get(player.getId());

		if (planetCount != null) {
			Long planetCountPartner = playerToPlanetCountMap.get(partner.getId());

			float playerBonus = data.getOrDefault(player.getId(), 100f);
			playerBonus += bonus * (planetCountPartner / planetCount);
			data.put(player.getId(), playerBonus);
		}
	}
}