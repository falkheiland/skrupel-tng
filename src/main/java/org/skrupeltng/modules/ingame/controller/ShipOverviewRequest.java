package org.skrupeltng.modules.ingame.controller;

public class ShipOverviewRequest extends AbstractPageRequest {

	private static final long serialVersionUID = -4548475816222564748L;

	private long gameId;
	private String name;
	private String templateId;
	private Boolean idle;
	private String fleetName;

	public long getGameId() {
		return gameId;
	}

	public void setGameId(long gameId) {
		this.gameId = gameId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public Boolean getIdle() {
		return idle;
	}

	public void setIdle(Boolean idle) {
		this.idle = idle;
	}

	public String getFleetName() {
		return fleetName;
	}

	public void setFleetName(String fleetName) {
		this.fleetName = fleetName;
	}
}