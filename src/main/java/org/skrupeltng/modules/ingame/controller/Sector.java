package org.skrupeltng.modules.ingame.controller;

public class Sector {

	private String label;
	private int x;
	private int y;
	private final int galaxySize;

	public Sector(String label, int x, int y, int galaxySize) {
		this.label = label;
		this.x = x;
		this.y = y;
		this.galaxySize = galaxySize;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public boolean showCoordinateLabel() {
		if (galaxySize == 250) {
			return true;
		}

		return (x == 0 || y == 0 || x == galaxySize - 250 || y == galaxySize - 250 || (x == galaxySize - 500 && y == galaxySize - 500)) && (x != 0 || y != 0);
	}

	public String createCoordinateStyle() {
		if (galaxySize == 250) {
			return "left: " + 0 + "px; top: " + 227 + "px; text-align: right; ";
		}

		if (x == 0 || y == 0) {
			return "left: " + x + "px; top: " + y + "px; ";
		}

		if (x == galaxySize - 250 && y == galaxySize - 250) {
			return "left: " + x + "px; top: " + (y + 230) + "px;";
		}

		if (x == galaxySize - 500 && y == galaxySize - 500) {
			return "left: " + (x + 250) + "px; top: " + (y + 250) + "px; text-align: right;";
		}

		if (x == galaxySize - 250) {
			return "left: " + x + "px; top: " + y + "px; text-align: right;";
		}

		if (y == galaxySize - 250) {
			return "left: " + x + "px; top: " + (y + 227) + "px;";
		}

		return "";
	}

	public String createCoordinateText() {
		if (galaxySize == 250) {
			return "250";
		}

		if (x == 0) {
			return y + "";
		}

		if (y == 0) {
			return x + "";
		}

		if (x == galaxySize - 500 && y == galaxySize - 500) {
			return (x + 250) + "";
		}

		if (y == galaxySize - 250) {
			return x + "";
		}

		if (x == galaxySize - 250) {
			if (y != 0) {
				return y + "";
			}

			return x + "";
		}

		return "";
	}

	public String createLabelStyle() {
		String style = "left: " + x + "px; top: " + y + "px; ";

		int overflowX = galaxySize - x - 250;
		if (overflowX < 0) {
			style += "width: " + (250 + overflowX) + "px; ";
		}

		int overflowY = galaxySize - y - 250;
		if (overflowY < 0) {
			style += "height: " + (250 + overflowY) + "px; padding-top: " + (((250 + overflowY) / 2) - 10) + "px;";
		}

		return style;
	}

	public String createLineStyle() {
		String style = "left: " + x + "px; top: " + y + "px; border-right-width: 1px; border-bottom-width: 1px; ";

		if (x == 0) {
			style += "border-left-width: 1px; ";
		}

		if (y == 0) {
			style += "border-top-width: 1px; ";
		}

		int overflowX = galaxySize - x - 250;
		if (overflowX < 0) {
			style += "width: " + (250 + overflowX) + "px; ";
		}

		int overflowY = galaxySize - y - 250;
		if (overflowY < 0) {
			style += "height: " + (250 + overflowY) + "px; ";
		}

		return style;
	}
}