package org.skrupeltng.modules.ingame.controller;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

public class PlanetEntry implements Serializable, Coordinate {

	private static final long serialVersionUID = -2215400594425014224L;

	private long id;
	private String name;
	private int x;
	private int y;
	private String type;
	private String image;

	private Long playerId;
	private String playerColor;
	private String playerName;
	private String playerAiLevel;

	private Long starbaseId;
	private String starbaseName;
	private String starbaseLog;

	private boolean hasShips;
	private String shipPlayerColor;
	private List<Ship> foreignShips;
	private List<Ship> ownShips;

	private int colonists;
	private int money;
	private int supplies;
	private int fuel;
	private int mineral1;
	private int mineral2;
	private int mineral3;
	private int untappedFuel;
	private int untappedMineral1;
	private int untappedMineral2;
	private int untappedMineral3;
	private int mines;
	private int factories;
	private int planetaryDefense;
	private boolean autoBuildMines;
	private boolean autoBuildFactories;
	private boolean autoBuildPlanetaryDefense;
	private String log;
	private boolean hasRevealableNativeSpecies;
	private int scanRadius;

	private boolean visibleByLongRangeScanners;
	private boolean hasCloakingFieldGenerator;

	private int builtOrbitalSystemCount;
	private int orbitalSystemCount;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	@Override
	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Long playerId) {
		this.playerId = playerId;
	}

	public String getPlayerColor() {
		return playerColor;
	}

	public void setPlayerColor(String playerColor) {
		this.playerColor = playerColor;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public String getPlayerAiLevel() {
		return playerAiLevel;
	}

	public void setPlayerAiLevel(String playerAiLevel) {
		this.playerAiLevel = playerAiLevel;
	}

	public Long getStarbaseId() {
		return starbaseId;
	}

	public void setStarbaseId(Long starbaseId) {
		this.starbaseId = starbaseId;
	}

	public String getStarbaseName() {
		return starbaseName;
	}

	public void setStarbaseName(String starbaseName) {
		this.starbaseName = starbaseName;
	}

	public String getStarbaseLog() {
		return starbaseLog;
	}

	public void setStarbaseLog(String starbaseLog) {
		this.starbaseLog = starbaseLog;
	}

	public String getShipPlayerColor() {
		return shipPlayerColor;
	}

	public void setShipPlayerColor(String shipPlayerColor) {
		this.shipPlayerColor = shipPlayerColor;
	}

	public boolean isHasShips() {
		return hasShips;
	}

	public void setHasShips(boolean hasShips) {
		this.hasShips = hasShips;
	}

	public List<Ship> getForeignShips() {
		return foreignShips;
	}

	public void setForeignShips(List<Ship> foreignShips) {
		this.foreignShips = foreignShips;
	}

	public List<Ship> getOwnShips() {
		return ownShips;
	}

	public void setOwnShips(List<Ship> ownShips) {
		this.ownShips = ownShips;
	}

	public int getColonists() {
		return colonists;
	}

	public void setColonists(int colonists) {
		this.colonists = colonists;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public int getSupplies() {
		return supplies;
	}

	public void setSupplies(int supplies) {
		this.supplies = supplies;
	}

	public int getFuel() {
		return fuel;
	}

	public void setFuel(int fuel) {
		this.fuel = fuel;
	}

	public int getMineral1() {
		return mineral1;
	}

	public void setMineral1(int mineral1) {
		this.mineral1 = mineral1;
	}

	public int getMineral2() {
		return mineral2;
	}

	public void setMineral2(int mineral2) {
		this.mineral2 = mineral2;
	}

	public int getMineral3() {
		return mineral3;
	}

	public void setMineral3(int mineral3) {
		this.mineral3 = mineral3;
	}

	public int getUntappedFuel() {
		return untappedFuel;
	}

	public void setUntappedFuel(int untappedFuel) {
		this.untappedFuel = untappedFuel;
	}

	public int getUntappedMineral1() {
		return untappedMineral1;
	}

	public void setUntappedMineral1(int untappedMineral1) {
		this.untappedMineral1 = untappedMineral1;
	}

	public int getUntappedMineral2() {
		return untappedMineral2;
	}

	public void setUntappedMineral2(int untappedMineral2) {
		this.untappedMineral2 = untappedMineral2;
	}

	public int getUntappedMineral3() {
		return untappedMineral3;
	}

	public void setUntappedMineral3(int untappedMineral3) {
		this.untappedMineral3 = untappedMineral3;
	}

	public int getMines() {
		return mines;
	}

	public void setMines(int mines) {
		this.mines = mines;
	}

	public int getFactories() {
		return factories;
	}

	public void setFactories(int factories) {
		this.factories = factories;
	}

	public int getPlanetaryDefense() {
		return planetaryDefense;
	}

	public void setPlanetaryDefense(int planetaryDefense) {
		this.planetaryDefense = planetaryDefense;
	}

	public boolean isAutoBuildMines() {
		return autoBuildMines;
	}

	public void setAutoBuildMines(boolean autoBuildMines) {
		this.autoBuildMines = autoBuildMines;
	}

	public boolean isAutoBuildFactories() {
		return autoBuildFactories;
	}

	public void setAutoBuildFactories(boolean autoBuildFactories) {
		this.autoBuildFactories = autoBuildFactories;
	}

	public boolean isAutoBuildPlanetaryDefense() {
		return autoBuildPlanetaryDefense;
	}

	public void setAutoBuildPlanetaryDefense(boolean autoBuildPlanetaryDefense) {
		this.autoBuildPlanetaryDefense = autoBuildPlanetaryDefense;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public boolean isHasRevealableNativeSpecies() {
		return hasRevealableNativeSpecies;
	}

	public void setHasRevealableNativeSpecies(boolean hasRevealableNativeSpecies) {
		this.hasRevealableNativeSpecies = hasRevealableNativeSpecies;
	}

	public boolean isVisibleByLongRangeScanners() {
		return visibleByLongRangeScanners;
	}

	public void setVisibleByLongRangeScanners(boolean visibleByLongRangeScanners) {
		this.visibleByLongRangeScanners = visibleByLongRangeScanners;
	}

	public boolean isHasCloakingFieldGenerator() {
		return hasCloakingFieldGenerator;
	}

	public void setHasCloakingFieldGenerator(boolean hasCloakingFieldGenerator) {
		this.hasCloakingFieldGenerator = hasCloakingFieldGenerator;
	}

	public int getBuiltOrbitalSystemCount() {
		return builtOrbitalSystemCount;
	}

	public void setBuiltOrbitalSystemCount(int builtOrbitalSystemCount) {
		this.builtOrbitalSystemCount = builtOrbitalSystemCount;
	}

	public int getOrbitalSystemCount() {
		return orbitalSystemCount;
	}

	public void setOrbitalSystemCount(int orbitalSystemCount) {
		this.orbitalSystemCount = orbitalSystemCount;
	}

	@Override
	public int getScanRadius() {
		return scanRadius;
	}

	public void setScanRadius(int scanRadius) {
		this.scanRadius = scanRadius;
	}

	public String retrieveOwnShipNames() {
		return retrieveShipNames(ownShips);
	}

	public String retrieveForeignShipNames() {
		return retrieveShipNames(foreignShips);
	}

	private String retrieveShipNames(List<Ship> ships) {
		List<String> names = ships.stream().map(Ship::getName).collect(Collectors.toList());
		return String.join(";", names);
	}

	public String retrieveScannerRadiusStyle() {
		int size = scanRadius * 2;
		return "width: " + size + "px; height: " + size + "px; bottom: " + scanRadius + "px; right: " + scanRadius + "px;";
	}

	public Long retrievePlayerId() {
		return playerId;
	}

	public String createFullImagePath() {
		return "/images/planets/" + image + ".jpg";
	}
}