package org.skrupeltng.modules;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpeciesType;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.service.ShipFactionAbilityItem;
import org.skrupeltng.modules.masterdata.service.ShipFactionItem;
import org.unbescape.html.HtmlEscape;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

public class FactionFormatMigration {

	private static final ObjectMapper yamlMapper = new ObjectMapper(new YAMLFactory());
	private static final CSVFormat format = CSVFormat.newFormat(':');

	private static final String[] PLANET_TYPES = { "", "M", "N", "J", "L", "G", "I", "C", "K", "F" };

	public static void main(String[] args) throws Exception {
		yamlMapper.setSerializationInclusion(Include.NON_NULL);

		String factionName = args[0];
		String dir = "factions2/";

		InputStream dataStream = new FileInputStream(dir + factionName + "/daten.txt");
		InputStream shipsStream = new FileInputStream(dir + factionName + "/schiffe.txt");
		InputStream descriptionInput = new FileInputStream(dir + factionName + "/beschreibung.txt");

		addFaction(factionName, dataStream, shipsStream, descriptionInput);
	}

	private static void addFaction(String factionName, InputStream dataInput, InputStream shipsInput, InputStream descriptionInput)
			throws Exception {
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(dataInput, StandardCharsets.ISO_8859_1));) {
			List<String> lines = reader.lines().collect(Collectors.toList());

			String[] mainAttributes = lines.get(2).split(":");
			Faction faction = new Faction(factionName);

			int preferredTemperature = Integer.valueOf(mainAttributes[0]);
			faction.setPreferredTemperature(preferredTemperature == 0 ? 0 : preferredTemperature - 35);
			faction.setTaxRate(Float.valueOf(mainAttributes[1]));
			faction.setMineProductionRate(Float.valueOf(mainAttributes[2]));
			faction.setGroundCombatAttackRate(Float.valueOf(mainAttributes[3]));
			faction.setGroundCombatDefenseRate(Float.valueOf(mainAttributes[4]));
			faction.setFactoryProductionRate(Float.valueOf(mainAttributes[5]));

			String planetTypeNumber = mainAttributes[6];
			faction.setPreferredPlanetType(PLANET_TYPES[Integer.valueOf(planetTypeNumber)]);

			String[] assimiliationAttributes = lines.get(4).split(":");

			faction.setAssimilationRate(Float.valueOf(assimiliationAttributes[0]));
			int assimilationTypeId = Integer.valueOf(assimiliationAttributes[1]);

			if (assimilationTypeId > 0) {
				NativeSpeciesType assimilationType = Arrays.asList(NativeSpeciesType.values()).stream().filter(e -> e.getId() == assimilationTypeId).findAny()
						.get();
				faction.setAssimilationType(assimilationType);
			}

			String homePlanetName = lines.get(5);
			faction.setHomePlanetName(homePlanetName);
			faction.setForbiddenOrbitalSystems(getOrbitalSystems(lines, 6));
			faction.setUnlockedOrbitalSystems(getOrbitalSystems(lines, 7));

			String targetFactionPath = "src/main/resources/static/factions/" + factionName;
			File dataFile = new File(targetFactionPath + "/data.yml");
			if (!dataFile.exists()) {
				dataFile.createNewFile();
			}
			yamlMapper.writeValue(new FileWriter(dataFile), faction);

			CSVParser parser = format.parse(new InputStreamReader(shipsInput, StandardCharsets.ISO_8859_1));

			ShipAbilityType[] shipAbilityTypes = ShipAbilityType.values();

			List<ShipFactionItem> ships = new ArrayList<>();

			for (CSVRecord record : parser) {
				ShipFactionItem ship = new ShipFactionItem();
				ships.add(ship);

				List<Map<String, String>> names = new ArrayList<>();
				Map<String, String> germanName = new HashMap<>();
				germanName.put("de", record.get(0));
				names.add(germanName);
				ship.setName(names);
				ship.setId(Integer.valueOf(record.get(1)));
				ship.setTechLevel(Integer.valueOf(record.get(2)));
				ship.setCostMoney(Integer.valueOf(record.get(5)));
				ship.setCostMineral1(Integer.valueOf(record.get(6)));
				ship.setCostMineral2(Integer.valueOf(record.get(7)));
				ship.setCostMineral3(Integer.valueOf(record.get(8)));
				ship.setEnergyWeaponsCount(Integer.valueOf(record.get(9)));
				ship.setProjectileWeaponsCount(Integer.valueOf(record.get(10)));
				ship.setHangarCapacity(Integer.valueOf(record.get(11)));
				ship.setStorageSpace(Integer.valueOf(record.get(12)));
				ship.setFuelCapacity(Integer.valueOf(record.get(13)));
				ship.setPropulsionSystemsCount(Integer.valueOf(record.get(14)));
				ship.setCrew(Integer.valueOf(record.get(15)));
				ship.setMass(Integer.valueOf(record.get(16)));

				String abilityString = record.get(17);

				List<ShipFactionAbilityItem> abilities = new ArrayList<>();

				for (ShipAbilityType shipAbilityType : shipAbilityTypes) {
					Optional<ShipAbility> abilityOpt = shipAbilityType.createAbility(abilityString);

					if (abilityOpt.isPresent()) {
						ShipAbility shipAbility = abilityOpt.get();

						ShipFactionAbilityItem ability = new ShipFactionAbilityItem();
						ability.setName(shipAbility.getType().name());

						Map<String, String> values = shipAbility.getValues();

						if (values != null && !values.isEmpty()) {
							List<Map<String, String>> params = new ArrayList<>();
							ability.setParams(params);

							for (Entry<String, String> entry : values.entrySet()) {
								Map<String, String> param = new HashMap<>();
								param.put(entry.getKey(), entry.getValue());
								params.add(param);
							}
						}

						abilities.add(ability);
					}
				}

				if (!abilities.isEmpty()) {
					ship.setAbilities(abilities);
				}
			}

			File shipsFile = new File(targetFactionPath + "/ships.yml");
			if (!shipsFile.exists()) {
				shipsFile.createNewFile();
			}
			yamlMapper.writeValue(new FileWriter(shipsFile), ships);

			String germanDescription = new String(descriptionInput.readAllBytes(), StandardCharsets.ISO_8859_1);
			germanDescription = germanDescription.replace("<br><br>", "<br/>");
			germanDescription = HtmlEscape.unescapeHtml(germanDescription);

			Map<String, Object> i18nData = new HashMap<>();

			Map<String, String> factionNameData = new HashMap<>();
			factionNameData.put("de", lines.get(0));
			factionNameData.put("en", lines.get(0));
			i18nData.put("factionName", factionNameData);

			Map<String, String> descriptionData = new HashMap<>();
			descriptionData.put("de", germanDescription);
			i18nData.put("description", descriptionData);

			File textsFile = new File(targetFactionPath + "/texts.yml");
			if (!textsFile.exists()) {
				textsFile.createNewFile();
			}
			yamlMapper.writeValue(new FileWriter(textsFile), i18nData);
			//
			// Files.copy(new File("factions2/" + factionName + "/bilder_allgemein/menu.png").toPath(), new File(targetFactionPath + "/logo.png").toPath(),
			// StandardCopyOption.REPLACE_EXISTING);
			//
			// Files.list(new File("factions2/" + factionName + "/bilder_schiffe/").toPath()).forEach(imageFile -> {
			// try {
			// String imageFileName = imageFile.getFileName().toString();
			//
			// if (!imageFileName.contains("_klein")) {
			// Files.copy(imageFile, new File(targetFactionPath + "/ship_images/" + imageFileName).toPath(), StandardCopyOption.REPLACE_EXISTING);
			// }
			// } catch (IOException e) {
			// e.printStackTrace();
			// }
			// });
		}
	}

	private static String getOrbitalSystems(List<String> lines, int index) {
		if (lines.size() > index) {
			String line = lines.get(index);

			if (StringUtils.isNotBlank(line)) {
				String[] orbitalSystemStrings = line.split(":");

				OrbitalSystemType[] orbitalSystemTypes = OrbitalSystemType.values();

				List<String> orbitalSystems = new ArrayList<>(orbitalSystemStrings.length);

				for (String orbitalSystemId : orbitalSystemStrings) {
					int id = Integer.valueOf(orbitalSystemId);
					OrbitalSystemType type = orbitalSystemTypes[id - 1];
					orbitalSystems.add(type.name());
				}

				return String.join(",", orbitalSystems);
			}
		}

		return null;
	}
}