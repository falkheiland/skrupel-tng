package org.skrupeltng.modules.dashboard.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.skrupeltng.modules.ingame.database.player.Player;

public class DeathFoeTeam implements Comparable<DeathFoeTeam> {

	private final List<Player> players = new ArrayList<>(2);

	public List<Player> getPlayers() {
		return players;
	}

	@Override
	public int compareTo(DeathFoeTeam o) {
		Optional<Player> min1 = players.stream().min(Comparator.naturalOrder());
		Optional<Player> min2 = o.players.stream().min(Comparator.naturalOrder());

		if (min1.isPresent() && min2.isPresent()) {
			return Long.compare(min1.get().getId(), min2.get().getId());
		}

		if (min1.isPresent()) {
			return -1;
		}

		if (min2.isPresent()) {
			return 1;
		}

		return 0;
	}
}
