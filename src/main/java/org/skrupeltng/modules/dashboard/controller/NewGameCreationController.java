package org.skrupeltng.modules.dashboard.controller;

import javax.validation.Valid;

import org.skrupeltng.modules.dashboard.NewGameRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class NewGameCreationController extends AbstractGameController {

	@Autowired
	private NewGameValidator namGameValidator;

	@InitBinder
	protected void initBinder(final WebDataBinder binder) {
		binder.addValidators(namGameValidator);
	}

	@PostMapping("/new-game")
	public String createNewGame(@Valid @ModelAttribute("request") NewGameRequest request, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			prepareNewGameModel(model);
			model.addAttribute("canEdit", true);
			model.addAttribute("started", false);
			return "dashboard/new-game";
		}

		long gameId = dashboardService.createdNewGame(request, userDetailService.getLoginId()).getId();

		return "redirect:game?id=" + gameId;
	}
}
