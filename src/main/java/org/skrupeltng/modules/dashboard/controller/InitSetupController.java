package org.skrupeltng.modules.dashboard.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.dashboard.InitSetupRequest;
import org.skrupeltng.modules.dashboard.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class InitSetupController extends AbstractController {

	@Autowired
	private LoginService loginService;

	@Autowired
	private RememberMeServices rememberMeService;

	@Autowired
	private InitSetupValidator initSetupValidator;

	@InitBinder
	protected void initBinder(final WebDataBinder binder) {
		binder.addValidators(initSetupValidator);
	}

	@GetMapping("/init-setup")
	public String initSetup(Model model) {
		if (!loginService.noPlayerExist()) {
			return "redirect:login";
		}

		addLanguages(model);
		model.addAttribute("request", new InitSetupRequest());

		return "dashboard/init-setup";
	}

	@PostMapping("/init-setup")
	public String initSetup(@Valid @ModelAttribute("request") InitSetupRequest request, BindingResult bindingResult, Model model, HttpServletRequest req,
			HttpServletResponse resp) {
		if (loginService.noPlayerExist()) {
			if (bindingResult.hasErrors()) {
				addLanguages(model);
				return "dashboard/init-setup";
			}

			loginService.createAdmin(request);

			UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword());
			SecurityContext sc = SecurityContextHolder.getContext();
			sc.setAuthentication(authReq);

			rememberMeService.loginSuccess(req, resp, authReq);

			return "redirect:open-games";
		}

		return "redirect:login";
	}

	private void addLanguages(Model model) {
		model.addAttribute("english", Locale.ENGLISH.getDisplayLanguage());
		model.addAttribute("german", Locale.GERMAN.getDisplayLanguage());
	}
}