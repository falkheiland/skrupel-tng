package org.skrupeltng.modules.dashboard.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.skrupeltng.config.LoginDetails;
import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class PasswordRecoveryController extends AbstractController {

	@Autowired
	private LoginService loginService;

	@Autowired
	private ResetPasswordValidator resetPasswordValidator;

	@InitBinder
	protected void initBinder(final WebDataBinder binder) {
		binder.addValidators(resetPasswordValidator);
	}

	@GetMapping("/reset-password")
	public String getResetPassword(Model model) {
		model.addAttribute("request", new PasswordRecoveryRequest());
		return "dashboard/reset-password";
	}

	@PostMapping("/reset-password")
	public String initResetPassword(@Valid @ModelAttribute("request") PasswordRecoveryRequest request, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			return "dashboard/reset-password";
		}

		loginService.sendPasswordRecoveryEmail(request.getEmail());

		return "dashboard/password-recovery-email-sent";
	}

	@GetMapping("/reset-password/token/{token}")
	public String showResetPasswordForm(@PathVariable("token") String token, Model model) {
		Optional<Login> result = loginService.getLoginByPasswordRecoveryToken(token);

		if (!result.isPresent()) {
			return "dashboard/password-recovery-not-found";
		}

		model.addAttribute("request", new ResetPasswordRequest());
		model.addAttribute("token", token);
		return "dashboard/password-recovery-form";
	}

	@PostMapping("/reset-password/token/{token}")
	public String resetPassword(@Valid @ModelAttribute("request") ResetPasswordRequest request, BindingResult bindingResult,
			@PathVariable("token") String token, Model model) {
		Optional<Login> result = loginService.getLoginByPasswordRecoveryToken(token);

		if (!result.isPresent()) {
			return "dashboard/password-recovery-not-found";
		}

		if (bindingResult.hasErrors()) {
			model.addAttribute("token", token);
			return "dashboard/password-recovery-form";
		}

		result = loginService.resetPassword(token, request);

		if (!result.isPresent()) {
			return "dashboard/password-recovery-not-found";
		}

		LoginDetails loginDetails = userDetailService.loadUserByUsername(result.get().getUsername());
		UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(loginDetails, null, loginDetails.getAuthorities());
		SecurityContext sc = SecurityContextHolder.getContext();
		sc.setAuthentication(authReq);

		return "dashboard/password-recovery-success";
	}
}