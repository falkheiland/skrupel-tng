package org.skrupeltng.modules.dashboard.controller;

import java.io.Serializable;

public class LoginFactionStatsItem implements Serializable {

	private static final long serialVersionUID = -140061993682508243L;

	private String factionName;
	private int gamesCreated;
	private int gamesPlayed;
	private int gamesWon;
	private int gamesLost;
	private int planetsColonized;
	private int planetsConquered;
	private int planetsMinedOut;
	private int planetsLost;
	private int shipsBuilt;
	private int shipsDestroyed;
	private int shipsCaptured;
	private int shipsLost;
	private int starbasesCreated;
	private int starbasesConquered;
	private int starbasesLost;
	private int starbasesMaxed;

	public int getGamesCreated() {
		return gamesCreated;
	}

	public void setGamesCreated(int gamesCreated) {
		this.gamesCreated = gamesCreated;
	}

	public String getFactionName() {
		return factionName;
	}

	public void setFactionName(String factionName) {
		this.factionName = factionName;
	}

	public int getGamesPlayed() {
		return gamesPlayed;
	}

	public void setGamesPlayed(int gamesPlayed) {
		this.gamesPlayed = gamesPlayed;
	}

	public int getGamesWon() {
		return gamesWon;
	}

	public void setGamesWon(int gamesWon) {
		this.gamesWon = gamesWon;
	}

	public int getGamesLost() {
		return gamesLost;
	}

	public void setGamesLost(int gamesLost) {
		this.gamesLost = gamesLost;
	}

	public int getPlanetsColonized() {
		return planetsColonized;
	}

	public void setPlanetsColonized(int planetsColonized) {
		this.planetsColonized = planetsColonized;
	}

	public int getPlanetsConquered() {
		return planetsConquered;
	}

	public void setPlanetsConquered(int planetsConquered) {
		this.planetsConquered = planetsConquered;
	}

	public int getPlanetsMinedOut() {
		return planetsMinedOut;
	}

	public void setPlanetsMinedOut(int planetsMinedOut) {
		this.planetsMinedOut = planetsMinedOut;
	}

	public int getPlanetsLost() {
		return planetsLost;
	}

	public void setPlanetsLost(int planetsLost) {
		this.planetsLost = planetsLost;
	}

	public int getShipsBuilt() {
		return shipsBuilt;
	}

	public void setShipsBuilt(int shipsBuilt) {
		this.shipsBuilt = shipsBuilt;
	}

	public int getShipsDestroyed() {
		return shipsDestroyed;
	}

	public void setShipsDestroyed(int shipsDestroyed) {
		this.shipsDestroyed = shipsDestroyed;
	}

	public int getShipsCaptured() {
		return shipsCaptured;
	}

	public void setShipsCaptured(int shipsCaptured) {
		this.shipsCaptured = shipsCaptured;
	}

	public int getShipsLost() {
		return shipsLost;
	}

	public void setShipsLost(int shipsLost) {
		this.shipsLost = shipsLost;
	}

	public int getStarbasesCreated() {
		return starbasesCreated;
	}

	public void setStarbasesCreated(int starbasesCreated) {
		this.starbasesCreated = starbasesCreated;
	}

	public int getStarbasesConquered() {
		return starbasesConquered;
	}

	public void setStarbasesConquered(int starbasesConquered) {
		this.starbasesConquered = starbasesConquered;
	}

	public int getStarbasesLost() {
		return starbasesLost;
	}

	public void setStarbasesLost(int starbasesLost) {
		this.starbasesLost = starbasesLost;
	}

	public int getStarbasesMaxed() {
		return starbasesMaxed;
	}

	public void setStarbasesMaxed(int starbasesMaxed) {
		this.starbasesMaxed = starbasesMaxed;
	}
}