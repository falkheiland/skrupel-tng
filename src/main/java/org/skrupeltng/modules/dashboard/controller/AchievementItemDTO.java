package org.skrupeltng.modules.dashboard.controller;

import java.time.Instant;

import org.skrupeltng.modules.dashboard.database.AchievementType;

public class AchievementItemDTO {

	private final AchievementType type;
	private final Instant earnedDate;

	private Integer progress;
	private Integer maxValue;

	public AchievementItemDTO(AchievementType type, Instant earnedDate) {
		this.type = type;
		this.earnedDate = earnedDate;
	}

	public AchievementType getType() {
		return type;
	}

	public Instant getEarnedDate() {
		return earnedDate;
	}

	public Integer getProgress() {
		return progress;
	}

	public void setProgress(Integer progress) {
		this.progress = progress;
	}

	public Integer getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(Integer maxValue) {
		this.maxValue = maxValue;
	}
}