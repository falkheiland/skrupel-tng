package org.skrupeltng.modules.dashboard.controller;

import java.io.Serializable;

public class OrbitalCombatCalculatorRequest implements Serializable {

	private static final long serialVersionUID = 6883413947172166924L;

	private String hull;
	private String energy;
	private String projectile;
	private int damage;
	private int shield;
	private int projectileCount;

	private int planetaryDefenseCount;
	private String starbaseType;

	public String getHull() {
		return hull;
	}

	public void setHull(String hull) {
		this.hull = hull;
	}

	public String getEnergy() {
		return energy;
	}

	public void setEnergy(String energy) {
		this.energy = energy;
	}

	public String getProjectile() {
		return projectile;
	}

	public void setProjectile(String projectile) {
		this.projectile = projectile;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public int getShield() {
		return shield;
	}

	public void setShield(int shield) {
		this.shield = shield;
	}

	public int getProjectileCount() {
		return projectileCount;
	}

	public void setProjectileCount(int projectileCount) {
		this.projectileCount = projectileCount;
	}

	public int getPlanetaryDefenseCount() {
		return planetaryDefenseCount;
	}

	public void setPlanetaryDefenseCount(int planetaryDefenseCount) {
		this.planetaryDefenseCount = planetaryDefenseCount;
	}

	public String getStarbaseType() {
		return starbaseType;
	}

	public void setStarbaseType(String starbaseType) {
		this.starbaseType = starbaseType;
	}
}