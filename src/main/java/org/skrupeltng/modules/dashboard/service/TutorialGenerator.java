package org.skrupeltng.modules.dashboard.service;

import java.util.ArrayList;
import java.util.List;

import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.dashboard.database.GalaxyConfigRepository;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.ingame.database.FogOfWarType;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.LoseCondition;
import org.skrupeltng.modules.ingame.database.Tutorial;
import org.skrupeltng.modules.ingame.database.TutorialRepository;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystem;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetType;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetTypeRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class TutorialGenerator {

	@Autowired
	private TutorialRepository tutorialRepository;

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private UserDetailServiceImpl userService;

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private OrbitalSystemRepository orbitalSystemRepository;

	@Autowired
	private StarbaseRepository starbaseRepository;

	@Autowired
	private PlanetTypeRepository planetTypeRepository;

	@Autowired
	private GalaxyConfigRepository galaxyConfigRepository;

	@Autowired
	private MasterDataService masterDataService;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public long generateTutorial() {
		Game game = new Game();
		game.setTutorial(true);
		game.setGalaxySize(1000);
		game.setGalaxyConfig(galaxyConfigRepository.getOne("gala_0"));

		game.setCreator(loginRepository.getOne(userService.getLoginId()));
		game.setEnableMineFields(true);
		game.setEnableTactialCombat(true);
		game.setFogOfWarType(FogOfWarType.LONG_RANGE_SENSORS);
		game.setLoseCondition(LoseCondition.LOSE_HOME_PLANET);
		game.setWinCondition(WinCondition.SURVIVE);
		game.setName("Tutorial");

		game = gameRepository.save(game);

		Tutorial tutorial = new Tutorial();
		tutorial.setGame(game);
		tutorial.setLogin(game.getCreator());
		tutorial = tutorialRepository.save(tutorial);

		addPlayer(game, tutorial);
		Player enemy = addEnemy(game);

		addPlanets(game, tutorial);

		Planet enemyColony = tutorial.getEnemyColony();
		enemyColony.setPlayer(enemy);
		enemyColony.setColonists(1100);
		enemyColony.setMoney(500);
		enemyColony.setSupplies(50);
		enemyColony.setMines(10);
		enemyColony.setFactories(10);
		planetRepository.save(enemyColony);

		tutorialRepository.save(tutorial);

		return game.getId();
	}

	protected void addPlayer(Game game, Tutorial tutorial) {
		Login login = game.getCreator();

		Player player = new Player();
		player.setLogin(login);
		player.setGame(game);
		player.setColor("00ff00");

		Faction faction = masterDataService.getFactionData("orion");
		player.setFaction(faction);

		player = playerRepository.save(player);

		setupHomePlanet(player, 200, 200);

		tutorial.setHomePlanet(player.getHomePlanet());
	}

	protected Player addEnemy(Game game) {
		Login login = loginRepository.findByUsername(AILevel.AI_EASY.name()).get();

		Player player = new Player();
		player.setLogin(login);
		player.setGame(game);
		player.setColor("ff0000");
		player.setAiLevel(AILevel.AI_EASY);

		Faction faction = masterDataService.getFactionData("orion");
		player.setFaction(faction);

		player = playerRepository.save(player);

		setupHomePlanet(player, 800, 800);

		return player;
	}

	protected void setupHomePlanet(Player player, int x, int y) {
		Faction faction = player.getFaction();

		Planet planet = new Planet();
		planet.setGame(player.getGame());
		planet.setPlayer(player);
		planet.setX(x);
		planet.setY(y);
		planet.setName(faction.getHomePlanetName());
		planet.setTemperature(faction.getPreferredTemperature());

		PlanetType planetType = planetTypeRepository.getOne(faction.getPreferredPlanetType());
		planet.setType(planetType.getId());
		String image = planetType.getImagePrefix() + "_1";
		planet.setImage(image);

		planet.setColonists(75000);
		planet.setMoney(7500);
		planet.setSupplies(5);
		planet.setMines(5);
		planet.setFactories(5);
		planet.setPlanetaryDefense(5);
		planet.setFuel(70);
		planet.setMineral1(99);
		planet.setMineral2(96);
		planet.setMineral3(118);

		planet.setUntappedFuel(5000);
		planet.setUntappedMineral1(4500);
		planet.setUntappedMineral2(5500);
		planet.setUntappedMineral3(4700);

		planet.setNecessaryMinesForOneFuel(1);
		planet.setNecessaryMinesForOneMineral1(2);
		planet.setNecessaryMinesForOneMineral2(1);
		planet.setNecessaryMinesForOneMineral3(2);

		Starbase starbase = new Starbase();
		starbase.setName("Starbase 1");
		starbase.setType(StarbaseType.STAR_BASE);
		starbase = starbaseRepository.save(starbase);
		planet.setStarbase(starbase);

		List<OrbitalSystem> orbitalSystems = new ArrayList<>(4);
		orbitalSystems.add(new OrbitalSystem(planet));
		orbitalSystems.add(new OrbitalSystem(planet));
		orbitalSystems.add(new OrbitalSystem(planet));
		orbitalSystems.add(new OrbitalSystem(planet));
		orbitalSystemRepository.saveAll(orbitalSystems);

		planet = planetRepository.save(planet);

		player.setHomePlanet(planet);

		playerRepository.save(player);
	}

	protected void addPlanets(Game game, Tutorial tutorial) {
		Planet firstColony = addPlanet(game, 210, 240, "Tau Ceti", -10, planetTypeRepository.getOne("J"));
		tutorial.setFirstColony(firstColony);

		addPlanet(game, 120, 180, "Halut", 20, planetTypeRepository.getOne("M"));
		addPlanet(game, 140, 260, "Canops", 25, planetTypeRepository.getOne("C"));
		addPlanet(game, 171, 92, "Zenope", 5, planetTypeRepository.getOne("N"));
		addPlanet(game, 255, 146, "Kicholia", -30, planetTypeRepository.getOne("J"));
		addPlanet(game, 39, 217, "Chenvion", -10, planetTypeRepository.getOne("K"));
		addPlanet(game, 203, 350, "Bonzeathea", 38, planetTypeRepository.getOne("L"));
		addPlanet(game, 337, 322, "Imides", 54, planetTypeRepository.getOne("I"));
		addPlanet(game, 392, 131, "Yeria", 46, planetTypeRepository.getOne("C"));

		Planet enemyColony = addPlanet(game, 270, 210, "Rigel", 35, planetTypeRepository.getOne("G"));
		tutorial.setEnemyColony(enemyColony);
	}

	protected Planet addPlanet(Game game, int x, int y, String name, int temperature, PlanetType type) {
		Planet planet = new Planet();
		planet.setGame(game);
		planet.setX(x);
		planet.setY(y);
		planet.setName(name);
		planet.setTemperature(temperature);
		planet.setType(type.getId());

		String image = type.getImagePrefix() + "_1";
		planet.setImage(image);

		planet.setFuel(100);
		planet.setMineral1(110);
		planet.setMineral2(105);
		planet.setMineral3(120);

		planet.setUntappedFuel(5000);
		planet.setUntappedMineral1(4500);
		planet.setUntappedMineral2(5500);
		planet.setUntappedMineral3(4700);

		planet.setNecessaryMinesForOneFuel(5);
		planet.setNecessaryMinesForOneMineral1(4);
		planet.setNecessaryMinesForOneMineral2(3);
		planet.setNecessaryMinesForOneMineral3(6);

		List<OrbitalSystem> orbitalSystems = new ArrayList<>(3);
		orbitalSystems.add(new OrbitalSystem(planet));
		orbitalSystems.add(new OrbitalSystem(planet));
		orbitalSystems.add(new OrbitalSystem(planet));
		orbitalSystemRepository.saveAll(orbitalSystems);

		return planetRepository.save(planet);
	}
}