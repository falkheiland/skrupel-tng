package org.skrupeltng.modules.dashboard.service;

import java.util.List;
import java.util.Optional;

import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.dashboard.NewGameRequest;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.ingame.database.FogOfWarType;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class DebugGameCreator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private DashboardService dashboardService;

	@Autowired
	private MasterDataService masterDataService;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private LoginRepository loginRepository;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public Game createDebugGame(long adminLoginId) {
		NewGameRequest newGameRequest = NewGameRequest.createDefaultRequest();
		newGameRequest.setName("Debug");
		newGameRequest.setPlayerCount(((int)masterDataService.getFactionCount() * 2) + 1);
		newGameRequest.setFogOfWarType(FogOfWarType.NONE.name());
		newGameRequest.setGalaxyConfigId("gala_" + MasterDataService.RANDOM.nextInt(10));

		int galaxySize = 1000 + (500 * (newGameRequest.getPlayerCount() / 4));
		newGameRequest.setGalaxySize(galaxySize);

		return dashboardService.createdNewGame(newGameRequest, adminLoginId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void setFactionForAdminPlayer(long gameId) {
		Game game = dashboardService.getGame(gameId).get();
		Player adminPlayer = game.getPlayers().get(0);
		List<Faction> allFaction = masterDataService.getAllFactions();
		Faction faction = allFaction.get(MasterDataService.RANDOM.nextInt(allFaction.size()));
		adminPlayer.setFaction(faction);
		playerRepository.save(adminPlayer);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public long addAIPlayerToDebugGame(long gameId, AILevel level) {
		try {
			Optional<Login> aiLogin = loginRepository.findByUsername(level.name());
			long playerId = dashboardService.addPlayer(gameId, aiLogin.get().getId(), null).getId();

			Player player = playerRepository.getOne(playerId);
			player.setAiLevel(level);
			player = playerRepository.save(player);

			return player.getId();
		} catch (GameFullException e) {
			log.error("Error while adding AI player to debug game: ", e);
			return 1l;
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void setPlayerFactionForDebugGame(long playerId, String faction) {
		dashboardService.selectFactionForPlayer(faction, playerId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void startDebugGame(long gameId) {
		dashboardService.startGame(gameId);
	}
}