package org.skrupeltng.modules.dashboard.database;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.skrupeltng.modules.ingame.database.player.Player;

@Entity
@Table(name = "login")
public class Login implements Serializable {

	private static final long serialVersionUID = -1602632454810654846L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(length = 64)
	private String username;

	@Column(length = 256)
	private String password;

	@Column(length = 256)
	private String email;

	@Column(name = "tutorial_finished")
	private boolean tutorialFinished;

	@Column(name = "hide_scenario_tutorial_hint")
	private boolean hideScenarioTutorialHint;

	private boolean active;

	@Column(name = "activation_code")
	private String activationCode;

	@Column(name = "password_recovery_token")
	private String passwordRecoveryToken;

	private String language;

	private String theme;

	@Column(name = "round_notifications_enabled")
	private boolean roundNotificationsEnabled;

	@Column(name = "join_notifications_enabled")
	private boolean joinNotificationsEnabled;

	@Column(name = "game_full_notifications_enabled")
	private boolean gameFullNotificationsEnabled;

	@Column(name = "invited_to_game_notifications_enabled")
	private boolean invitedToGameNotificationsEnabled;

	@Column(name = "not_finished_turn_notifications_enabled")
	private boolean notFinishTurnNotificationsEnabled;

	@Column(name = "last_to_finish_turn_notifications_enabled")
	private boolean lastToFinishTurnNotificationsEnabled;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "login")
	private Set<LoginStatsFaction> factionStats;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "login")
	private Set<LoginRole> roles;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "login")
	private Set<Player> players;

	public Login() {

	}

	public Login(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isRoundNotificationsEnabled() {
		return roundNotificationsEnabled;
	}

	public void setRoundNotificationsEnabled(boolean roundNotificationsEnabled) {
		this.roundNotificationsEnabled = roundNotificationsEnabled;
	}

	public boolean isJoinNotificationsEnabled() {
		return joinNotificationsEnabled;
	}

	public void setJoinNotificationsEnabled(boolean joinNotificationsEnabled) {
		this.joinNotificationsEnabled = joinNotificationsEnabled;
	}

	public boolean isGameFullNotificationsEnabled() {
		return gameFullNotificationsEnabled;
	}

	public void setGameFullNotificationsEnabled(boolean gameFullNotificationsEnabled) {
		this.gameFullNotificationsEnabled = gameFullNotificationsEnabled;
	}

	public boolean isInvitedToGameNotificationsEnabled() {
		return invitedToGameNotificationsEnabled;
	}

	public void setInvitedToGameNotificationsEnabled(boolean invitedToGameNotificationsEnabled) {
		this.invitedToGameNotificationsEnabled = invitedToGameNotificationsEnabled;
	}

	public boolean isNotFinishTurnNotificationsEnabled() {
		return notFinishTurnNotificationsEnabled;
	}

	public void setNotFinishTurnNotificationsEnabled(boolean notFinishTurnNotificationsEnabled) {
		this.notFinishTurnNotificationsEnabled = notFinishTurnNotificationsEnabled;
	}

	public boolean isLastToFinishTurnNotificationsEnabled() {
		return lastToFinishTurnNotificationsEnabled;
	}

	public void setLastToFinishTurnNotificationsEnabled(boolean lastToFinishTurnNotificationsEnabled) {
		this.lastToFinishTurnNotificationsEnabled = lastToFinishTurnNotificationsEnabled;
	}

	public boolean isTutorialFinished() {
		return tutorialFinished;
	}

	public void setTutorialFinished(boolean tutorialFinished) {
		this.tutorialFinished = tutorialFinished;
	}

	public boolean isHideScenarioTutorialHint() {
		return hideScenarioTutorialHint;
	}

	public void setHideScenarioTutorialHint(boolean hideScenarioTutorialHint) {
		this.hideScenarioTutorialHint = hideScenarioTutorialHint;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public String getPasswordRecoveryToken() {
		return passwordRecoveryToken;
	}

	public void setPasswordRecoveryToken(String passwordRecoveryToken) {
		this.passwordRecoveryToken = passwordRecoveryToken;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public Set<LoginStatsFaction> getFactionStats() {
		return factionStats;
	}

	public void setFactionStats(Set<LoginStatsFaction> factionStats) {
		this.factionStats = factionStats;
	}

	public Set<LoginRole> getRoles() {
		return roles;
	}

	public void setRoles(Set<LoginRole> roles) {
		this.roles = roles;
	}

	public Set<Player> getPlayers() {
		return players;
	}

	public void setPlayers(Set<Player> players) {
		this.players = players;
	}

	@Override
	public String toString() {
		return "Login [id=" + id + ", username=" + username + "]";
	}
}