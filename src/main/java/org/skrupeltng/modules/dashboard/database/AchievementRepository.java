package org.skrupeltng.modules.dashboard.database;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface AchievementRepository extends JpaRepository<Achievement, Long> {

	@Modifying
	@Query("DELETE FROM Achievement a WHERE a.login.id = ?1")
	void deleteByLoginId(long loginId);

	@Query("SELECT a FROM Achievement a WHERE a.login.id = ?1")
	List<Achievement> findByLoginId(long loginId);

	@Query("SELECT a FROM Achievement a WHERE a.login.id = ?1 AND a.type = ?2")
	Optional<Achievement> findByLoginIdAndType(long loginId, AchievementType type);

	@Query("SELECT a FROM Achievement a WHERE a.login.id = ?1")
	List<Achievement> getEarnedAchievements(long loginId);
}