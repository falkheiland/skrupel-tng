package org.skrupeltng.modules.dashboard.database;

import org.skrupeltng.modules.dashboard.modules.admin.controller.UserListResultDTO;
import org.skrupeltng.modules.dashboard.modules.admin.controller.UserSearchParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface LoginRepositoryCustom {

	Page<UserListResultDTO> searchUsers(UserSearchParameters params, Pageable pageRequest);
}