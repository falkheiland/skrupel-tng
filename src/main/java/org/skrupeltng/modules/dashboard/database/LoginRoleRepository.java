package org.skrupeltng.modules.dashboard.database;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LoginRoleRepository extends JpaRepository<LoginRole, Long> {

}