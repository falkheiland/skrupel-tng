package org.skrupeltng.modules.dashboard;

import org.skrupeltng.modules.ingame.database.WinCondition;

public class GameSearchParameters {

	private String name;
	private WinCondition winCondition;
	private Boolean started;
	private Boolean onlyOwnGames;
	private Boolean turnNotDone;
	private long currentLoginId;
	private boolean isAdmin;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public WinCondition getWinCondition() {
		return winCondition;
	}

	public void setWinCondition(WinCondition winCondition) {
		this.winCondition = winCondition;
	}

	public Boolean getStarted() {
		return started;
	}

	public void setStarted(Boolean started) {
		this.started = started;
	}

	public Boolean getOnlyOwnGames() {
		return onlyOwnGames;
	}

	public void setOnlyOwnGames(Boolean onlyOwnGames) {
		this.onlyOwnGames = onlyOwnGames;
	}

	public Boolean getTurnNotDone() {
		return turnNotDone;
	}

	public void setTurnNotDone(Boolean turnNotDone) {
		this.turnNotDone = turnNotDone;
	}

	public long getCurrentLoginId() {
		return currentLoginId;
	}

	public void setCurrentLoginId(long currentLoginId) {
		this.currentLoginId = currentLoginId;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
}