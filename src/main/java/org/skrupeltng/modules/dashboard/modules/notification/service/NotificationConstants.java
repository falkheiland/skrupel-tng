package org.skrupeltng.modules.dashboard.modules.notification.service;

public interface NotificationConstants {

	String notification_game_started = "notification_game_started";
	String notification_new_round = "notification_new_round";
	String notification_player_joined = "notification_player_joined";
	String notification_game_can_be_started = "notification_game_can_be_started";
	String notification_achievement_unlocked = "notification_achievement_unlocked";
}