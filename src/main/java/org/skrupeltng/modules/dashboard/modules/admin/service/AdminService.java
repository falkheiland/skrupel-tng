package org.skrupeltng.modules.dashboard.modules.admin.service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.InstallationDetailsHelper;
import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.dashboard.Roles;
import org.skrupeltng.modules.dashboard.database.DataPrivacyStatement;
import org.skrupeltng.modules.dashboard.database.DataPrivacyStatementRepository;
import org.skrupeltng.modules.dashboard.database.InstallationDetails;
import org.skrupeltng.modules.dashboard.database.InstallationDetailsRepository;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.dashboard.modules.admin.controller.DataPrivacyStatementsData;
import org.skrupeltng.modules.dashboard.modules.admin.controller.LegalTextData;
import org.skrupeltng.modules.dashboard.modules.admin.controller.UserListResultDTO;
import org.skrupeltng.modules.dashboard.modules.admin.controller.UserSearchParameters;
import org.skrupeltng.modules.dashboard.service.LoginRemoval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AdminService {

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private UserDetailServiceImpl userDetailService;

	@Autowired
	private DataPrivacyStatementRepository dataPrivacyStatementRepository;

	@Autowired
	private InstallationDetailsRepository installationDetailsRepository;

	@Autowired
	private InstallationDetailsHelper installationDetailsHelper;

	@Autowired
	private LoginRemoval loginRemoval;

	@PreAuthorize("hasAnyRole('" + Roles.ADMIN + "')")
	public Page<UserListResultDTO> searchUsers(UserSearchParameters params, Pageable pageRequest) {
		return loginRepository.searchUsers(params, pageRequest);
	}

	@PreAuthorize("hasAnyRole('" + Roles.ADMIN + "')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteUser(long loginId) {
		Login login = loginRepository.getOne(loginId);

		if (login.getId() == userDetailService.getLoginId()) {
			throw new IllegalArgumentException("The admin cannot be deleted!");
		}

		try {
			AILevel level = AILevel.valueOf(login.getUsername());

			if (level != null) {
				throw new IllegalArgumentException("AI users cannot be deleted!");
			}
		} catch (Exception e) {

		}

		loginRemoval.delete(login);
	}

	@PreAuthorize("hasAnyRole('" + Roles.ADMIN + "')")
	public DataPrivacyStatementsData getDataPrivacyStatementsData() {
		List<DataPrivacyStatement> statements = dataPrivacyStatementRepository.findAll();

		DataPrivacyStatementsData data = new DataPrivacyStatementsData();

		for (DataPrivacyStatement statement : statements) {
			if (statement.getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				data.setTextEnglish(statement.getText());
			} else if (statement.getLanguage().equals(Locale.GERMAN.getLanguage())) {
				data.setTextGerman(statement.getText());
			}
		}

		return data;
	}

	@PreAuthorize("hasAnyRole('" + Roles.ADMIN + "')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void changeDataPrivacyStatements(DataPrivacyStatementsData request) {
		updateDataPrivacyStatement(request.getTextEnglish(), Locale.ENGLISH);
		updateDataPrivacyStatement(request.getTextGerman(), Locale.GERMAN);
	}

	private void updateDataPrivacyStatement(String newText, Locale locale) {
		String language = locale.getLanguage();
		Optional<DataPrivacyStatement> opt = dataPrivacyStatementRepository.findByLanguage(language);

		DataPrivacyStatement result = opt.orElseGet(() -> new DataPrivacyStatement(language));
		result.setText(newText);
		dataPrivacyStatementRepository.save(result);
	}

	@PreAuthorize("hasAnyRole('" + Roles.ADMIN + "')")
	public LegalTextData getLegalText() {
		String text = installationDetailsRepository.getLegalText();
		return new LegalTextData(text);
	}

	@PreAuthorize("hasAnyRole('" + Roles.ADMIN + "')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void changeLegalTextData(LegalTextData request) {
		InstallationDetails installationDetails = installationDetailsRepository.getOne(1L);
		installationDetails.setLegalText(request.getText());
		installationDetailsRepository.save(installationDetails);

		installationDetailsHelper.clearHasLegalTextCache();
	}
}