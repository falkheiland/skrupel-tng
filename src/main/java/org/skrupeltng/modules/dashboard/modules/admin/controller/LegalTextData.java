package org.skrupeltng.modules.dashboard.modules.admin.controller;

import java.io.Serializable;

public class LegalTextData implements Serializable {

	private static final long serialVersionUID = 3015607438066391831L;

	private String text;

	public LegalTextData() {

	}

	public LegalTextData(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}