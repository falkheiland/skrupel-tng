package org.skrupeltng.modules.ai.hard;

import org.skrupeltng.modules.ai.medium.ships.AIMediumShips;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_HARD")
public class AIHardShips extends AIMediumShips {

}