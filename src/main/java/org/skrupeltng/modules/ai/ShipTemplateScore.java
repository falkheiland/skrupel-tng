package org.skrupeltng.modules.ai;

import java.util.function.Function;

import org.skrupeltng.modules.masterdata.database.ShipTemplate;

public class ShipTemplateScore implements Comparable<ShipTemplateScore> {

	private final ShipTemplateScoringData data;
	private final int score;

	public ShipTemplateScore(ShipTemplateScoringData data, Function<ShipTemplateScoringData, Integer> scoreProvider) {
		this.data = data;
		this.score = scoreProvider.apply(data);
	}

	public ShipTemplate getShipTemplate() {
		return data.getShipTemplate();
	}

	public int getScore() {
		return score;
	}

	@Override
	public int compareTo(ShipTemplateScore o) {
		return Integer.compare(score, o.score);
	}
}