package org.skrupeltng.modules.ai.medium.ships;

import org.skrupeltng.modules.ai.easy.ships.AIEasyFreighters;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_MEDIUM")
public class AIMediumFreighters extends AIEasyFreighters {

	@Override
	protected boolean loadNewColonists(Ship ship, ShipTransportRequest transportRequest, int targetColonists) {
		boolean result = super.loadNewColonists(ship, transportRequest, targetColonists);

		int newSupplies = transportRequest.getSupplies() + ship.getSupplies();
		int suppliesForNewColony = getSuppliesForNewColony();

		int newColonists = transportRequest.getColonists() + ship.getColonists();
		int colonistsForNewColony = getColonistsForNewColony();

		if (newSupplies / suppliesForNewColony < newColonists / colonistsForNewColony) {
			ship.resetTravel();
			return false;
		}

		return result;
	}
}