package org.skrupeltng.modules.ai.medium;

import org.skrupeltng.modules.ai.AIConstants;
import org.skrupeltng.modules.ai.ShipTemplateScoringData;
import org.skrupeltng.modules.ai.easy.AIEasyStarbases;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.service.StarbaseUpgradeType;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_MEDIUM")
public class AIMediumStarbases extends AIEasyStarbases {

	@Override
	protected int getFreighterScore(ShipTemplateScoringData data) {
		ShipTemplate s = data.getShipTemplate();

		if (s.getAbility(ShipAbilityType.QUARK_REORGANIZER).isPresent() || s.getAbility(ShipAbilityType.SUB_PARTICLE_CLUSTER).isPresent()) {
			return 0;
		}

		int priority = getPriorityValue(data, AIConstants.AI_FREIGHTER_NAME);

		return priority + s.getStorageSpace() - (s.getEnergyWeaponsCount() + s.getProjectileWeaponsCount() + s.getHangarCapacity()) * 10;
	}

	protected int getPriorityValue(ShipTemplateScoringData data, String name) {
		if (!data.getPlayer().getShips().stream().anyMatch(ship -> ship.getName().equals(name)) &&
				data.getStarbase().canBeProduced(data.getShipTemplate(), 1)) {
			return 1000;
		}

		return 0;
	}

	@Override
	protected int getScoutScore(ShipTemplateScoringData data) {
		return super.getScoutScore(data) + getPriorityValue(data, AIConstants.AI_SCOUT_NAME);
	}

	@Override
	protected Starbase upgradeStarbase(Starbase starbase) {
		if (starbase.getHullLevel() < 6 || starbase.getPropulsionLevel() < 6) {
			upgradeStarbase(starbase, 6, StarbaseUpgradeType.HULL, StarbaseUpgradeType.PROPULSION);

			if (starbase.getEnergyLevel() == 0) {
				upgradeStarbase(starbase, 1, StarbaseUpgradeType.ENERGY);
			}

			if (starbase.getProjectileLevel() == 0) {
				upgradeStarbase(starbase, 1, StarbaseUpgradeType.PROJECTILE);
			}
		} else if (starbase.getHullLevel() >= 6 && (starbase.getEnergyLevel() < 5 || starbase.getProjectileLevel() < 5)) {
			upgradeStarbase(starbase, 5, StarbaseUpgradeType.ENERGY, StarbaseUpgradeType.PROJECTILE);
		} else if (starbase.getHullLevel() < 10 && (starbase.getEnergyLevel() >= 5 || starbase.getProjectileLevel() >= 5)) {
			upgradeStarbase(starbase, 10, StarbaseUpgradeType.HULL);
		} else {
			upgradeStarbase(starbase, null, StarbaseUpgradeType.values());
		}

		planetRepository.save(starbase.getPlanet());

		return starbaseRepository.save(starbase);
	}
}