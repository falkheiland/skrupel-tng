package org.skrupeltng.modules.ai.easy;

import org.skrupeltng.modules.ai.AIRoundCalculator;
import org.skrupeltng.modules.ai.easy.ships.AIEasyShips;
import org.skrupeltng.modules.ingame.modules.scenario.database.ScenarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component("AI_EASY")
public class AIEasyRoundCalculator implements AIRoundCalculator {

	@Autowired
	@Qualifier("AI_EASY")
	private AIEasyShips ships;

	@Autowired
	@Qualifier("AI_EASY")
	private AIEasyPlanets planets;

	@Autowired
	@Qualifier("AI_EASY")
	private AIEasyStarbases starbases;

	@Autowired
	@Qualifier("AI_EASY")
	private AIEasyPolitics politics;

	@Autowired
	private ScenarioRepository scenarioRepository;

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void calculateRound(long playerId) {
		ships.processShips(playerId);
		planets.processPlanets(playerId);
		starbases.processStarbases(playerId);

		if (!scenarioRepository.isScenario(playerId)) {
			politics.processPolitics(playerId);
		}
	}
}