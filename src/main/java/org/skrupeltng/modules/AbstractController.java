package org.skrupeltng.modules;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.config.PermissionEvaluatorImpl;
import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.dashboard.service.TutorialService;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

public class AbstractController {

	@Autowired
	protected UserDetailServiceImpl userDetailService;

	@Autowired
	protected MessageSource messageSource;

	@Autowired
	protected ConfigProperties configProperties;

	@Autowired
	protected PermissionEvaluatorImpl permissionEvaluator;

	@Autowired
	protected TutorialService tutorialService;

	protected Pageable createPageable(Integer page, Integer pageSize, String sortField, Boolean sortDirection) {
		if (page == null) {
			page = 0;
		}

		if (pageSize == null) {
			pageSize = 20;
		}

		Sort sort = null;

		if (sortField != null && sortDirection != null) {
			String sortFieldString = SortField.valueOf(sortField).getDatabaseField();
			sort = Sort.by(sortDirection ? Direction.ASC : Direction.DESC, sortFieldString);

			return PageRequest.of(page, pageSize, sort);
		}

		return PageRequest.of(page, pageSize);
	}

	protected boolean turnDoneForPlanet(long planetId) {
		return !permissionEvaluator.checkTurnNotDonePlanet(planetId);
	}

	protected boolean turnDoneForShip(long shipId) {
		return !permissionEvaluator.checkTurnNotDoneShip(shipId);
	}

	protected boolean turnDoneForStarbase(long starbaseId) {
		return !permissionEvaluator.checkTurnNotDoneStarbase(starbaseId);
	}

	protected boolean turnDoneForFleet(long fleetId) {
		return !permissionEvaluator.checkTurnNotDoneFleet(fleetId);
	}

	protected String getPlayerName(Player player) {
		return player.retrieveDisplayName(messageSource);
	}
}