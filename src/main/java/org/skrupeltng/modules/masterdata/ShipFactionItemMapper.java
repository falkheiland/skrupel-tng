package org.skrupeltng.modules.masterdata;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.service.ShipFactionItem;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ShipFactionItemMapper {

	ShipFactionItemMapper INSTANCE = Mappers.getMapper(ShipFactionItemMapper.class);

	ShipTemplate newShipTemplate(ShipFactionItem item);
}