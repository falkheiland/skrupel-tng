package org.skrupeltng.modules.masterdata.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class ShipFactionAbilityItem implements Serializable {

	private static final long serialVersionUID = -4567525323165861181L;

	private String name;
	private List<Map<String, String>> params;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Map<String, String>> getParams() {
		return params;
	}

	public void setParams(List<Map<String, String>> params) {
		this.params = params;
	}
}