package org.skrupeltng.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("configProperties")
public class ConfigProperties {

	@Value("${skr.enable_account_validation_by_email:false}")
	private boolean enableAccountValidationByEmail;

	@Value("${skr.galaxy_structure_file_size:50}")
	private int galaxyStructureFileSize;

	@Value("${skr.min_planet_distance:55}")
	private int minPlanetDistance;

	@Value("${skr.max_planet_iterations:35}")
	private int maxPlanetIterations;

	@Value("${skr.min_anomaly_distance:30}")
	private int minAnomalyDistance;

	@Value("${skr.min_galaxy_map_border:50}")
	private int minGalaxyMapBorder;

	@Value("${skr.visibilityRadius:125}")
	private int visibilityRadius;

	@Value("${skr.disablePermissionChecks:false}")
	private boolean disablePermissionChecks;

	@Value("${skr.debug:false}")
	private boolean debug;

	@Value("${skr.randomness_seed:0}")
	private long randomnessSeed;

	@Value("${skr.max_experience:5}")
	private int maxExperience;

	@Value("${skr.distance_travelled_for_experience:1000}")
	private double distanceTravelledForExperience;

	@Value("${skr.evade_distance:20}")
	private int evadeDistance;

	@Value("${skr.space_fold_travel_speed:12.67}")
	private float spaceFoldTravelSpeed;

	@Value("${skr.sub_space_distortion_range:83}")
	private int subSpaceDistortionRange;

	@Value("${skr.hunter_training_money_costs:100}")
	private int hunterTrainingMoneyCosts;

	@Value("${skr.hunter_training_supply_costs:10}")
	private int hunterTrainingSupplyCosts;

	@Value("${skr.hunter_training_fuel_costs:50}")
	private int hunterTrainingFuelCosts;

	@Value("${skr.reservation_size:10000}")
	private int reservationSize;

	public boolean isEnableAccountValidationByEmail() {
		return enableAccountValidationByEmail;
	}

	public int getGalaxyStructureFileSize() {
		return galaxyStructureFileSize;
	}

	public int getMinPlanetDistance() {
		return minPlanetDistance;
	}

	public int getMaxPlanetIterations() {
		return maxPlanetIterations;
	}

	public int getMinAnomalyDistance() {
		return minAnomalyDistance;
	}

	public int getMinGalaxyMapBorder() {
		return minGalaxyMapBorder;
	}

	public int getVisibilityRadius() {
		return visibilityRadius;
	}

	public boolean isDisablePermissionChecks() {
		return disablePermissionChecks;
	}

	public boolean isDebug() {
		return debug;
	}

	public long getRandomnessSeed() {
		return randomnessSeed;
	}

	public int getMaxExperience() {
		return maxExperience;
	}

	public double getDistanceTravelledForExperience() {
		return distanceTravelledForExperience;
	}

	public int getEvadeDistance() {
		return evadeDistance;
	}

	public float getSpaceFoldTravelSpeed() {
		return spaceFoldTravelSpeed;
	}

	public int getSubSpaceDistortionRange() {
		return subSpaceDistortionRange;
	}

	public int getHunterTrainingMoneyCosts() {
		return hunterTrainingMoneyCosts;
	}

	public int getHunterTrainingSupplyCosts() {
		return hunterTrainingSupplyCosts;
	}

	public int getHunterTrainingFuelCosts() {
		return hunterTrainingFuelCosts;
	}

	public int getReservationSize() {
		return reservationSize;
	}
}