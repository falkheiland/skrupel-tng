package org.skrupeltng.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.skrupeltng.modules.dashboard.Roles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, jsr250Enabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		return bCryptPasswordEncoder;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/", "/error", "/favicon.ico", "/sitemap.xml", "/logo.png", "/css/**", "/js/**", "/images/**", "/factions/**",
						"/webfonts/**", "/login", "/credits", "/init-setup", "/register", "/register-successfull", "/legal", "/data-privacy",
						"/activate-account/**", "/activation-failure/**", "/reset-password/**", "/infos/**")
				.permitAll()
				.antMatchers("/**").access("not( hasRole('" + Roles.AI + "') ) and hasAnyRole('" + Roles.ADMIN + "', '" + Roles.PLAYER + "')")
				.antMatchers("/admin/**").access("hasAnyRole('" + Roles.ADMIN + "')")
				.and()
				.csrf().disable()
				.formLogin()
				.loginPage("/login").failureHandler(handleAuthenticationFailure()).defaultSuccessUrl("/my-games")
				.and().rememberMe().key("skrupel-tng-remember-me")
				.and().logout().logoutSuccessUrl("/").permitAll();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	@Bean
	public AuthenticationFailureHandler handleAuthenticationFailure() {
		return new SimpleUrlAuthenticationFailureHandler() {
			@Override
			public void onAuthenticationFailure(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
					AuthenticationException authenticationException) throws IOException, ServletException {
				String errorType = "unexpected";

				if (authenticationException instanceof DisabledException) {
					errorType = "inactive";
				} else if (authenticationException instanceof BadCredentialsException) {
					errorType = "badcredentials";
				}

				setDefaultFailureUrl("/login?error=" + errorType);
				super.onAuthenticationFailure(httpRequest, httpResponse, authenticationException);
			}
		};
	}
}