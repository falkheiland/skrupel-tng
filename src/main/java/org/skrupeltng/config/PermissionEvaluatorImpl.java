package org.skrupeltng.config;

import java.io.Serializable;
import java.util.Optional;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class PermissionEvaluatorImpl implements PermissionEvaluator {

	@Autowired
	private UserDetailServiceImpl userService;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private StarbaseRepository starbaseRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private ShipRouteEntryRepository shipRouteEntryRepository;

	@Autowired
	private OrbitalSystemRepository orbitalSystemRepository;

	@Autowired
	private FleetRepository fleetRepository;

	@Autowired
	private ConfigProperties configProperties;

	@Override
	public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
		if (configProperties.isDisablePermissionChecks()) {
			return true;
		}

		if (permission.equals("game")) {
			return checkGame(targetDomainObject);
		}

		if (permission.equals("planet")) {
			return checkPlanet(targetDomainObject);
		}

		if (permission.equals("starbase")) {
			return checkStarbase(targetDomainObject);
		}

		if (permission.equals("ship")) {
			return checkShip(targetDomainObject);
		}

		if (permission.equals("route")) {
			return checkRoute(targetDomainObject);
		}

		if (permission.equals("orbitalSystem")) {
			return checkOrbitalSystem(targetDomainObject);
		}

		if (permission.equals("fleet")) {
			return checkFleet(targetDomainObject);
		}

		if (permission.equals("turnNotDonePlanet")) {
			return checkTurnNotDonePlanet(targetDomainObject);
		}

		if (permission.equals("turnNotDoneShip")) {
			return checkTurnNotDoneShip(targetDomainObject);
		}

		if (permission.equals("turnNotDoneStarbase")) {
			return checkTurnNotDoneStarbase(targetDomainObject);
		}

		if (permission.equals("turnNotDoneRoute")) {
			return checkTurnNotDoneRoute(targetDomainObject);
		}

		if (permission.equals("turnNotDoneOrbitalSystem")) {
			return checkTurnNotDoneOrbitalSystem(targetDomainObject);
		}

		if (permission.equals("turnNotDoneFleet")) {
			return checkTurnNotDoneFleet(targetDomainObject);
		}

		return false;
	}

	public boolean checkGame(Object targetDomainObject) {
		Optional<Player> optional = playerRepository.findByGameIdAndLoginId((long)targetDomainObject, userService.getLoginId());
		return optional.isPresent();
	}

	public boolean checkPlanet(Object targetDomainObject) {
		return planetRepository.loginOwnsPlanet((long)targetDomainObject, userService.getLoginId());
	}

	public boolean checkStarbase(Object targetDomainObject) {
		return starbaseRepository.loginOwnsStarbase((long)targetDomainObject, userService.getLoginId());
	}

	public boolean checkShip(Object targetDomainObject) {
		return shipRepository.loginOwnsShip((long)targetDomainObject, userService.getLoginId());
	}

	public boolean checkRoute(Object targetDomainObject) {
		return shipRouteEntryRepository.loginOwnsRoute((long)targetDomainObject, userService.getLoginId());
	}

	public boolean checkOrbitalSystem(Object targetDomainObject) {
		return orbitalSystemRepository.loginOwnsOrbitalSystem((long)targetDomainObject, userService.getLoginId());
	}

	public boolean checkFleet(Object targetDomainObject) {
		return fleetRepository.loginOwnsFleet((long)targetDomainObject, userService.getLoginId());
	}

	public boolean checkTurnNotDonePlanet(Object targetDomainObject) {
		return playerRepository.playersTurnNotDoneForPlanet((long)targetDomainObject, userService.getLoginId());
	}

	public boolean checkTurnNotDoneShip(Object targetDomainObject) {
		return playerRepository.playersTurnNotDoneForShip((long)targetDomainObject, userService.getLoginId());
	}

	public boolean checkTurnNotDoneStarbase(Object targetDomainObject) {
		return playerRepository.playersTurnNotDoneForStarbase((long)targetDomainObject, userService.getLoginId());
	}

	public boolean checkTurnNotDoneRoute(Object targetDomainObject) {
		return playerRepository.playersTurnNotDoneForRoute((long)targetDomainObject, userService.getLoginId());
	}

	public boolean checkTurnNotDoneOrbitalSystem(Object targetDomainObject) {
		return playerRepository.playersTurnNotDoneForOrbitalSystem((long)targetDomainObject, userService.getLoginId());
	}

	public boolean checkTurnNotDoneFleet(Object targetDomainObject) {
		return playerRepository.playersTurnNotDoneForFleet((long)targetDomainObject, userService.getLoginId());
	}

	@Override
	public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
		return false;
	}
}