class Themes {
	toggleTheme() {
		$.ajax({
			url: '/theme',
			type: 'POST',
			success: () => {
				window.location.reload(true);
			}
		});
	}
}

const themes = new Themes();
window.themes = themes;
