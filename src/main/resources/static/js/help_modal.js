$(document).on('click', '.skr-ingame-help-show-modal-button', function() {
	const id = $(this)
		.prop('id')
		.replace('skr-ingame-help-show-modal-button-', '');
	showCopyOfModal('#skr-ingame-help-modal-' + id);
});

function showCopyOfModal(id) {
	const original = $(id);
	const copy = original.clone();
	copy.addClass('current-open-modal');
	copy.appendTo('body');
	copy.modal('show');

	copy.on('hidden.bs.modal', function() {
		copy.remove();
	});
}
