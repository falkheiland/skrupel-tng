class Skrupel {
	resize(content, subtractions) {
		setTimeout(function() {
			let sum = 0;

			for (let elem of subtractions) {
				sum += $(elem).outerHeight();
			}

			const contentHeight = $(window).height() - sum + 'px';
			$(content).height(contentHeight);
		}, 100);
	}

	notification(message, link, notificationId) {
		if ('Notification' in window) {
			if (Notification.permission === 'granted') {
				this.showNotification(message, link, notificationId);
			} else if (Notification.permission !== 'denied') {
				Notification.requestPermission().then(permission => {
					if (permission === 'granted') {
						this.showNotification(message, link, notificationId);
					}
				});
			}
		}
	}

	showNotification(message, link, notificationId) {
		const notification = new Notification(message);
		notification.onclick = () => {
			this.markNotificationAsRead(notificationId);
		};
	}

	markAllNotificationsAsRead(dontUpdate) {
		$.ajax({
			url: '/notifications',
			type: 'POST',
			success: () => {
				if (!dontUpdate) {
					this.updateNotificationDropdowns();
				}
			}
		});
	}

	updateNotificationDropdowns() {
		$('#skr-dashboard-notifications-wrapper-small').load('/notifications/dropdown');
		$('#skr-dashboard-notifications-wrapper-big').load('/notifications/dropdown');
	}

	markNotificationAsRead(notificationId) {
		$.ajax({
			url: '/notifications/' + notificationId,
			type: 'POST',
			success: link => {
				window.location.href = link;
			}
		});
	}

	initNotificationPolling() {
		setTimeout(() => {
			this.pollNotifications();
		}, 10000);
	}

	pollNotifications() {
		$.ajax({
			url: '/notifications/unread-notification-count',
			type: 'GET',
			success: response => {
				if (response.unreadCount > 0) {
					this.updateNotificationDropdowns();

					if (response.desktopNotificationId) {
						this.notification(response.message, response.link, response.desktopNotificationId);
					}
				}

				this.initNotificationPolling();
			},
			error: () => {
				this.initNotificationPolling();
			}
		});
	}
}

export const skrupel = new Skrupel();
window.skrupel = skrupel;

$(document).ready(() => {
	setTimeout(() => {
		skrupel.initNotificationPolling();
	}, 1000);
});
