function unsupporteddBrowser(uaString) {
	try {
		eval('class TestClass { testField1; constructor() {} testFunction1() {}}');
	} catch (error) {
		console.error(error);
		return true;
	}

	return false;
}

$(document).ready(function() {
	if (unsupporteddBrowser(window.userAgent)) {
		$('#skr-browser-warning').show();
	}
});
