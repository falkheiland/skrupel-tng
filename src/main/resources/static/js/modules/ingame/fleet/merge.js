import { ingame } from '../ingame.js';
import { fleetsTable } from '../../ingame/overview-table.js';
import { fleetDetails } from './fleet.js';

class FleetMerge {
	mergeFleets() {
		const fleetId = ingame.getSelectedId();
		const otherFleetId = $('#skr-ingame-fleet-merge-select').val();

		$.ajax({
			type: 'POST',
			url: 'fleet/merge?fleetId=' + fleetId + '&otherFleetId=' + otherFleetId,
			success: response => {
				$('#skr-game-merge-content').replaceWith(response);

				fleetDetails.updateStats(() => {
					fleetDetails.updateTravelData();
				});

				fleetsTable.updateTableContent();
			}
		});
	}
}

export const fleetMerge = new FleetMerge();
window.fleetMerge = fleetMerge;
