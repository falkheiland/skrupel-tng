import { ingame } from '../ingame.js';

class FleetShips {
	constructor() {
		$(document).on('click', '.fleet-leader-radio', event => {
			const fleetId = $('#skr-ingame-fleet-id').val();
			const shipId = $(event.target)
				.prop('id')
				.replace('leader-radio-', '');

			$.ajax({
				type: 'POST',
				url: 'fleet/leader?shipId=' + shipId + '&fleetId=' + fleetId,
				success: travelData => {
					travelData.forEach(entry => {
						const shipId = entry.shipId;

						ingame.drawShipTravelLineWithParams(shipId, entry.destinationX, entry.destinationY, entry.x, entry.y, entry.color, '.skr-game-ship-travel-line-wrapper', 'dotted');
						ingame.updateShipMouseOver(shipId);
					});
				}
			});
		});
	}
}

export const fleetShips = new FleetShips();
window.fleetShips = fleetShips;
