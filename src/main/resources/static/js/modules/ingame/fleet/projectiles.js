import { ingame } from '../ingame.js';
import { fleet } from './fleet.js';

class FleetProjectiles {
	constructor() {}

	buildProjectiles() {
		const fleetId = ingame.getSelectedId();

		$.ajax({
			type: 'POST',
			url: 'fleet/build-projectiles?fleetId=' + fleetId,
			success: planetIds => {
				ingame.refreshSelection(fleet);

				planetIds.forEach(planetId => {
					ingame.updatePlanetMouseOver(planetId);
				});
			}
		});
	}
}

const fleetProjectiles = new FleetProjectiles();
window.fleetProjectiles = fleetProjectiles;
