import { ingame } from '../ingame.js';

class FleetTactics {
	constructor() {
		$(document).on('input', '#skr-ingame-aggressiveness-slider', event => {
			const agg = parseInt($(event.target).val());
			$('#skr-ingame-fleet-tactics-aggressiveness-label').text(agg + '%');
		});
	}

	save() {
		const fleetId = ingame.getSelectedId();
		const aggressiveness = parseInt($('#skr-ingame-aggressiveness-slider').val());

		$.ajax({
			url: 'fleet/tactics?fleetId=' + fleetId + '&aggressiveness=' + aggressiveness,
			type: 'POST',
			success: response => {
				$('#skr-game-tactics-content').replaceWith(response);
			}
		});
	}
}

const fleetTactics = new FleetTactics();
window.fleetTactics = fleetTactics;
