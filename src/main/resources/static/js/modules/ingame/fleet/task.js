import { ingame } from '../ingame.js';
import { fleetsTable } from '../../ingame/overview-table.js';
import { fleetDetails } from './fleet.js';

class FleetTask {
	constructor() {
		$(document).on('change', '#skr-ingame-fleet-task-select', () => {
			const selectedValue = $('#skr-ingame-fleet-task-select').val();

			if (selectedValue === 'ACTIVE_ABILITY@VIRAL_INVASION') {
				$('#skr-ingame-fleet-task-viral-invasion-target-select').show();
			} else {
				$('#skr-ingame-fleet-task-viral-invasion-target-select').hide();
			}

			if (selectedValue === 'ACTIVE_ABILITY@SIGNATURE_MASK') {
				$('#skr-ingame-fleet-task-signature-mask-select').show();
			} else {
				$('#skr-ingame-fleet-task-signature-mask-select').hide();
			}
		});
	}

	changeTask() {
		const selectedValue = $('#skr-ingame-fleet-task-select').val();

		if (selectedValue === '-') {
			return;
		}

		const parts = selectedValue.split('@');
		const fleetId = ingame.getSelectedId();

		const taskType = parts[0];
		const activeAbilityType = parts.length > 1 ? parts[1] : null;

		const viralInvasionSelect = $('#skr-ingame-fleet-task-viral-invasion-target-select');
		const signatureMaskSelect = $('#skr-ingame-fleet-task-signature-mask-select');
		const taskValue = viralInvasionSelect.is(':visible') ? viralInvasionSelect.val() : signatureMaskSelect.is(':visible') ? signatureMaskSelect.val() : null;

		$.ajax({
			type: 'POST',
			url: 'fleet/task?fleetId=' + fleetId + '&taskType=' + taskType + '&activeAbilityType=' + activeAbilityType + '&taskValue=' + taskValue,
			success: response => {
				$('#skr-ingame-fleet-task').replaceWith(response);
				fleetsTable.updateTableContent();
				
				fleetDetails.updateStats(() => {
					fleetDetails.updateTravelData();
				});
			}
		});
	}
}

export const fleetTask = new FleetTask();
window.fleetTask = fleetTask;
