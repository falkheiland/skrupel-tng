import { ingame } from './ingame.js';

class Hotkeys {
	selectionHotkeys = [];
	selectionDetailsHotkeys = [];

	initGlobalHotkeys() {
		this.initHeaderHotkeys();
	}

	initHeaderHotkeys() {
		Mousetrap.bind('shift+q', () => {
			$('.modal').modal('hide');
			$('#skr-ingame-overview-button').click();
		});
		Mousetrap.bind('shift+w', () => {
			$('.modal').modal('hide');
			$('#skr-ingame-planets-button').click();
		});
		Mousetrap.bind('shift+e', () => {
			$('.modal').modal('hide');
			$('#skr-ingame-ships-button').click();
		});
		Mousetrap.bind('shift+r', () => {
			$('.modal').modal('hide');
			$('#skr-ingame-starbases-button').click();
		});
		Mousetrap.bind('shift+t', () => {
			$('.modal').modal('hide');
			$('#skr-ingame-fleets-button').click();
		});
		Mousetrap.bind('0', () => {
			$('.modal').modal('hide');
			$('#skr-dashboard-user-menu-button').click();
		});
	}

	updateHotkeys(page) {
		if (page === 'planet') {
			this.initPlanetHotkeys();
		} else if (page === 'ship') {
			this.initShipHotkeys();
		} else if (page === 'starbase') {
			this.initStarbaseHotkeys();
		}
	}

	initPlanetHotkeys() {
		this.unbindSelectionHotkeys();

		this.addSelectionHotkey('q', () => $('#skr-ingame-planet-details-button').click());
		this.addSelectionHotkey('w', () => $('#skr-ingame-planet-orbitalsystems-button').click());
		this.addSelectionHotkey('e', () => $('#skr-ingame-planet-starbase-button').click());
		this.addSelectionHotkey('r', () => $('#skr-ingame-planet-mines-button').click());

		this.addSelectionHotkey('a', () => $('#skr-ingame-planet-nativefaction-button').click());
		this.addSelectionHotkey('s', () => $('#skr-ingame-planet-logbook-button').click());
		this.addSelectionHotkey('d', () => $('#skr-ingame-planet-shipsinorbit-button').click());
		this.addSelectionHotkey('f', () => $('#skr-ingame-planet-factories-button').click());

		this.addSelectionHotkey('v', () => $('#skr-ingame-planet-planetarydefense-button').click());
	}

	initShipHotkeys() {
		this.unbindSelectionHotkeys();

		this.addSelectionHotkey('q', () => $('#skr-ingame-ship-navigation-button').click());
		this.addSelectionHotkey('w', () => $('#skr-ingame-ship-task-button').click());
		this.addSelectionHotkey('e', () => $('#skr-ingame-ship-route-button').click());
		this.addSelectionHotkey('r', () => $('#skr-ingame-ship-weapon-systems-button').click());
		this.addSelectionHotkey('t', () => $('#skr-ingame-ship-projectiles-button').click());

		this.addSelectionHotkey('a', () => $('#skr-ingame-ship-scanner-button').click());
		this.addSelectionHotkey('s', () => $('#skr-ingame-ship-transporter-button').click());
		this.addSelectionHotkey('d', () => $('#skr-ingame-ship-logbook-button').click());
		this.addSelectionHotkey('f', () => $('#skr-ingame-ship-tactics-button').click());
		this.addSelectionHotkey('g', () => $('#skr-ingame-ship-propulsion-button').click());

		this.addSelectionHotkey('v', () => $('#skr-ingame-ship-storage-button').click());
		this.addSelectionHotkey('b', () => $('#skr-ingame-ship-options-button').click());
	}

	initStarbaseHotkeys() {
		this.unbindSelectionHotkeys();

		this.addSelectionHotkey('q', () => $('#skr-ingame-starbase-upgrade-button').click());
		this.addSelectionHotkey('w', () => $('#skr-ingame-starbase-defensesystems-button').click());
		this.addSelectionHotkey('e', () => $('#skr-ingame-starbase-spacefold-button').click());

		this.addSelectionHotkey('a', () => $('#skr-ingame-starbase-hullproduction-button').click());
		this.addSelectionHotkey('s', () => $('#skr-ingame-starbase-propulsionproduction-button').click());
		this.addSelectionHotkey('d', () => $('#skr-ingame-starbase-shipconstruction-button').click());
		this.addSelectionHotkey('f', () => $('#skr-ingame-starbase-logbook-button').click());

		this.addSelectionHotkey('x', () => $('#skr-ingame-starbase-energyweaponproduction-button').click());
		this.addSelectionHotkey('c', () => $('#skr-ingame-starbase-projectileweaponproduction-button').click());
	}

	updateSubPageHotkeys(subPage) {
		this.unbindSelectionDetailsHotkeys();

		if (ingame.selectedPage === 'planet') {
			if (subPage.indexOf('mines') >= 0) {
				this.addSelectionDetailsHotkey('alt+q', () => $('#skr-planet-mines-build-mines').click());
				this.addSelectionDetailsHotkey('alt+a', () => $('#skr-planet-mines-automated-checkbox').click());
			} else if (subPage.indexOf('factories') >= 0) {
				this.addSelectionDetailsHotkey('alt+q', () => $('#skr-planet-factories-build-factories').click());
				this.addSelectionDetailsHotkey('alt+a', () => $('#skr-planet-factories-automated-checkbox').click());

				this.addSelectionDetailsHotkey('alt+w', () => $('#skr-planet-factories-sell-supplies').click());
				this.addSelectionDetailsHotkey('alt+s', () => $('#skr-planet-sell-supplies-automated-checkbox').click());
			} else if (subPage.indexOf('planetary-defense') >= 0) {
				this.addSelectionDetailsHotkey('alt+q', () => $('#skr-planet-planetarydefense-build-planetarydefense').click());
				this.addSelectionDetailsHotkey('alt+a', () => $('#skr-planet-planetarydefense-automated-checkbox').click());
			}
		}

		if (ingame.selectedPage === 'ship') {
			if (subPage.indexOf('navigation') >= 0) {
				this.addSelectionDetailsHotkey('alt+q', () => $('#skr-ship-courseselect-checkbox').click());
				this.addSelectionDetailsHotkey('alt+s', () => $('#skr-ingame-ship-save-course-button').click());
				this.addSelectionDetailsHotkey('alt+d', () => $('#skr-ship-courseselect-delete-course').click());
			} else if (subPage.indexOf('route') >= 0) {
				this.addSelectionDetailsHotkey('alt+q', () => $('#skr-ingame-route-add-take').click());
				this.addSelectionDetailsHotkey('alt+w', () => $('#skr-ingame-route-add-ignore').click());
				this.addSelectionDetailsHotkey('alt+e', () => $('#skr-ingame-route-add-leave').click());
				this.addSelectionDetailsHotkey('alt+r', () => $('#skr-route-select-by-map-button').click());
			}
		}
	}

	unbindSelectionHotkeys() {
		this.selectionHotkeys.forEach(key => {
			Mousetrap.unbind(key);
		});

		this.selectionHotkeys = [];

		this.unbindSelectionDetailsHotkeys();
	}

	unbindSelectionDetailsHotkeys() {
		this.selectionDetailsHotkeys.forEach(key => {
			Mousetrap.unbind(key);
		});

		this.selectionDetailsHotkeys = [];
	}

	addSelectionHotkey(hotkey, action) {
		Mousetrap.bind(hotkey, action);
		this.selectionHotkeys.push(hotkey);
	}

	addSelectionDetailsHotkey(hotkey, action) {
		Mousetrap.bind(hotkey, action);
		this.selectionDetailsHotkeys.push(hotkey);
	}
}

export const hotkeys = new Hotkeys();
