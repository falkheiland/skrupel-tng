class Logbook {
	constructor() {}

	saveLogbook() {
		const logbook = $('#skr-ingame-logbook-input').val();

		const prefix = ingame.selectedPage;
		const selectedId = ingame.getSelectedId();

		$.ajax({
			type: 'POST',
			url: prefix + '/logbook?' + prefix + 'Id=' + selectedId + '&logbook=' + logbook,
			success: response => {
				$('#skr-game-logbook-content').replaceWith(response);

				if (prefix === 'planet') {
					ingame.updatePlanetMouseOver(selectedId);
				} else if (prefix === 'ship') {
					ingame.updateShipMouseOver(selectedId);
				} else {
					ingame.updateStarbaseMouseOver(selectedId);
				}
			}
		});
	}
}

export const logbook = new Logbook();
window.logbook = logbook;
