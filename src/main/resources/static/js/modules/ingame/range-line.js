export class RangeLine {
	prefix;

	constructor(prefix) {
		this.prefix = prefix;

		$(document).on('input', this.prefix + ' input[type=range]', event => {
			this.rangeInputChanged(event);
		});

		$(document).on('input', this.prefix + ' .left-input', event => {
			this.leftInputChanged(event);
		});

		$(document).on('input', this.prefix + ' .right-input', event => {
			this.rightInputChanged(event);
		});

		$(document).on('click', this.prefix + ' .left-max-button', event => {
			this.leftMaxButtonClicked(event);
		});

		$(document).on('click', this.prefix + ' .right-max-button', event => {
			this.rightMaxButtonClicked(event);
		});
	}

	rangeInputChanged(event) {
		this.updateLabels($(event.target));
	}

	leftInputChanged(event) {
		const inputValue = parseInt($(event.target).val());
		const fieldName = this.getFieldName($(event.target));
		const slider = $('#slider-' + fieldName);
		const max = parseInt(slider.prop('max'));

		let sliderValue = max - inputValue;

		if (sliderValue < 0) {
			sliderValue = 0;
		}

		slider.val(sliderValue);
		$('#right-input-' + fieldName).val(sliderValue);
	}

	rightInputChanged(event) {
		const inputValue = parseInt($(event.target).val());
		const fieldName = this.getFieldName($(event.target));
		const slider = $('#slider-' + fieldName);
		const max = parseInt(slider.prop('max'));

		let sliderValue = inputValue;

		if (sliderValue > max) {
			sliderValue = max;
		}

		slider.val(sliderValue);
		$('#left-input-' + fieldName).val(max - inputValue);
	}

	leftMaxButtonClicked(event) {
		const fieldName = this.getFieldName($(event.target));
		const slider = $('#slider-' + fieldName);
		slider.val(0).trigger('input');
	}

	rightMaxButtonClicked(event) {
		const fieldName = this.getFieldName($(event.target));
		const slider = $('#slider-' + fieldName);
		const max = parseInt(slider.prop('max'));

		slider.val(max).trigger('input');
	}

	updateLabels(elem) {
		const fieldName = this.getFieldName(elem);
		const max = parseInt(elem.prop('max'));
		const value = parseInt(elem.val());

		$('#left-input-' + fieldName).val(max - value);
		$('#right-input-' + fieldName).val(value);
	}

	getFieldName(elem) {
		const parts = elem.prop('id').split('-');
		return parts[parts.length - 1];
	}
}
