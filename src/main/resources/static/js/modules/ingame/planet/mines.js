import { ingame } from '../ingame.js';
import { tutorial } from '../tutorial.js';

class PlanetMines {
	constructor() {
		if (!tutorial) {
			$(document).on('click', '#skr-planet-mines-automated-checkbox', function() {
				$.ajax({
					url: 'planet/automated-mines?planetId=' + ingame.getSelectedId(),
					type: 'POST',
					contentType: 'application/json',
					success: function() {
						ingame.updatePlanetMouseOver(ingame.getSelectedId());
					},
				});
			});

			$(document).on('click', '#skr-planet-mines-build-mines', function() {
				const quantity = $('#skr-planet-mines-quantity-select').val();

				$.ajax({
					url: 'planet/mines?planetId=' + ingame.getSelectedId() + '&quantity=' + quantity,
					type: 'POST',
					contentType: 'application/json',
					success: function() {
						ingame.refreshSelection(planet);
						ingame.updatePlanetMouseOver(ingame.getSelectedId());
					},
				});
			});
		}
	}
}

const planetMines = new PlanetMines();
