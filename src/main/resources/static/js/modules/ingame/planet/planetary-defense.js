import { ingame } from '../ingame.js';
import { tutorial } from '../tutorial.js';

class PlanetPlanetaryDefense {
	constructor() {
		if (!tutorial) {
			$(document).on('click', '#skr-planet-planetarydefense-automated-checkbox', function() {
				$.ajax({
					url: 'planet/automated-planetary-defense?planetId=' + ingame.getSelectedId(),
					type: 'POST',
					contentType: 'application/json',
				});
			});

			$(document).on('click', '#skr-planet-planetarydefense-build-planetarydefense', function() {
				const quantity = $('#skr-planet-planetarydefense-quantity-select').val();

				$.ajax({
					url: 'planet/planetary-defense?planetId=' + ingame.getSelectedId() + '&quantity=' + quantity,
					type: 'POST',
					contentType: 'application/json',
					success: function() {
						ingame.refreshSelection(planet);
						ingame.updatePlanetMouseOver(ingame.getSelectedId());
					},
				});
			});
		}
	}
}

const factories = new PlanetPlanetaryDefense();
