class OrbitalSystems {
	showDetails(orbitalSystemId) {
		$('#skr-planet-selection').load('orbital-system?orbitalSystemId=' + orbitalSystemId);
	}
}

const orbitalSystems = new OrbitalSystems();
window.orbitalSystems = orbitalSystems;
