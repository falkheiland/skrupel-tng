import { ingame } from '../ingame.js';

class ShipProjectiles {
	constructor() {
		$(document).on('click', '#skr-ship-projectiles-automated-checkbox', function() {
			$.ajax({
				url: 'ship/automated-projectiles?shipId=' + ingame.getSelectedId(),
				type: 'POST',
				contentType: 'application/json',
				success: function() {
					ingame.updatePlanetMouseOver(ingame.getSelectedId());
				}
			});
		});

		$(document).on('click', '#skr-ship-projectiles-build-projectiles', function() {
			const quantity = $('#skr-ship-projectiles-quantity-select').val();

			$.ajax({
				url: 'ship/projectiles?shipId=' + ingame.getSelectedId() + '&quantity=' + quantity,
				type: 'POST',
				contentType: 'application/json',
				success: function() {
					ingame.refreshSelection(ship);
					ingame.updateShipMouseOver(ingame.getSelectedId());
				}
			});
		});

		$(document).on('click', '#skr-ship-projectiles-build-projectiles-auto', function() {
			const planetId = parseInt($('#skr-ship-projectiles-planet-id'));

			$.ajax({
				url: 'ship/projectiles-auto?shipId=' + ingame.getSelectedId(),
				type: 'POST',
				contentType: 'application/json',
				success: function() {
					ingame.refreshSelection(ship);
					ingame.updateShipMouseOver(ingame.getSelectedId());
					ingame.updatePlanetMouseOver(planetId);
					ingame.updateShipStatsDetails(ingame.getSelectedId());
				}
			});
		});
	}
}

const shipProjectiles = new ShipProjectiles();
