import { ingame } from '../ingame.js';
import { tutorial } from '../tutorial.js';

class ShipTask {
	constructor() {}

	changeTask() {
		const selectedInput = $('#skr-ingame-ship-task-form input[name=task]:checked');
		const taskType = selectedInput.attr('tasktype');

		let activeAbilityType = null;
		let taskValue = $('#' + taskType + '_value').val();

		if (taskType === 'ACTIVE_ABILITY') {
			activeAbilityType = selectedInput.prop('id');
			const valueElem = $('#' + activeAbilityType + '_value');

			if (valueElem.is(':checkbox')) {
				taskValue = valueElem.prop('checked');
			} else {
				taskValue = valueElem.val();
			}
		}

		const request = {
			shipId: ingame.getSelectedId(),
			activeAbilityType: activeAbilityType,
			taskType: taskType,
			taskValue: taskValue
		};

		if ($('#escort').prop('checked')) {
			request.escortTargetId = parseInt($('#skr-ingame-escort-target-select').val());
			request.escortTargetSpeed = parseInt($('#skr-ship-courseselect-speed-select').val());
		}

		$.ajax({
			url: 'ship/task',
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(request),
			success: response => {
				$('#skr-ingame-ship-task').replaceWith(response);

				ingame.updateShipTravelData();

				if (tutorial && tutorial.getStage() === 'TASKS') {
					if (activeAbilityType === 'VIRAL_INVASION') {
						tutorial.next('TASKS');
					} else {
						tutorial.revert('OPEN_TASKS');
					}
				}
			}
		});
	}
}

export const shipTask = new ShipTask();
window.shipTask = shipTask;
