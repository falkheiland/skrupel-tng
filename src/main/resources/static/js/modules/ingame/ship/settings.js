class Settings {
	save() {
		const name = $('#skr-ingame-ship-name-input').val();

		if (!name || name.length < 1) {
			const message = $('#skr-ingame-ship-options-empty-message').val();
			window.alert(message);
			return;
		}

		const request = {
			name: name,
		};

		const newOwnerIdValue = $('#skr-ingame-allied-players-select').val();

		if (newOwnerIdValue && newOwnerIdValue !== '') {
			request.newOwnerId = parseInt(newOwnerIdValue);
		}

		$.ajax({
			url: 'ship/options?shipId=' + ingame.getSelectedId(),
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(request),
			success: response => {
				if (request.newOwnerId) {
					window.alert($('#skr-ingame-ship-owner-changed-message').val());
					window.location.href = '/ingame/game?id=' + new URLSearchParams(window.location.search).get('id');
				} else {
					$('#skr-ingame-ship-options-content').replaceWith(response);
					ingame.updateShipMouseOver(ingame.getSelectedId());
					ingame.updateShipStatsDetails(ingame.getSelectedId());
				}
			},
		});
	}
}

const settings = new Settings();
window.settings = settings;
