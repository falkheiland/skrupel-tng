import { ingame } from '../ingame.js';
import { tutorial } from '../tutorial.js';

class Route {
	planetSelectionActive = false;

	constructor() {
		$(document).on('change', '.skr-ingame-route-action-select', event => {
			const parts = $(event.target)
				.prop('id')
				.split('-');
			const type = parts[0];
			const id = parts[1];
			const action = $(event.target).val();

			$.ajax({
				url: 'ship/route-entry-action?entryId=' + id + '&type=' + type + '&action=' + action,
				type: 'POST',
			});
		});

		$(document).on('click', '.skr-ingame-route-wait-for-full-storage-checkbox', event => {
			const parts = $(event.target)
				.prop('id')
				.split('-');
			const id = parts[1];

			$.ajax({
				url: 'ship/route-entry-wait-for-full-storage?entryId=' + id,
				type: 'POST',
			});
		});

		$(document).on('change', '#skr-ship-courseselect-speed-select', event => {
			const travelSpeed = parseInt($(event.target).val());

			$.ajax({
				url: 'ship/route-travel-speed?shipId=' + ingame.getSelectedId() + '&travelSpeed=' + travelSpeed,
				type: 'POST',
			});
		});

		$(document).on('change', '#skr-ingame-route-primary-resource-select', event => {
			const primaryResource = $(event.target).val();

			$.ajax({
				url: 'ship/route-primary-resource?shipId=' + ingame.getSelectedId() + '&primaryResource=' + primaryResource,
				type: 'POST',
			});
		});

		$(document).on('change', '#skr-ingame-route-min-fuel-input', event => {
			const minFuel = parseInt($(event.target).val());
			const maxValue = parseInt($(event.target).prop('max'));

			if (minFuel < 0 || minFuel > maxValue) {
				$('#skr-ingame-route-min-value-error-text').show();
			} else {
				$('#skr-ingame-route-min-value-error-text').hide();
				$.ajax({
					url: 'ship/route-min-fuel?shipId=' + ingame.getSelectedId() + '&minFuel=' + minFuel,
					type: 'POST',
					success: function() {
						if (tutorial) {
							const stage = tutorial.getStage();

							if (stage === 'SET_MIN_ROUTE_FUEL' && minFuel === 20) {
								tutorial.next('SET_MIN_ROUTE_FUEL');
							}
						}
					},
				});
			}
		});

		$(document).on('change', '#skr-ingame-route-disabled-select', function() {
			const disabled = $(this).val();

			$.ajax({
				url: 'ship/route-disabled?shipId=' + ingame.getSelectedId() + '&disabled=' + disabled,
				type: 'POST',
				success: () => {
					ingame.updateShipTravelData();
				}
			});
		});

		$(document).on('change', '.route-entry-planet-select', function() {
			const entryId = $(this).prop('id').replace('route-entry-planet-select-', '');
			const planetId = $(this).val();

			$.ajax({
				url: 'ship/route-entry-planet?entryId=' + entryId + '&planetId=' + planetId,
				type: 'POST',
				success: () => {
					ingame.updateShipTravelData();
				}
			});
		});

		if (tutorial) {
			$(document).on('change', '#route-entry-planet-select-0', function() {
				const stage = tutorial.getStage();
				const displayText = $('#route-entry-planet-select-0 option:selected').text();

				if (stage === 'SELECT_ROUTE_PLANET_ONE' && displayText === 'Beteigeuze') {
					tutorial.next('SELECT_ROUTE_PLANET_ONE');
				} else if (stage === 'SELECT_ROUTE_PLANET_TWO' && displayText === 'Tau Ceti') {
					tutorial.next('SELECT_ROUTE_PLANET_TWO');
				}
			});
		}

		$(document).on('click', '.skr-game-planet', event => {
			if (this.planetSelectionActive) {
				event.stopPropagation();

				const elem = $(event.target).parent();

				if (elem.find('.owned').length > 0) {
					const planetId = parseInt(elem.find('.skr-game-planet-mouse').attr('planetid'));
					$('#route-entry-planet-select-0')
						.val(planetId)
						.trigger('change');
					this.deactivateSelection();
				}
			}
		});
	}

	selectByGalaxyMap() {
		if (this.planetSelectionActive) {
			this.deactivateSelection();
		} else {
			this.planetSelectionActive = true;
			ingame.hideDetailsOfSmallDisplay();

			$('#skr-route-select-by-map-button').addClass('btn-danger');
		}
	}

	deactivateSelection() {
		this.planetSelectionActive = false;
		$('#skr-route-select-by-map-button').removeClass('btn-danger');
	}

	updateView(newContent) {
		const container = $('#skr-ingame-route-content').parent();
		container.empty();
		container.append(newContent);
		window.scrollTo(0, document.body.scrollHeight);

		ingame.updateShipTravelData();
	}

	add(action) {
		const planetId = parseInt($('#route-entry-planet-select-0').val());

		if (planetId) {
			$.ajax({
				url: 'ship/route-entry?shipId=' + ingame.getSelectedId() + '&planetId=' + planetId + '&action=' + action,
				type: 'PUT',
				success: response => {
					if (tutorial) {
						const stage = tutorial.getStage();

						if (stage === 'ADD_ROUTE_PLANET_ONE' && action === 'LEAVE') {
							tutorial.next('ADD_ROUTE_PLANET_ONE');
						} else if (stage === 'ADD_ROUTE_PLANET_TWO' && action === 'TAKE') {
							tutorial.next('ADD_ROUTE_PLANET_TWO');
						}
					}

					this.updateView(response);
				},
			});
		} else {
			window.alert($('#skr-ingame-route-select-planet-message').val());
		}
	}

	up(entryId) {
		$.ajax({
			url: 'ship/move-route-entry-up?entryId=' + entryId,
			type: 'POST',
			success: this.updateView,
		});
	}

	down(entryId) {
		$.ajax({
			url: 'ship/move-route-entry-down?entryId=' + entryId,
			type: 'POST',
			success: this.updateView,
		});
	}

	deleteEntry(entryId) {
		$.ajax({
			url: 'ship/route-entry?entryId=' + entryId,
			type: 'DELETE',
			success: this.updateView,
		});
	}
}

export const route = new Route();
window.route = route;
