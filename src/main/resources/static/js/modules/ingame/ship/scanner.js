class ShipScanner {
	constructor() {
		$(document).on('click', '.skr-ingame-native-species-details-button', function() {
			const planetId = $(this)
				.prop('id')
				.replace('skr-ingame-native-species-details-button-', '');
			showCopyOfModal('#skr-ingame-native-species-details-' + planetId);
			this.resetPing();
		});

		$(document).on('mouseenter', '.skr-ship-scanner-planet-details, .skr-ship-scanner-ship-details', function() {
			const x = parseInt($(this).attr('x'));
			const y = parseInt($(this).attr('y'));

			$('#skr-ingame-selection-ping-wrapper').css({ left: x, top: y, display: 'block' });
		});

		$(document).on('mouseleave', '.skr-ship-scanner-planet-details, .skr-ship-scanner-ship-details', () => {
			this.resetPing();
		});
	}

	resetPing() {
		const x = parseInt($('#skr-ingame-current-x').val());
		const y = parseInt($('#skr-ingame-current-y').val());

		$('#skr-ingame-selection-ping-wrapper').css({ left: x, top: y, display: 'block' });
	}

	showPlanetDetails(planetId) {
		$.ajax({
			url: 'ship/scanner/planet?shipId=' + ingame.getSelectedId() + '&scannedPlanetId=' + planetId,
			type: 'GET',
			success: response => {
				$('#skr-game-ship-scanner').replaceWith(response);
				this.resetPing();
			}
		});
	}

	showShipDetails(shipId) {
		$.ajax({
			url: 'ship/scanner/ship?shipId=' + ingame.getSelectedId() + '&scannedShipId=' + shipId,
			type: 'GET',
			success: response => {
				$('#skr-game-ship-scanner').replaceWith(response);
				this.resetPing();
			}
		});
	}

	showShipDetailsByOrbitalSystem(shipId, orbitalSystemId) {
		$.ajax({
			url: 'orbital-system/scanner/ship?orbitalSystemId=' + orbitalSystemId + '&scannedShipId=' + shipId,
			type: 'GET',
			success: response => {
				$('#skr-game-ship-scanner').replaceWith(response);
				this.resetPing();
			}
		});
	}
}

const shipScanner = new ShipScanner();
window.shipScanner = shipScanner;
