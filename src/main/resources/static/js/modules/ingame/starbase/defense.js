import { ingame } from '../ingame.js';

class StarbaseDefense {
	constructor() {
		$(document).on('click', '#skr-starbase-defense-build-defense', function() {
			const quantity = $('#skr-starbase-defense-quantity-select').val();

			$.ajax({
				url: 'starbase/defense?starbaseId=' + ingame.getSelectedId() + '&quantity=' + quantity,
				type: 'POST',
				contentType: 'application/json',
				success: function() {
					ingame.refreshSelection(starbase);
					
					const planetId = parseInt($('#skr-ingame-starbase-defense-planet-id').val());
					ingame.updatePlanetMouseOver(planetId);
				}
			});
		});
	}
}

const starbaseDefense = new StarbaseDefense();
