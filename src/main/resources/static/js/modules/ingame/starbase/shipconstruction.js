import { ingame } from '../ingame.js';
import { tutorial } from '../tutorial.js';

class StarbaseShipConstruction {
	constructor() {}

	selectHull() {
		const stockId = $('#skr-starbase-shipconstruction-hull-select').val();

		if (tutorial) {
			if (tutorial.getStage() === 'SELECT_SHIP_CONSTRUCTON_FREIGHTER') {
				this.checkTutorial(stockId, 'orion_2', 'SELECT_SHIP_CONSTRUCTON_FREIGHTER', 'OPEN_SHIP_CONSTRUCTON_FREIGHTER');
			} else if (tutorial.getStage() === 'SELECT_SHIP_CONSTRUCTON_BOMBER') {
				this.checkTutorial(stockId, 'orion_14', 'SELECT_SHIP_CONSTRUCTON_BOMBER', 'OPEN_SHIP_CONSTRUCTON_BOMBER');
			}
		}

		$('#skr-starbase-selection').load('starbase/shipconstruction-details?hullStockId=' + stockId + '&starbaseId=' + ingame.getSelectedId(), () => {});
	}

	checkTutorial(stockId, templateId, next, revert) {
		let found = false;

		$('#skr-starbase-shipconstruction-hull-select option').each(function() {
			if (!found && stockId === $(this).val()) {
				const shipTemplateId = $(this).attr('name');

				if (shipTemplateId === templateId) {
					tutorial.next(next);
					found = true;
				}
			}
		});

		if (!found) {
			tutorial.revert(revert);
		}
	}

	construct() {
		const name = $('#skr-starbase-shipconstruction-shipname-input').val();

		if (!name || name.length === 0) {
			$('#skr-starbase-shipconstruction-form').addClass('was-validated');
			$('#skr-starbase-shipconstruction-shipname-input').focus();
		} else {
			const hullId = $('#skr-starbase-shipconstruction-hull-id').val();
			const propulsionSystemId = $('#skr-starbase-shipconstruction-propulsion-select').val();
			const energyWeaponId = $('#skr-starbase-shipconstruction-energy-select').val();
			const projectileWeaponId = $('#skr-starbase-shipconstruction-projectile-select').val();

			const request = {
				name: name,
				hullStockId: hullId,
				propulsionSystemStockId: propulsionSystemId,
				energyStockId: energyWeaponId,
				projectileStockId: projectileWeaponId
			};

			let url = 'starbase/shipconstruction?starbaseId=' + ingame.getSelectedId();

			const moduleSelect = $('#skr-starbase-shipconstruction-ship-module-select');

			if (moduleSelect.length) {
				request.shipModule = moduleSelect.val();
			} else {
				const starbaseType = $('#skr-starbase-shipconstruction-starbase-type').val();

				if (starbaseType === 'WAR_BASE') {
					url = 'starbase/ship-module-selection?starbaseId=' + ingame.getSelectedId();
				}
			}

			$.ajax({
				url: url,
				type: 'POST',
				contentType: 'application/json',
				data: JSON.stringify(request),
				success: function(response) {
					if (tutorial) {
						const stage = tutorial.getStage();
						const shipTemplateId = $('#skr-starbase-shipconstruction-ship-template-id').val();

						if (stage === 'BUILD_SHIP_CONSTRUCTON_FREIGHTER') {
							if (shipTemplateId === 'orion_2') {
								tutorial.next('BUILD_SHIP_CONSTRUCTON_FREIGHTER');
							} else {
								tutorial.revert('OPEN_HULLS_FREIGHTER');
							}
						} else if (stage === 'BUILD_SHIP_CONSTRUCTON_BOMBER') {
							if (shipTemplateId === 'orion_14') {
								tutorial.next('BUILD_SHIP_CONSTRUCTON_BOMBER');
							} else {
								tutorial.revert('OPEN_HULLS_BOMBER');
							}
						}
					}

					$('#skr-starbase-shipconstruction-content').replaceWith(response);
					ingame.updateStarbaseMouseOver();
				}
			});
		}
	}
}

const starbaseShipConstruction = new StarbaseShipConstruction();
window.starbaseShipConstruction = starbaseShipConstruction;
