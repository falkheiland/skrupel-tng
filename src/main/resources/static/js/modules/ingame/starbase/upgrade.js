import { ingame } from '../ingame.js';
import { tutorial } from '../tutorial.js';

class StarbaseUpgrade {
	constructor() {
		$(document).on('change', '.starbase-upgrade-select', () => {
			let total = 0;

			const hull = parseInt($('#skr-starbase-upgrade-hull-select option:selected').attr('name'));
			const propulsion = parseInt($('#skr-starbase-upgrade-propulsion-select option:selected').attr('name'));
			const energy = parseInt($('#skr-starbase-upgrade-energy-select option:selected').attr('name'));
			const projectile = parseInt($('#skr-starbase-upgrade-projectile-select option:selected').attr('name'));

			if (hull > 0) {
				total += hull;
			}
			if (propulsion > 0) {
				total += propulsion;
			}
			if (energy > 0) {
				total += energy;
			}
			if (projectile > 0) {
				total += projectile;
			}

			if (total > 0) {
				const availableMoney = parseInt($('#skr-ingame-starbase-upgrade-money').val());

				if (availableMoney < total) {
					$('#skr-ingame-starbase-upgrade-costs-wrapper').addClass('text-danger');
					$('#skr-ingame-starbase-performupgrade-button').attr('disabled', true);
				} else {
					$('#skr-ingame-starbase-upgrade-costs-wrapper').removeClass('text-danger');
					$('#skr-ingame-starbase-performupgrade-button').attr('disabled', false);
				}

				$('#skr-ingame-starbase-upgrade-costs')
					.text(total + '')
					.trigger('change');
				$('#skr-ingame-starbase-upgrade-costs-wrapper').show();
			} else {
				$('#skr-ingame-starbase-upgrade-costs-wrapper').hide();
			}
		});
	}

	performUpgrade() {
		const request = {
			hullLevel: parseInt($('#skr-starbase-upgrade-hull-select').val()),
			propulsionLevel: parseInt($('#skr-starbase-upgrade-propulsion-select').val()),
			energyLevel: parseInt($('#skr-starbase-upgrade-energy-select').val()),
			projectileLevel: parseInt($('#skr-starbase-upgrade-projectile-select').val()),
		};

		if (tutorial) {
			const stage = tutorial.getStage();

			if (stage !== 'UPGRADE_TECHLEVELS_FIRST' && stage !== 'UPGRADE_TECHLEVELS_SECOND') {
				return;
			}

			if (stage === 'UPGRADE_TECHLEVELS_FIRST' && (request.hullLevel !== 3 || request.propulsionLevel !== 7 || request.energyLevel !== 0 || request.projectileLevel !== 0)) {
				const message = $('#skr-ingame-starbase-tutorial-upgrade-message-UPGRADE_TECHLEVELS_FIRST').val();
				window.alert(message);
				return;
			}

			if (stage === 'UPGRADE_TECHLEVELS_SECOND' && (request.hullLevel !== 5 || request.energyLevel !== 2 || request.projectileLevel !== 2 || request.propulsionLevel !== 0)) {
				const message = $('#skr-ingame-starbase-tutorial-upgrade-message-UPGRADE_TECHLEVELS_SECOND').val();
				window.alert(message);
				return;
			}
		}

		$.ajax({
			url: 'starbase/upgrade?starbaseId=' + ingame.getSelectedId(),
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(request),
			success: () => {
				ingame.refreshSelection(starbase);
				ingame.updateStarbaseMouseOver();
				
				const planetId = parseInt($('#skr-ingame-starbase-upgrade-planet-id').val());
				ingame.updatePlanetMouseOver(planetId);

				if (tutorial) {
					if (tutorial.getStage() === 'UPGRADE_TECHLEVELS_FIRST') {
						tutorial.next('UPGRADE_TECHLEVELS_FIRST');
					} else if (tutorial.getStage() === 'UPGRADE_TECHLEVELS_SECOND') {
						tutorial.next('UPGRADE_TECHLEVELS_SECOND');
					}
				}
			},
		});
	}
}

export const starbaseUpgrade = new StarbaseUpgrade();
window.starbaseUpgrade = starbaseUpgrade;
