import { planet } from '../planet/planet.js';
import { ingame } from '../ingame.js';

class OrbitalSystemConstruction {
	construct(type) {
		const orbitalSystemId = parseInt($('#skr-ingame-orbitalsystem-id').val());

		$.ajax({
			url: 'orbital-system?orbitalSystemId=' + orbitalSystemId + '&type=' + type,
			type: 'POST',
			success: function() {
				planet.show('orbital-systems');
				ingame.refreshSelection(planet);
				ingame.updatePlanetMouseOver(ingame.getSelectedId());
			},
		});
	}

	showModal(type) {
		showCopyOfModal('#skr-ingame-orbital-system-type-modal-' + type);
	}
}

const orbitalSystemConstruction = new OrbitalSystemConstruction();
window.orbitalSystemConstruction = orbitalSystemConstruction;
