class GameOptions {
	showGameModeModal() {
		$('#skr-game-mode-modal').modal('show');
	}

	apply() {
		const winCondition = $("input:radio[name ='winCondition']:checked").val();

		if (winCondition === 'MINERAL_RACE') {
			const quantity = parseInt($('#mineralRaceQuantity').val());

			if (isNaN(quantity)) {
				this.showMineralRaceQuantityError('skr-mineral-race-quantity-not-set');
				return;
			} else if (quantity < 1000) {
				this.showMineralRaceQuantityError('skr-mineral-race-quantity-too-low');
				return;
			} else if (quantity > 100000) {
				this.showMineralRaceQuantityError('skr-mineral-race-quantity-too-high');
				return;
			}
		} else {
			$('#mineralRaceQuantity')
				.val('')
				.trigger('change');
		}

		$('#skr-mineral-race-quantity-error-text').text('');

		const gameModeText = $('#skr-win-condition-name-' + winCondition).val();
		$('#skr-game-mode-text').text(gameModeText);
		$('#skr-game-mode-modal').modal('hide');
	}

	showMineralRaceQuantityError(id) {
		const message = $('#' + id).val();
		$('#skr-mineral-race-quantity-error-text').text(message);
	}
}

const gameOptions = new GameOptions();
window.gameOptions = gameOptions;
