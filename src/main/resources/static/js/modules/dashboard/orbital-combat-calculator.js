class OrbitalCombatCalculator {
	constructor() {
		$(document).on('change', '#skr-infos-orbital-combat-calculator input, #skr-infos-orbital-combat-calculator select', () => {
			this.calculate();
		});
	}

	calculate() {
		const request = {
			hull: $('#hull').val(),
			energy: $('#energy').val(),
			projectile: $('#projectile').val(),
			damage: $('#damage').val(),
			shield: $('#shield').val(),
			projectileCount: $('#projectileCount').val(),

			planetaryDefenseCount: $('#planetaryDefenseCount').val(),
			starbaseType: $('#starbaseType').val()
		};

		$.ajax({
			url: '/infos/combat-calculator/orbit',
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(request),
			success: response => {
				$('#result1').text(' ' + response.me);
				$('#result2').text(' ' + response.enemy);
			}
		});
	}
}

const orbitalCombatCalculator = new OrbitalCombatCalculator();
window.orbitalCombatCalculator = orbitalCombatCalculator;
