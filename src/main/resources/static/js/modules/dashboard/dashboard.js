$(document).ready(function() {
	let id = '#skr-dashboard-my-games';

	if (window.location.pathname.indexOf('/') >= 0) {
		id = window.location.pathname.replace('/', '#skr-dashboard-');

		const parts = id.split('/');
		id = parts[0];
	}

	$(id).addClass('active');

	$(document).on('click', '#skr-dashboard-start-tutorial-button', function() {
		$.ajax({
			url: '/start-tutorial',
			type: 'POST',
			contentType: 'application/json',
			success: function(gameId) {
				window.location.href = '/ingame/game?id=' + gameId;
			}
		});
	});

	$(document).on('click', '#skr-dashboard-game-options .card-title', function() {
		$(this).find('i').each(function() {
			if ($(this).hasClass('fa-caret-up')) {
				$(this).removeClass('fa-caret-up');
				$(this).addClass('fa-caret-down');
			} else {
				$(this).removeClass('fa-caret-down');
				$(this).addClass('fa-caret-up');
			}
		});
	});
});