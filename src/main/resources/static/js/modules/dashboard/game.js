const gameId = new URLSearchParams(window.location.search).get('id');

class Game {
	selectedPlayer;

	constructor() {
		$(document).on('change', '.skr-game-faction-select', event => {
			const selectBox = $(event.target);
			const faction = selectBox.val();

			if (faction) {
				const aiLevel = selectBox.prop('name');

				if (aiLevel) {
					const idParts = selectBox.prop('id').split('-');
					const playerId = idParts[idParts.length - 1];

					$.ajax({
						type: 'PUT',
						url: 'game/faction?&faction=' + faction + '&playerId=' + playerId,
						success: function() {
							window.location.reload();
						}
					});
				} else {
					$.ajax({
						type: 'POST',
						url: 'game/faction?gameId=' + gameId + '&faction=' + faction,
						success: function() {
							window.location.reload();
						}
					});
				}
			}
		});

		const searchInput = $('.skr-game-player-search');

		if (searchInput.length > 0) {
			searchInput.typeahead({
				dynamic: true,
				minLength: 0,
				searchOnFocus: true,
				order: 'asc',
				display: 'playerName',
				source: {
					player: {
						ajax: {
							url: 'game/' + gameId + '/players',
							type: 'GET',
							data: {
								name: '{{query}}'
							}
						}
					}
				},
				callback: {
					onClick: (node, a, item, event) => {
						this.selectedPlayer = item;
						$('#skr-game-add-player-button').prop('disabled', false);
					}
				}
			});
		}

		$(document).on('click', '#skr-start-game-button', function() {
			$('#skr-game-creation-modal').modal('show');
			$.ajax({
				url: 'start-game?gameId=' + gameId,
				type: 'POST',
				success: function() {
					window.location.href = '/ingame/game?id=' + gameId;
				}
			});
		});

		$(document).on('click', '#skr-join-game-button', function() {
			$.ajax({
				url: 'game/player?gameId=' + gameId,
				type: 'POST',
				success: function(errorMessage) {
					if (errorMessage) {
						window.alert(errorMessage);
					} else {
						window.location.reload();
					}
				}
			});
		});

		$(document).on('click', '#skr-leave-game-button', function() {
			$.ajax({
				url: 'game/player/self?gameId=' + gameId,
				type: 'DELETE',
				success: function() {
					window.location.href = '/open-games?page=0&sortField=EXISTING_GAMES_CREATED&sortDirection=false';
				}
			});
		});

		this.updateStartGameButton();
	}

	addPlayer(teamIndex) {
		if (!this.selectedPlayer) {
			return;
		}

		let url = 'game/player?gameId=' + gameId + '&loginId=' + this.selectedPlayer.id;

		if (teamIndex !== null && teamIndex !== undefined) {
			url += '&teamIndex=' + teamIndex;
		}

		$.ajax({
			url: url,
			type: 'PUT',
			success: function(errorMessage) {
				if (errorMessage) {
					window.alert(errorMessage);
				} else {
					window.location.reload();
				}
			}
		});
	}

	joinTeam(teamIndex) {
		let url = 'game/player?gameId=' + gameId;

		if (teamIndex !== null && teamIndex !== undefined) {
			url += '&teamIndex=' + teamIndex;
		}

		$.ajax({
			url: url,
			type: 'POST',
			success: function(errorMessage) {
				if (errorMessage) {
					window.alert(errorMessage);
				} else {
					window.location.reload();
				}
			}
		});
	}

	removePlayer(playerId) {
		$.ajax({
			type: 'DELETE',
			url: 'game/player?gameId=' + gameId + '&playerId=' + playerId,
			success: function() {
				window.location.reload();
			}
		});
	}

	updateStartGameButton() {
		const button = $('#skr-start-game-button');

		if (button) {
			let allSet = true;

			$('.skr-game-faction-select').each(function() {
				if (!$(this).val()) {
					allSet = false;
				}
			});

			button.prop('disabled', !allSet);
		}
	}

	showDeleteConfirmModal() {
		$('#skr-game-confirm-delete-modal').modal('show');
	}

	deleteGame() {
		$('#skr-game-deletion-modal').modal('show');
		$.ajax({
			type: 'DELETE',
			url: 'game?id=' + gameId,
			success: function() {
				window.location.href = '/my-games?page=0&sortField=EXISTING_GAMES_CREATED&sortDirection=false';
			}
		});
	}
}

export const game = new Game();
window.game = game;

$(document).ready(function() {
	const parts = window.location.href.split('#');

	if (parts.length == 2) {
		const tabId = parts[1];

		$('#skr-game-tab-' + tabId).tab('show');

		setTimeout(function() {
			$(window).scrollTop(0);
		}, 100);
	} else if (window.location.href.indexOf('?id=') < 0) {
		$('#skr-game-tab-options').tab('show');

		setTimeout(function() {
			$(window).scrollTop(0);
		}, 100);
	}

	$(document).on('click', '.nav-link', event => {
		const tab = $(event.target)
			.prop('id')
			.replace('skr-game-tab-', '');
		window.history.pushState(null, null, '#' + tab);
	});
});
