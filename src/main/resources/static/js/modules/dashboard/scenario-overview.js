class ScenarioOverview {
	startScenario(type) {
		$.ajax({
			url: 'scenario?type=' + type,
			type: 'POST',
			success: gameId => {
				window.location.href = '/ingame/game?id=' + gameId;
			}
		});
	}

	hideScenarioTutorialHint() {
		$.ajax({
			url: 'hide-scenario-tutorial-hint',
			type: 'POST',
			success: () => {
				$('#skr-scenario-tutorial-hint').hide();
			}
		});
	}
}

const scenarioOverview = new ScenarioOverview();
window.scenarioOverview = scenarioOverview;
