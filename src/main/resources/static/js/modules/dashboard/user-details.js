class UserDetails {
	constructor() {
		$(document).on('click', '#skr-dashboard-delete-user-button', () => {
			$('#skr-delete-user-modal').modal('show');
		});

		$(document).on('click', '#delete-user-button', () => {
			$.ajax({
				url: 'user',
				type: 'DELETE',
				success: () => {
					window.location.pathname = '/logout';
				},
			});
		});
	}

	changeLanguage(language) {
		$.ajax({
			url: 'user/language?language=' + language,
			type: 'POST',
		});
	}
}

export const userDetails = new UserDetails();
window.userDetails = userDetails;
