export class FullpageSearchTable {
	constructor() {
		const urlParams = new URLSearchParams(window.location.search);

		let pageFromUrl = parseInt(urlParams.get('page'));
		const currentPage = pageFromUrl ? pageFromUrl : 0;

		const maxPage = parseInt($('#pagination-max-page').val());

		if (currentPage === 0) {
			$('#pagination-prev').addClass('disabled');
		} else {
			if (currentPage === maxPage - 1) {
				$('#pagination-next').addClass('disabled');
			}
		}

		const currentSortField = urlParams.get('sortField');
		const currentSortDirection = urlParams.get('sortDirection');

		if (currentSortField) {
			const iconClass = currentSortDirection === 'true' ? 'fas fa-caret-up' : 'fas fa-caret-down';
			$('#icon-' + currentSortField).addClass(iconClass);
		}

		$(document).on('click', '.pagination-number', function() {
			const pageNumber = parseInt($(this).text()) - 1;
			urlParams.set('page', pageNumber);
			window.location.search = urlParams.toString();
		});

		$(document).on('click', '#pagination-prev', () => {
			if (currentPage > 0) {
				urlParams.set('page', currentPage - 1);
				window.location.search = urlParams.toString();
			}
		});

		$(document).on('click', '#pagination-next', () => {
			if (currentPage < maxPage - 1) {
				urlParams.set('page', currentPage + 1);
				window.location.search = urlParams.toString();
			}
		});

		$(document).on('change', '.skr-search-sort-select', event => {
			const parts = $(event.target)
				.val()
				.split('-');
			const sortField = parts[0];
			const ascending = parts[1] === 'true';
			urlParams.set('sortField', sortField);
			urlParams.set('sortDirection', ascending);
			window.location.search = urlParams.toString();
		});
	}
}
