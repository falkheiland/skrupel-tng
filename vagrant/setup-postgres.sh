apt update
apt install -y postgresql-10

echo 'listen_addresses = '"'"'*'"'" >> /etc/postgresql/10/main/postgresql.conf
echo 'host    all             all             all            trust' >> /etc/postgresql/10/main/pg_hba.conf
sudo systemctl restart postgresql

sudo su postgres -c "psql -c \"CREATE ROLE skrupeladmin SUPERUSER LOGIN PASSWORD 'skrupel'\" "
sudo su postgres -c "psql -c \"CREATE DATABASE skrupel TEMPLATE template0 OWNER skrupeladmin\" "
